<!DOCTYPE html>
<html lang="en">
    
    <head>
        <title>Modus | En mantenimiento...</title>
        <link rel="shortcut icon" href="favicon.ico">
        <style type="text/css">
            html { 
                background: url(img/background.gif) no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='.myBackground.jpg', sizingMethod='scale');
                -ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='myBackground.jpg', sizingMethod='scale')";
                
            }
        </style>
    </head>
    <body>
        
    </body>
</html>