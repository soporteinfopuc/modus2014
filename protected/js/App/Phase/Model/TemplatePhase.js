/**
 * ID : Phase.Model.TemplatePhase
 * require : Phase.Model.RuleList
 */
Modus.Phase.Model.TemplatePhase = Backbone.Model.extend(
{
    parse : function(resp, options)
    {
        resp = resp.success||resp;
        resp.rules && (resp.rules = new Modus.Phase.Model.RuleList(resp.rules));
        return resp;
    },
    initialize : function(params,opts)
    {
        //params && params.rules && this.set("rules", new Model.RuleList(params.rules));
    }
   
});