/**
 * ID : Phase.Model.RuleList
 */
Modus.Phase.Model.RuleList = Backbone.Collection.extend(
{
    model : Modus.Phase.Model.Rule,
    parse : function(response, options)
    {
        return response.success||response;
    },
    totalScore : function()
    {
        var score = 0;
        this.each(function(r){
            score += parseInt(r.get('score'));
        });
        return score;
    }
});