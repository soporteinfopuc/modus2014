/**
 * ID : Phase.Model.TemplatePhaseList
 * require : Phase.Model.TemplatePhase
 */
Modus.Phase.Model.TemplatePhaseList = Backbone.Collection.extend(
{
    url : Modus.uriRoot+'/phases',
    model : Modus.Phase.Model.TemplatePhase,
    parse : function(response, options)
    {
        return response.success||response;
    }
});