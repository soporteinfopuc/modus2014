/**
 * ID : Phase.App
 * require : Phase.Model.TemplatePhaseList
 * require : Phase.View.Form
 * require : Phase.View.Phase
 */
Modus.Phase.App = Backbone.View.extend(
{
    tagName:  "div",
    className : "phases",
    template: _.template($('#tmpl-phases-phases').html()),

    initialize : function(params)
    {
        if(params.phaseId>0)
        {
            this.tab = 'edit';
            this.phaseId = params.phaseId;
        }
        else
        {
            if(params.tab=='add')
            {
                this.tab = 'add';
            }
            else
            {
                this.tab = 'view';
            }
        }
    },

    render : function()
    {
        this.$el.html($(this.template()));
        this.viewArea = this.$("#view-area");
        this.alert = this.$("#alert-area");
        this.addEditArea = this.$("#add-edit-area");
        this.viewBtn = this.$("#view-btn");
        this.addEditBtn = this.$("#add-edit-btn");

        this.addEditBtn.find("a").text(this.tab=='edit'?"Editar":"Agregar");

        if(this.tab=="view")
        {
            this.goToView();
        }
        else if(this.tab=="add")
        {
            this.goToAdd();
        }else if(this.tab=="edit")
        {
            this.goToEdit();
        }
        return this;
    },

    events : 
    {
        "click #view-btn" : "goToView",
        "click #add-edit-btn" : "goToAddEdit"
    },

    showMsg : function(msj, css)
    {
        if(this.alert)
        {
            var css = css || "warning";
            this.alert.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
            this.alert.addClass("alert-" + css );
            var title = css=='danger'?"Error!":(css=="success"?"Correcto!":(css=='info'?"Información":"Advertencia!"))
            this.alert.html('<h4><i class="fa fa-'+css+'"></i> '+title+'</h4>' + msj);
            this.alert.show();
        }
        else
        {
            alert(msj);
        }
    },

    showMsgXhr : function(xhr, msg)
    {
        var error = Modus.Helper.getMsgXhr(xhr,msg);
        this.showMsg(error,"danger");
    },

    hideMsg : function()
    {
        this.alert.hide();
    },

    goToView : function()
    {
        var that = this;
        that.virtualRoute("view");
        if(!that.Phases)
        {
            that.Phases = new Modus.Phase.Model.TemplatePhaseList();
            that.listenTo(that.Phases, 'add', that.addOne);
            that.listenTo(that.Phases, 'reset', that.addAll);
        }
        if(that.Phases.length==0)
        {
            that.viewArea.html('<center><i class="fa fa-circle-o-notch fa-spin"></i> Cargando...</center>');
            that.Phases.fetch(
            {
                success : function(m)
                {
                    if(m.length == 0)
                    {
                        that.showMsg("No se encontraron fases.");
                        that.viewArea.html('');
                    }
                },
                error: function(m,xhr)
                {
                    var msg = Modus.Helper.getMsgXhr(xhr,"Error al obtener estrategias");
                    that.viewArea.html('<div style="margin:10em;color:red"><h2 class="text-center"><i class="fa fa-times-circle"></i> Ocurrio un error<br/>'+msg+'</h2></div>')
                }
            });
        }
    },

    goToAddEdit : function()
    {
        if(this.phaseId)
        {
            if(this.tab=='edit')return;
            this.goToEdit();
        }
        else
        {
            if(this.tab=='add')return;
            this.goToAdd();
        }
    },

    goToAdd : function(){
        this.virtualRoute("add");

        if(this.PhaseFormView) this.PhaseFormView.remove();
        this.hideMsg();
        this.PhaseFormView = new Modus.Phase.View.Form({AppPhase:this});

        this.PhaseFormView.on("save", function(model)
        {
            this.showMsg("Se guardó la fase <strong>\""+model.get('name')+"\"</strong> correctamente","success");
            if(this.Phasess) this.Phases.add(model);
            this.goToView(); 
        }, this);
        this.PhaseFormView.on("cancel", this.goToView, this);

        this.addEditArea.html(this.PhaseFormView.render().el);
    },

    goToEdit : function(model)
    {
        if(this.PhaseFormView) this.PhaseFormView.remove();

        if(model)
        {
            this.PhaseFormView = new Modus.Phase.View.Form({model : model,AppPhase:this});
            this.phaseId = model.id;
        }
        else
        {
            this.PhaseFormView = new Modus.Phase.View.Form({phaseId : this.phaseId,AppPhase:this});
        }

        this.PhaseFormView.on("save", this.goToView, this);
        this.PhaseFormView.on("cancel", this.goToView, this);

        this.virtualRoute("edit");

        this.addEditArea.html(this.PhaseFormView.render().el);
    },

    updateOnRemove : function()
    {
        if(this.Strategies.length==0)
        {
            this.goToView();
        }
    },

    addOne: function(phase)
    {
        var view = new Modus.Phase.View.Phase({model: phase});
        view.on("goToEdit",this.goToEdit,this);
        view.on("remove", this.updateOnRemove, this);
        if(this.viewArea.find('.panel-modus').size()==0)
        {
            this.viewArea.empty();
        }
        this.viewArea.append(view.render().el);
    },

    addAll: function() {
      this.Phases.each(this.addOne, this);
    },

    virtualRoute : function(tab)
    {
        this.addEditBtn.hasClass('active') && this.addEditBtn.removeClass('active');
        this.addEditArea.hasClass('active') && this.addEditArea.removeClass('active');

        this.viewBtn.hasClass('active') && this.viewBtn.removeClass('active');
        this.viewArea.hasClass('active') && this.viewArea.removeClass('active');

        this.tab = tab;
        if(this.tab == 'view' )
        {
            this.phaseId = null;

            this.addEditBtn.find("a").text("Agregar");

            this.viewBtn.hasClass('active') || this.viewBtn.addClass('active');
            this.viewArea.hasClass('active') || this.viewArea.addClass('active');
        }
        else
        {
            this.addEditBtn.find("a").text(this.tab=='edit'?"Editar":"Agregar");

            this.addEditBtn.hasClass('active') || this.addEditBtn.addClass('active');
            this.addEditArea.hasClass('active') || this.addEditArea.addClass('active');
        }     

        var url = '/phase/'+(this.tab)+(tab=='edit'?'/'+this.phaseId:'');
         Modus.GeneralRouter.navigate(url, { trigger: false,replace:true });
    }
});
