/**
 * ID : User.Controller.PageableUsers
 * require : User.Model.User
 */
Modus.User.Controller.PageableUsers = Backbone.PageableCollection.extend(
{
    model: Modus.User.Model.User,
    url: Modus.uriRoot + "/users",
    state: {
        pageSize: 20,
    },
    parseRecords: function (resp) {
        return (resp.success&&resp.success.collection)?resp.success.collection:resp;
    },
    parseState: function (resp){
        return resp.success&&resp.success.options?resp.success.options:resp;
    },
    mode: "server"
});