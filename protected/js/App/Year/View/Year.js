/**
 * ID : Year.View.Year
 */
Modus.Year.View.Year = Backbone.View.extend(
{
    tagName :  "tr",
    className : "input-year",
    template : _.template(document.getElementById("tmpl-admin-years-year").innerHTML),
    events : 
    {
        "click .toggle"   : "toggleDone",
        "dblclick .view"  : "edit",
        "click .action-active"  : "activeThis",
        "click .action-edit"  : "edit",
        "click .action-delete"  : "clear",
        "keypress .edit"  : "updateOnEnter",
        "blur .edit"      : "close"
    },

    initialize : function()
    {
      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'destroy', this.remove);
    },

    render : function() {
      this.$el.html(this.template(this.model.toJSON()));
      this.input = this.$('.edit');
      return this;
    },

    activeThis : function()
    {
        var that = this;
        var active = this.model.get('active');
        if(active == 0)
        {
            this.model.save({active : 1},{
                success : function()
                {
                    that.trigger("active", that.model);
                },
                error : function(model, xhr)
                {
                    model.set('active', active);
                    var msj = Modus.Helper.getMsgXhr(xhr, "Ocurrió un error, no se pudo cambiar el periodo activo.");
                    alert(msj);
                }
            });
        }
    },

    edit : function() {
      this.$el.addClass("editing");
      this.input.focus();
    },

    close : function() {
      var value = this.input.val();
      if (!value) {
        this.clear();
      } else {
        this.model.save({name: value});
        this.$el.removeClass("editing");
      }
    },

    updateOnEnter: function(e) {
      if (e.keyCode == 13) this.close();
    },

    clear: function()
    {
        if(confirm("¿Est\u00e1 seguro que desea eliminar este periodo?"))
        {
            this.model.destroy({wait: true, error : function(model,response){
                var resp = response.responseJSON;
                alert(resp.error?resp.error:'No se pudo eliminar.');
            }});
        }
    }
});


