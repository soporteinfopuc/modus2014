/**
 * ID : Year.App
 * require Year.Model.YearList
 * require Year.View.Year
 */
Modus.Year.App = Backbone.View.extend(
{
    tagName:  "div",
    className : "row",
    template: _.template(document.getElementById('tmpl-admin-years').innerHTML),
    Years : null,

    events: 
    {
        "keypress #new-year":  "createOnEnter",
        "click #add-year-btn" : "create",
        "click #clear-completed": "clearCompleted",
        "click #toggle-all": "toggleAllComplete"
    },
            
    initialize: function() 
    {
        this.Years = new Modus.Year.Model.YearList();

        this.$el.html(this.template());
        
        this.inputGroup = this.$('.input-group');
        this.input = this.$("#new-year");
        this.alert = this.$(".alert");
        
        this.listenTo(this.Years, 'add', this.addOne);
        this.listenTo(this.Years, 'all', this.render);

        this.footer = this.$('footer');
        this.main = $('#main');
        
        this.Years.fetch();
    },
            
    render: function()
    {
        if(this.Years.length === 0)
        {
            this.showMsg("No existen periodos en el sistema.","info");
        }
        else
        {
            this.alert.hide();
        }
    },
    
    addOne : function(year)
    {
        var view = new Modus.Year.View.Year({model: year});
        this.views = this.views || [];
        this.views.push(view);
        view.on("active", this.active, this);
        this.$("#years-list").append(view.render().el);
    },
    
    active : function(model)
    {
        _.each(this.views,function(view){
            if(view.model != model)
            {
                view.model.set('active', 0);
            }
            else
            {
                view.model.set('active', 1);
            }
            view.render();
        });
    },

    addAll : function()
    {
        this.Years.each(this.addOne, this);
    },

    createOnEnter : function(e) {
      if (e.keyCode != 13) return;
      this.create();
    },
    
    showMsg : function(msj, css)
    {
        this.alert.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
        this.alert.addClass("alert-" + (css || 'warning'));
        this.alert.text(msj);
        this.alert.show();
    },
    
    create : function()
    {
        var input = this.input;
        if (!input.val())
        {
            input.focus();
            return;
        }
        var that = this;
        this.Years.create({name:input.val()},{wait: true,
            success: function()
            {
                that.input.val('');
                that.alert && that.alert.hide && that.alert.hide();
            },
            error : function()
            {
                that.showMsg("Se produjo un error al crear el periodo, intentelo nuevamente.\n ","danger");
            }
        });
    }
});
