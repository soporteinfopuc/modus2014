 /**
  * ID : Strategy.App
  * require : Strategy.Model.ToolList
  * require : Strategy.View.Tool
  * require : Strategy.View.Add
  * require : Strategy.View.Edit
  * require : Common.Modus.GeneralRouter
  */
Modus.Strategy.App = Backbone.View.extend(
{
    tagName:  "div",
    className : "strategies container",
    template: _.template(document.getElementById('tmpl-strategies-strategies').innerHTML),

    initialize : function(params)
    {
        var that = this;
        if(params.strategyId>0)
        {
            that.tab = 'edit';
            that.strategyId = params.strategyId;
        }
        else
        {
            if(params.tab=='add')
            {
                that.tab = 'add';
            }
            else
            {
                that.tab = 'view';
            }
        }

    },

    render : function()
    {
        this.$el.html(this.template());
        this.viewArea = this.$("#view-area");
        this.alert = this.$("#alert-area");
        this.addEditArea = this.$("#add-edit-area");
        this.viewBtn = this.$("#view-btn");
        this.addEditBtn = this.$("#add-edit-btn");

        this.addEditBtn.find("a").text(this.tab=='edit'?"Editar":"Agregar");

        if(this.tab=="view")
        {
            this.goToView();
        }
        else if(this.tab=="add")
        {
            this.goToAdd();
        }else if(this.tab=="edit")
        {
            this.goToEdit();

        }
        return this;
    },

    events : 
    {
        "click #view-btn" : "goToView",
        "click #add-edit-btn" : "goToAddEdit"
    },

    showMsg : function(msj, css)
    {
        if(this.alert)
        {
            var css = css || "warning";
            this.alert.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
            this.alert.addClass("alert-" + css );
            var title = css=='danger'?"Error!":(css=="success"?"Correcto!":(css=='info'?"Información":"Advertencia!"))
            this.alert.html('<h4><i class="fa fa-'+css+'"></i> '+title+'</h4>' + msj);
            this.alert.show();
        }
        else
        {
            alert(msj);
        }
    },

    showMsgXhr : function(xhr, msg)
    {
        var error = Modus.Helper.getMsgXhr(xhr,msg);
        this.showMsg(error,"danger");
    },

    hideMsg : function()
    {
        this.alert.hide();
    },

    goToView : function()
    {
        var that = this;
        that.virtualRoute("view");
        if(!that.Strategies){
            that.Strategies = new Modus.Strategy.Model.StrategyList;
            that.listenTo(that.Strategies, 'add', that.addOne);
            that.listenTo(that.Strategies, 'reset', that.addAll);
        }
        if(that.Strategies.length==0)
        {
            that.viewArea.html('<center><i class="fa fa-circle-o-notch fa-spin"></i> Cargando...</center>');
            that.Strategies.fetch(
            {
                success : function(m)
                {
                    if(m.length == 0)
                    {
                        that.showMsg("No se encontraron estrategias.");
                        that.viewArea.html('');
                    }
                },
                error: function(m,xhr)
                {
                    var msg = Modus.Helper.getMsgXhr(xhr,"Error al obtener estrategias");
                    that.viewArea.html('<div style="margin:10em;color:red"><h2 class="text-center"><i class="fa fa-times-circle"></i> Ocurrio un error<br/>'+msg+'</h2></div>')
                }
            });
        }
    },

    goToAddEdit : function()
    {
        if(this.strategyId)
        {
            if(this.tab=='edit')return;
            this.goToEdit();
        }
        else
        {
            if(this.tab=='add')return;
            this.goToAdd();
        }

    },

    goToAdd : function()
    {
        this.virtualRoute("add");

        if(this.StrategyAdd) this.StrategyAdd.remove();
        this.hideMsg();
        this.StrategyAdd = new Modus.Strategy.View.Add({AppStrategy:this});

        this.StrategyAdd.on("save", function(model)
        {
            this.showMsg("Se guardó la estrategia <strong>\""+model.get('name')+"\"</strong> correctamente","success");
            if(this.Strategies) this.Strategies.add(model);
            this.goToView(); 
        }, this);
        this.StrategyAdd.on("cancel", this.goToView, this);

        this.addEditArea.html(this.StrategyAdd.render().el);
    },

    goToEdit : function(model)
    {
        if(this.StrategyEdit) this.StrategyEdit.remove();
        this.hideMsg();
        if(model)
        {
            this.StrategyEdit = new Modus.Strategy.View.Edit({model : model, AppStrategy : this});
            this.strategyId = model.id;
        }
        else
        {
            this.StrategyEdit = new Modus.Strategy.View.Edit({strategyId : this.strategyId, AppStrategy : this});
        }

        this.StrategyEdit.on("save", this.goToView, this);
        this.StrategyEdit.on("cancel", this.goToView, this);

        this.virtualRoute("edit");

        this.addEditArea.html(this.StrategyEdit.render().el);
    },

    updateOnRemove : function()
    {
        if(this.Strategies.length==0)
        {
            this.goToView();
        }
    },

    addOne: function(strategy)
    {
        var view = new Modus.Strategy.View.Strategy({model: strategy});
        view.on("goToEdit",this.goToEdit,this);
        view.on("remove", this.updateOnRemove, this);
        if(this.viewArea.find('.panel-modus').size()==0)
        {
            this.viewArea.empty();
        }
        this.viewArea.append(view.render().el);
    },

    addAll: function() {
      this.Strategies.each(this.addOne, this);
    },

    virtualRoute : function(tab)
    {
        this.addEditBtn.hasClass('active') && this.addEditBtn.removeClass('active');
        this.addEditArea.hasClass('active') && this.addEditArea.removeClass('active');

        this.viewBtn.hasClass('active') && this.viewBtn.removeClass('active');
        this.viewArea.hasClass('active') && this.viewArea.removeClass('active');

        this.tab = tab;
        if(this.tab == 'view' )
        {
            this.strategyId = null;

            this.addEditBtn.find("a").text("Agregar");

            this.viewBtn.hasClass('active') || this.viewBtn.addClass('active');
            this.viewArea.hasClass('active') || this.viewArea.addClass('active');
        }
        else
        {
            this.addEditBtn.find("a").text(this.tab=='edit'?"Editar":"Agregar");

            this.addEditBtn.hasClass('active') || this.addEditBtn.addClass('active');
            this.addEditArea.hasClass('active') || this.addEditArea.addClass('active');
        }     

        var url = '/strategy/'+(this.tab)+(tab=='edit'?'/'+this.strategyId:'');
        Modus.GeneralRouter.navigate(url, { trigger: false,replace:true });
    }
});