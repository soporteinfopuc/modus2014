/**
 * ID : Strategy.Model.StrategyList
 * require : Strategy.Model.Strategy
 */
Modus.Strategy.Model.StrategyList = Backbone.Collection.extend(
{
    url : Modus.uriRoot + '/strategies',
    model : Modus.Strategy.Model.Strategy,
    parse : function(response, options){
        return response.success?response.success:response;
    },
    comparator: function(c){
            return -c.get('modified');
    }
});