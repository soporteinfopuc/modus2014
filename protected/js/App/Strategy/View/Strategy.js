/**
 * ID : Strategy.View.Strategy
 */
Modus.Strategy.View.Strategy = Backbone.View.extend(
{
    tagName:  "div",
    className : "col-sm-6",
    template: _.template(document.getElementById('tmpl-strategies-view').innerHTML),

    initialize : function()
    {
        this.model.on("change", this.render, this);
    },

    render : function(){
        this.$el.html(this.template({model : this.model.toJSON()}));
        return this;
    },

    events : {
        "click .action-delete"  : "clear",
        "click .goToEdit" : "goToEdit"
    },

    goToEdit : function()
    {
        this.trigger("goToEdit", this.model);
        return false;
    },

    clear : function()
    {
        if(confirm("¿Está seguro que desea eliminar esta estrategia?"))
        {
            var that = this;
            this.model.url = Modus.uriRoot+'/strategies/'+this.model.id;
            this.model.destroy(
            {
                wait: true, 
                error : function(model,response)
                {
                    var resp = response.responseJSON;
                    alert(resp.error?resp.error:'No se pudo eliminar.');
                },
                success : function()
                {
                    that.trigger("remove",that.model);
                    that.remove();
                }
            });
        }
    }
});