/**
 * ID : Tool.View.Add
 * require Tool.Model.Tool
 */
Modus.Tool.View.Add = Backbone.View.extend(
{
    tagName:  "div",
    className : "tab-pane active",
    template: _.template(document.getElementById('tmpl-tools-add').innerHTML),

    initialize : function()
    {
        this.model = new Modus.Tool.Model.Tool;
        this.model.url = Modus.uriRoot+'/tools/';
    },

    render : function()
    {
        this.$el.html($(this.template({tab:this.tab})));

        this.title = this.$('#tool-title');
        this.url = this.$('#tool-url');
        this.image = this.$('#tool-image');
        Modus.Helper.activeImageSelector(this.image);
        this.description = this.$('#tool-description');
        Modus.Helper.activateSummerNote(this.description);
        this.image.find('.status').removeClass("hidden");
        return this;
    },

    events: {
        "click .btn-save"   : "save",
        "click .btn-cancel"  : "cancel",
        "click #tool-image" : "selectImage",
    },

    save : function(){
        var that = this;
        if(that.title.val().length==0)
        {
            that.title.focus();
            return;
        }
        this.model.save(
            {
                name        : this.title.val(),
                url         : this.url.val(),
                image_id    : that.image.attr('image_id'),
                description : this.description.code()
            },
            {
                wait: true,
                success : function(e){
                    that.trigger("save",that.model);
                },
                error : function(model, xhr)
                {
                    that.trigger("showMsgXhr", xhr, "No se pudo guardar la herramienta");
                }
            });
    },

    cancel: function(e) {
        this.trigger("cancel",this.model);
    }

});
