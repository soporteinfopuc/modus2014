/**
 * ID : Tool.Model.Tool
 */
Modus.Tool.Model.Tool = Backbone.Model.extend(
{
    url : Modus.uriRoot+'/tools',
    parse : function(response, options){
        if(response.success){
            return response.success;
        }else{
            return response;
        }
    }
});