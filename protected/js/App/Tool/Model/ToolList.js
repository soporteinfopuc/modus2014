/* 
 * ID : Tool.Model.ToolList
 * require : Tool.Model.Tool
 */
Modus.Tool.Model.ToolList = Backbone.Collection.extend(
{
    model : Modus.Tool.Model.Tool,
    url : Modus.uriRoot+'/tools',
    parse : function(response, options){
        return response.success?response.success:response;
    }
});

