/**
 * ID : Project.View.SideMarks
 */
Modus.Project.View.SideMarks = Backbone.View.extend(
{
    tagName : "div",
    className : "panel panel-default side-block",
    template  : _.template($('#tmpl-projects-sidebar-marks').html()),
    
    initialize : function(params)
    {
        this.rooms = params.rooms;
        this.options = params.options;
        this.on('selectMark', this.onSelectMark, this);
    },

    render : function()
    {
        this.$el.html($(this.template({rooms: this.rooms})));
        return this;
    },

    events :
    {
        "click .gotoTeamBtn" : "eventSelectAssessment",
        "show.bs.collapse #score-side-list" : "onCollapse"
    },

    eventSelectAssessment : function(e)
    {
        e.preventDefault();
        var current = $(e.currentTarget),
            room_id = current.attr('room-id');
        if(room_id>0)
        {
            this.changeRoomId(room_id);
        }
    },

    changeRoomId : function(room_id)
    {
        var room = _.find(this.rooms, function(room){ return room.id == room_id});
        if(room)
        {
            this.trigger('selectMark', room);
        }
    },

    onSelectMark : function(room)
    {
        var room_id = room.id;
        this.$('.gotoTeamBtn').removeClass('active');
        this.$("[room-id='"+room_id+"']").addClass('active');
    },

    unselect : function()
    {
        this.$('.gotoTeamBtn').removeClass('active');
        this.collapse('hide');
    },

    collapse : function(action)
    {
        action = action || 'toggle';
        this.$('.collapse-btn').addClass('collapsed');
        this.$('#score-side-list').collapse(action);
    },

    onCollapse : function(e)
    {
        if(e && e.type == "show")
        {
            this.trigger("expand", this);
        }
    }
});