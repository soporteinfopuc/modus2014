/**
 * ID : Project.View.Log
 */
Modus.Project.View.Log = Backbone.View.extend(
{
    tagName     : "div",
    className   : "",
    template    : _.template($('#tmpl-projects-logs').html()),
    template_log    : _.template($('#tmpl-projects-logs-log').html()),
    
    initialize : function(params)
    {
        this.options = params.options;
        
        if(!this.model)
        {
            if(!params.project.get('log'))
            {
                this.model = new Modus.Project.Model.LogList();
                this.model.url = Modus.uriRoot + '/projects/log/' + params.project.id;
                this.model.on('sync', this.onSync, this);
                params.project.set('log', this.model);
            }
            else
            {
                this.model = params.project.get('log');
            }
        }
        this.load();
    },

    render : function()
    {
        this.$el.html(this.template());
        this.logContent = this.$('#log-content');
        
        if(this.model.length === 0)
        {
            this.showMsg("No existen eventos en el proyecto", "info");
        }
        else
        {
            this.showMsg();
            this.addAll();
        }
            
        return this;
    },
    
    load : function()
    {
        this.model.fetch();
    },
    
    onSync : function()
    {
        this.render();
    },
    
    addOne : function(log)
    {
        var template = this.template_log({log : log.toJSON()});
        this.logContent.append(template);
    },

    addAll : function()
    {
        this.model.each(this.addOne, this);
    },
    
    showMsg : function(msg, css)
    {
        if(msg && msg.length>0)
        {
            var alert = $('<tr><td colspan="5" id="alert"></td></tr>');
            alert.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
            alert.addClass("alert-" + (css || 'warning'));
            alert.find('td').html(msg);
            this.logContent.prepend(alert).fadeIn('slow');
        }
        else
        {
            var alert = this.logContent.find("#alert");
            alert && alert.hide('slow', function(){ alert.remove(); })
        }
    }
});