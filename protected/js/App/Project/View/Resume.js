/**
 * ID : Project.View.Resume
 */
Modus.Project.View.Resume = Backbone.View.extend(
{
    tagName     : "div",
    className   : "row",
    template    : _.template($('#tmpl-projects-resume-view').html()),

    initialize : function(params)
    {
        this.options = params.options;
    },

    render : function()
    {
        this.$el.html($(this.template({project : this.model})));
        this.phasesList = this.$("#phaseslist");
        this.generalbarTitle = this.$('.general-bar-title');

        this.loadPhases();
        return this;
    },

    changeTeam : function(idTeam)
    {
        _.each(this.phasesAppView,function(phaseAppView){
            phaseAppView.loadOtherTeam(idTeam);
        });
    },

    loadPhases : function()
    {
        if(this.model.getPhases())
            setTimeout($.proxy(this.addAllPhases,this),0);
        else
        {
            var phases = new Modus.Project.Model.PhaseList();
            this.model.set('phases',phases);
            phases.url = Modus.uriRoot + '/projects/view/'+this.model.id+'/phases';
            phases.once("sync",this.addAllPhases,this);
            phases.fetch();
        }
    },

    addPhase : function(phase)
    {
        var phaseAppView = new Modus.Project.View.Phases({model:phase, options: this.options ,project:this.model});
        if(!this.phasesAppView)
        {
            this.phasesAppView = new Array();
            this.phasesList.empty();
        }
        this.phasesAppView.push(phaseAppView);
        this.phasesList.append(phaseAppView.render().el);
    },

    addAllPhases : function()
    {
        this.phasesList.empty();
        var phases = this.model.getPhases();
        if(phases)
            phases.each(this.addPhase,this);
    },
});