/**
 * ID : Project.View.Answer
 */
Modus.Project.View.Answer = Backbone.View.extend(
{
    tagName : "div",
    className : "entry",
    template : _.template($('#tmpl-projects-phase-view-answer').html()),
    
    initialize : function(params)
    {
        this.options = params.options;
        this.phase = params.phase;
        this.project = params.project;
        this.model.url = Modus.uriRoot + '/projects/view/' + this.project.id + '/phases/' + this.phase.id + '/teams/' + this.options.idTeam + '/answers/' + this.model.id;
        this.model.once('destroy',this.remove,this);
    },

    render : function()
    {
        var answer = this.model.toJSON();
        var project = this.project.toJSON();
        this.$el.html($(this.template({answer:answer,project:project,text_answer:this.phase.get('text_answer')})));

        setTimeout($.proxy(function(){
        if(this.project.get('is_manager') || this.project.get('is_owner') || this.model.get('reanswers'))
        {
            this.reAnswerView = new Modus.Project.View.ReAnswer({
                reanswers : this.model.get('reanswers'),
                project_id : this.project.id,
                phase_id : this.phase.id,
                team_id : this.options.idTeam,
                answer_id : this.model.id,
                can_remove : (Modus.isAdmin || Modus.isInstructor) && (this.project.get('is_manager') || this.project.get('is_owner'))
            });
            this.$el.append(this.reAnswerView.render().el);
        }
        },this));
        return this;
    },

    events : 
    {
        "click .respond-answer-btn" : "showReAnswer",
        "click .delete-answer-btn"  : "deleteAnswer",
    },

    showReAnswer : function()
    {
        this.reAnswerView.showReAnswer();
    },

    deleteAnswer : function(e)
    {
        if(this.model.id && confirm("Se va a eliminar esta respuesta. ¿Desea continuar?"))
        {
            var target = $(e.currentTarget);
            target.find('i').addClass('fa-spinner fa-spin').empty();
            this.model.destroy({
                wait:true,
                error : function(model, xhr, options) {
                    (xhr && xhr.responseJSON && xhr.responseJSON.error && !alert(xhr.responseJSON.error)) || alert('No se pudo eliminar, ocurrió un error desconocido.');
                    target.find('i').removeClass('fa-spinner fa-spin').text(' ');
                }
            });
        }
    },

    deleteAll : function()
    {
        if(this.reAnswerView)this.reAnswerView.remove();
        return this.remove();
    }
});
  