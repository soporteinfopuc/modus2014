/**
 * ID : Project.View.SideAnswers
 */
Modus.Project.View.SideAnswers = Backbone.View.extend(
{
    tagName : "div",
    className : "panel panel-default side-block",
    template  : _.template($('#tmpl-projects-sidebar-answers').html()),
    type      : "resume",
    initialize : function(params)
    {
        this.rooms = params.rooms;
        this.options = params.options;
        this.on('selectTeam', this.onChangeTeam, this);
    },

    render : function()
    {
        var selected_team = this.options.ordTeam || false;
        this.$el.html(this.template({rooms : this.rooms, selected_team : selected_team}));
        return this;
    },

    events :
    {
        "click .gotoTeamBtn" : "eventSelectTeam",
        "show.bs.collapse #answers-side-list" : "onCollapse"
    },

    eventSelectTeam : function(e)
    {
        e.preventDefault();
        var idTeam = $(e.currentTarget).attr('team-id');
        if(this.options.idTeam != idTeam)
        {
            this.changeTeamId(idTeam);
        }
        return false;
    },

    changeTeamId : function(idTeam)
    {
        if(idTeam>0)
        {
            var nameRoom = false;
            var ordTeam = false;
            _.each(this.rooms, function(room){
                _.each(room.teams, function(team){
                    if(team.id == idTeam)
                    {
                        nameRoom = room.name;
                        ordTeam = team.ord;
                    }
                });
            });

            if(nameRoom && ordTeam && idTeam)
            {
                this.changeTeam(nameRoom, ordTeam, idTeam);
            }
        }
    },

    changeTeam : function(nameRoom, ordTeam, idTeam)
    {
        this.options.type = this.type;
        this.options.nameRoom = nameRoom;
        this.options.ordTeam = ordTeam;
        this.options.idTeam = idTeam;
        this.trigger('selectTeam', this.options);
    },

    onChangeTeam : function()
    {
        this.$('.gotoTeamBtn').removeClass('active');
        this.$("[team-id='"+this.options.idTeam+"']").addClass('active');
    },

    unselect : function()
    {
        this.$('.gotoTeamBtn').removeClass('active');
        this.collapse('hide');
        delete this.options.nameRoom;
        delete this.options.ordTeam;
        delete this.options.idTeam;
    },

    collapse : function(action)
    {
        action = action || 'toggle';
        this.$('.collapse-btn').addClass('collapsed');
        this.$('#answers-side-list').collapse(action);
    },

    onCollapse : function(e)
    {
        if(e && e.type == "show")
        {
            this.trigger("expand", this);
        }
    }
});