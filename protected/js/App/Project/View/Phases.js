/**
 * ID : Project.View.Phases
 * require : Project.Model.AnswerList
 */
Modus.Project.View.Phases = Backbone.View.extend(
{
    tagName : "div",
    className : "accordion phases-list accordion-team panel",
    template : _.template($('#tmpl-projects-phase-view').html()),
    template_content : _.template($('#tmpl-projects-phase-view-content').html()),

    initialize : function(params)
    {
        this.options = params.options;
        this.phaseId = this.model.id || 0;
        this.project = params.project || null;

        this.answers = new Modus.Project.Model.AnswerList();
        this.answers.on('add', this.addAnswer, this);
        this.answers.on('reset', this.resetAnswer, this);

        this.qualifications = new Modus.Project.Model.QualificationList();

        this.phaseQualifications = new Modus.Project.Model.PhaseQualifications();
        this.phaseAnswers = new Modus.Project.Model.PhaseAnswers();
    },

    render : function()
    {
        var phase = this.model.toJSON();
        this.$el.html($(this.template({phase : phase})));
        this.phaseBody              = this.$('.phase-body');

        this.phaseAnswers.on('sync',this.fillAllHomework,this);
        this.phaseQualifications.on('sync', this.fillQualifications, this);

        return this;
    },

    events : {
        "click #btn-save-draft"           : "saveAnswerDraft",
        "click #btn-publish"              : "publishAnswer",
        "click #attach-btn"               : "attachInAnswer",
        "shown.bs.collapse .phase-body"   : "shownCollapse",
        "show.bs.collapse .phase-body"    : "showCollapse",
        "show.bs.tab .answers-tab"        : "onShowAnswers",
        "show.bs.tab .qualifications-tab" : "onShowQualifications",
        "click [member-mark] .fa-pencil-square-o"  : "showModalMarkStudent",
        "click [manager-mark] .fa-pencil-square-o"    : "showModalMarkAdmin",
        "click .refresh-qualifications" : "loadQualifications",
        "click .refresh-answers" : "loadAnswers"
    },

    showModalMarkAdmin : function(e)
    {
        this.showModalMark(e,'manager');
    },

    showModalMarkStudent : function(e)
    {
        this.showModalMark(e,'member');
    },

    showModalMark : function(e,who)
    {
        who==='manager'?'manager':'member';
        var rule_id = $(e.currentTarget).parent().attr(who+'-mark');
        var rules = this.model.get('rules');
        var rule = _.find(rules, function(rule){
            return rule.id == rule_id;
        });

        if(!this.qualificationModal)
        {
            this.qualificationModal = new Modus.Project.View.QualificationModal({el : this.$('.assign-qualification-modal')});
            this.qualificationModal.on('saved',this.loadQualifications,this);
        }

        var q = this.qualifications.findWhere({rule_id:rule_id,team_id:this.options.idTeam,phase_id:this.model.id}),
            params = {rule : rule, phase_id:this.model.id, team_id :this.options.idTeam};

        if(q && q.id>0)
        {
            params.id       = q.id;
            params.mark     = q.get('mark_'+who);
            params.comment  = q.get('mark_'+who+'_comment');
            params.date     = q.get('mark_'+who+'_date');
        }
        this.qualificationModal.set(params).show();
    },

    shownCollapse : function(e)
    {
        window.setTimeout($.proxy(this.showPhase,this),0);
        $('body').animate({scrollTop: $(e.currentTarget).offset().top + -80}, 100);
    },

    showCollapse : function(e)
    {

    },

    showPhase : function()
    {
        if(this.phaseBody.html().length) return;
        var phase = this.model.toJSON();
        //phase.attachments = phase.attachments.map(function(m){return m.toJSON();});
        //phase.rules = phase.rules.map(function(m){return m.toJSON();});
        this.phaseBody.html($(this.template_content({phase : phase})));

        this.textarea               = this.$('#input-answer');
        this.isFinalAnswer          = this.$('#is-final-answer');
        this.statusSendAnswer       = this.$('#status-saving');
        this.attachBtn              = this.$('#attach-btn');
        this.attachValues           = this.$('#attach-values');
        this.answersArea            = this.$('#answers');
        this.headerAnswers          = this.$('.header-answers');
        this.headerIncludeAnswers   = this.$('.header-include-answers');

        this.answersTab             = this.$('.answers-tab');
        this.qualificationsTab      = this.$('.qualifications-tab');

        this.formPostAnswer             = this.$('#form-post-answer');
        this.refreshQualificationsBtn   = this.$('.refresh-qualifications');
        this.refreshAnswersBtn          = this.$('.refresh-answers');

        if(!this.options.idTeam || phase.can_answer!=1)
        {
            this.$('.phase-tab-content.answer').remove();
            this.answersTab.parent().hide();
        }
        if(!this.options.idTeam || phase.can_mark!=1)
        {
            this.qualificationsTab.parent().hide();
        }

        var tab = ((this.project.get('is_manager') || this.project.get('is_owner')) && this.model.get('can_answer')==1)?"answers"
        :(this.model.get('strategies').length?"strategies"
        :(this.model.get('tools').length?"tools":("answers")));
        this.$('.'+tab+'-tab').tab('show');
    },

    onShowQualifications : function()
    {
        this.qualifications.length==0 && this.loadQualifications();
    },

    onShowAnswers : function()
    {
        this.answers.length==0 && this.loadAnswers();
    },

    fillQualifications : function()
    {
        this.cleanQualifications();
        var rules = this.model.get('rules');
        _.each(rules, function(rule){
            var _q = this.qualifications.findWhere({rule_id:rule.id}),
                q = _q?_q.toJSON():false,
                options = this.optionsQualification,
                iconAssign = '<i class="fa fa-pencil-square-o noshow" title="Asignar nota"></i>',
                iconComment = $('<i class="fa fa-comment-o"></i>'),
                studentEditBtn = options.can_member_mark?$(iconAssign):false,
                adminEditBtn = options.can_manager_mark?$(iconAssign):false
            ;

            (!q||!q.mark_member)&&studentEditBtn&&studentEditBtn.removeClass('noshow');
            var studentMark = this.$("[member-mark='"+rule.id+"']")
                    .html(q&&q.mark_member?(q.mark_member.toString().length==1?'0'+q.mark_member.toString():q.mark_member)+" ":'')
                    .append(studentEditBtn?studentEditBtn:'');
            if(q && q.id>0) studentMark.attr('qualification-id',q.id);
            if(q&&!options.can_member_mark&&q.mark_member_comment) studentMark.append(iconComment.prop('title',q.mark_member_comment));

            (!q||!q.mark_manager)&&adminEditBtn&&adminEditBtn.removeClass('noshow');
            var adminMark = this.$("[manager-mark='"+rule.id+"']")
                    .html(q&&q.mark_manager?(q.mark_manager.toString().length==1?'0'+q.mark_manager.toString():q.mark_manager)+" ":'')
                    .append(adminEditBtn?adminEditBtn:'');
            if(q && q.id>0) adminMark.attr('qualification-id',q.id);
            if(q&&!options.can_manager_mark&&q.mark_manager_comment) adminMark.append(iconComment.prop('title',q.mark_manager_comment));

            this.$("[assigned-mark='"+rule.id+"']").html(q&&q.mark_assigned?(q.mark_assigned.toString().length==1?'0'+q.mark_assigned.toString():q.mark_assigned):'-');
        },this);
        if(this.optionsQualification && this.optionsQualification.final_mark>=0)
            this.$('.final-mark').html(this.optionsQualification.final_mark);
        else
            this.$('.final-mark').empty();

        this.refreshQualificationsBtn.removeClass('fa-spin fa-refresh').addClass('fa-check');
        setTimeout($.proxy(function(){this.refreshQualificationsBtn.removeClass('fa-check').addClass('fa-refresh');},this),2000);
    },

    cleanQualifications : function()
    {
        var rules = this.model.get('rules');
        _.each(rules, function(rule)
        {
            this.$("[member-mark='"+rule.id+"']").empty();
            this.$("[manager-mark='"+rule.id+"']").empty();
            this.$("[assigned-mark='"+rule.id+"']").empty();
        },this);
    },

    fillAllHomework : function()
    {
        this.fillOptions();
        this.fillAnswers();
        this.fillDraft();   
        this.refreshAnswersBtn.removeClass('fa-spin fa-refresh').addClass('fa-check');
        setTimeout($.proxy(function(){this.refreshAnswersBtn.removeClass('fa-check').addClass('fa-refresh');},this),2000);
    },

    fillOptions : function()
    {
        var options = this.optionsAnswers;
        if(options.can_answer)
        {
            Modus.Helper.activateSummerNote(this.textarea);
            if(options.can_last_answer){
                this.isFinalAnswer.removeAttr('disabled');
            }
            this.formPostAnswer.show();
        }
        else
        {
            this.formPostAnswer.remove();
        }
        if(options.showfinal_text)
        {
            this.headerIncludeAnswers.find('.panel-body').html(options.showfinal_text);
            this.headerIncludeAnswers.show();
        }
        else
        {
            this.headerIncludeAnswers.find('.panel-body').html("");
            this.headerIncludeAnswers.hide();
        }
    },

    fillDraft : function()
    {
        if(this.newAnswer)
        {
            var answer = this.newAnswer;
            this.textarea.code(answer.get('answer'));
            if(answer.get('attachment_id'))
            {
                var data = {id : answer.get('attachment_id'), full_cdn : answer.get('attachment_url'), original_name : answer.get('attachment_name')};
                this.attachView(data);
            }
            (this.newAnswer.get('is_last')==true) && this.isFinalAnswer.attr('checked','checked');
            if(answer.get('modified_ago')) this.$('#draftinfo').text('Cargado borrador guardado hace '+answer.get('modified_ago')); else  this.$('#draftinfo').text('');
        }
    },

    fillAnswers : function()
    {
        if(!this.answersArea)this.answersArea = this.$('#answers');


        var answers = this.phaseAnswers.get('answers');
        if(answers && answers.length>0)
        {
            this.answers.add(answers);
            this.headerAnswers.html('<i class="fa fa-check"></i> Se encontró ' + answers.length + ' '+ this.model.get('text_answer')  + (answers.length>1?'s':'') + '.');
        }
        else
        {
            this.headerAnswers.html('<i class="fa fa-exclamation-triangle"></i> No se encontraron más ' + this.model.get('text_answer') + 's.')
        }
    },

    addAnswer : function(answer)
    {
        if(!this.answersView) this.answersView = [];
        var answerView = new Modus.Project.View.Answer({model : answer,project:this.project, phase:this.model, options : this.options});
        this.answersView.push(answerView);
        this.answersArea.append(answerView.render().el);
    },

    resetAnswer : function()
    {
        if(this.answersView && this.answersView.length)
            _.each(this.answersView,function(answerView){
                answerView.deleteAll();
            });
        this.answersView = [];
    },

    attachInAnswer : function()
    {
        var that = this;
        var percent = false;
        var end = function()
        {
            that.$('#btn-publish').prop('disabled', false);
            that.$('#btn-save-draft').prop('disabled', false);
            that.$('#attach-btn').prop('disabled', false);
        }
        $('<input/>').attr('type','file').fileupload({
            add: Modus.Helper.fileUploadRestriction,
            url: Modus.uriRoot+'/uploads/file',
            dataType: 'json',
            success: function (data)
            {
                data = data.success;
                that.attachView(data);
                that.attachBtn.html('<i class="fa fa-search-plus"></i> Adjuntar archivo');
                end();
            },
            error : function(xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr, "No se pudo cargar el archivo.");
                alert(msg);
                that.attachBtn.html('<i class="fa fa-search-plus"></i> Adjuntar archivo');
                end();
            },
            progressall: function (e, data)
            {
                var info = "";
                if(data && data.loaded && data.total) info = Math.round((data.loaded/data.total)*100) + "%";
                if(!percent)
                {
                    that.attachBtn.html('<i class="fa fa-spinner fa-spin"></i> A&ntilde;adiendo... <span class="percent"></span>');
                    percent = that.attachBtn.find('.percent');
                }
                else
                {
                    percent.text(info);
                }
                that.$('#btn-publish').prop('disabled', true);
                that.$('#btn-save-draft').prop('disabled', true);
                that.$('#attach-btn').prop('disabled', true);
            }
        }).click();
    },

    attachView : function(attach)
    {
        var that = this;
        var file = $('<a><i class="fa fa-paperclip"></i> '+attach.original_name +' <i title="Click para quitar este adjunto" class="fa fa-times-circle"></i></a>');
        that.attachBtn.data('attachment', attach);
        file.on('click','i.fa-times-circle', function()
        {
            that.attachBtn.removeData('attachment');
            that.attachValues.empty();
        });
        that.attachValues.html(file);
    },

    saveAnswerDraft : function()
    {
        this.sendAnswer(false);
    },

    publishAnswer : function()
    {
        this.sendAnswer(true);
    },

    sendAnswer : function(is_published)
    {
        this.$('#draftinfo').empty();
        var answer = this.textarea.code(),
            is_last = this.optionsAnswers.can_last_answer && this.isFinalAnswer.is(':checked'),
            attachment = this.attachBtn.data('attachment'),
            phase_id = this.model.id;

        if(!answer || !answer.length>0)
        {
            this.statusSendAnswer.html('<i class="fa fa-exclamation-triangle"></i> Ingrese un texto.').fadeTo(0, 1);
            return;
        }

        if(!this.sendingAnswer)this.sendingAnswer=true;else return;
        this.statusSendAnswer.html('<i class="fa fa-spinner fa-spin"></i> enviando...').fadeTo(0, 1);

        if(!this.newAnswer){
            this.newAnswer = new Modus.Project.Model.Answer();
        }
        this.newAnswer.url = Modus.uriRoot + '/projects/view/' + this.options.id + '/phases/' + this.phaseId + '/teams/' + this.options.idTeam + '/answers';
        this.newAnswer.save({
            answer : answer,
            is_published : is_published,
            is_last : is_last,
            attachment_id : attachment?attachment.id:null,
            last_time : this.optionsAnswers && this.optionsAnswers.last_time ? this.optionsAnswers.last_time:0
        },{
            success : $.proxy(this.successSendAnswer,this),
            error : $.proxy(this.errorSendAnswer,this),
            wait : true
        });
    },

    successSendAnswer : function(model,opts)
    {
        var answers = model.get('answers');
        if(answers && answers.length>0)
        {
            this.answers.add(answers);
            this.headerAnswers.html('<i class="fa fa-check"></i> Se agregó ' + answers.length + ' respuesta' + (answers.length>1?'s':'') + ' más.');
        }
        else
        {

        }
        if(typeof model.get('options') != 'undefined')
        {
            var options = model.get('options');
            this.optionsAnswers.can_answer = options.can_answer==true?true:false;
            this.optionsAnswers.can_last_answer = options.can_last_answer==true?true:false;
            this.optionsAnswers.last_time = options.last_time?options.last_time:0;

        }
        if(model.get('is_published') === true)
        {
            this.newAnswer = new Modus.Project.Model.Answer();
            this.textarea.code("");
            this.attachBtn.removeData('attachment');
            this.attachValues.empty();
            this.statusSendAnswer.html('<i class="fa fa-check"></i> Se envió su respuesta.').delay(5000).fadeTo(1000, 0);
        }else{
            this.statusSendAnswer.html('<i class="fa fa-check"></i> Guardado en borrador.').delay(5000).fadeTo(1000, 0);
        }
        model.unset('answers');
        model.unset('options');
        this.finalSendAnswer();
    },

    errorSendAnswer : function(model,opts)
    {
        var error = (opts && opts.responseJSON && opts.responseJSON.error)?opts.responseJSON.error:"No se pudo enviar.";
        this.statusSendAnswer.html('<i class="fa fa-exclamation-triangle"></i> '+error).delay(5000).fadeTo(1000, 0);
        this.finalSendAnswer();
    },

    finalSendAnswer : function()
    {
        if(this.optionsAnswers.can_answer==false){
            this.textarea.destroy();
            this.formPostAnswer.remove();
        }else{
            (this.optionsAnswers && this.optionsAnswers.can_last_answer==true) && this.isFinalAnswer.removeAttr('disabled');
        }
        this.sendingAnswer = false;
    },

    loadAnswers : function()
    {
        if(this.options.idTeam>0 && this.model.get('can_answer')==1)
        {
            this.answersTab.parent().show();

            var that = this;
            this.refreshAnswersBtn.addClass('fa-spin');
            if(this.headerAnswers) this.headerAnswers.html('<i class="fa fa-spinner fa-spin"></i> Cargando respuestas...');

            var last_time = this.optionsAnswers && this.optionsAnswers.last_time ? this.optionsAnswers.last_time:0;
            this.phaseAnswers.url = Modus.uriRoot + '/projects/view/' + this.options.id + '/phases/' + this.phaseId + '/teams/' + this.options.idTeam + '/answers/' + last_time;
            this.phaseAnswers.fetch({
                success : function(model, response, options)
                    {
                        var draft =  model.get('draft');
                        if(draft) that.newAnswer = new Modus.Project.Model.Answer(draft);

                        that.optionsAnswers = model.get('options');
                        that.headerAnswers && that.headerAnswers.html('');
                    },
                error : function(model,xhr,c)
                    {
                        var msg = Modus.Helper.getMsgXhr(xhr, "Ocurrió un error desconocido al intentar obtener las respuestas del grupo, intente recargar la página");
                        that.headerAnswers && that.headerAnswers.html('<a style="color:#a94442;"><i class="fa fa-exclamation-triangle"></i> ' + msg + '</a>'); 
                        that.refreshAnswersBtn.removeClass('fa-spin');
                    }
            });
        }
    },

    loadQualifications : function()
    {
        if(this.options.idTeam>0 && this.model.get('can_mark')==1)
        {
            this.qualificationsTab.parent().show();

            var that = this;
            this.refreshQualificationsBtn.addClass('fa-spin');
            if(this.qualificationArea) this.qualificationArea.html('<i class="fa fa-spinner fa-spin"></i> Cargando calificaciones...');
            this.phaseQualifications.url = Modus.uriRoot + '/projects/view/' + this.options.id + '/phases/' + this.phaseId + '/teams/' + this.options.idTeam + '/marks';
            this.phaseQualifications.fetch(
            {
                success : function(model, response, options)
                    {
                        that.qualifications.reset();
                        that.qualifications.add(model.get('qualifications'));
                        that.optionsQualification = model.get('options');
                        if(that.optionsQualification.cant_mark)
                        {
                            that.$('.message-area').html('<span style="color:#d9534f"><i class="fa fa-exclamation-triangle"></i>&nbsp;' + that.optionsQualification.cant_mark + '</div>')
                        }
                        else
                        {
                            that.$('.message-area').empty();
                        }
                    },
                error : $.proxy(function(model,xhr,c)
                    {
                        this.cleanQualifications();

                        (xhr && xhr.responseJSON && xhr.responseJSON.error) && 
                        this.qualificationArea && this.qualificationArea.html('<i class="fa fa-exclamation-triangle"></i> ' + xhr.responseJSON.error); 

                        this.refreshQualificationsBtn.removeClass('fa-spin fa-refresh').addClass('fa-times').css('color','red');
                        setTimeout($.proxy(function(){this.refreshQualificationsBtn.removeClass('fa-times').addClass('fa-refresh').css('color','');},this),2000);
                    },this)
            });
        }
    },

    loadOtherTeam : function(team_id)
    {
        if(team_id>0)
        {

            this.answers.reset();
            this.optionsAnswers = null;
            this.newAnswer = null;
            this.qualifications.reset();
            this.options.idTeam = team_id;

            if(this.$('.phase-body').hasClass('in')){
                if(this.$('.answers-tab').parent().hasClass('active')) this.loadAnswers();
                if(this.$('.qualifications-tab').parent().hasClass('active')) this.loadQualifications();
            }
        }
    }
});