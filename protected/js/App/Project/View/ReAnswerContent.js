/**
 * ID : Project.View.ReAnswerContent
 */
Modus.Project.View.ReAnswerContent = Backbone.View.extend(
{
    tagName : "div",
    className : "entry",
    can_remove : false,
    template : _.template($('#tmpl-projects-phase-view-reanswer-content').html()),

    initialize : function(params)
    {
        this.project_id = params.project_id;
        this.phase_id = params.phase_id;
        this.team_id = params.team_id;
        this.answer_id = params.answer_id;
        this.can_remove = params.can_remove;
        this.model.url = Modus.uriRoot + '/projects/view/' + this.project_id + '/phases/' + this.phase_id + '/teams/' + this.team_id + '/answers/' + this.answer_id + '/reanswers/' + +this.model.id;
        this.model.on('destroy',this.remove, this);
    },

    render : function()
    {
        var reanswer = this.model.toJSON();
        this.$el.html($(this.template({reanswer : reanswer, can_remove : this.can_remove})));
        return this;
    },

    events : 
    {
        "click .delete-reanswer-btn" : "removeReAnswer"
    },

    removeReAnswer : function(e)
    {
        if(this.model.id && confirm("Se va a eliminar esta respuesta. ¿Desea continuar?"))
        {
            var target = $(e.currentTarget);
            target.find('i').addClass('fa-spinner fa-spin').empty();
            this.model.destroy({
                wait:true,
                error : function(model, xhr, options) {
                    (xhr && xhr.responseJSON && xhr.responseJSON.error && !alert(xhr.responseJSON.error)) || alert('No se pudo eliminar, ocurrió un error desconocido.');
                    target.find('i').removeClass('fa-spinner fa-spin').text(' ');
                }
            });
        }
    },

    deleteAll : function()
    {
        this.remove();
        return this;
    }
});