/**
 * ID : Project.View.SideMyTeam
 */
Modus.Project.View.SideMyTeam = Backbone.View.extend(
{
    tagName : "div",
    className : "panel panel-default side-block",
    template  : _.template($('#tmpl-projects-sidebar-myteam').html()),

    initialize : function(params)
    {
        this.team = params.team || null;
        this.room = params.room || null;
        this.on('change:team', this.render, this);
    },

    render : function()
    {

        if(this.team && this.room)
        {
            this.$el.html(this.template({team : this.team,room : this.room}));
            this.$el.show();
        }
        else
            this.$el.hide();
        return this;
    },        

    changeTeam : function(room,team)
    {
        this.team = team;
        this.room = room;
        this.trigger('change:team',room,team);
    },

    unselect : function()
    {
        this.changeTeam(null,null);
    },

    collapse : function(action)
    {
        action = action || 'toggle';
        this.$('.collapse-btn').addClass('collapsed');
        this.$('#my-team-side-list').collapse(action);
    }
});