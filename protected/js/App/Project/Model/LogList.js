/**
 * ID : Project.Model.LogList
 */
Modus.Project.Model.LogList = Backbone.Collection.extend(
{
    model : Modus.Project.Model.Log,
    initialize : function(attrs, options)
    {
        
    },
    parse : function(response, options)
    {
            var resp = response.success||response;
            return resp;
    },
});