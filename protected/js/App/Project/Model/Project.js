/**
 * ID : Project.Model.Project
 */
Modus.Project.Model.Project = Backbone.Model.extend(
{
    url : Modus.uriRoot+'/projects',
    parse : function(response, options)
    {
            var resp = response.success||response;
            return resp;
    },
    
    getPhases : function()
    {
        return this.get("phases");
    }
});