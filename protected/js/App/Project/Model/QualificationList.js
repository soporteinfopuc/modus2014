/**
 * ID : Project.Model.QualificationList
 * require : Project.Model.Qualification
 */
Modus.Project.Model.QualificationList = Backbone.Collection.extend(
{
    model : Modus.Project.Model.Qualification,
    parse : function(response, options)
    {
        return response.success||response;
    }
});