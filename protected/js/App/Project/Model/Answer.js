/**
 * ID : Project.Model.Answer
 * require Project.Model.AnswerList
 */
Modus.Project.Model.Answer = Backbone.Model.extend(
{
    initialize : function(attrs, options)
    {
        attrs && attrs.reanswers    && (this.set('reanswers', new Modus.Project.Model.AnswerList(attrs.reanswers)));
    },
    parse : function(response, options){
            var resp = response.success||response;
                   
            return resp;
    },
});