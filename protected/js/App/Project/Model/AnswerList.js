/**
 * ID : Project.Model.AnswerList
 * require Project.Model.Answer
 */
Modus.Project.Model.AnswerList = Backbone.Collection.extend(
{
    model : Modus.Project.Model.Answer,
    parse : function(response, options)
    {
        return response.success?response.success:response;
    }
});