Modus.Helper = 
{
    getMsgXhr : function(xhr, def)
    {
        var error = "";
        if(xhr && xhr.responseJSON && xhr.responseJSON.error)
        {
            if(_.isObject(xhr.responseJSON.error))
            {
                _.each(xhr.responseJSON.error,function(err){
                    error += err + "<br/>";
                });
            }
            else
            {
                error = xhr.responseJSON.error;
            }
        }
        else
        {
            error = def;
        }
        return error;
    },
    
    substringMatcher : function(strs)
    {
        return function findMatches(q, cb) {
            var matches, substrRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
              if (substrRegex.test(str)) {
                // the typeahead jQuery plugin expects suggestions to a
                // JavaScript object, refer to typeahead docs for more info
                matches.push({ value: str });
              }
            });

            cb(matches);
        };
    },
    
    activateSummerNote : function(element)
    {
        element.summernote({
            onImageUpload: function(files, editor, welEditable) {
                var data = new FormData();
                data.append("files", files[0]);
                $.ajax(
                {
                    data: data, type: "POST", dataType: 'json',
                    url: Modus.uriRoot+'/uploads/image', 
                    cache: false, contentType: false,
                    processData: false,
                    success: function(url,b,c)
                    {
                        if(url && url.success && url.success.full_url)
                        {
                            editor.insertImage(welEditable, url.success.full_url);
                        }
                    },
                    error : function(xhr)
                    {
                        var msg = Modus.Helper.getMsgXhr(xhr,"Error al subir imagen");
                        alert(msg);
                    }
                });
            }
        });
    },
    
    activeImageSelector : function(element)
    {
        element.addClass("img-thumbnail middle").attr({'title' : "Click para seleccionar una imagen"}).html('<a class="status"><br/>Seleccionar<br/><i class="fa fa-plus"></i><br/><br/></a>');
        element.css({width:100,height:100});
        element.on("click", function()
        {
            var status = element.find('.status');
            var showImageStatus = function(html, isError)
            {
                if(isError)
                {
                    element.addClass("down");
                }
                else
                {
                    element.removeClass("down");
                }
                status.removeClass("hidden").html(html);
            };
            
            var actualStatus = status.html();
            
            $('<input/>').attr({type:'file', accept : "image/*"}).fileupload({
                add: Modus.Helper.imgUploadRestriction,
                url: Modus.uriRoot+'/uploads/image',
                dataType: 'json',
                done: function (e,data)
                {
                    if(data._response.result.success)
                    {
                        data=data._response.result.success;
                        element.css('background',"url('"+data.full_url+"') no-repeat center center");
                        element.attr("image_id", data.id);
                        status.addClass("hidden").html(actualStatus);
                    }
                },
                progressall: function (e, data)
                {
                    if(data && data.loaded && data.total)
                    {
                        var percent = Math.round((data.loaded/data.total)*100);
                        var info = status.find(".info");
                        if(!info.size())
                        {
                            showImageStatus('<i class="fa fa-circle-o-notch fa-spin"></i> Cargando... <br/><i class="info"></i>');
                        }
                        else
                        {
                            info.text((percent<100)?(percent+"%"):"");
                        }
                    }
                },
                error : function(xhr)
                {
                    var msg = Modus.Helper.getMsgXhr(xhr,"Error al subir");
                    showImageStatus('<span class="danger"><i class="fa fa-times"></i> '+msg+'</span>', true);
                }
            }).click();
        });
    },
    
    loadTemplate : function(id)
    {
        var el = document.getElementById(id);
        if(el && el.innerHTML)
        {
            return _.template(el.innerHTML);
        }
    },
    
    getSlug : function(str)
    {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
        var to   = "aaaaaeeeeeiiiiooooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
          str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
          .replace(/\s+/g, '-') // collapse whitespace and replace by -
          .replace(/-+/g, '-'); // collapse dashes

        return str;
    },
    
    fileUploadRestriction : function(e, data)
    {
        var uploadErrors = [];
        var acceptFileTypes = '.+\.('+Modus.fileExtAccept+')';
        if(data.originalFiles[0]['name'] && !data.originalFiles[0]['name'].match(acceptFileTypes))
        {
            uploadErrors.push("No se acepta este tipo de archivo, sólo se aceptan las siguientes extensiones: \n" + Modus.fileExtAccept.replace(/\|/g,", "));
        }
        if(data.originalFiles[0]['size'] && data.originalFiles[0]['size'] > Modus.maxUploadSize) 
        {
            uploadErrors.push('El archivo seleccionado es demasiado grande, se aceptan archivos de ' + Modus.maxUploadSizeDesc);
        }
        if(uploadErrors.length > 0) 
        {
            alert(uploadErrors.join("\n"));
        }
        else
        {
            data.submit();
        }
    }
    ,
    imgUploadRestriction : function(e, data)
    {
        var uploadErrors = [];
        var acceptFileTypes = '.+\.('+Modus.imgExtAccept+')';
        if(data.originalFiles[0]['name'] && !data.originalFiles[0]['name'].match(acceptFileTypes))
        {
            uploadErrors.push("No se acepta este tipo de imagen, sólo se aceptan las siguientes extensiones: \n" + Modus.imgExtAccept.replace(/\|/g,", "));
        }
        if(data.originalFiles[0]['size'] && data.originalFiles[0]['size'] > Modus.maxUploadSize) 
        {
            uploadErrors.push('La imagen seleccionada es demasiado grande, se aceptan imágenes de ' + Modus.maxUploadSizeDesc);
        }
        if(uploadErrors.length > 0) 
        {
            alert(uploadErrors.join("\n"));
        }
        else
        {
            data.submit();
        }
    }
};