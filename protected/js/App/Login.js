(function()
{
    var LoginRouter = Backbone.Router.extend(
    {
      routes: {
        "" : "login",
        "login" : "login",
        "recoverPassword": "recoverpassword",
        "recoverpassword": "recoverpassword",
      }

    });
    window.AppView = Backbone.View.extend(
    {
        initialize : function ()
        {
            this.loginRouter = new LoginRouter();
            this.loginRouter.on('route:login', function() {
                new LoginView;
            });
            this.loginRouter.on('route:recoverpassword', function() {
                new RecoverPasswordView;
            });
            Backbone.history.start();
        }
    });
    window.RecoverPasswordView = Backbone.View.extend(
    {
        initialize:function ()
        {
            var $email = this.$el.find('#inputEmail');
            this.inputEmail = $email;
            this.render();
        },
        el : $('#panel-principal'),
        events: {
            "click #recoverButton": "recover"
        },
        template : _.template($('#tmpl-user-recover').html()),
        render:function () {
            var renderized = $(this.template( {} ));
            this.$el.html(renderized);
            this.$el = renderized;
            return this;
        },
        retries : 0,
        disabled : false,
        disable : function(){
            this.$el.find('input').attr('disabled','disabled');
            this.enabled = false;
            this.disabled = true;
        },
        enabled : true,
        enable : function(){
            this.$el.find('input').removeAttr('disabled');
            this.enabled = true;
            this.disabled = false;
        },
        showError : function(attrs){

        },
        cleanError : function(){
            this.$el.find('.has-error').removeClass('has-error');
        },
        urlLogin : 'modus/recover_password',
        validate : function(){
            var email = this.inputEmail.val();
            if( email.length <= 0 ){
                this.inputEmail.parents('.form-group').addClass('has-error');
                return false;
            }else{
                return email;
            }
        },
        recover:function (event) {
            event.preventDefault();
            this.cleanError();
            if( this.enabled ){
                this.disable();
                var formValues = this.validate();
                if( formValues ){
                    $.ajax({
                        url:this.url, type:'POST', dataType:"json",
                        data: { 'email':formValues },
                        success:function (data) {
                            if(data.error) {
                                this.showError(data.error);
                            }
                            else if(data.success && (data.success === true)){
                                alert('login correct');
                            }
                        }
                    });
                }
            }
        }
    });
    
    window.LoginView = Backbone.View.extend(
    {

        el : $('#panel-principal'),
        template : _.template($('#tmpl-user-login').html()),
        initialize:function () {
            this.render();
        },
        events: {
            "click #loginButton": "login",
            "keypress input" : "onEnter"
        },
        inputUser:null,
        inputPassword:null,
        render:function () {
            var renderized = $(this.template( {} ));
            this.$el.html(renderized);
            this.$el = renderized;
            this.login_btn = this.$('#loginButton');
            var $user = this.$el.find('#inputUser');
            var $password = this.$el.find('#inputPassword');
            this.inputUser = $user;
            this.inputPassword = $password;
            this.inputUser.on('keypress',$.proxy(this.cleanError,this));
            this.inputPassword.on('keypress',$.proxy(this.cleanError,this));
            return this;
        },
        cleanInput : function(){

        },
        retries : 0,
        disabled : false,
        disable : function(){
            this.$el.find('input').attr('disabled','disabled');
            this.enabled = false;
            this.disabled = true;
        },
        enabled : true,
        enable : function(){
            this.$el.find('input').removeAttr('disabled');
            this.enabled = true;
            this.disabled = false;
        },
        showError : function(attrs){

        },
        cleanError : function(){
            this.$el.find('.has-error').removeClass('has-error');
        },
        url : Modus.uriRoot + '/modus/login',
        validate : function(){
            var formValues = {
                    username: this.inputUser.val(),
                    password: this.inputPassword.val()
            };
            if( formValues.username.length <= 0 || formValues.password.length <= 0 ){
                if( formValues.password.length <= 0 ){
                    this.inputPassword.parents('.form-group').addClass('has-error');
                }
                if( formValues.username.length <= 0 ){
                    this.inputUser.parents('.form-group').addClass('has-error');
                }
                return false;
            }else{
                return formValues;
            }
        },
        onEnter : function(e)
        {
            if (e.keyCode != 13) return;
            this.login(e);
        },
        login:function (event) {
            event.preventDefault();
            var that = this;

            var original_text = that.login_btn.text();
            this.cleanError();
            if( this.enabled ){
                var formValues = this.validate();
                if( formValues )
                {
                    that.login_btn.html('<i class="fa fa-spinner fa-spin"></i> Iniciando...')
                    this.disable();
                    $.ajax({
                        url:this.url, type:'POST', dataType:"json",
                        data: formValues,
                        success:function (data) {
                            if(data.success && (data.success === true)){
                                if(data.redirect){
                                    //window.location.href=data.redirect;
                                    window.location.href=window.location.href;
                                }else{
                                    window.location.href=window.location.href;
                                }
                            }
                        }
                    }).fail(function(xhr){
                        xhr && xhr.responseJSON && xhr.responseJSON.error && alert(xhr.responseJSON.error) ||
                        xhr && !xhr.responseJSON && alert("Ocurrió un error desconocido, por favor contacte al administrador.") ||
                        that.inputPassword.val('') && that.inputPassword.focus();
                        that.enable();
                        that.login_btn.html(original_text);
                    });
                }
            }
        }
    });

    new window.AppView;
})();
