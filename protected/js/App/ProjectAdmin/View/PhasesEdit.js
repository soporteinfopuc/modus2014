/**
 * ID : Project.View.PhasesEdit
 */
 Modus.ProjectAdmin.View.PhasesEdit = Backbone.View.extend(
{
    tagName:  "div",
    template : _.template($('#tmpl-projects-phases').html()),

    initialize : function()
    {

    },

    render : function()
    {
        this.$el.html($(this.template()));

        this.phasesList     = this.$('#phases-list');
        this.saveButton     = this.$(".btn-save");

        this.phases = this.model.get("phases");

        if(this.phases.length>0)
            setTimeout($.proxy(this.addAll,this),0);
        else
            this.phases.once('sync', this.addAll, this);
        return this;
    },

    events : 
    {
        "click .btn-save" : "save",
        "click .cancel" : "cancel"
    },

    addOne : function(phase)
    {
        var view = new Modus.ProjectAdmin.View.PhaseEdit({model : phase,project:this.model});
        if(!this.phasesview) this.phasesview = [];
        this.phasesview.push(view);

        this.phasesList.append(view.render().el);
    },

    addAll : function()
    {
        this.phases.each(this.addOne,this);
    },

    save : function(e)
    {
        _.each(this.phasesview,function(p)
        {
            p.save();
        });
        this.trigger("save",e);
    }
});