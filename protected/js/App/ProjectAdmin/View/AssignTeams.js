/**
 * ID : Project.View.AssignTeams
 */
 Modus.ProjectAdmin.View.AssignTeams = Backbone.View.extend(
{
    tagName:  "div",
    className: "row",
    template : _.template($('#tmpl-projects-assign-teams').html()),

    initialize : function()
    {
        var that = this;
        that.on("onChangeRoom", that.onChangeRoom, that);

        that.roomsTeams = new Modus.School.Model.RoomList();
        //this.model.set("rooms",this.roomsTeams);

        that.roomsTeams.url = Modus.uriRoot+'/projects/'+that.model.id+'/teams';
        that.roomsTeams.fetch({
            error : function(xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr,"Error al obtener grupos");
                that.$el.html('<div style="color:red"><h2 class="text-center"><i class="fa fa-times-circle"></i> Ocurrio un error<br/>'+msg+'</h2></div>')
            }
        });
    },

    render : function()
    {
        this.$el.html(this.template());

        this.roomsSelect = this.$("#teams-select-room");
        this.roomsSelectDDM = this.$("#teams-select-room-ddm");
        this.roomsSelect.html('<i class="fa fa-spinner fa-spin"></i> Cargando grupos...');
        this.teamsZone = this.$("#team-zone");

        if(this.roomsTeams && this.roomsTeams.length>0)
        {
            setTimeout($.proxy(this.showSelectRoom,this), 0);
        }else
        {
            this.roomsTeams.once("sync",this.showSelectRoom,this);
        }

        return this;
    },

    events : 
    {
        "click #btn-save" : "save",
        "click #end-add-project" : "saveEnd",
    },

    save : function(e)
    {
        this.trigger('save',e);
    },

    saveEnd : function(e)
    {
        this.trigger('saveEnd',e);
    },

    onChangeRoom : function(room)
    {
        if(room && room.id){
            this.Selected = room;
            var name = this.Selected.get('name');
            this.roomsSelect.text('"'+name+'"');

            this.loadTeams();
        }
    },

    loadTeams : function()
    {
        if(this.Selected)
        {
            var teamsView = new Modus.ProjectAdmin.View.Teams({
                room : this.Selected,
                projectId : this.model.id
            });
            this.teamsZone.html(teamsView.render().el);
        }
    },

    showSelectRoom : function()
    {
        var rooms = this.roomsTeams;
        this.roomsSelectDDM.empty();
        rooms.each(function(room){
            var li = $('<li/>');
            var a = $('<a/>');
            var label = $('<label/>');
            label.append(room.get("name"));
            a.html(label);
            li.html(a).click($.proxy(function(){
                    this.trigger("onChangeRoom",room);
            }, this));
            this.roomsSelectDDM.append(li);
        },this);
        if(!this.Selected)
        {
            this.trigger("onChangeRoom",rooms.first());
        }
    }
});
