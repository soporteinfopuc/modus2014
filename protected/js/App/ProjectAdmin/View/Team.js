/**
 * ID : Project.View.Team
 */
 Modus.ProjectAdmin.View.Team = Backbone.View.extend(
{
    tagName:  "div",
    className : "form-team",
    template : _.template($('#tmpl-projects-assign-teams-cont-team').html()),
    render : function()
    {

        this.$el.html(this.template());
        this.students = this.$("#container-team-student");
        this.teamName = this.$("#team-name");
        this.teamCount = this.$("#team-count");
        this.container = this.$(".container-team-student");

        this.container.sortable({
            connectWith: ".container-team-student",
            remove : $.proxy(this.removeStudent,this),
            receive: $.proxy(this.receiveStudent,this),
            scroll : true,
        });
        this.container.parent().disableSelection();
        this.updateName();
        this.addAllStudents();
        return this;
    },

    reRender : function()
    {
        this.addAllStudents();
    },

    events : 
    {
        "click .action-delete-team" : "destroyTeam"
    },

    updateName : function()
    {
        this.teamName.text(this.model.get("name"));
        this.teamCount.text(this.model.get("students").length);
    },

    addStudent : function(student)
    {
        var answer_count = student.get('answer_count');
        var html = student.get("name");
        if(answer_count>=0)
        {
            html += ' <span class="badge badge-info" title="'+answer_count+' respuestas enviadas por este alumno">'+answer_count+'</span>';
        }
        var span = $("<span>").addClass("team-student").attr('title',student.get("username")).html(html).attr('student-id',student.id);
        this.students.append(span);
    },

    receiveStudent : function(e,ui)
    {
        if($(e.originalEvent.target).attr('student-id'))
        {
            var studentId = $(e.originalEvent.target).attr('student-id');
            this.trigger("receiveStudent",this.model,studentId);
        }
    },

    removeStudent : function(e,ui)
    {
        if($(e.originalEvent.target).attr('student-id'))
        {
            var studentId = $(e.originalEvent.target).attr('student-id');
            this.trigger("removeStudent",this.model,studentId);
        }
    },

    addAllStudents : function()
    {
        this.students.empty();
        if(this.model.id>0)
        {
            this.students.html('<button type="button" class="close action-delete-team" aria-hidden="true" title="Eliminar grupo">&times;</button>');
        }
        var students = this.model.get('students');
        if(students && students.length)
        {
            students.each(this.addStudent,this);
        }
        else
        {
            this.students.append('Este grupo no tiene alumnos.');
        }

    },

    destroyTeam : function()
    {
        var add = "";
        var students = this.model.get('students');
        var has_answer = 0;
        students.each(function(student){
            has_answer += student.get('answer_count');
        });
        var has_marks = this.model.get('has_marks');
        var std_len = students.length;
        if(std_len>0)
        {
            add += " - Se quitará"+(std_len>1?"n":"")+" " + std_len + " alumno"+(std_len>1?"s":"")+" de este grupo\n";
        }
        if(has_answer>0)
        {
            add += " - Se eliminará las "+ has_answer +" respuestas enviadas por este grupo\n";
        }
        if(has_marks>0)
        {
            add += " - Se eliminará las "+ has_marks +" calificaciones asignadas a este grupo\n";
        }

        if( (students.length==0) || confirm(add + "\n\u00BFEstá seguro que desea eliminar este grupo?\n"))
        {
            this.trigger("destroyTeam", this, this.model, $.proxy(this.remove,this));
        }
    }
});