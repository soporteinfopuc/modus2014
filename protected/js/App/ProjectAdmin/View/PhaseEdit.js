/**
 * ID : ProjectAdmin.View.PhaseEdit
 * require : Modus.Tool.Model.ToolList
 * require : Modus.Strategy.Model.StrategyList
 */
Modus.ProjectAdmin.View.PhaseEdit = Backbone.View.extend(
{
    tagName:  "div",
    className : 'panel',
    template : _.template($('#tmpl-projects-phases-phase').html()),

    initialize : function(param)
    {
        if( !Modus.Cache.Strategies )
        {
            Modus.Cache.Strategies = new Modus.Strategy.Model.StrategyList();
            Modus.Cache.Strategies.url = Modus.uriRoot+'/strategies/list';
            Modus.Cache.Strategies.fetch();
        }
        if( !Modus.Cache.Tools )
        {
            Modus.Cache.Tools = new Modus.Tool.Model.ToolList();
            Modus.Cache.Tools.url = Modus.uriRoot+'/tools/list';
            Modus.Cache.Tools.fetch();
        }
    },

    render : function()
    {
        this.$el.html(this.template({name:this.model.get("name"),ord:this.model.get("ord")}));
        this.description = this.$("#phase-description");
        this.attach = this.$("#phase-attach-values");
        this.attachLink = this.$("#phase-attach-link");
        this.image = this.$("#phase-image");Modus.Helper.activeImageSelector(this.image);
        this.strategies = this.$("#strategy-select");
        this.tools = this.$("#tool-select");
        Modus.Helper.activateSummerNote(this.description);

        this.attachLink.data("attachments",new Modus.Project.Model.AttachmentList());
        this.strategies.data("strategieslist", new Modus.Strategy.Model.StrategyList());
        this.tools.data("toolslist", new Modus.Tool.Model.ToolList());

        this.fill();
        return this;
    },

    events :
    {
        "click #phase-attach-link"  : "attachFile",
        "keyup #strategy-select"    : "showSelectStrategy",
        "show.bs.dropdown .group-strategy-select" : "showSelectStrategy",
        "keyup #tool-select"        : "showSelectTool",
        "show.bs.dropdown .group-tool-select" : "showSelectTool",
        "shown.bs.collapse .panel-collapse"   : "shownCollapse"
    },

    shownCollapse : function(e)
    {
        $('html, body').animate({scrollTop: $(e.currentTarget).offset().top + -75}, 100);
        return true;
    },

    fill : function()
    {
        var that = this;
        var model = that.model.toJSON();

        this.description.code(model.description);
        if(model.image)
        {
            var image = model.image;
            this.image.css('background',"url('"+ image.cdn_url +"') no-repeat center center");
            var w = image.width || 100;
            var h = image.height || 120;
            var showWidth = Math.round(120*w/h);
            this.image.css({width:showWidth,height:120});
            this.image.attr("image_id", image.id);
            this.image.find('.status').addClass("hidden");
        }
        else
        {
            that.image.find('.status').removeClass("hidden");
        }

        if(this.model.get('attachments'))
        {
            this.model.get('attachments').each(this.addAttachBy,this);
        }

        if(Modus.Cache.Strategies.length) this.fillStrategies();
        else Modus.Cache.Strategies.once('sync',this.fillStrategies,this);

        if(Modus.Cache.Tools.length) this.fillTools();
        else Modus.Cache.Tools.once('sync',this.fillTools,this);
    },

    fillStrategies : function()
    {
        if(this.model.get('strategies'))
        {
            _.each(this.model.get('strategies'), this.addStrategyById, this);
        }
    },

    fillTools : function()
    {
        if(this.model.get('tools'))
            _.each(this.model.get('tools'), this.addToolById, this);
    },

    attachFile : function()
    {
        var that = this;
        var percent = false;
        $('<input/>').attr('type','file').fileupload({
            add: Modus.Helper.fileUploadRestriction,
            url: Modus.uriRoot+'/uploads/file',
            dataType: 'json',
            success: function (data)
            {
                that.addAttachBy(new Modus.Project.Model.Attachment(data.success));
                that.attachLink.text('Añadir archivo');
            },
            error : function(xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr, "No se pudo cargar el archivo.");
                alert(msg);
                that.attachLink.text('Añadir archivo');
            },
            progressall: function (e, data)
            {
                var info = "";
                if(data && data.loaded && data.total) info = Math.round((data.loaded/data.total)*100) + "%";
                if(!percent)
                {
                    that.attachLink.html('<i class="fa fa-spinner fa-spin"></i> A&ntilde;adiendo... <span class="percent"></span>');
                    percent = that.attachLink.find('.percent');
                }
                else
                {
                    percent.text(info);
                }
            }
        }).click();
    },

    addAttachBy : function(attach)
    {
        var cdn_url = attach.get("full_url"), original_name = attach.get("original_name");
        if(cdn_url && original_name)
        {
            var selected = this.attachLink.data("attachments")?this.attachLink.data("attachments").pluck("original_name"):[];
            if( !_.contains(selected,original_name) )
            {
                if(!this.attachLink.data("attachments"))this.attachLink.data("attachments", new Modus.Project.Model.AttachmentList);
                this.attachLink.data("attachments").add(attach);
                this.addAttach(attach);
            }
        }
    },

    addAttach : function(attach)
    {
        var original_name = attach.get("original_name");
        var cdn_url = attach.get("full_url");
        var label = $('<li/>').html('<a class="btn btn-sm btn-warning" link-modus target="_blank" href="'+cdn_url+'">'+original_name+'</a>');
        var that = this;
        var button = $('<button type="button" class="close" title="Click para quitar este adjunto">&times;</button>').
        click(function(){
            that.attachLink.data("attachments").remove(attach);
            label.remove();
        });
        label.append(button);

        this.attach.append(label);
    },

    showSelectStrategy : function()
    {
        var that = this;
        var search = this.strategies.val();
        var container = this.strategies.parent().parent();

        var strategies = [];
        var dropdown = container.find('.dropdown-menu');
        if( search.length > 0 )
        {
            strategies = new Modus.Strategy.Model.StrategyList(Modus.Cache.Strategies.filter(function(strategy)
            {
                var name = strategy.get("name").toLowerCase();
                var find = search.toLowerCase();
                return (name.indexOf(find) >= 0);
            }));
        }
        else
        {
            strategies = Modus.Cache.Strategies;
        }
        if( strategies.length > 0 )
        {
            dropdown.empty();
            strategies.each(function(strategy)
            {
                var li = $('<li/>').append($('<a/>').html(strategy.get('name')))
                .click(function()
                {
                    that.addStrategy(strategy);
                });
                dropdown.append(li);
            });
        }
        else
        {
            dropdown.html("<li><a>No se encontraron resultados</a></li>");
        }
    },

    addStrategyById : function(id)
    {
        var strategy = Modus.Cache.Strategies.get(id);
        if(strategy)
        {
            this.addStrategy(strategy);
        }
    },

    addStrategy : function(strategy)
    {
        if(!this.strategies.data('strategieslist')) this.strategies.data('strategieslist', new Modus.Strategy.Model.StrategyList());
        var strategies = this.strategies.data('strategieslist');
        if(strategies.get(strategy.id)) return;

        strategies.add(strategy);
        var container = this.strategies.parent().parent();
        var panel = container.parent().find('.panel-selected');
        if( panel.length<=0 )
        {
            var formgroup = $('<div/>');
            formgroup.addClass('form-group');
            var colsm = $('<div/>');
            colsm.addClass('col-sm-10 col-sm-offset-2')
            formgroup.append(colsm);
            var selected = $('<div/>');
            selected.addClass('panel-selected');
            colsm.append(selected);
            container.after(formgroup);
            panel = selected;
        }

        if( strategy && strategy.id )
        {
            var div = $('<div/>');
            var button_close = $('<button/>')
                .attr({type:'button','aria-hidden':"true"})
                .addClass('close pull-right unselect-strategy')
                .html('&times;');

            var title = strategy.get('name');
            var content='<div class="media"><div class="media-body">'+strategy.get('description')+'</div></div>';
            div.addClass('panel-content')
                    .append(strategy.get('name'))
                    .popover({html:true, trigger: "hover", container:'body',
                        title:title,content:content,placement:'right'});

            div.append(button_close);
            button_close.click(function()
            {
                div.popover('destroy');
                div.remove();
                strategies.remove(strategy);
            });
            panel.append(div);
        }
    },

    showSelectTool : function()
    {
        var that = this;
        var search = this.tools.val();
        var container = this.tools.parent().parent();

        var tools = [];
        var dropdown = container.find('.dropdown-menu');
        if( search.length > 0 )
        {
            tools = new Modus.Tool.Model.ToolList(Modus.Cache.Tools.filter(function(tool)
            {
                var name = tool.get("name").toLowerCase();
                var find = search.toLowerCase();
                return (name.indexOf(find) >= 0);
            }));
        }
        else
        {
            tools = Modus.Cache.Tools;
        }
        if( tools.length > 0 )
        {
            dropdown.empty();
            tools.each(function(tool)
            {
                var li = $('<li/>').append($('<a/>').html(tool.get('name')))
                .click(function()
                {
                    that.addTool(tool);
                });
                dropdown.append(li);
            });
        }
        else
        {
            dropdown.html("<li><a>No se encontraron resultados</a></li>");
        }
    },

    addToolById : function(id)
    {
        var tool = Modus.Cache.Tools.get(id);
        if(tool)
        {
            this.addTool(tool);
        }
    },

    addTool : function(tool)
    {
        if(!this.tools.data('toolslist')) this.tools.data('toolslist', new Modus.Strategy.Model.StrategyList());
        var tools = this.tools.data('toolslist');
        if(tools.get(tool.id)) return;

        tools.add(tool);
        var container = this.tools.parent().parent();
        var panel = container.parent().find('.panel-selected');
        if( panel.length<=0 ){
            var formgroup = $('<div/>');
            formgroup.addClass('form-group');
            var colsm = $('<div/>');
            colsm.addClass('col-sm-10 col-sm-offset-2')
            formgroup.append(colsm);
            var selected = $('<div/>');
            selected.addClass('panel-selected');
            colsm.append(selected);
            container.after(formgroup);
            panel = selected;
        }

        if( tool && tool.id )
        {
            var div = $('<div/>');
            var button_close = $('<button/>')
                .attr({type:'button','aria-hidden':"true"})
                .addClass('close pull-right unselect-tool')
                .html('&times;');
            var title = tool.get('name');
            var content = '<div class="media"><div class="media-body">'+tool.get('description')+'</div></div>';
            div.addClass('panel-content')
                    .append(tool.get('name'))
                    .popover({html:true, trigger: "hover", container:'body',
                        title:title,content:content,placement:'right'});

            div.append(button_close);
            button_close.click(function()
            {
                    div.popover('destroy');
                    div.remove();
                    tools.remove(tool);
            });

            panel.append(div);
        }
    },

    save : function()
    {
        this.model.set("description", this.description.code());
        var attach_ids = this.attachLink.data('attachments')?this.attachLink.data('attachments').pluck('id'):[];
        if(attach_ids && attach_ids.length>0)
        {
            this.model.set("attachments", attach_ids);
        }
        else
        {
            this.model.unset("attachments");
        }
        this.model.set("image_id", this.image.attr('image_id'));

        var strategies_ids = this.strategies.data('strategieslist')?this.strategies.data('strategieslist').pluck('id'):[];
        if(strategies_ids && strategies_ids.length>0)
        {
            this.model.set("strategies", strategies_ids);
        }
        else
        {
            this.model.unset("strategies");
        }

        var tools_ids = this.tools.data('toolslist')?this.tools.data('toolslist').pluck('id'):[];
        if(tools_ids && tools_ids.length>0)
        {
            this.model.set("tools", tools_ids);
        }
        else
        {
            this.model.unset("tools");
        }

        return true;
    }
});