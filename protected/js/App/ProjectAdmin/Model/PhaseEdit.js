/**
 * ID : ProjectAdmin.Model.PhaseEdit
 * require : Projet.Model.AttachmentList
 */
Modus.ProjectAdmin.Model.PhaseEdit = Backbone.Model.extend(
{
    parse : function(response, options){
            return response.success||response;
    },
    initialize : function(params,opts)
    {
        //params && params.strategies && this.set("strategies",new Model.StrategyList(params.strategies));
        //params && params.rules && this.set("rules",new Model.RuleList(params.rules));
        params && params.attachments && this.set("attachments", new Modus.Project.Model.AttachmentList(params.attachments));
    }
});