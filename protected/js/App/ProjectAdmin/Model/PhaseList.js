/**
 * ID : ProjectAdmin.Model.PhaseList
 * require : ProjectAdmin.Model.Phase
 */
Modus.ProjectAdmin.Model.PhaseList = Backbone.Collection.extend(
{
    url : Modus.uriRoot+'/phases',
    model : Modus.ProjectAdmin.Model.Phase,
    parse : function(response, options)
    {
            return response.success||response;
    }
});