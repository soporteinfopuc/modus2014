/**
 * ID : ProjectAdmin.Model.SelectedRoom
 * require : School.Model.SchoolList
 */
Modus.ProjectAdmin.Model.SelectedRoom = Backbone.Model.extend(
{
    defaults:
    {
        "schoolId":  0,
        "levelId":     0,
        "gradeId":    0,
        "rooms_ids" : [],
        "instructors_ids" : [],
        schools : null
    },

    initialize : function()
    {
        this.set('schools', new Modus.School.Model.SchoolList());
    },

    getSchool : function()
    {
        var schoolId = this.get('schoolId');
        var Schools = this.get('schools');
        if(Schools && schoolId>0 && Schools.get(schoolId))
        {
            return Schools.get(schoolId);
        }
        else
        {
            this.set('schoolId', 0);
        }
    },

    getLevel : function()
    {
        var school = this.getSchool(), levelId = this.get('levelId');
        if(school && levelId>0 && school.getLevel(levelId))
        {
            return school.getLevel(levelId);
        }
        else
        {
            this.set('levelId', 0);
        }
    },
    getGrade : function()
    {
        var school = this.getSchool();
        var levelId = this.get('levelId'), gradeId = this.get('gradeId');
        if(school && levelId && gradeId)
        {
            var level = school.getLevels().get(levelId);
            if(level) return level.getGrade(gradeId);
        }
        else
        {
            this.set({'levelId' : 0, 'gradeId': 0});
        }
    },

    getRooms : function()
    {
        var grade = this.getGrade();
        if(grade)
        {
            return grade.getRooms();
        }
        else
        {
            this.set({rooms_ids: [], instructors_ids: []});
        }
    },

    getInstructors : function()
    {
        var rooms = this.getSelectedRooms();
        if(rooms)
        {
            var instructors = new Modus.Instructor.Model.InstructorList();
            rooms.each(function(s)
            {
                s.get('instructors').each(function(i)
                {
                    if(!instructors.get(i.id))
                    {
                        instructors.add(i);
                    }
                });
            });
            return instructors;
        }
    },

    getSelectedRooms : function()
    {
        var rooms_ids = this.get('rooms_ids');
        var selectedRooms = new Modus.School.Model.RoomList();
        var rooms = this.getRooms();
        _.each(rooms_ids, function(room_id)
        {
            selectedRooms.add(rooms.get(room_id));
        });
        return selectedRooms;
    },

    getSelectedInstructors : function()
    {
        var instructors_ids = this.get('instructors_ids');
        var instructors_list = new Modus.Instructor.Model.InstructorList();
        var rooms = this.getSelectedRooms();
        rooms && rooms.each(function(room)
        {
            var instructors = room.get('instructors');
            instructors && instructors.each(function(instructor)
            {
                if(!instructors_list.get(instructor.id) && _.contains(instructors_ids, instructor.id))
                {
                    instructors_list.add(instructor);
                }
            });
        });
        return instructors_list;
    },

    //Setters

    setSchool : function(school_id)
    {
        var schoolId = this.get('schoolId');
        if((school_id != schoolId))
        {
            this.set({schoolId : school_id});
            if(!this.get('levelId') && !this.get('gradeId'))
            {
                if(this.getSchool())
                {
                    var levels = this.getSchool().get('levels');
                    var level_id = levels.at(0).id, grade_id = levels.at(0).getGrades().at(0).id;
                    this.set({levelId:level_id, gradeId:grade_id});
                }
            }
            this.resetAllRooms();
            this.trigger('changeSelected');
        }
    },

    setGrade : function(level_id, grade_id)
    {
        var levelId = this.get('levelId');
        var gradeId = this.get('gradeId');

        if(level_id && grade_id)
        {
            this.set({levelId : level_id, gradeId : grade_id});
            this.resetAllRooms();
            this.trigger('changeSelected');
        }
    },

    addRoom : function(room)
    {
        var rooms = this.get('rooms_ids');
        if(!_.contains(rooms, room.id))
        {
            rooms.push(room.id);
            this.updateAllInstructors();
            this.trigger('changeSelected');
        }
    },

    removeRoom : function(room)
    {
        var rooms = this.get('rooms_ids');
        if(_.contains(rooms, room.id))
        {
            this.set('rooms_ids', _.without(rooms, room.id));
            this.updateAllInstructors();
            this.trigger('changeSelected');
        }
    },

    addInstructor : function(instructor)
    {
        var selected = this.get('instructors_ids');
        if(!_.contains(selected, instructor.id))
        {
            selected.push(instructor.id);
            this.trigger('changeSelected');
        }
    },

    removeInstructor : function(instructor)
    {
        var instructors_ids = this.get('instructors_ids');
        if(_.contains(instructors_ids, instructor.id))
        {
            this.set('instructors_ids', _.without(instructors_ids, instructor.id));
            this.trigger('changeSelected');
        }
    },

    addAllRooms : function()
    {
        var grade = this.getGrade();
        if(grade)
        {
            var rooms = grade.getRooms();
            if(rooms)
            {
                this.set('rooms_ids', rooms.pluck("id"));
                this.updateAllInstructors();
            }
        }
    },

    removeAllRooms : function()
    {
        var rooms = this.get('rooms_ids')
        if(rooms)
        {
            this.set('rooms_ids', []);
            this.updateAllInstructors();
        }
    },

    resetAllRooms : function()
    {
        this.removeAllRooms();
        this.addAllRooms();
    },

    updateAllInstructors : function()
    {
        var instructors_list = new Modus.Instructor.Model.InstructorList();
        var rooms = this.getSelectedRooms();
        rooms && rooms.each(function(room)
        {
            var instructors = room.get('instructors');
            instructors && instructors.each(function(instructor)
            {
                if(!instructors_list.get(instructor.id))
                {
                    instructors_list.add(instructor);
                }
            });
        });
        this.set('selectedInstructors', instructors_list);
    },

    selectRooms : function(rooms)
    {
        this.get('selectedRooms').reset(rooms);
        this.trigger('change:selectedRooms');
    },

    isAllSelected : function()
    {
        return this.getSelectedRooms().length === this.getGrade().getRooms().length;
    },

    isNoneSelected : function()
    {
        return this.getSelectedRooms().length === 0;
    },
});