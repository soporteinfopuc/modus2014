/**
 * ID : Creator.View.School
 */
Modus.Creator.View.School = Backbone.View.extend(
{
    tagName :  "div",
    className : "input-creators input-group-sm",
    template : _.template($('#tmpl-admin-creator-school').html()),
    
    initialize : function(params)
    {
        this.AppCreatorSchools = params.AppCreatorSchools;
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.model, 'destroy', this.remove);
    },

    render : function() 
    {
      this.$el.html(this.template(this.model.toJSON()));
      this.input = this.$('.edit');
      return this;
    },
    
    events : 
    {
      "click a.delete"  : "clear",
    },

    clear: function()
    {
        if(confirm("¿Est\u00e1 seguro que desea eliminar al usuario creador?"))
        {
            var that = this;
            this.model.destroy({wait: true, 
                success : function()
                {
                    that.trigger("remove");
                },
                error : function(model,response){
                    var resp = response.responseJSON;
                    alert(resp.error?resp.error:'No se pudo eliminar.');
            }});
        }
    }
});