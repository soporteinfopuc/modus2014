/**
 * ID : Creator.Model.CreatorList
 * require : Creator.Model.Creator
 */
Modus.Creator.Model.CreatorList = Backbone.Collection.extend(
{
    model : Modus.Creator.Model.Creator,
    url : Modus.uriRoot+'/users/creators',
    parse : function(response, options)
    {
        return response.success?response.success:response;
    }
});