/**
 * ID Boot
 * require GeneralWorkspace
 */

(function()
{
    "use strict";
    
    $.xhrPool = {
        pool:[],
        abortAll:function() {
        $(this.pool).each(function(idx, jqXHR) {
            jqXHR.abort();
        });
        $.xhrPool.pool = [];
    }};
    $.ajaxSetup({
        beforeSend: function(jqXHR) {
            $.xhrPool.pool.push(jqXHR);
        },
        complete: function(jqXHR) {
            var index = $.xhrPool.pool.indexOf(jqXHR);
            if (index > -1) {
                $.xhrPool.pool.splice(index, 1);
            }
        }
    });

    $(document).on("click", "a[modus-link]", function(event)
    {
        if (!event.altKey && !event.ctrlKey && !event.metaKey && !event.shiftKey) {
            if(this.hasAttribute('modus-link') && !this.hasAttribute('target')) {
                event.preventDefault();
                $.xhrPool.abortAll();
                NProgress.done();
                var url = this.getAttribute('href').replace(Modus.uriRoot, "");
                if(url=='/')url='/home';//fix complete slash
                Modus.GeneralRouter.navigate(url, { trigger: true });
            }
        }
    });
    $(document).on("click", "a[modus-link-back]", function(event)
    {
        event.preventDefault();
        $.xhrPool.abortAll();
        NProgress.done();
        history.back();
        return false;
    });
    
    $( document ).ajaxStart(function() {
        NProgress.inc();
    });
    $( document ).ajaxStop(function() {
        NProgress.done();
    });
    $('.year-select').on("click", function()
    {
        var btn = $(this);
        var id = btn.attr('y-id');
        var url = document.URL;
        window.location.href = Modus.uriRoot + '/years/' + id + '?url=' + url;
    })
    
    $('body').tooltip({
        selector: 'a[rel="tooltip"], [data-toggle="tooltip"]'
    });
    
    Modus.Cache = Modus.Cache || {};
    
    
    Backbone.emulateHTTP = true;Backbone.emulateJSON = true;
    
    Modus.GeneralRouter = new Modus.GeneralWorkspace();
    
    Backbone.history.start({pushState: true, root: Modus.uriRoot});
})();