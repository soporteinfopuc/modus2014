/**
 * ID : School.Model.Level
 * require School.Model.GradeList
 */
Modus.School.Model.Level = Backbone.Model.extend(
{
  initialize: function(attrs, opts){
  	this.set('id',attrs.id);
  	this.set('name',attrs.name);
  	this.set('grades', new Modus.School.Model.GradeList(attrs.grades) );
  },
  getGrades : function()
  {
      return this.get('grades');
  },
  getGrade : function(gradeId)
  {
      return this.getGrades().get(gradeId);
  }
});