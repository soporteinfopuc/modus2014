/**
 * ID : School.Model.School
 * require School.Model.LevelList
 */
Modus.School.Model.School = Backbone.Model.extend(
{
    parse : function(response, options){
        var r = response.success?response.success:response;
        r && r.levels && (r.levels = new Modus.School.Model.LevelList(r.levels));
        r && r.coordinators && Modus.Coordinator && Modus.Coordinator.Model.CoordinatorList && (r.coordinators = new Modus.Coordinator.Model.CoordinatorList(r.coordinators));
        r && r.creators && Modus.Creator && Modus.Creator.Model.CreatorList && (r.creators = new Modus.Creator.Model.CreatorList(r.creators));
        return r;
    },
    initialize : function(r,opts)
    {
        //r && r.levels && this.set('levels', new Model.LevelList(r.levels));
    },
    getCoordinators : function()
    {
        return this.get('coordinators');
    },
    getCreators : function()
    {
        return this.get('creators');
    },
    getLevels : function(){
        return this.get('levels');
    },
    getLevel : function(levelId){
        return this.getLevels().get(levelId);
    }
    
});