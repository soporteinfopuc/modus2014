/**
 * ID : School.Model.GradeList
 * require : School.Model.Grade
 */
Modus.School.Model.GradeList = Backbone.Collection.extend(
{
    model : Modus.School.Model.Grade,
    comparator: function(c)
    {
        var name = c.get('name');
        return name;
    }
});