/**
 * ID : School.Model.LevelList
 * require : School.Model.Level
 */
Modus.School.Model.LevelList = Backbone.Collection.extend(
{
	model : Modus.School.Model.Level
});