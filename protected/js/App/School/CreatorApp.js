/**
 * ID : School.CreatorApp
 * require : School.Model.School
 * require : School.View.Creator
 */
Modus.School.CreatorApp = Backbone.View.extend(
{
    tagName:  "div",
    className : "row",
    template: _.template(document.getElementById('tmpl-admin-school-creators').innerHTML),

    events:
    {
      "click #clear-completed": "clearCompleted",
      "click #toggle-all": "toggleAllComplete"
    },

    initialize: function(params) 
    {
        var that = this;
        that.School = new Modus.School.Model.School();
        that.School.url = Modus.uriRoot+"/schools/"+params.schoolId+'/creators';

        that.$el.html('<center style="margin-top: 10%;"><h1><i class="fa fa-circle-o-notch fa-spin"></i> Cargando...</h1></center>');
        that.School.fetch({
            success : function()
            {
                that.Creators = that.School.getCreators();
                that.Creators.url = Modus.uriRoot+"/schools/"+that.School.id+'/creators';
                that.render();

                that.listenTo(that.Creators, 'add', that.addOne);
            },
            error : function(m, xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr,"Error al obtener Creadores de Colegios");
                that.$el.html('<div style="margin-top:10%;color:red"><h2 class="text-center"><i class="fa fa-times-circle"></i> Ocurrio un error<br/>'+msg+'</h2></div>')
            }
        });
    },

    initializeTypeHead : function()
    {
        var that = this;
        var creators = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            limit: 10,
            prefetch: {
              url: Modus.uriRoot + '/users/creators',
              filter: function(list) {
                return $.map(list.success, function(cread) { return { name: cread.name + '(' + cread.username + ')', username:cread.username, id : cread.id }; });
              }
            }
        });
        creators.initialize();
        this.input.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'creators',
            displayKey: 'name',
            source: creators.ttAdapter()
        });
        this.input.on("typeahead:selected", function(event, suggest, nameds)
        {
            that.create(suggest);
        });
    },

    render: function()
    {
        var that = this;
        if(that.Creators)
        {
            that.$el.html($(that.template({school_name : that.School.get('name')})));
            that.inputGroup = that.$('.input-group');
            that.input = that.$("#select-creator");
            that.creatorsList = that.$(".creators-list");
            that.alert = that.$(".alert");
            that.footer = that.$('footer');
            that.main = $('#main');

            that.initializeTypeHead();


            if(that.Creators.length === 0)
            {
                that.showMsg("No existen Creadores del colegio.","info");
            }
            else
            {
                that.alert.hide();
                that.addAll();
            }
        }
        return this;
    },

    addOne : function(creator)
    {
        var that = this;
        var view = new Modus.School.View.Creator({model: creator, AppSchoolCreators : this});
        view.on("remove", function()
        {
            if(that.Creators.length == 0)
            {
                that.showMsg("No existen Creadores del colegio.","info");
            }
        });
        this.creatorsList.append(view.render().el);
    },

    addAll : function()
    {
        this.Creators.each(this.addOne, this);
    },

    createOnEnter : function(e) {
      if (e.keyCode != 13) return;
      this.create();
    },

    showMsg : function(msj, css)
    {
        this.alert.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
        this.alert.addClass("alert-" + (css || 'warning'));
        this.alert.text(msj);
        this.alert.show();
    },

    create : function(creator)
    {
        var that = this;
        this.Creators.create(creator,{wait: true,
            success: function()
            {
                that.input.typeahead('val', "");
                that.alert.hide();
            },
            error : function(m, xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr,"Se produjo un error al asignar un Creador, intentelo nuevamente.\n ");
                that.showMsg(msg,"danger");
            }
        });
    }
});