/**
 * ID : School.CoordinatorApp
 * require : School.Model.School
 * require : School.View.Coordinator
 */
Modus.School.CoordinatorApp = Backbone.View.extend(
{
    tagName:  "div",
    className : "row",
    template: _.template(document.getElementById('tmpl-admin-school-coordinators').innerHTML),

    events:
    {
      "click #clear-completed": "clearCompleted",
      "click #toggle-all": "toggleAllComplete"
    },
            
    initialize: function(params) 
    {
        var that = this;
        that.School = new Modus.School.Model.School();
        that.School.url = Modus.uriRoot+"/schools/"+params.schoolId+'/coordinators';
        
        that.$el.html('<center style="margin-top: 10%;"><h1><i class="fa fa-circle-o-notch fa-spin"></i> Cargando...</h1></center>');
        that.School.fetch({
            success : function()
            {
                that.Coordinators = that.School.getCoordinators();
                that.Coordinators.url = Modus.uriRoot+"/schools/"+that.School.id+'/coordinators';
                that.render();
                
                that.listenTo(that.Coordinators, 'add', that.addOne);
            },
            error : function(m, xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr,"Error al obtener Coordinadores de Colegios");
                that.$el.html('<div style="margin-top:10%;color:red"><h2 class="text-center"><i class="fa fa-times-circle"></i> Ocurrio un error<br/>'+msg+'</h2></div>')
            }
        });
    },
    
    render: function()
    {
        var that = this;
        if(that.Coordinators)
        {
            that.$el.html(that.template({school_name : that.School.get('name')}));
            that.inputGroup = that.$('.input-group');
            that.input = that.$("#select-coordinator");
            that.coordinatorsList = that.$(".coordinators-list");
            that.alert = that.$(".alert");
            that.footer = that.$('footer');
            that.main = $('#main');
            
            that.initializeTypeHead();
                
                
            if(that.Coordinators.length === 0)
            {
                that.showMsg("No existen Coordinadores del colegio.","info");
            }
            else
            {
                that.alert.hide();
                that.addAll();
            }
        }
        return this;
    },
         
    initializeTypeHead : function()
    {
        var that = this;
        var coordinators = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            limit: 10,
            prefetch: {
              url: Modus.uriRoot + '/users/coordinators',
              filter: function(list) {
                return $.map(list.success, function(coord) { return { name: coord.name + '(' + coord.username + ')', username:coord.username, id : coord.id }; });
              }
            }
        });
        coordinators.initialize();
        this.input.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'coordinators',
            displayKey: 'name',
            source: coordinators.ttAdapter()
        });
        this.input.on("typeahead:selected", function(event, suggest, nameds)
        {
            that.create(suggest);
        });
    },
        
    addOne : function(coordinator)
    {
        var that = this;
        var view = new Modus.School.View.Coordinator({model: coordinator, AppSchoolCoordinators : this});
        view.on("remove", function()
        {
            if(that.Coordinators.length == 0)
            {
                that.showMsg("No existen Coordinadores del colegio.","info");
            }
        });
        this.coordinatorsList.append(view.render().el);
    },

    addAll : function()
    {
        this.Coordinators.each(this.addOne, this);
    },

    createOnEnter : function(e) {
      if (e.keyCode != 13) return;
      this.create();
    },
    
    showMsg : function(msj, css)
    {
        this.alert.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
        this.alert.addClass("alert-" + (css || 'warning'));
        this.alert.text(msj);
        this.alert.show();
    },
    
    create : function(coordinator)
    {
        var that = this;
        this.Coordinators.create(coordinator,{wait: true,
            success: function()
            {
                that.input.typeahead('val', "");
                that.alert.hide();
            },
            error : function(m, xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr,"Se produjo un error al asignar un Coordinador, intentelo nuevamente.\n ");
                that.showMsg(msg,"danger");
            }
        });
    }
});