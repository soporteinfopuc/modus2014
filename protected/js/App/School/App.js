/**
 * ID : School.App
 * require : School.Model.List
 * require : School.View.School
 */
Modus.School.App = Backbone.View.extend(
{
    tagName:  "div",
    className : "row",
    template: Modus.Helper.loadTemplate('tmpl-admin-schools'),
    Schools : null,

    events: {
      "keypress #new-school":  "createOnEnter",
      "click #add-school-btn" : "create",
      "click #clear-completed": "clearCompleted",
      "click #toggle-all": "toggleAllComplete",
      "click .reload" : "loadSchools"
    },
            
    initialize: function()
    {
        this.Schools = new Modus.School.Model.SchoolList();
        this.loadSchools();
    },
    
    render: function()
    {
        var that = this;
        that.$el.html(that.template());
        
        that.inputGroup = that.$('.input-group');
        that.input = that.$("#new-school");
        that.alert = that.$(".alert");
        that.footer = that.$('footer');
        that.main = that.$('#main');
        
        that.showMsg('<center><i class="fa fa-spinner fa-spin"></i> Cargando...</center>');
        if(that.Schools && that.Schools.length>0)
        {
            that.listenTo(that.Schools, 'add', that.addOne);
            setTimeout($.proxy(that.addAll,that), 0);
        }
        else
        {
            that.Schools.once("sync", function()
            {
                that.addAll();
                that.Schools.on('add', that.addOne, that);
            },that);
        }
        return this;
    },
         
    loadSchools : function()
    {
        var that = this;
        that.showMsg('<center><i class="fa fa-spinner fa-spin"></i> Cargando...</center>');
        this.Schools.fetch({
            error : function(model,xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr,"No se pudo cargar los colegios");
                that.showMsg('<div style="color:red"><h4 class="text-center"><i class="fa fa-times-circle"></i> Ocurrio un error<br/>'+msg+'<br/><br/><a class="reload"><i class="fa fa-refresh"></i> Reintentar</a></h4></div>', 'danger');
            }
        });
    },
    
    addOne : function(school)
    {
        var view = new Modus.School.View.School({model: school});
        this.$("#schools-list").append(view.render().el);
    },

    addAll : function()
    {
        if(this.Schools.length == 0)
        {
            this.showMsg("No existen colegios en el sistema.","info");
        }
        else
        {
            this.alert.hide();
            this.Schools.each(this.addOne, this);
        }
    },

    createOnEnter : function(e) {
      if (e.keyCode != 13) return;
      this.create();
    },
    
    showMsg : function(msj, css)
    {
        if(this.alert)
        {
            this.alert.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
            this.alert.addClass("alert-" + (css || 'warning'));
            this.alert.html(msj);
            this.alert.show();
        }
    },
    
    create : function()
    {
        var input = this.input;
        if (!input.val())
        {
            input.focus();
            return;
        }
        var that = this;
        this.Schools.create({name:input.val()},{wait: true,
            success: function()
            {
                that.input.val('');
                that.alert && that.alert.hide && that.alert.hide();
            },
            error : function()
            {
                that.showMsg("Se produjo un error al crear el colegio, intentelo nuevamente.\n ","danger");
            }
        });
    }
});