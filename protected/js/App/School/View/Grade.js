/**
 * ID : School.View.Grade
 * require : School.View.Room
 */
Modus.School.View.Grade = Backbone.View.extend(
{
        tagName:  "div",
        className : "grade-view",
        template: _.template(document.getElementById('tmpl-admin-school-level-grade').innerHTML),

        initialize : function(param){
            this.school_id = param.school_id;
            this.level_id = param.level_id;
            this.AppRooms = param.AppRooms;
        },

        render : function(){
            this.$el.html(this.template(this.model.toJSON()));
            this.input = this.$('.input-new-room');

            this.Rooms = this.model.getRooms();
            this.listenTo(this.Rooms, 'add', this.addOne);
            
            this.rooms = this.$('.room-list');
            this.addAll();

            return this;
        },

        events:
        {
            "keypress .input-new-room":  "createOnEnter",
        },

        addOne: function(room) 
        {
            var view = new Modus.School.View.Room({model: room, AppRooms : this.AppRooms, gradeId: this.model.id, levelId:this.level_id, schoolId: this.school_id});
            this.rooms.append(view.render().el);
        },
        
        addAll: function() 
        {
            this.Rooms.each(this.addOne, this);
        },

        createOnEnter: function(e) {
            if (e.keyCode != 13) return;
            if (!this.input.val()) return;
            var that = this;
            var name = that.input.val();
            var grade_id = that.model.get('id');
            this.Rooms.create(
                {
                    name: name,
                    school_id: that.school_id,    
                    grade_id: grade_id,
                }, 
                {   wait: true,
                    success : function()
                    {
                        that.AppRooms.showMsg("Se creó la sección \"" + name +"\".","success");
                    },
                    error : function(model,xhr)
                    {
                        that.AppRooms.showMsgXhr(xhr);
                    }
                }
            );
            this.input.val('');
        }    
    });