/**
 * ID : School.View.Level
 * require : School.View.Grade
 */
Modus.School.View.Level = Backbone.View.extend(
{
    tagName:  "div",
    className : "col-sm-6",
    template: _.template(document.getElementById('tmpl-admin-school-level').innerHTML),

    initialize : function(param){
        this.school_id = param.school_id;
        this.AppRooms = param.AppRooms;
    },

    render : function(){
        this.$el.html(this.template(this.model.toJSON()));
        this.Grades = this.model.getGrades();
        this.grades = this.$('.panel-body');
        this.addAll();
        return this;
    },
    count : 0,
    addOne: function(grade) {
        var view = new Modus.School.View.Grade({model: grade, school_id: this.school_id, AppRooms:this.AppRooms, level_id : this.model.id});
        this.grades.append(view.render().el);
        if(this.count++%2) this.grades.append('<div class="clearfix"/>')
    },
    addAll: function() {
      this.Grades.each(this.addOne, this);
    }
});