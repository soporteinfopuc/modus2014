/**
 * ID : School.View.School
 */
Modus.School.View.School = Backbone.View.extend(
{
    tagName :  "tr",
    className : "input-school",
    template : _.template(document.getElementById('tmpl-admin-schools-school').innerHTML),
    events : 
    {
        "click .toggle"   : "toggleDone",
        "dblclick .view"  : "edit",
        "click a.delete"  : "clear",
        "keypress .edit"  : "updateOnEnter",
        "blur .edit"      : "close"
    },

    initialize : function() {
      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'destroy', this.remove);
    },

    render : function() {
      this.$el.html(this.template(this.model.toJSON()));
      this.input = this.$('.edit');
      return this;
    },

    edit : function() {
      this.$el.addClass("editing");
      this.input.focus();
    },

    close : function() {
      var value = this.input.val();
      if (!value) {
        this.clear();
      } else {
        this.model.save({name: value});
        this.$el.removeClass("editing");
      }
    },

    updateOnEnter: function(e) {
      if (e.keyCode == 13) this.close();
    },

    clear: function(e)
    {
        if(confirm("¿Est\u00e1 seguro que desea eliminar este colegio?"))
        {
            var $el = $(e.currentTarget);
            $el.find('.fa').removeClass('fa-times').addClass('fa-spin fa-spinner');
            this.model.destroy({wait: true, error : 
                function(model,xhr)
                {
                    var msg = Modus.Helper.getMsgXhr(xhr,"No se pudo eliminar, recargue la página y vuelva a intentarlo.")
                    alert(msg);
                    $el.find('.fa').removeClass('fa-spin fa-spinner').addClass('fa-times');
                }
            });
        }
    }
});
