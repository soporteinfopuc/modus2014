/**
 * ID : Student.View.StudentView
 */ 
Modus.Student.View.StudentView = Backbone.View.extend(
    {
        tagName :  "tr",
        className : "",
        template : _.template($('#tmpl-students-content').html()),
        events : {
            "click .checbox-room" : "selectRoom",
            "click .action-edit" : "showEditModal",
            "click .action-delete" : "clear"
        },

        initialize : function(params)
        {
            this.model.set('ord', params.ord);
            this.listenTo(this.model, 'change', this.render);
        },

        render : function()
        {
            var student = this.model.toJSON();
            this.$el.html(this.template({student:student}));

            this.ordview = this.$('#ord-view');
            this.nameview = this.$('#name-view');
            this.lastnameview = this.$('#lastname-view');
            this.usernameview = this.$('#username-view');
            this.passwordview = this.$('#password-view');
            this.emailview = this.$('#email-view');
            this.views = this.$('.view');
            this.views.each($.proxy(function(i,e){
                var el = $(e);
                var val = this.model.get(el.attr('rel'));
                el.text(val);
            },this));
            return this;
        },
        
        showEditModal : function()
        {
            this.trigger("editUserStudent", this.model);
        },

        clear: function() 
        {
            var that = this;
            that.trigger('onPreRemove');
            
            that.$('.action-delete i').removeClass("fa-trash-o").addClass("fa-cog fa-spin");
            var name = this.model.get('name');
            this.model.url = this.model.collection.url + '/' + this.model.id;
            this.model.destroy(
            {
                wait: true,
                success : $.proxy(function(model,response){
                    that.trigger('onRemoveSuccess',name);
                    that.remove();
                },this),
                error : function(model,response)
                {
                    that.$('.action-delete i').removeClass("fa-cog fa-spin").addClass("fa-trash-o");
                    var resp = Modus.Helper.getMsgXhr(response, "No se pudo quitar al alumno de esta sección.");
                    alert(resp);
                }
            });
        }
    });
