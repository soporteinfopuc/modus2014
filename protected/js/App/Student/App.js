/**
 * ID : Student.App
 */
Modus.Student.App = Backbone.View.extend(
{
    tagName : "div",
    className : "row",
    template : _.template($('#tmpl-students-students').html()),
    template_ap : _.template($('#tmpl-students-subpanel').html()),//Additional panel
    template_student : _.template($('#tmpl-students-panel').html()),//Students zone
    ini_ord : 1,
    initialize : function(data)
    {
        this.data = data;
        this.Selected = new Modus.Student.Model.Selected();
        this.Selected.on({"changeSelected" : $.proxy(this.onSelected,this)});
    },

    render : function()
    {
        var that = this;
        var container = $(this.template({
                    schoolId:this.schoolId, levelId:this.levelId,
                    gradeId:this.gradeId, roomId: this.roomId
                })
        ); 
        that.$el.html(container);

        that.leftpanel = that.$('.student-panel-left');
        that.RoomSelect = new Modus.Student.View.RoomSelect(that.Selected);
        that.leftpanel.html(that.RoomSelect.el);
        that.assignForm = that.$('.assign-student-form')


        that.RoomSelect.on("error", function(msg)
        {
            that.$el.html('<div style="margin-top:10%;color:red"><h2 class="text-center"><i class="fa fa-times-circle"></i> Ocurrio un error<br/>'+msg+'</h2></div>')
        });
        var data = that.data;
        that.RoomSelect.on("loadSuccess", function()
        {
            that.initializeTypeHead();

            //Add leftpanel
            var leftpanel = that.RoomSelect.$el;
            leftpanel.append(that.template_ap());
            that.leftpanel.html(leftpanel);
            that.Selected.set({
                schoolId : data.schoolId||0,
                levelId : data.levelId||0,
                gradeId : data.gradeId||0,
                roomId : data.roomId||0
            });
            if(data.schoolId && data.levelId && data.gradeId)
            {
                that.onSelected();
            }
        });

        return this;
    },

    initializeTypeHead : function()
    {
        var that = this;
        that.inputAssign = that.$('#assign-student-input');
        that.searchList = new Bloodhound({
            datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name + ' ' + d.username); },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch:  {
              url: Modus.uriRoot + '/students/listAllNoSigned',
              filter: function(list) {
                return list.success;
              }
            }
        });
        that.searchList.clearPrefetchCache();
        that.searchList.initialize(true);

        that.inputAssign.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'students',
            displayKey : "name",
            source: that.searchList.ttAdapter(),
            templates: {
                suggestion: _.template('<p><%=name%> (<i><%=username%></i>)</p>')
            }
        });
        that.inputAssign.on("typeahead:selected", function(event, suggest, nameds)
        {
            that.assignStudent(suggest);
        });
    },

    events : 
    {
        "click .btn-save"   : "saveChangesStudent",
        "click .btn-delete" : "deleteSelected",
        "click .delete"  : "deleteSelected",
        "click .create-user-student" : "createUserStudent",
        "click .import-from-csv" :"importStudents",
        "click .retry-load-students" : "onSelected",
        "click .export-to-csv" : "exportStudents"
    },

    assignStudent : function(student)
    {
        var that = this;
        var sel = this.Selected;
        var schoolId = sel.get('schoolId'),levelId = sel.get('levelId'),gradeId = sel.get('gradeId'), roomId = sel.get('roomId');
        //that.showMsgAssign("<i class='fa fa-cog fa-spin'></i> Asignando...","info");
        var students;
        var room = this.Selected.getRoom();
        if(!room.get('students'))
        {
            students = new Modus.Student.Model.StudentList();
            students.url = Modus.uriRoot+'/students/'+ schoolId +'/' + gradeId + '/' + roomId;
            room.set('students', students);
        }
        students = room.get('students');

        var success = function()
            {
                that.showMsgAssign("Se asignó correctamente el alumno.","success");
                that.inputAssign.typeahead('val', "");
                that.fillStudents();
                that.reloadSearchList();
            };
        var error = function(m, xhr)
            {
                that.inputAssign.typeahead('val', "");
                var msg = Modus.Helper.getMsgXhr(xhr,"Se produjo un error al asignar un Alumno, intentelo nuevamente.\n ");
                that.showMsgAssign(msg,"danger");
            };
        if(students.get(student.id))
        {
            that.showMsgAssign("Este alumno ya se encuentra en la lista","warning");
        }
        else
        {
            that.showMsgAssign("<i class='fa fa-spinner fa-spin'></i> Agregando alumno...","warning");
            students.create(student,{ wait: true,success: success, error : error });
        }
    },

    showMsgAssign : function(msj, css, elem)
    {
        elem = elem || this.$('.alertassign');
        elem.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
        elem.addClass("alert-" + (css || 'warning'));
        elem.html(msj);
        elem.show();
    },

    setStatus : function(msg,hide)
    {
        if(!this.status)this.status=this.$('#who-students');
        if(this.status){
            this.status./*show().*/fadeTo(0, 1);
            this.status.html(msg);
            if(hide)
                this.status.delay(5000).fadeTo(2000, 0/*, function(){$(this).hide();}*/);
        }
    },

    onSelected : function()
    {
        var sel = this.Selected;
        var schoolId = sel.get('schoolId'),levelId = sel.get('levelId'),gradeId = sel.get('gradeId'), roomId = sel.get('roomId');

            this.gradeIdActual = gradeId;
            this.schoolIdActual = schoolId;
            if(schoolId>0 && levelId>0 && gradeId>0 && roomId>0)
            {
                this.loadStudents();
            }
            else
            {
                this.$('#panel-students').html('<div class="alert alert-info">Para comenzar seleccione el colegio, grado y sección.</div>');
                this.$('.assign-student-form').addClass('hidden');
                this.setStatus("");
            }
    },

    loadStudents : function()
    {
        var that = this;
        var sel = this.Selected;
        var s = sel.get('Schools');
        var schoolId = sel.get('schoolId'),gradeId = sel.get('gradeId'), roomId = sel.get('roomId');

        this.$('#panel-students').html('<div class="text-center"><i class="fa fa-spinner fa-spin"></i> Cargando...</div>');
        that.$('.assign-student-form').addClass('hidden');
        that.setStatus("");

        var room = this.Selected.getRoom();
        if(!room) return;
        if(!room.get('students'))
        {


            room.set('students', new Modus.Student.Model.StudentList);
            var students = room.get('students');
            students.url = Modus.uriRoot+'/students/'+ schoolId +'/' + gradeId + '/' + roomId;
            that.fillStudents();
            that.students.html('<tr class="loading"><td colspan="7" style="display:table-cell;background-color:#fff;"><center style="margin-top:10%;margin-bottom:10%"><h4><i class="fa fa-circle-o-notch fa-spin"></i> Cargando...</h4></center></td></tr>');
            students.once('sync', that.onSync, that);
            students.on("add", that.addOne, that);
            students.on("remove", that.removeOne, that);
            students.fetch({
                success : $.proxy(that.successLoadStudents, that),
                error : function(model, xhr)
                {
                    var msg = Modus.Helper.getMsgXhr(xhr, "No se pudo cargar la lista de alumnos.");
                    that.students.html('<tr class="additional-info"><td colspan="4"><div  class="text-center" style="color:red;"><span><i class="fa fa-times-circle"></i> '+msg+'<br/><u style="cursor:pointer;" class="retry-load-students">Reintentar</u></span></div></td></tr>');
                    room.unset('students');
                }
            });
        }
        else
        {
            that.fillStudents();
        }
    },

    reloadStudents : function()
    {
        var that = this;
        var room = this.Selected.getRoom();
        if(!room) return;
        var students = room.get('students');
        that.fillStudents();
        students.fetch({
            success : $.proxy(that.successLoadStudents, that),
            error : function(model, xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr, "No se pudo cargar la lista de alumnos.");
                that.students.html('<tr class="additional-info"><td colspan="4"><div  class="text-center" style="color:red;"><span><i class="fa fa-times-circle"></i> '+msg+'<br/><u style="cursor:pointer;" class="retry-load-students">Reintentar</u></span></div></td></tr>');
                room.unset('students');
            }
        });
    },

    successLoadStudents : function()
    {
        var that = this;
        var room = this.Selected.getRoom();
        if(!room.get('students').length)
        {
            that.students.html('<tr class="additional-info"><td colspan="5"><div class="text-center"><span><i class="fa fa-exclamation-circle"></i> No se encontraron alumnos asignados.</span></div></td></tr>');
            that.exportBtn.hide();
        }
        else
        {
            that.exportBtn.show();
        }
    },

    fillStudents : function()
    {
        var that = this;
        var sel = this.Selected;
        var roomId = sel.get('roomId');
        var school = sel.getSchool(),level = sel.getLevel(),grade = sel.getGrade(), room = sel.getRoom();
        this.setStatus("Lista de Alumnos: <span style='color:gray'>\"" + school.get('name')+"\"  "+ grade.get("name") + "° de "+ level.get('name')+""+((roomId>0)?("  Sección \""+room.get('name')+"\""):"") + "</span>" , false);

        var t = $(that.template_student()); 
        that.$('#panel-students').html(t);
        that.count = t.find('#allcount');
        that.students = t.find('#students-list');
        that.exportBtn = t.find('.export-to-csv');
        that.checkboxAll = t.find('#all-check');
        that.checkboxAll.on("change", function(e){
            var checked = e.currentTarget.checked;
            that.students.find('.select-check').each(function()
            {
                if(checked && !this.checked)
                    $(this).click();
                else if(!checked && this.checked)
                    $(this).click();
            });
        });
        that.$('.assign-student-form').removeClass('hidden');
        var room = this.Selected.getRoom();
        if(room.get('students').length == 0 && this.students)
        {
            that.students.html('<tr class="additional-info hidden"><td colspan="5" style="background-color: #fff;"><div class="text-center"><span><i class="fa fa-exclamation-circle"></i> No se encontraron alumnos asignados.</span></div></td></tr>');
        }
        else
        {
            this.ini_ord = 1;
            room.get('students').sort();
            room.get('students').each($.proxy(that.addOne,that));
            this.successLoadStudents();
        }
    },
    
    onSync : function(model)
    {
        this.ini_ord = 1;
        model.sort();
    },
    
    addOne : function(student) 
    {
        var that = this;
        if(that.students.find(".loading").length)
        {
            that.students.empty();
        }
        this.students.find('.additional-info').addClass("hidden");
        var sel = this.Selected;
        var view = new Modus.Student.View.StudentView({model: student, ord : that.ini_ord});
        that.ini_ord++;
        view.on("changed", this.changedOne, this);
        view.on("editUserStudent", this.editUserStudent, this);
        view.on("onRemoveSuccess", this.onRemoveSuccessStudent, this);
        view.on("onPreRemove", this.onPreRemoveInstructor, this);
        this.students.append(view.render().el);
        this.successLoadStudents();
    },

    onPreRemoveInstructor : function()
    {
        this.showMsgAssign("<i class='fa fa-cog fa-spin'></i> Quitando alumno...","warning");
    },
    
    removeOne : function()
    {
        this.fillStudents();
        this.reloadSearchList();
    },
    
    reloadSearchList : function()
    {
        this.searchList.clear();
        this.searchList.clearPrefetchCache();
        this.searchList.initialize(true);
    },

    changedOne : function(view)
    {
        this.successLoadStudents();
    },

    showMsg : function(msj, css, elem)
    {
        elem = elem || this.alert;
        elem.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
        elem.addClass("alert-" + (css || 'warning'));
        elem.html(msj);
        elem.show();
    },

    saveChangesStudent : function()
    {
        var unindexed_array = this.modal.find('#form-new-student').serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });
        indexed_array['name'] = indexed_array['firstname'] + ' ' + indexed_array['lastname'];
        var that = this;
        var success = function()
        {
            if(that.modal) that.modal.modal('hide');
            that.searchList.clear();
            that.searchList.clearPrefetchCache();
            that.searchList.initialize(true);
        };
        var error = function(model, xhr, options)
        {
            var msg = Modus.Helper.getMsgXhr(xhr,"Se produjo un error al crear/modificar el alumno, intentelo nuevamente.");
            that.showMsg(msg,"danger", that.modal.find('.alert'));
        };
        if(this.editingUser)
        {
            this.editingUser.url = Modus.uriRoot + '/users/students/' + this.editingUser.id;
            this.editingUser.save(indexed_array, {
                wait: true,
                success : success, error : error
            });
        }
        else
        {
            var student = new Modus.Student.Model.Student();
            student.url = Modus.uriRoot + '/users/students';
            student.save(indexed_array, { wait: true, success : function(model, xhr)
                {
                    var ids = [model.id];
                    that.assignMultipleStudents(ids);
                    if(that.modal) that.modal.modal('hide');
                }, error : error });
        }
    },

    createUserStudent : function()
    {
        this.editingUser = null;
        this.modal = this.$('#modal-student');
        this.modal.find('.alert').hide();
        this.modal.find(".modal-title").text("Agregar Nuevo Alumno");
        this.modal.find('#form-new-student').trigger("reset");
        this.modal.find('#newUser').removeAttr("disabled");
        this.modal.modal();
    },

    editUserStudent : function(user)
    {
        this.editingUser = user;
        this.modal = this.$('#modal-student');
        this.modal.find('.alert').hide();
        this.modal.find(".modal-title").text("Editar Alumno");
        this.modal.find('#form-new-student').trigger("reset");

        this.modal.find('#newUser').attr("disabled","disabled");
        this.modal.find('#newUser').val(user.get('username'));
        this.modal.find('#newName').val(user.get('firstname'));
        this.modal.find('#newLastname').val(user.get('lastname'));
        this.modal.find('#newEmail').val(user.get('email'));
        this.modal.find('#newPassword').val('');
        this.modal.modal();
    },

    onRemoveSuccessStudent : function(name)
    {
        this.showMsgAssign("Se quitó correctamente al alumno " + name, 'success');
    },
    
    assignMultipleStudents : function(ids, done)
    {
        var that = this;
        if(this.Selected.get('roomId') > 0)
        {
            var url = Modus.uriRoot+'/students/import'
            $.post(url,{model : {ids:ids, roomId: this.Selected.get('roomId')}}, 
                function(response)
                {
                    that.reloadStudents();
                    if(response.success && response.success.msg)
                    {
                        alert(response.success.msg);
                    }
                    if(response.success && response.success.error)
                    {
                        _.each(response.success.error,function(err){
                            alert(err);
                        });
                    }
                    done && done();
                }
            , 'json').fail(
                function(xhr)
                {
                    var msg = Modus.Helper.getMsgXhr(xhr, "Ocurrió un error al asignar los alumnos a la sección.");
                    alert(msg);
                    done && done();
                }
            )
        }
        else
        {
            if(ids.length)
            {
                alert("No se asignaron alumnos a la sección, ya que no se había seleccionado una sección.")
            }
            done && done();
        }
    },

    importStudents : function(e)
    {
        var that = this;
        var button = $(e.currentTarget);
        var $upload = $(' <input/>').attr({type:'file', accept : "application/csv"});
        var done = function()
        {
            button.removeAttr("disabled");
            button.find('.fa-spin').remove();

        };
        $upload.fileupload({
            add: Modus.Helper.fileUploadRestriction,
            url: Modus.uriRoot+'/users/students/import/',
            formData : {},
            dataType: 'json',
            done : function (e,data) 
            {
                var assig = false;
                if(data.result && data.result.success && data.result.success.success )
                {
                    if(data.result.success.success.ids)
                    {
                        var ids = data.result.success.success.ids;
                        assig = true;
                        that.assignMultipleStudents(ids, done);
                    }
                    if(data.result.success.success.msg)
                    {
                        alert(data.result.success.success.msg);
                    }
                }
                if(data.result && data.result.success && data.result.success.error)
                {
                    var error = data.result.success.error;
                    if(_.isObject(error) || _.isArray(error)){
                        _.each(error,function(err){
                            alert(err);
                        });
                    }
                    else
                    {
                        alert(error);
                    }
                }
                if(!assig)
                {
                    done();
                }
            },
            error : function(e, data)
            {
                alert("Se produjo un error al importar. comuníquese con el administrador");
                done();
            },
            progressall: function (e, data)
            {
                if(!button.find('.fa-spin').length)
                {
                    button.attr("disabled", "disabled");
                    button.append('&nbsp;<i class="fa fa-spinner fa-spin"></i>');
                }
            }
        }).click();

        return true;
    },
    exportStudents : function(e)
    {
        var that = this;
        var button = $(e.currentTarget);
        if(this.Selected.get('roomId')>0){
            var roomId = this.Selected.get('roomId');
            window.location.replace(Modus.uriRoot+'/students/export/'+roomId);
        }else{
            alert('Para exportar primero seleccione una sección.');
        }
        return true;
    }
});