/**
 * ID : Student.Model.StudentList
 * require : Student.Model.Student
 */
Modus.Student.Model.StudentList = Backbone.Collection.extend({
    model : Modus.Student.Model.Student,
    url : Modus.uriRoot + '/students',
    parse : function(response, options)
    {
        return response.success?response.success:response;
    },
    
    comparator : function(item)
    {
        return item.get('name').toLowerCase();
    },
    
    countNew : function()
    {
        return this.getNew().length;
    },
    getNew : function()
    {
        return this.filter(function(n) { return n.isNew(); });
    },
    countModified : function()
    {
         return this.filter(function(n) { 
            return n.get('isChanged');
        }).length;
    },    
    countSelected : function()
    {
        return this.getSelected().length;
    },
    getSelected : function()
    {
        return this.where({'isSelected':true});
    }
});