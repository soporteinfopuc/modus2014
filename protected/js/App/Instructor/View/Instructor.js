/**
 * ID : Instructor.View.Instructor
 */
Modus.Instructor.View.Instructor = Backbone.View.extend(
{
    tagName :  "tr",
    className : "",
    template : _.template(document.getElementById('tmpl-instructors-content').innerHTML),

    events : 
    {
        "click .checbox-room" : "selectRoom",
        "click .action-edit" : "showEditModal",
        "click .action-delete" : "clear"
    },

    initialize : function(params)
    {
        this.rooms = params.rooms;
        this.listenTo(this.model, 'change', this.render);
    },

    render : function()
    {
        this.$el.html(this.template({instructor:this.model.toJSON(),rooms:this.rooms.toJSON()}));

        this.nameview = this.$('#name-view');
        this.lastnameview = this.$('#lastname-view');
        this.usernameview = this.$('#username-view');
        this.passwordview = this.$('#password-view');
        this.emailview = this.$('#email-view');
        this.views = this.$('.view');
        this.views.each($.proxy(function(i,e){
            var el = $(e);
            el.text(this.model.get(el.attr('rel')));
        },this));
        return this;
    },

    showEditModal : function()
    {
        this.trigger("editUserInstructor", this.model);
    },

    clear: function() 
    {
        var that = this;
        that.trigger('onPreRemove');
        
        that.$('.action-delete i').removeClass("fa-trash-o").addClass("fa-cog fa-spin");
        this.model.url = this.model.collection.url + '/' + this.model.id;
        this.model.destroy(
        {
            wait: true,
            success : $.proxy(function(model,response){
                that.remove();
            },this),
            error : function(model,response)
            {
                that.$('.action-delete i').removeClass("fa-cog fa-spin").addClass("fa-trash-o");
                var resp = Modus.Helper.getMsgXhr(response, "No se pudo eliminar");
                alert(resp);
            }
        });
    },

    selectRoom : function(e)
    {
        var that = this;
        var $current = $(e.currentTarget);
        var room_id = $current.attr('value');

        var instructor = this.model;
        var rooms = instructor.get('rooms');

        var $label = $current.find('label');

        var $newCheckbox = $('<input/>').attr({type:'checkbox',value:room_id});
        if( !_.contains(rooms,room_id) )
        {
            rooms.push(room_id);
            $newCheckbox.prop('checked', true);
        }
        else
        {
            var index = rooms.indexOf(room_id);
            rooms.splice(index,1);
        }
        if(rooms.length>0)
        {
            $label.find('input').replaceWith($newCheckbox);
            var showRooms = function()
            {
                var allRoom = _.filter(that.rooms.toJSON(),function(room)
                {
                    return _.contains(rooms,room.id);
                });
                var allRoom_name = _.pluck(allRoom,'name');
                that.$('[data-toggle="dropdown"]').html(allRoom_name.join(','));
            }
            that.$('[data-toggle="dropdown"]').html("<a><i class='fa fa-cog fa-spin'></i></a>")
            instructor.save({rooms : rooms},
            {
                wait : true,
                success : function(model,xhr)
                {
                    showRooms();
                    that.trigger("changed", that);
                },
                error : function(model,xhr)
                {
                    showRooms();
                }
            });
        }
        else
        {
            this.clear();
        }
        return false;
    }
});
