/**
 * ID : Instructor.Model.Selected
 */
Modus.Instructor.Model.Selected = Backbone.Model.extend(
{
        defaults:
        {
            "schoolId":  0,
            "levelId":     0,
            "gradeId":    0,
            "roomId":    0,
            "Schools" : null,
        },
        getSchool : function()
        {
            var schoolId = this.get('schoolId');
            var Schools = this.get('Schools');
            if(Schools && schoolId>0){
                return Schools.get(schoolId);
            }
        },
        getLevel : function()
        {
            var school = this.getSchool(), levelId = this.get('levelId');
            if(school && levelId>0){
                return school.getLevel(levelId);
            }
        },
        getGrade : function()
        {
            var level = this.getLevel(), gradeId = this.get('gradeId');
            if(level && gradeId>0){
                return level.getGrade(gradeId);
            }
        },
        getRoom : function()
        {
            var grade = this.getGrade(), roomId = this.get('roomId');
            if(grade && roomId>0){
                return grade.getRoom(roomId);
            }
        }
    });
