/**
 * ID : Instructor.Model.Instructor
 */
Modus.Instructor.Model.Instructor = Backbone.Model.extend(
{
    parse : function(response, options)
    {
        return response.success?response.success:response;
    },
    defaults : {
            isSelected : false
    },
    initialize : function(){
        this.set('isSelected',false);
    },
    select : function(){
        this.set('isSelected',true);  
    },
    noSelect : function(){
        this.set('isSelected',false);
    },
    isSelected : function(){
        return this.get('isSelected');
    },
    getFullName : function(){
        return this.get('name') + ' ' + this.get('lastname');
    }
});