/**
 * ID : Coordinator.View.School
 */
Modus.Coordinator.View.School = Backbone.View.extend(
{
    tagName :  "tr",
    className : "",
    template : _.template(document.getElementById('tmpl-admin-coordinator-school').innerHTML),

    initialize : function(params)
    {
        this.AppCoordinatorSchools = params.AppCoordinatorSchools;
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.model, 'destroy', this.remove);
    },

    render : function() 
    {
      this.$el.html(this.template(this.model.toJSON()));
      this.input = this.$('.edit');
      return this;
    },

    events : {
      "click .toggle"   : "toggleDone",
      "click a.delete"  : "clear",
    },
    
    clear: function()
    {
        if(confirm("¿Est\u00e1 seguro que desea eliminar al coordinador?"))
        {
            var that = this;
            this.model.destroy({wait: true, 
                success : function()
                {
                    that.trigger("remove");
                },
                error : function(model,response){
                    var resp = response.responseJSON;
                    alert(resp.error?resp.error:'No se pudo eliminar.');
            }});
        }
    }
});
