/**
 * ID : Coordinator.View.Coordinator
 */
Modus.Coordinator.View.Coordinator = Backbone.View.extend(
{
    tagName :  "tr",
    className : "",
    template : _.template(document.getElementById('tmpl-admin-coordinators-coordinator').innerHTML),
    
    initialize : function() {
      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'destroy', this.remove);
    },

    render : function()
    {
        this.model.set("name", this.model.get('firstname') + ' ' + this.model.get('lastname'));
        this.$el.html(this.template(this.model.toJSON()));
        this.input = this.$('.edit');
        return this;
    },

    events : 
    {
      "click .edit-coordinator" : "showEdit",
      "click a.delete"  : "clear",
    },
    
    showEdit : function()
    {
        this.trigger("showEdit", this.model);
    },

    clear: function(e)
    {
        if(confirm("¿Est\u00e1 seguro que desea eliminar al coordinador?"))
        {
            var $el = $(e.currentTarget);
            $el.find('.fa').removeClass('fa-times').addClass('fa-spin fa-spinner');
            this.model.destroy({wait: true, 
                error : function(model,xhr)
                {
                    var msg = Modus.Helper.getMsgXhr(xhr,"No se pudo eliminar, recargue la página y vuelva a intentarlo.")
                    alert(msg);
                    $el.find('.fa').removeClass('fa-spin fa-spinner').addClass('fa-times');
                }
            });
        }
    }
});