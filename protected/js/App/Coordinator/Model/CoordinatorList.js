/**
 * ID : Coordinator.Model.CoordinatorList
 * require : Coordinator.Model.Coordinator
 */
Modus.Coordinator.Model.CoordinatorList = Backbone.Collection.extend(
{
    model : Modus.Coordinator.Model.Coordinator,
    url : Modus.uriRoot+'/users/coordinators',
    parse : function(response, options)
    {
        return response.success?response.success:response;
    }
});