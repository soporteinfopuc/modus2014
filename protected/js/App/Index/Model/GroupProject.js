/**
 * ID : Index.Model.GroupProject
 */
Modus.Index.Model.GroupProject = Backbone.Model.extend(
{
    parse : function(response, options)
    {
        var r = response.success || response;
        r.projects && (r.projects = new Modus.Project.Model.ProjectList(r.projects));
        return r;
    }
});