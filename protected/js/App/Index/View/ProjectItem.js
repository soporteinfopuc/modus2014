/**
 * ID : Index.View.ProjectItem
 */
Modus.Index.View.ProjectItem = Backbone.View.extend(
{
    tagName:  "div",
    className : "col-sm-6",
    template : _.template($('#tmpl-project-item').html()),
    initialize : function(params)
    {
        this.appIndex = params.appIndex;
    },

    render : function()
    {
        document.title = ('Modus: Inicio');
        this.$el.html(this.template({model : this.model.toJSON()}));
        return this;
    },

    events : 
    {
        "click .action-duplicate" : "duplicateProject",
        "click .action-delete" : "deleteProject"
    },

    deleteProject : function()
    {
        var that = this;
        if(confirm("\u00BFEst\u00e1 seguro que desea eliminar este proyecto?")){
            $.post(
                Modus.uriRoot+'/projects/delete',
                {id : that.model.id},
                function(data){
                    that.remove();
                    if(that.appIndex && that.appIndex.model)
                        that.appIndex.model.fetch({reset:true});
                },
                'json'
            ).fail(function(xhr) {
                if(xhr && xhr.responseJSON && xhr.responseJSON.error && confirm(xhr.responseJSON.error + "\n\u00BFDesea forzar la eliminaci\u00F3n?"))
                {
                    $.post(
                    Modus.uriRoot+'/projects/delete',
                    {id : that.model.id,force:true},
                    function(data){
                        that.remove();
                        if(that.appIndex && that.appIndex.model)
                            that.appIndex.model.fetch({reset:true});
                    },
                    'json').fail(function(a)
                    {
                        if(a && a.responseJSON && a.responseJSON.error)
                            alert(a.responseJSON.error);
                    });
                }
            });
        }
    },
    duplicateProject : function(e)
    {
        var that = this;
        that.trigger('duplicate', this.model);
    },
});