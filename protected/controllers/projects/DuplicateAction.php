<?php

class DuplicateAction extends CAction
{
    public $project_id = 0;
    
    function run()
    {
        $project_id = Yii::app()->request->getPost("id");
        $rooms_ids = Yii::app()->request->getPost("values");
        $user_id = Yii::app()->user->dbid;
        
        $original = Project::model()->findByPk($project_id);
        if($original)
        {
            $isManager = $original->creator_id == $user_id || Yii::app()->user->checkAccess("manageOwnSchoolProjects") || Yii::app()->user->checkAccess("admin");
            if( $isManager )
            {
                if(is_array($rooms_ids) && count($rooms_ids) > 0)
                {
                    $this->controller->error = NULL;
                    $transaction = Yii::app()->db->beginTransaction();
                    $now = time();
                    $saves = 0;
                    
                    foreach ($rooms_ids as $room_id)
                    {
                        $room_id = intval($room_id);
                        $room = Room::model()->findByPk($room_id);
                        if($room)
                        {
                            $level_id = $room->level_id;
                            $school_id = $room->school_id;
                            
                            $new_project = new Project;
                            $new_project->attributes = $original->attributes;
                            $new_project->level_id = $level_id;
                            $new_project->school_id = $school_id;
                            $new_project->created = $now;
                            $new_project->modified = $now;
                            $new_project->creator_id = $user_id;

                            if($new_project->save())
                            {
                                $this->project_id = $new_project->id;
                                
                                $name = $new_project->title;
                                CLog::logProject("createProject", "Se creó el proyecto duplicado $name", $new_project->title);
                                
                                $saves++;
                                
                                //Guardar Sección
                                if($room_id)
                                {
                                    ProjectHelper::updateRooms(array($room_id));
                                }
                                
                                //Guardar profesores
                                $instructors_db = User::model()->with("rooms","authassignment")->findAll("rooms.id=:room_id AND itemname='instructor'", array('room_id' => $room_id), array('select' => 'id'));
                                $instructors_ids = Functions::list_pluck($instructors_db, "id");
                                if($instructors_ids && count($instructors_ids)>0)
                                {
                                    ProjectHelper::updateManagers($instructors_ids);
                                }
                                
                                //Guardar Fases
                                $phases = $original->phases;
                                $order = 1;
                                if(count($phases)){foreach ($phases as $original_phase)
                                {
                                    $new_phase = new Phase;
                                    $new_phase->attributes = $original_phase->attributes;
                                    $new_phase->project_id = $new_project->id;
                                    $new_phase->created = $now;
                                    $new_phase->modified = $now;

                                    if($new_phase->save())
                                    {
                                        
                                        $name = $new_phase->name;
                                        CLog::logProject("createPhase", "Se creó la fase duplicada $name", $new_phase->name);
                                        
                                        $_phase = array('id' => $new_phase->id, 'name' => $new_phase->name);
                                         
                                        //Guardar Adjuntos
                                        if(isset($original_phase->files))
                                        {
                                            $_phase['attachments'] = Functions::list_pluck($original_phase->files, "id");
                                            ProjectHelper::updatePhaseAttachments($_phase);
                                        }
                                        
                                        //Guardar Herramientas y Estrategias
                                        if(isset($original_phase->strategies))
                                        {
                                            $_phase['strategies'] = Functions::list_pluck($original_phase->strategies, "id");
                                            ProjectHelper::updatePhaseStrategies($_phase);
                                        }
                                        if(isset($original_phase->tools))
                                        {
                                            $_phase['tools'] = Functions::list_pluck($original_phase->tools, "id");
                                            ProjectHelper::updatePhaseTools($_phase);
                                        }
                                        
                                        //Guardar criterios de evaluacion
                                        if(isset($original_phase->rules))
                                        {
                                            $rules_ids = array();
                                            $rules = $original_phase->rules;
                                            foreach($original_phase->rules as $original_rule)
                                            {
                                                $rule = new Rule();
                                                $rule->attributes = $original_rule->attributes;
                                                $rule->phase_id = $new_phase->id;
                                                $rule->save();
                                                $rules_ids []= $rule->id;
                                            }
                                            if($rules_ids)
                                            {
                                                $count = count($rules_ids);
                                                $s = $count>1?"s":"";
                                                CLog::logProject("createPhase", "Se agregó $count criterio$s de calificación", $rules_ids);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $this->controller->error = "Error al duplicar fase";
                                    }
                                }}
                            }
                            else
                            {
                                $this->controller->error = "Error al duplicar proyecto";
                            }
                        }
                    }

                    if(!$this->controller->error)
                    {
                        $transaction->commit();
                        $this->controller->success = $saves;
                    }
                    else
                    {
                        $transaction->rollback();
                    }
                }
                else
                {
                    $this->controller->error = "Se necesitan las secciones donde se desea duplicar el proyecto";
                }
            }
            else
            {
                $this->controller->error = "No tiene permiso para duplicar este proyecto.";
            }
        }
        else
        {
            $this->controller->error = "No se encontró el proyecto";
        }
    }
}