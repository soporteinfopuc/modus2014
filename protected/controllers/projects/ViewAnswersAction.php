<?php

class ViewAnswersAction extends CAction
{
    
    
    function run($project_id, $phase_id, $team_id=0, $last_time=0)
    {
        if($project_id>0 && $phase_id>0) 
        {
            if($team_id>0)
            {
                $options = $this->getAnswersOptions($project_id, $phase_id,$team_id,$last_time);
                if($options && count($options))
                {
                    $this->controller->success = $options;
                }
            }
            else
            {
                $this->controller->error = "Se necesita el identificador del equipo";
            }
        }
        else
        {
            $this->controller->error = "Se necesita el identificador del proyecto y la fase";
        }
    }
    
    private function getAnswersOptions($project_id, $phase_id,$team_id,$last_time=0)
    {
        $now = time();
        $result = array();
        $user = Yii::app()->user;
        $user_id = $user->dbid;
        
        $phase = Phase::model()->findByPk($phase_id);
        $project = $phase->project;
        $team = $this->_getTeamById($team_id);
        if($team && isset($team['id']))
        {
            $team_users_id = array();
            if(isset($team['students']))
            {
                $team_users_id = Functions::list_pluck($team['students'], "id");
            }
            $is_my_team = in_array($user_id, $team_users_id);
            
            $is_my_project = $is_my_team //Si pertenece al grupo
                    || ($project->creator_id == $user_id)   //Si es el creador del proyecto
                    || (ProjectManager::model()->countByAttributes(array('project_id' => $project->id, 'manager_id' => $user_id)) > 0) //Si es un profesor asignado como administrador
                    || (SchoolManager::model()->countByAttributes( array('school_id' => $project->school_id, 'user_id' => $user_id)) > 0) //Si es un administrador del colegio (coordinador o creador)
                    || $user->checkAccess("admin"); //Si es administrador del sistema
            
            //Para usuarios del mismo grado (profesores y alumnos)
            $is_same_level = !$is_my_project && (Sign::model()->with('room.level')->countByAttributes(array('year_id' => $this->controller->year(), 'user_id' => $user_id), 'level.id=:levelId', array(':levelId' => $project->level_id) ) > 0);
            
            $project_not_start = $now < $project['start_date'];
            $project_running = $now > $project['start_date'] &&  $now < $project['end_date'];
            $project_end = $now > $project['end_date'];
            
            if( $is_my_project || $is_same_level || $project_end )
            {
                $answers = $this->_getAnswers($phase_id, $team['id'], $is_my_project, $last_time ); //Ver las respuestas a respuestas sólo si pertenece al proyecto
                $result['answers'] = $answers;
                
                $result['options']['can_answer'] = FALSE;
                $result['options']['can_last_answer'] = FALSE;
                $result['options']['last_time'] = $now;
                
                $phases = Phase::model()->findAllByAttributes(array('project_id'=>$project->id), array('select'=>array('id','showfinal_others')));
                $phases_count = count($phases);
                $i = 0;
                $found = false;
                while ( $i < $phases_count && !$found)
                {
                    $phase = $phases[$i];
                    if($phase->id != $phase_id)
                    {
                        if(isset($phase['showfinal_others']) && $phase['showfinal_others'] == TRUE)
                        {
                            $final_answer = Answer::model()->findByAttributes(array('is_last' => 1, 'is_published' => 1, 'phase_id' => $phase->id, 'team_id' => $team_id ),array('select'=>'answer'));
                            if($final_answer && $final_answer->answer)
                            {
                                $result['options']['showfinal_text'] = $final_answer->answer;
                            }
                            $found = true;
                        }
                    }
                    $i++;
                }
                
                if($is_my_team && $project_running)
                {
                    $draft = $this->_getDraftAnswer($phase_id, $team['id'],$user_id);
                    if(count($draft)) $result['draft'] = $draft;
                    
                    $result['options']['can_answer'] = TRUE;
                    $all_published_answers = Answer::model()->findAllByAttributes(array('phase_id' => $phase_id, 'team_id' => $team_id, 'is_published' => 1), array('select' => array('author_id', 'is_last')));
                    foreach ($all_published_answers as $ans)
                    {
                        if($ans['is_last'] == 1)
                        {
                            $result['options']['can_answer'] = FALSE;
                        }
                    }
                    if($result['options']['can_answer'] == TRUE)
                    {
                        $result['options']['can_last_answer'] = TRUE;
                        $all_publihed_answers_ids = Functions::list_pluck($all_published_answers, 'author_id');
                        foreach ($team_users_id as $team_user_id)
                        {
                            if(!in_array($team_user_id, $all_publihed_answers_ids))
                            {
                                $result['options']['can_last_answer'] = FALSE;
                            }
                        }
                    }
                    else
                    {
                        $result['options']['can_last_answer'] = FALSE;
                    }
                }
            }
            else
            {
                $this->controller->error = "No tiene permiso para ver estas respuestas.";
            }
        }
        else
        {
            $this->controller->error = "No se encontro el grupo";
        }
        return $result;
    }
    
    function _getDraftAnswer($phase_id,$team_id,$user_id=NULL)
    {
        if(!$user_id)$user_id = Yii::app()->user->dbid;
        $a = Answer::model()
                ->findByAttributes(array('phase_id' => $phase_id, 'team_id' => $team_id, 'is_published'=> 0, 'author_id' => $user_id));
        $result = $this->controller->_parseAnswer($a);
        return $result;
    }
    
    function _getAnswers($phase_id, $team_id, $view_reanswer = FALSE, $last_time = 0)
    {
        $answers = Answer::model()
                ->findAll(array(
                        'condition'=>'phase_id=:phase_id AND team_id=:team_id AND is_published=1 AND modified > :modified',
                        'params'=>array(':phase_id'=>$phase_id, ':team_id'=>$team_id, 'modified'=>$last_time),
                        'order' => 't.modified ASC'
                        ));
        $result = array();
        foreach ($answers as $k => $a)
            $result[$k] = $this->controller->_parseAnswer($a, $view_reanswer);
        
        return array_values($result);
    }
    
    
    
    /**
     * Obtiene el grupo y los integrantes
     * @param Integer $team_id
     * @param Boolean $inc_users
     * @return Array
     */
    private function _getTeamById($team_id,$inc_users=true)
    {
        $result = array();
        
        $team_db = Team::model()->findByPk($team_id);
        if($team_db)
        {
            $result['id'] = $team_db->id;
            $result['ord'] = $team_db->ord;
            $result['name'] = $team_db->name;
            if($inc_users && $team_db->users)
            {
                $students = array();
                foreach($team_db->users as $k => $u)
                {
                    $students[] = array('id'=>$u->id, 'username'=>$u->username, 'email'=>$u->email, 'firstname'=>$u->firstname, 'lastname'=>$u->lastname);
                }
                $result['students'] = $students;
            }
        }

        return $result;
    }
}