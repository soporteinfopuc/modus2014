<?php

class PostMarkAction extends CAction
{
    
    public $project_id = 0;
    
    public function run()
    {
        $time = time();
        $user_id = Yii::app()->user->dbid;
        
        if(!empty($_POST) && !empty($_POST['model']))
        {
            $model = json_decode($_POST['model'],true);
            $team_id   = isset($model['team'])&&$model['team']>0?$model['team']:FALSE;
            $phase_id   = isset($model['phase'])&&$model['phase']>0?$model['phase']:FALSE;
            $mark       = isset($model['mark'])&&$model['mark']>=0?$model['mark']:-1;
            $rule_id    = isset($model['rule'])&&$model['rule']>0?$model['rule']:FALSE;
            $comment    = isset($model['comment'])?$model['comment']:NULL;
            if($team_id && $phase_id)
            {
                if($rule_id)
                {
                    if($mark>=0)
                    {
                        $options = $this->controller->getQualificationsOptions($phase_id,$team_id);
                        $can_member_mark = $options['options']['can_member_mark'];
                        $can_manager_mark = $options['options']['can_manager_mark'];
                        if($can_manager_mark || $can_member_mark)
                        {
                            $transaction = Yii::app()->db->beginTransaction();
                            $q = Mark::model()->findByAttributes(array('team_id'=>$team_id,'phase_id'=>$phase_id,'rule_id'=>$rule_id));
                            $is_new = FALSE;
                            if(!$q)
                            {
                                $is_new = TRUE;
                                $q = new Mark;
                                $q->team_id    = $team_id;
                                $q->phase_id    = $phase_id;
                                $q->rule_id     = $rule_id;
                            }
                            if($can_member_mark)
                            {
                                $q->mark_member_by     = $user_id;
                                $q->mark_member        = $mark;
                                $q->mark_member_date   = $time;
                                $q->mark_member_comment= $comment;
                            }
                            else if($can_manager_mark)
                            {
                                $q->mark_manager_by       = $user_id;
                                $q->mark_manager          = $mark;
                                $q->mark_manager_date     = $time;
                                $q->mark_manager_comment  = $comment;
                            }
                            if($q->save())
                            {
                                $phase = Phase::model()->findByPk($phase_id, array('select' => array('name','project_id') ));
                                $this->project_id = $phase->project_id;
                                $phase_name = $phase['name'];
                                $data = array($phase_id, $q->attributes);
                                if($is_new)
                                {
                                    CLog::logProject("addMark", "Se agregó una calificación en la fase $phase_name", $data);
                                }
                                else
                                {
                                    CLog::logProject("updateMark", "Se modificó una calificación en la fase $phase_name", $data);
                                }
                                $transaction->commit();
                                $this->controller->success = array('id'=>$q->id);
                            }
                            else
                            {
                                $transaction->rollback();
                                $this->controller->error = "Ocurrio un error al guardar.";
                                if(YII_DEBUG)
                                {
                                    $this->controller->debug = $q->getErrors();
                                }
                            }
                        }
                        else
                        {
                            $this->controller->error = "No puede calificar.";
                        }
                    }
                    else
                    {
                        $this->controller->error = "Debe calificar con alguna nota 0 (cero) o superior.";
                    }
                }
                else
                {
                    $this->controller->error = "No se conoce el criterio a evaluar.";
                }
            }
            else
            {
                $this->controller->error = "Faltan datos del grupo y la fase.";
            }
        }
    }
}