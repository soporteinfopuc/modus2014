<?php

class CreateAction extends CAction
{
    
    public $project_id = 0;
    
    public function run()
    {
        $post = Yii::app()->request->getPost("model");
        if($post)
        {
            $model = json_decode($post, true);
            $order = 1;
            $teams = Team::model()->findAllByAttributes(array('project_id'=>$model['project_id'],'room_id'=>$model['room_id']));
            if($teams) foreach ($teams as $team){
                if($team['ord']==$order)$order++;
            }
            
            $project_id = $model['project_id'];
            
            $this->project_id = $project_id;
            
            $team = new Team;
            $team->project_id = $project_id;
            $team->room_id = $model['room_id'];
            $team->ord = $order;
            $team->name = "Grupo " . $order;
            if($team->save())
            {
                $attributes = $team->attributes;
                $name = $team->name;
                CLog::logProject("createTeam", "Se agregó el grupo $name", $attributes);
                $this->controller->success = array('id'=>$team->id,'ord'=>$team->ord,'name'=>$team->name);
            }
            else
            {
                $this->controller->error = "No se pudo agregar";
            }
        }
    }
}