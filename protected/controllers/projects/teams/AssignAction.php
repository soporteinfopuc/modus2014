<?php

class AssignAction extends CAction
{
    public $project_id = 0;
    
    public function run($project_id)
    {
        $request = Yii::app()->request;
            
        $team_from_id = $request->getPost('team_from');
        $team_to_id = $request->getPost('team_to');
        $user_id = $request->getPost('student');
        if($team_from_id == $team_to_id)
        {
            $this->success = TRUE;
            return;
        }
        $user = User::model()->findByPk($user_id, array('select' => array('id','username')));
        $user_name = $user->username;
        
        $transaction = Yii::app()->db->beginTransaction();
        
        //Si viene de otro grupo, o no ha tenido grupo antes
        if($team_from_id>0)
        {
            $team_from = Team::model()->findByPk($team_from_id);
            $this->project_id = $team_from->project_id;
            ProjectHelper::removeTeamUser($team_from, $user);
        }
        
        //Si se va a agregar a otro grupo, o se va a eliminar del grupo actual
        if($team_to_id>0)
        {
            $teamUser = TeamUser::model()->findByAttributes(array('user_id'=>$user_id,'team_id'=>$team_from_id));
            if(!$teamUser) $teamUser = new TeamUser;
            $teamUser->user_id = $user_id;
            $teamUser->team_id = $team_to_id;
            if($teamUser->save())
            {
                $team_to = Team::model()->findByPk($team_to_id, array('select' => array('name','project_id')));
                $this->project_id = $team_to->project_id;
                $team_to_name = $team_to->name;
                CLog::logProject("createTeamUser", "Se agregó al usuario $user_name al grupo $team_to_name ", array($team_to_id, $user_id));
                $transaction->commit();
                $this->controller->success = true;
            }
            else
            {
                $transaction->rollback();
                $this->controller->error = 'No se pudo asignar';
            }
        }
        else
        {
            TeamUser::model()->deleteAllByAttributes(array('user_id'=>$user_id,'team_id'=>$team_from_id));
            $transaction->commit();
            $this->controller->success = true;
        }
    }
}