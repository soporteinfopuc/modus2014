<?php

class DeleteAction extends CAction
{
    public $project_id = 0;
    
    public function run($team_id)
    {
        $team = Team::model()->findByPk($team_id);
        if($team)
        {
            $transaction = Yii::app()->db->beginTransaction();
            
            $attributes = $team->attributes;
            $this->project_id = $team->project_id;
            
            //Eliminar usuarios del grupo
            if($team->users)
            {
                foreach($team->users as $user)
                {
                    ProjectHelper::removeTeamUser($team, $user);
                }
            }
            
            $team_name = $team->name;
            $room = $team->room;
            $level = $room->level;
            $room_name = $level->name . "° Grado de " . $level->level_name . " sección '" . $room->name . "'";
            
            //Eliminar calificaciones si tuviera
            if($team->marks)
            {
                $marks_db = $team->marks;
                $marks = array();
                foreach ($marks_db as $mark_db)
                {
                    $marks []= $mark_db->attributes;
                }
                $s = count($marks)>1?"s":"";
                $on = count($marks)>1?"ones":"ón";
                
                CLog::logProject("removeMarks","Se quitó la$s calificaci$on del grupo $team_name", array($team->id, $marks) );
            }
            
            
            if($team->delete())
            {
                CLog::logProject("removeTeam","Se quitó el grupo $team_name de $room_name", $attributes );
                
                $transaction->commit();
                $this->controller->success = true;
            }
            else
            {
                $transaction->rollback();
                $this->controller->error = "Ocurrió un error al eliminar el grupo, vuelva a intentarlo";
            }
        }
        else
        {
            $this->controller->error = "El grupo que intenta eliminar no existe o ya se ha eliminado.";
        }
    }
}