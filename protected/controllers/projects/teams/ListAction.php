<?php

class ListAction extends CAction
{
    
    public function run($project_id)
    {
        $project = Project::model()
                ->with(array(
                    'rooms' => array('select' => array('id', 'name', 'level_id')),
                    'rooms.users' => array('select' => array('id', 'username', 'firstname', 'lastname'), 'order' => 'rooms_users.firstname', 'alias' => 'rooms_users'),
                    'rooms.users.authassignment' => array('select' => 'itemname', 'alias' => 'rua'),
                    'teams' => array('select' => array('id', 'ord', 'name','room_id')),
                    'teams.users' => array('select' => array('id', 'username', 'firstname', 'lastname'), 'order' => 'team_users.firstname', 'alias' => 'team_users'),
                    'teams.users.authassignment' => array('select' => 'itemname', 'alias' => 'tua'),
                ))
                ->together()
                ->findByAttributes(array('id' => $project_id), array('select' => array('id', 'level_id') ));
        
        if($project)
        {
            $rooms = $project->rooms;
            //$school = $project->school;
            //$level = $project->level;
            $teams = $project->teams;
            $result = array();
            foreach ($rooms as $room)
            {
                if($project->level_id == $room->level_id)
                {
                    $add = array('id' => $room->id, 'name' => $room->name);
                    $users = $room->users;
                    $room_teams = array();
                    foreach ($teams as $team)
                    {
                        if($team->room_id == $room->id)
                        {
                            $room_teams []= $team;
                        }
                    }
                    $add['teams'] = $this->getTeamsByProjectRoom($users, $room_teams);

                    //$add['school'] = array('id' => $school->id, 'name' => $school->name);

                    //$add['grade'] = array('id' => $level->id, 'name' => $level->name);
                    //$add['grade']['level'] = array('id' => $level->level_num, 'name' => $level->level_name);

                    $result[] = $add;
                }
            }
            $this->controller->success = $result;
        }
        else
        {
            $this->controller->error = "El proyecto no tiene ";
        }
    }
    
    function getTeamsByProjectRoom($users, $teams)
    {
        
        $withoutGrup_db = $users?$users:array();

        $withoutGrup = array();
        foreach ($withoutGrup_db as $k => $u)
        {
            $user_id = $u['id'];

            if($u->authassignment->itemname == 'student')
            {
                $exist = false;
                foreach ($teams as $team)
                {
                    $ids = Functions::list_pluck($team['users'], "id");
                    if(in_array($user_id, $ids))
                    {
                        $exist = true;
                    }
                }
                if(!$exist)
                {
                    $withoutGrup[] = array("id" => $u['id'], 'username'=>$u['username'], 'name' => $u['firstname'] . " " . $u['lastname'], 'firstname' => $u['firstname'], 'lastname' => $u['lastname']);
                }
            }
        }

        $result = array();
        $result[] = array('name' => "Alumnos sin grupo", 'students' => array_values($withoutGrup),'ord'=>0, 'id' => 0);
        foreach($teams as $team)
        {
            $team_id = $team->id;
            $answers = $team->answers;
            $authors_ids = Functions::list_pluck($answers, "author_id");
            $users = array();
            foreach($team['users'] as $u)
            {
                $u_id = $u['id'];
                if($u->authassignment->itemname == 'student')
                {
                    $answer_count = 0;
                    foreach($authors_ids as $author_id)
                    {
                        if($author_id == $u_id)
                        {
                            $answer_count++;
                        }
                    }
                    $users[] = array("id" => $u_id, 'username'=>$u['username'], 'name' => $u['firstname'] . " " . $u['lastname'], 'firstname' => $u['firstname'], 'lastname' => $u['lastname'], 'answer_count' => $answer_count);
                }
            }
            
            $marks =   Mark::model()->countByAttributes(array('team_id' => $team_id));
            
            $result[] = array('name' => $team['name'], 'students' => $users,'ord' => $team['ord'], "id" => $team['id'], 'has_answer' => count($answers), 'has_marks' => $marks);
        }
        return array_values($result);
    }
}