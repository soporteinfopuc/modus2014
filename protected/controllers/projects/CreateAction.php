<?php

class CreateAction extends CAction
{
    
    public $project_id = 0;
    
    public function run()
    {
        $time = time();
        $user_id = Yii::app()->user->dbid;
        $result = array();
        $post = Yii::app()->request->getPost("model");
        if($post)
        {
            $model = json_decode($post, TRUE);
            
            $values = array();
            
            if(isset($model['school_id']) && $model['school_id'])
            {
                $values['school_id'] = $model['school_id'];
            }
            
            if(isset($model['grade_id']) && $model['grade_id'])
            {
                $values['level_id'] = $model['grade_id'];
            }
            
            $values['creator_id'] = $user_id;
            $values['year_id'] = $this->controller->year();
            
            if(isset($model['title']) && $model['title'])
            {
                $values['title'] = trim($model['title']);
                $values['slug'] = Functions::create_slug($values['title']);
            }
            else
            {
                $this->controller->error = "El nombre del proyecto no puede estar vacío.";
            }
            
            if(isset($model['description']) && $model['description'])
            {
                $values['description'] = $model['description'];
            }
            
            if(isset($model['image_id']) && $model['image_id'])
            {
                $values['image_id'] = $model['image_id'];
            }
            
            
            $values['created'] = $time;
            $values['modified'] = $time;
            
            if(isset($model['start_date']) && $model['start_date'])
            {
                $start_date = DateTime::createFromFormat('d/m/Y', $model['start_date']);
                $start_date->setTime(0,0,1);
                $values['start_date'] = is_numeric($model['start_date'])?$model['start_date']:$start_date->getTimestamp();
            }
            else
            {
                $this->controller->error = "Debe seleccionar una fecha de inicio de proyecto";
            }
            
            if(isset($model['end_date']) && $model['end_date'])
            {
                $end_date = DateTime::createFromFormat('d/m/Y', $model['end_date']);
                $end_date->setTime(23,59,59);
                $values['end_date'] = is_numeric($model['end_date'])?$model['end_date']:$end_date->getTimestamp();
            }
            else
            {
                $this->controller->error = "Debe seleccionar una fecha de fin de proyecto";
            }
            
            if(isset($model['type_id']) && $model['type_id'])
            {
                $values['type_id'] = $model['type_id'];
            }
            else
            {
                $values['type_id'] = NULL;
            }
            
            $values['active'] = 1;
            
            if(!$this->controller->error)
            {
            
                $transaction = Yii::app()->db->beginTransaction();

                $project = new Project();
                $project->attributes = $values;
                $save = $project->save();
                if($save)
                {
                    $this->project_id = $project->id;
                    $name = $project->title;
                    CLog::logProject("createProject", "Se creó el proyecto $name", $project->attributes);
                    
                    //Guardar profesores
                    if(isset($model['instructors_ids']))
                    {
                        $instructors_ids = $model['instructors_ids'];
                        ProjectHelper::updateManagers($instructors_ids);
                    }

                    //Guardar Secciones
                    if(isset($model['rooms_ids']))
                    {
                        $rooms_ids = $model['rooms_ids'];
                        ProjectHelper::updateRooms($rooms_ids);
                    }

                    //Guardar Fases
                    $phases = $model['phases'];
                    $phases_ids = $this->savePhases($phases, $project->id);
                }
                else
                {
                    $this->controller->error = $project->getErrors();
                }
            }
        }
        else
        {
            $this->controller->error = "No se envió correctamente el formulario.";
        }
        
        if(!$this->controller->error && $project->id && $project->slug)
        {
            $transaction->commit();
            
            $result['id'] = $project->id;
            $result['slug'] = $project->slug;
            $result['phases_ids'] = $phases_ids;
            $this->controller->success = $result;
        }
        else
        {
            if(isset($transaction)) $transaction->rollback();
        }
    }
    
    private function savePhases($phases, $project_id)
    {
        $phases_ids = array();
        foreach ($phases as $phase)
        {
            $p = new Phase;
            $p->template_phase_id   = isset($phase['template_phase_id'])?$phase['template_phase_id']:'';
            $p->ord                 = isset($phase['ord'])?$phase['ord']:$order;
            $p->project_id          = $project_id;
            $p->name                = isset($phase['name'])?$phase['name']:'';
            $p->description         = isset($phase['description'])?$phase['description']:'';
            if(isset($phase['image_id']))
            {
                $p->image_id  = $phase['image_id'];
            }
            
            //Copiar opciones
            $template_phase = TemplatePhase::model()->findByPk($phase['template_phase_id']);
            
            if(isset($template_phase['can_answer'])) $p['can_answer'] = intval($template_phase['can_answer']);
            if(isset($template_phase['can_mark'])) $p['can_mark'] = intval($template_phase['can_mark']);
            if(isset($template_phase['text_answer'])) $p['text_answer'] = $template_phase['text_answer'];
            if(isset($template_phase['showfinal_others'])) $p['showfinal_others'] = intval($template_phase['showfinal_others']);
                
                
            $time = time();
            $p->created             = $time;
            $p->modified            = $time;
            if($p->save())
            {
                $phase['id'] = $p->id;
                $name = $p->name;
                CLog::logProject("createPhase", "Se creó la fase $name", $p->attributes);
                
                //Guardar adjuntos
                ProjectHelper::updatePhaseAttachments($phase);
                

                //Guardar Herramientas y Estrategias
                ProjectHelper::updatePhaseStrategies($phase);
                ProjectHelper::updatePhaseTools($phase);
                
                //Guardar criterios de evaluacion
                $this->assignRules($template_phase, $p);
            }
            else
            {
                $this->controller->error = $p->getErrors();
            }
        }
        return $phases_ids;
    }
    
    private function assignRules($template_phase, $p)
    {
        $templateRules = $template_phase->templateRules;
        if($templateRules)
        {
            $rules_ids = array();
            foreach($templateRules as $templateRule)
            {
                $rule = new Rule();
                $rule->phase_id = $p->id;
                $rule->ord      = $templateRule->ord;
                $rule->score    = $templateRule->score;
                $rule->rule     = $templateRule->rule;
                $rule->save();
                $rules_ids []= $rule->id;
            }
            $count = count($templateRules);
            $s = $count>1?"s":"";
            CLog::logProject("createPhaseRules", "Se agregó $count criterio$s de calificación", $rules_ids);
        }
    }
}