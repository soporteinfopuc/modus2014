<?php

class UpdateAction extends CAction
{
    
    public $project_id = 0;
    
    public function run()
    {
        $user_id = Yii::app()->user->dbid;
        $result = array();
        $post = Yii::app()->request->getPost("model");
        if($post)
        {
            $model = json_decode($post,true);
            $project = Project::model()->findByPk($model['id']);
            if(!$project)
            {
                $this->controller->error = "El proyecto no existe o se ha eliminado.";
                return;
            }
            $this->project_id = $project->id;
            $old_project = $project->attributes;
            
            $transaction = Yii::app()->db->beginTransaction();
            if($project)
            {
                
                $is_owner = ($user_id == $project->creator_id) || Yii::app()->user->checkAccess("admin");
                $is_principal_manager = false;
                if($project->school_id > 0 && Yii::app()->user->checkAccess("manageOwnSchoolProjects"))
                {
                    $is_principal_manager = SchoolManager::model()->countByAttributes(array('school_id' => $project->school_id, 'user_id' => $user_id)) > 0;
                }
                if($is_owner || $is_principal_manager)
                {
                    
                    $values = array();

                    if(isset($model['school_id']) && $model['school_id'])
                    {
                        $values['school_id'] = $model['school_id'];
                    }
                    else
                    {
                        $values['school_id'] = NULL;
                    }

                    if(isset($model['grade_id']) && $model['grade_id'])
                    {
                        $values['level_id'] = $model['grade_id'];
                    }
                    else
                    {
                        $values['level_id'] = NULL;
                    }

                    if(isset($model['title']) && $model['title'])
                    {
                        $values['title'] = trim($model['title']);
                        $values['slug'] = Functions::create_slug($values['title']);
                    }
                    else
                    {
                        $this->controller->error = "El nombre del proyecto no puede estar vacío.";
                    }

                    if(isset($model['description']) && $model['description'])
                    {
                        $values['description'] = FileHelper::repareCDN($model['description']);
                    }

                    if(isset($model['image_id']) && $model['image_id'])
                    {
                        $values['image_id'] = $model['image_id'];
                    }
                    else
                    {
                        $values['image_id'] = NULL;
                    }

                    $time = time();
                    

                    if(isset($model['start_date']) && $model['start_date'])
                    {
                        if(is_numeric($model['start_date']))
                        {
                            $values['start_date'] = intval($model['start_date']);
                        }
                        else
                        {
                            $start_date = DateTime::createFromFormat('d/m/Y', $model['start_date']);
                            $start_date->setTime(0,0,1);
                            $values['start_date'] = $start_date->getTimestamp();
                        }
                    }
                    else
                    {
                        $this->controller->error = "Debe seleccionar una fecha de inicio de proyecto";
                    }

                    if(isset($model['end_date']) && $model['end_date'])
                    {
                        if(is_numeric($model['end_date']))
                        {
                            $values['end_date'] = intval($model['end_date']);
                        }
                        else
                        {
                            $end_date = DateTime::createFromFormat('d/m/Y', $model['end_date']);
                            $end_date->setTime(23,59,59);
                            $values['end_date'] = $end_date->getTimestamp();
                        }
                    }
                    else
                    {
                        $this->controller->error = "Debe seleccionar una fecha de fin de proyecto";
                    }

                    if(isset($model['type_id']) && $model['type_id'])
                    {
                        $values['type_id'] = $model['type_id'];
                    }
                    else
                    {
                        $values['type_id'] = NULL;
                    }


                    if(!$this->controller->error)
                    {
                        $project->attributes = $values;
                        
                        $diff = array_diff_assoc($project->attributes, $old_project);
                        $ffid = array_diff_assoc($old_project, $project->attributes);
                        if($diff || $ffid)
                        {
                            $project->modified = $time;
                            if(!$project->save())
                            {
                                $this->controller->error = "Ocurrió un error al guardar el proyecto" . json_encode($project->getErrors());
                            }
                            else
                            {
                                $count_diff = count($diff);
                                $s = $count_diff>1?"s":"";
                                CLog::logProject("updateProject", "Se modificó $count_diff atributo$s del proyecto", array('new' => $diff, 'old' => $ffid));
                                $result['slug'] = $project->slug;
                            }
                        }
                    }
                    if(!$this->controller->error)
                    {
                        //Guardar profesores
                        if(isset($model['instructors_ids']))
                        {
                            ProjectHelper::updateManagers($model['instructors_ids']);
                        }
                        
                        //Guardar Secciones
                        if(isset($model['rooms_ids']))
                        {
                            ProjectHelper::updateRooms($model['rooms_ids']);
                        }
                        
                        //Guardar Fases
                        $this->updatePhases($model);
                    }
                }
                else
                {
                    $this->controller->error = "No tiene permiso para modificar este proyecto";
                }
            }
            else
            {
                $this->controller->error = "No existe el proyecto";
            }
            if(!$this->controller->error)
            {
                
                $transaction->commit();
                $this->controller->success = $result;
            }else
            {
                $transaction->rollback();
            }
        }
    }
 
    private function updatePhases($model)
    {
        $phases = isset($model['phases'])?$model['phases']:array();;
        foreach ($phases as $phase)
        {
            $p = Phase::model()->findByPk($phase['id']);
            $original_phase = $p->attributes;
            
            $p->name                = $phase['name'];
            $p->description         = FileHelper::repareCDN($phase['description']);
            $p->image_id            = (isset($phase['image_id']))?$phase['image_id']:NULL;
            $p->project_id          = $model['id'];
            
            $diff = array_diff_assoc($p->attributes, $original_phase);
            $ffid = array_diff_assoc($original_phase, $p->attributes);
            if($diff || $ffid)
            {
                if($p->save())
                {
                    $phase_name = $original_phase['name'];
                    
                    $count_diff = count($diff);
                    $s = $count_diff>1?"s":"";
                    CLog::logProject("updatePhase","Se modificó $count_diff attributo$s de la fase $phase_name", array('new' => $diff, 'old' => $ffid));
                }
                else
                {
                    $this->controller->error = "Ocurrió un error al guardar la fase ".$p->ord . ": ". json_encode($p->getErrors());
                }
            }
            if($this->controller->error) return;
            
            //Guardar adjuntos
            ProjectHelper::updatePhaseAttachments($phase);

            //Guardar Herramientas y Estrategias
            ProjectHelper::updatePhaseTools($phase);
            ProjectHelper::updatePhaseStrategies($phase);
        }
    }
    
}