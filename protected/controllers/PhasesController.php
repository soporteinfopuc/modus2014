<?php

class PhasesController extends Controller
{
    
    public function accessRules()
    {
        return array_merge(parent::accessRules(), array(
            array('allow', 'actions' => array('list','listBasic','read','create','delete','update'),'roles' => array('manageTemplatePhases')),
            array('allow', 'actions' => array('listBasic'),'roles' => array('createProjects')),
            array('deny', 'users'=>array('*')),
        ));
    }
    
    public function actions()
    {
        return array(
            'create'=>'application.controllers.phases.CreateAction',
            'update'=>'application.controllers.phases.UpdateAction',
            'delete'=>'application.controllers.phases.DeleteAction',
        );
    }
    
    
    public function actionList()
    {

        $template_phases = $this->_getPhases();
        $this->success = $template_phases;
    }
    
    public function actionListBasic()
    {
        $template_phases = $this->_getPhases(NULL, FALSE);
        $this->success = $template_phases;
    }
    
    
    function actionRead($id=null)
    {
        $template_phases = $this->_getPhases($id);
        $this->success = reset($template_phases);
    }
    
    
    
    
    
    
    function actionGetListAllPhases($id)
    {
        $order = intval($id);
        if($order <= 0) return ;
        
        $phases_db = Phase::model()->findAllByAttributes(array('ord' => $order),array('select'=>"id"));
        $this->success = Functions::list_pluck($phases_db, "id");
        
    }
    
    
    function _getPhases($template_phase_id=NULL, $include_all = TRUE)
    {
        $template_phases_db;
        if($template_phase_id>0){
            $template_phases_db = TemplatePhase::model()->findAllByPk($template_phase_id);
        }else{
            $template_phases_db = TemplatePhase::model()->findAll(array('order'=>'t.ord'));
        }
        
        $template_phases = array();
        
        foreach($template_phases_db as $k => $template_phase)
        {
            $template_phases[$k]['id']               = $template_phase->id;
            $template_phases[$k]['template_phase_id'] = $template_phase->id;
            $template_phases[$k]['name']             = $template_phase->name;
            $template_phases[$k]['description']      = $template_phase->description;
            if($image = $template_phase->image)
            {
                $template_phases[$k]['image_id'] = $image->id;
                $template_phases[$k]['image_url'] = $this->config->uriCDN . "/uploads/" . $image->md5 . "/" . $image->original_name;
                if(isset($image['properties']))
                {
                    $prop = json_decode($image['properties'], TRUE);
                    if(isset($prop['width']) && isset($prop['height']))
                    {
                        $template_phases[$k]['image_width'] = $prop['width'];
                        $template_phases[$k]['image_height'] = $prop['height'];
                    }
                }
            }
            $template_phases[$k]['ord']      = $template_phase->ord;
            
            $template_phases[$k]['tools_ids']            = Functions::list_pluck($template_phase->tools, "id");
            $template_phases[$k]['strategies_ids']       = Functions::list_pluck($template_phase->strategies, "id");
            
            if($include_all)
            {
                
                $template_phases[$k]['can_mark']         = isset($template_phase['can_mark'])?intval($template_phase['can_mark']):0;
                $template_phases[$k]['can_answer']       = isset($template_phase['can_answer'])?intval($template_phase['can_answer']):0;
                $template_phases[$k]['text_answer']      = isset($template_phase['text_answer'])?($template_phase['text_answer']):"respuesta";
                $template_phases[$k]['showfinal_others'] = isset($template_phase['showfinal_others'])?intval($template_phase['showfinal_others']):0;;


                $template_phases[$k]['modified']         = date( 'd-m-Y H:i:s', $template_phase->modified );
                $template_phases[$k]['modified_ago']     = $this->_human_time_diff($template_phase->modified);



                $template_phases[$k]['rules'] = array();
                foreach($template_phase->templateRules as $r)
                {
                    $template_phases[$k]['rules'] []= array('rule'=>$r->rule, 'ord'=>$r->ord, 'id'=>$r->id,'score'=> intval($r->score) );
                }
            }
        }
        return $template_phases;
    }
    
    function _getNextPhaseOrder()
    {
        $order = 1;
        $template_phases = TemplatePhase::model()->findAll(array('order'=>'ord','select'=>'ord'));
        if($template_phases) foreach ($template_phases as $p){
            if($p['ord']==$order)$order++;
        }
        return $order;
    }
  
    
    
    function actionDeleteRule($id)
    {
        try{
            $model = TemplateRule::model()->findByPk($id);
            $model->delete();
            $this->success = true;
        } catch (Exception $ex) {
            $this->error = 'Esta regla no se puede eliminar.';
        }
    }
}
