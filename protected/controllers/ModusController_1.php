<?php

class ModusController extends Controller
{
    public $templates = array();
    public $js_included = array();
    public $js_main = array('jquery-2.0.3.min','backbone','backbone-pageable','backgrid.min','bootstrap.min','highcharts','jquery-ui-1.10.3.custom.min','jquery.fileupload');
    
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
            $this->js_included = array();
            $user = Yii::app()->user;
            if( $user->isGuest )
            {
                $this->js_included[] = 'login';
            }
            else
            {
                $isAdmin = $user->checkAccess("admin");
                $isCreator = $user->checkAccess("creator");
                $isCoordinator = $user->checkAccess("coordinator");
                $isInstructor = $user->checkAccess("instructor");
                $isStudent = $user->checkAccess("student");
                
                $this->js_included = array(
                        'header','app-model'
                );
                if( $isStudent || $isInstructor ||  $isCoordinator || $isCreator || $isAdmin)
                {
                    $this->js_included[]='project-sidebar';
                    $this->js_included[]='project-phase';
                    $this->js_included[]='project-statistic';
                    $this->js_included[]='project-view';
                }
                if( $isInstructor || $isCoordinator || $isCreator || $isAdmin)
                {
                    $this->js_included[]='project-add';
                }
                if( $isAdmin )
                {
                    $this->js_included[]='schools';
                    $this->js_included[]='rooms';
                    $this->js_included[]='students';
                    $this->js_included[]='instructors';
                
                    $this->js_included[]='admin-users';
                    $this->js_included[]='tools';
                    $this->js_included[]='strategies';
                    $this->js_included[]='phases';
                }
                $this->js_included[]='home';
                $this->js_included[]='boot'; 
            }
            
            // renders the view file 'protected/views/site/index.php'
            // using the default layout 'protected/views/layouts/main.php'
            $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
            if ($error = Yii::app()->errorHandler->error) {
                    if (Yii::app()->request->isAjaxRequest)
                            $this->error = $error['message'];
                    else
                            $this->render('error', $error);
            }
    }
    
    /**
    * Displays the login page
    */
    public function actionLogin()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            if(isset($_POST['username']) && isset($_POST['password']))
            {
                $username = $_POST['username'];
                $password = $_POST['password'];
                $identity=new UserIdentity($username,$password);
                if($identity->authenticate())
                {
                    Yii::app()->user->login($identity);
                    $this->success = true;
                }
                else
                {
                    $this->error = "Nombre de usuario o contraseña incorrecto";
                }
            }
        }
    }
    
    
    public function actionLogout()
    {
            Yii::app()->user->logout();
            $this->redirect(Yii::app()->homeUrl);
    }
    
    public function actionJscript($id, $time)
    {
        ob_get_clean();
        
        header('Cache-Control: max-age=9999999,public,');
        header('Pragma:cache');
        header('Date:Tue, 24 Jan 1989 14:04:14 GMT');
        header('Last-Modified, 24 Jan 1989 14:04:14 GMT');
        header('ETag:"'.$time.'"');
        header('Keep-Alive:timeout=5, max=90');
        header('Content-Type: application/javascript');
            
        if(key_exists('HTTP_IF_NONE_MATCH', $_SERVER))
        {
            header("HTTP/1.1 304 Not Modified"); 
        }
        else
        {
            $script_cache = Yii::app()->cache->get($id);
            if(isset($script_cache['cache'])) echo $script_cache['cache'];
        }
        Yii::app()->end();
    }
    
}