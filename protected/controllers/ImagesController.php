<?php

class ImagesController extends Controller
{
    function actionUpload($folder = "")
    {
        if( isset($_FILES['files']) || isset($_FILES['file']) );
        {
            $file = isset($_FILES['files'])?$_FILES['files']:$_FILES['file'];
            try {
                $tmp_name = is_array($file['tmp_name'])?reset($file['tmp_name']):$file['tmp_name'];
                $type = is_array($file['type'])?reset($file['type']):$file['type'];
                $name = is_array($file['name'])?reset($file['name']):$file['name'];

                $pathinfo = pathinfo($name);
                $extension = $pathinfo['extension'];
                $types = array("jpg","jpeg","gif","png");
                if(in_array(strtolower($extension), $types))
                {
                    $new_name = implode("_",array(rand(1000000,9999999), time(), rand(1000000000,9999999999)));
                    $file_name = $new_name . '_g.' . $extension;
                    $dirpath = Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'img'.($folder?DIRECTORY_SEPARATOR.$folder:"").DIRECTORY_SEPARATOR;
                    $file_path = $dirpath . $file_name;


                    $origen = fopen($tmp_name,"r");
                    $target = fopen($file_path, "w");
                    stream_copy_to_stream($origen, $target);
                    fclose($target);
                    fclose($origen);

                    $result = array();
                    $result['filename'] = $file_name;
                    $result['originalname'] = $name;

                    $this->success = $result;
                }
                else
                {
                    $this->error = "Este tipo de archivo no es aceptado ($extension), solo se acepta " . implode(", ", $types) . ".";
                }
            }
            catch(Exception $ex)
            {
                $this->error = "error";
            }
        }
        
        
    }
}
