<?php

class TeamsController extends Controller
{
   
    
    public function accessRules()
    {
        return array_merge(parent::accessRules(), array(
            array('allow', 'actions' => array('list','create','delete','assign'),'roles' => array('manageProjectTeams')),
            array('deny', 'users'=>array('*')),
        ));
    }
    
    public function actions()
    {
        return array(
            'list'=>'application.controllers.projects.teams.ListAction',
            'create'=>'application.controllers.projects.teams.CreateAction',
            'delete'=>'application.controllers.projects.teams.DeleteAction',
            'assign'=>'application.controllers.projects.teams.AssignAction',
        );
    }
}