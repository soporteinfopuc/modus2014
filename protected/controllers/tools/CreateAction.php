<?php

class CreateAction extends CAction
{
    function run()
    {
        $result = array();
        $post = Yii::app()->request->getPost("model");
        if($post)
        {  
            $model = json_decode($post,true);
            if($model['name'] && $model['url'])
            {
                $tool = new Tool;
                $tool->name = $model['name'];
                $tool->description = $model['description'];
                $tool->url = $model['url'];
                if(isset($model['image_id']))
                {
                        $tool->image_id = $model['image_id'];
                }
                $tool->created = time();
                $tool->modified = $tool->created;
                
                if($tool->save())
                {
                    $name = $tool->name;
                    CLog::logSystem("createTool", "Se creó la herramienta $name", $tool->attributes);
                    $this->controller->actionRead($tool->id);
                }
                else
                {
                    $this->controller->error = "Ocurrio un error al crear.";
                }
            }
            else
            {
                $this->controller->error = "Es necesario un nombre y una dirección web.";
            }
        }
        else
        {
            $this->controller->error = "Ocurrio un error al enviar el formulario";
        }
    }
}