<?php

class UpdateAction extends CAction
{
    function run()
    {
        $post = Yii::app()->request->getPost("model");
        if($post)
        {
            $model = json_decode($post,true);
            try 
            {
                $tool = Tool::model()->findByPk($model['id']); 
                $old_tool = $tool->attributes;
                
                $tool->name = $model['name'];
                $tool->url = $model['url'];
                if(isset($model['image_id']))
                {
                    $tool->image_id = $model['image_id'];
                }
                $tool->description = $model['description'];
                
                $diff = array_diff_assoc($tool->attributes, $old_tool);
                $ffid = array_diff_assoc($old_tool, $tool->attributes);
                if($diff || $ffid)
                {
                    $tool->modified = time();
                    if(!$tool->save())
                    {
                        $this->controller->error = "Ocurrió un error al guardar el proyecto";
                    }
                    else
                    {
                        $count_diff = count($diff);
                        $s = $count_diff>1?"s":"";
                        CLog::logSystem("updateTool", "Se modificó $count_diff atributo$s de la herramienta", array('old' => $ffid, 'new' => $diff));
                        $this->controller->actionRead($tool->id);
                    }
                }
                else
                {
                    $this->controller->success = TRUE;
                }
            }
            catch (Exception $e) 
            {
                $this->controller->error = "No se pudo actualizar.";
                if(YII_DEBUG)
                {
                    $this->controller->debug = $e->getMessage();
                }
            }
        }
    }
}