<?php

class DeleteAction extends CAction
{
    function run($id=NULL)
    {
        try
        {
            $model = Tool::model()->findByPk($id);
            $attr = $model->attributes;
            $name = $model->name;
            $model->delete();
            CLog::logSystem("deleteTool", "se eliminó la herramienta $name.", $attr);
            $this->controller->success = true;
        }
        catch(Exception $e)
        {
            $this->controller->error = 'Esta herramienta no se puede eliminar.';
            if(YII_DEBUG)
            {
                $this->controller->debug = $e->getMessage();
            }
        }
    }
}