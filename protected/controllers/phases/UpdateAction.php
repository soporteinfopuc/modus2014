<?php

class UpdateAction extends CAction
{
    public function run()
    {
        $post = Yii::app()->request->getPost("model");
        if($post)
        {
            $attributes = json_decode($post, true);
            try
            {
                $template_phase = TemplatePhase::model()->findByPk($attributes['id']); 
                $template_phase->name = $attributes['name'];
                $template_phase->description = $attributes['description'];
                if(isset($attributes['image_id'])) $template_phase->image_id = $attributes['image_id'];
                
                if(isset($attributes['can_answer'])) $template_phase['can_answer'] = intval($attributes['can_answer']);
                if(isset($attributes['can_mark'])) $template_phase['can_mark'] = intval($attributes['can_mark']);
                if(isset($attributes['text_answer'])) $template_phase['text_answer'] = $attributes['text_answer'];
                if(isset($attributes['showfinal_others'])) $template_phase['showfinal_others'] = intval($attributes['showfinal_others']);
            
                $template_phase->modified = time();
                $template_phase->ord = isset($attributes['ord'])?$attributes['ord']:$this->controller->_getNextPhaseOrder();
                
                $template_phase->save();
                
                if(isset($attributes['strategies']))
                {
                    TemplatePhaseStrategy::model()->deleteAllByAttributes(array('template_phase_id'=>$template_phase->id));
                    foreach($attributes['strategies'] as $strategy_id)
                    {
                        $dps                    = new TemplatePhaseStrategy;
                        $dps->template_phase_id  = $template_phase->id;
                        $dps->strategy_id       = $strategy_id;
                        $dps->save();
                    }
                }
                
                if(isset($attributes['tools']))
                {
                    TemplatePhaseTool::model()->deleteAllByAttributes(array('template_phase_id'=>$template_phase->id));
                    foreach($attributes['tools'] as $tool_id)
                    {
                        $dps                    = new TemplatePhaseTool;
                        $dps->template_phase_id  = $template_phase->id;
                        $dps->tool_id           = $tool_id;
                        $dps->save();
                    }
                }
                
                if(isset($attributes['rules']))
                {
                    TemplateRule::model()->deleteAllByAttributes(array('template_phase_id'=>$template_phase->id));
                    $order = 1;
                    foreach($attributes['rules'] as $rule)
                    {
                        $r                   = new TemplateRule;
                        $r->template_phase_id = $template_phase->id;
                        $r->rule             = $rule['rule'];
                        $r->score             = $rule['score'];
                        $r->ord              = $order;
                        $order++;
                        $r->save();
                    }
                }
            
                $this->controller->actionRead($template_phase->id);
            }
            catch (Exception $e) 
            {
                $this->controller->error = "No se pudo actualizar";
                if(YII_DEBUG)
                {
                    $this->controller->debug = $e->getMessage();
                }
            }
        }
    }
}