<?php

class DeleteAction extends CAction
{
    function run($id=NULL)
    {
        try
        {
            $transaction = Yii::app()->db->beginTransaction();
            
            $model = TemplatePhase::model()->findByPk($id);
            if(!$model)
            {
                $this->controller->error = "Esta plantilla de fase no existe o ya se ha eliminado.";
                return;
            }
            
            $attributes = $model->attributes;
            $name = $attributes['name'];
            
            if($templateRules = $model->templateRules)
            {
                foreach ($templateRules as $templateRule)
                {
                    CLog::logSystem("deleteTemplatePhaseTemplateRule", "Se eliminó un criterio de la plantilla de fase '$name'", $templateRule->attributes);
                }
            }
            
            if($tools = $model->tools)
            {
                $delete_tools = array();
                $delete_tools_ids = array();
                foreach ($tools as $tool)
                {
                    $delete_tools[] = $tool->name;
                    $delete_tools_ids []= intval($tool->id);
                }
                if($delete_tools)
                {
                    $names = join(", ", $delete_tools);
                    $s = count($delete_tools)>1?"s":"";
                    CLog::logSystem("deleteTemplatePhaseTool", "Se quitó la$s herramienta$s '$names' de la plantilla de fase '$name'", $delete_tools_ids);
                }
            }
            
            if($strategies = $model->tools)
            {
                $delete_strategies = array();
                $delete_strategies_ids = array();
                foreach ($strategies as $strategy)
                {
                    $delete_strategies[] = $strategy->name;
                    $delete_strategies_ids[] = intval($strategy->id);
                }
                if($delete_strategies)
                {
                    $names = join(", ", $delete_strategies);
                    $s = count($delete_strategies)>1?"s":"";
                    CLog::logSystem("deleteTemplatePhaseStrategy", "Se eliminó la$s estrategia$s '$names' de la plantilla de fase '$name'", $delete_strategies_ids);
                }
            }
            
            $model->delete();
            CLog::logSystem("deleteTemplatePhase", "Se eliminó la plantilla de fase '$name'", $attributes);
            
            $transaction->commit();
            $this->controller->success = true;
        }
        catch(Exception $e) 
        {
            $transaction->rollback();
            
            $this->controller->error = 'Esta plantilla de fase no se puede eliminar.';
            if(YII_DEBUG)
            {
                $this->controller->debug = $e->getMessage();
            }
        }
    }
}