<?php

class CreateAction extends CAction
{
    function run()
    {
        $post = Yii::app()->request->getPost("model");
        if($post)
        {
            $model = json_decode($post, true);

            $template_phase = new TemplatePhase();
            $template_phase->name = $model['name'];
            $template_phase->description = $model['description'];
            if(isset($model['image_id'])) $template_phase->image_id = $model['image_id'];
            
            
            if(isset($model['can_answer'])) $template_phase['can_answer'] = intval($model['can_answer']);
            if(isset($model['can_mark'])) $template_phase['can_mark'] = intval($model['can_mark']);
            if(isset($model['text_answer'])) $template_phase['text_answer'] = $model['text_answer'];
            if(isset($model['showfinal_others'])) $template_phase['showfinal_others'] = intval($model['showfinal_others']);
            
            
            $template_phase->modified = time();
            $template_phase->ord = $this->_getNextPhaseOrder();
            $template_phase->active = 1;
            
            if($template_phase->save())
            {
                $attributes = $template_phase->attributes;
                CLog::logSystem("createTemplatePhase", "Se creó la plantilla de fase '$template_phase->name'", $attributes);
                
                if(isset($model['strategies']))
                {
                    foreach($model['strategies'] as $strategy_id)
                    {
                        $dps                    = new TemplatePhaseStrategy;
                        $dps->template_phase_id  = $template_phase->id;
                        $dps->strategy_id       = $strategy_id;
                        $dps->save();
                    }
                }
                
                if(isset($model['tools']))
                {
                    foreach($model['tools'] as $tool_id)
                    {
                        $dps                    = new TemplatePhaseTool;
                        $dps->template_phase_id  = $template_phase->id;
                        $dps->tool_id           = $tool_id;
                        $dps->save();
                    }
                }
                
                if(isset($model['rules']))
                {
                    $order = 1;
                    foreach($model['rules'] as $rule)
                    {
                        $r                   = new TemplateRule;
                        $r->template_phase_id = $template_phase->id;
                        $r->rule             = $rule['rule'];
                        $r->ord              = $order;
                        $r->score            = $rule['score'];
                        $order++;
                        $r->save();
                    }
                }
                
                $this->actionRead($template_phase->id);
            }
            else
            {
                $this->error = "Ocurrió un error al intentar crear la Fase.";
            }
        }
        else
        {
            $this->error = "Ocurrió un error al enviar el formulario, vuelva a intentarlo.";
        }
    }
}