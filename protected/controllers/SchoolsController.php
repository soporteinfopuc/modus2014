<?php

class SchoolsController extends Controller
{
    
    public function accessRules()
    {
        return array_merge(parent::accessRules(), array(
            array('allow', 'actions' => array('list','read','create','delete','update'),'roles' => array('manageSchools')),
            array('allow', 'actions' => array('readCoordinators','assignCoordinator','refuseCoordinator','disableCoordinator'),'roles' => array('manageSchoolCoordinators')),
            array('allow', 'actions' => array('readCreators','assignCreator','refuseCreator','disableCreator'),'roles' => array('manageSchoolCreators')),
            array('allow', 'actions' => array('listAll','ownSchoolsAndGrades'),'roles' => array('canGetOwnSchoolAndGrades')),
            array('deny', 'users'=>array('*')),
        ));
    }
    
    public function actionList($id=0)
    {
        $select = array();
        $select = School::model()->findAll();
        $result = array();
        $user = Yii::app()->user;
        if(is_array($select))
        {
            foreach ($select as $s)
            {
                $coordinators = intval(User::model()->with('authassignment','schools')->count("schools.id=:school_id AND authassignment.itemname='coordinator'",array(':school_id' => $s->id)));
                $creators = intval(User::model()->with('authassignment','schools')->count("schools.id=:school_id AND authassignment.itemname='creator'",array(':school_id' => $s->id)));
                $rooms = intval(Room::model()->countByAttributes(array('school_id' => $s->id, 'year_id' => $this->year())));
                $result[] = array('id' => $s->id, 'name' => $s->name, 'count_coordinators' => $coordinators, 'count_creators' => $creators, 'count_rooms' => $rooms);
            }
        }
        else
        {
            $result = array('id' => $select->id, 'name' => $select->name);
        }
        $this->success = $result;
    }
    
    function actionRead($id=null)
    {
        $school_db = School::model()->findByPk($id);
        if($school_db)
        {
            $school = $school_db->attributes;

            $levels_grades_db = Level::model()->findAll('active=1');
            $levels_grades = array();
            foreach($levels_grades_db as $levels_grade)
            {
                $level_name = $levels_grade['level_name'];
                if(!array_key_exists($level_name, $levels_grades))
                {
                    $levels_grades[$level_name] = array();
                }
                $levels_grades[$level_name]['id'] = $levels_grade['level_num'];
                $levels_grades[$level_name]['name'] = $level_name;
                $levels_grades[$level_name]['grades'][] = array('id'=>$levels_grade->id,'name'=>$levels_grade->name,'rooms'=>array());
            }
            $levels_grades = array_values($levels_grades);


            $grades_db = Level::model()->with('rooms')->together()->findAll(
                    'school_id=:school_id AND active=1 AND year_id=:year_id', array('school_id' => $id, 'year_id' => $this->year()));
            $grades_rooms = array();
            
            $year = $this->year();
            
            foreach($grades_db as $g){
                $rooms = array();
                foreach($g->rooms as $s) 
                {
                    $instructors = User::model()->with('rooms','authassignment')->count("rooms.year_id=:year_id AND rooms.id=:room_id AND itemname='instructor'", array(':year_id' => $year, 'room_id' => $s->id));
                    $students = User::model()->with('rooms','authassignment')->count("rooms.year_id=:year_id AND rooms.id=:room_id AND itemname='student'", array(':year_id' => $year, 'room_id' => $s->id));
                    $rooms[] = array('id' => $s->id, 'name' => $s->name, 'count_instructors' => $instructors, 'count_students' => $students);
                }
                $grades_rooms[$g->id] = $g->attributes;
                $grades_rooms[$g->id]['rooms'] = $rooms;
            }
            $n = count($levels_grades);
            for ($i=0; $i<$n; $i++)
            {
                $level = $levels_grades[$i];
                $grades = $level['grades'];
                $m = count($grades);
                for ($j=0; $j<$m; $j++)
                {
                    $levels_grades[$i]['grades'][$j]['rooms'] = array();
                    if(array_key_exists($grades[$j]['id'], $grades_rooms))
                        $levels_grades[$i]['grades'][$j]['rooms'] = $grades_rooms[$grades[$j]['id']]['rooms'];
                }
            }

            $school['levels'] = $levels_grades;
            $this->success = $school;
        }
        else
        {
            $this->error = "El colegio ya no existe o se ha eliminado.";
        }
    }
    
    public function actionReadCoordinators($id)
    {
        $school_db = School::model()->findByPk($id);
        if(!$school_db)
        {
            $this->error = "El colegio indicado no existe.";
            return;
        }
        $school = array('id' => $school_db->id, 'name' => $school_db->name, 'coordinators' => array());
        $coordinators_db = User::model()->with('schools','authassignment')->findAll("authassignment.itemname='coordinator' AND schools.id=:school_id", array(':school_id' => $id));
        foreach( $coordinators_db as $coordinator_db)
        {
            $school['coordinators'][] = array('id' => $coordinator_db->id, 'name' => $coordinator_db->firstname . " " . $coordinator_db->lastname, 'username' => $coordinator_db->username);
        }
        $this->success = $school;
    }
    
    public function actionAssignCoordinator($school_id, $user_id)
    {
        if(!$user_id)
        {
            $this->error = "Se necesita el nombre de usuario para continuar";
            return;
        }
        $user = User::model()->with('authassignment')->findByPk($user_id, array('condition' => 'authassignment.itemname="coordinator"','select' => array('id','username')));
        if(!$user)
        {
            $this->error = "El usuario que intenta asignar, no es coordinador o no existe.";
            return;
        }
        $school = School::model()->findByPk($school_id, array('select' => array('id','name')));
        if(!$school)
        {
            $this->error = "El colegio que intenta asignar, no existe.";
        }
        
        
        $manager = SchoolManager::model()->findByAttributes(array('school_id' => $school_id, 'user_id' => $user->id));
        if(!$manager)
        {
            $transaction = Yii::app()->db->beginTransaction();

            $manager = new SchoolManager();
            $manager->school_id = $school_id;
            $manager->user_id = $user->id;
            $manager->save();

            $coord_name = $user->username;
            $school_name = $school->name;

            CLog::logSystem("createSchoolCoordinator", "Se asignó el coordinador '$coord_name' al colegio '$school_name'", $manager->attributes);
            $transaction->commit();
            $this->success = true;
        }
        else
        {
            $this->error = "El coordinador ya ha sido asignado a este colegio.";
        }
        
    }
    
    public function actionRefuseCoordinator($school_id, $user_id)
    {
        $user = User::model()->findByPk($user_id, array('select' => array('id','username')));
        if(!$user)
        {
            $this->error = "El usuario que intenta desasignar, no existe.";
            return;
        }
        $school = School::model()->findByPk($school_id, array('select' => array('id','name')));
        if(!$school)
        {
            $this->error = "El colegio que intenta desasignar, no existe.";
        }
        
        try
        {
            $attr = array('school_id' => $school_id, 'user_id' => $user_id);
            SchoolManager::model()->deleteAllByAttributes($attr);
            
            $coord_name = $user->username;
            $school_name = $school->name;
            
            CLog::logSystem("deleteSchoolCreator", "Se desasignó el coordinador '$coord_name' del colegio '$school_name'", $attr);
            
            $this->success = true;
        }
        catch (Exception $ex) 
        {
            $this->error = "No se pudo quitar al usuario";
        }
    }
    
    public function actionReadCreators($id)
    {
        $school_db = School::model()->findByPk($id);
        if(!$school_db)
        {
            $this->error = "El colegio indicado no existe.";
            return;
        }
        $school = array('id' => $school_db->id, 'name' => $school_db->name, 'creators' => array());
        $creators_db = User::model()->with('schools','authassignment')->findAll("authassignment.itemname='creator' AND schools.id=:school_id", array(':school_id' => $id));
        foreach( $creators_db as $creator_db)
        {
            $school['creators'][] = array('id' => $creator_db->id, 'name' => $creator_db->firstname . " " . $creator_db->lastname, 'username' => $creator_db->username);
        }
        $this->success = $school;
    }
    
    public function actionAssignCreator($school_id, $user_id)
    {
        if(!$user_id)
        {
            $this->error = "Se necesita el nombre de usuario para continuar";
            return;
        }
        $user = User::model()->with('authassignment')->findByPk($user_id, array('condition' => 'authassignment.itemname="creator"','select' => array('id','username')));
        if(!$user)
        {
            $this->error = "El usuario que intenta asignar, no es creador o no existe.";
            return;
        }
        $school = School::model()->findByPk($school_id, array('select' => array('id','name')));
        if(!$school)
        {
            $this->error = "El colegio que intenta asignar, no existe.";
        }

        $Creator = SchoolManager::model()->findByAttributes(array('school_id' => $school_id, 'user_id' => $user->id));
        if(!$Creator)
        {
            $transaction = Yii::app()->db->beginTransaction();

            $Creator = new SchoolManager();
            $Creator->school_id = $school_id;
            $Creator->user_id = $user->id;
            $Creator->save();

            $coord_name = $user->username;
            $school_name = $school->name;

            CLog::logSystem("createSchoolCreator", "Se asignó el creador '$coord_name' al colegio '$school_name'", $Creator->attributes);
            $transaction->commit();

            $this->success = true;
        }
        else
        {
            $this->error = "El creador ya ha sido asignado a este colegio.";
        }
    }
    
    public function actionRefuseCreator($school_id, $user_id)
    {
        $user = User::model()->findByPk($user_id, array('select' => array('id','username')));
        if(!$user)
        {
            $this->error = "El usuario que intenta desasignar, no existe.";
            return;
        }
        $school = School::model()->findByPk($school_id, array('select' => array('id','name')));
        if(!$school)
        {
            $this->error = "El colegio que intenta desasignar, no existe.";
        }
        
        try
        {
            $attr = array('school_id' => $school_id, 'user_id' => $user_id);
            SchoolManager::model()->deleteAllByAttributes($attr);
            $coord_name = $user->username;
            $school_name = $school->name;
            CLog::logSystem("deleteSchoolCreator", "Se desasignó el creador '$coord_name' del colegio '$school_name'", $attr);
            $this->success = true;
        }
        catch (Exception $ex) 
        {
            $this->error = "No se pudo quitar al usuario";
        }
    }
    
    
    function actionCreate()
    {
        $post = Yii::app()->request->getPost("model");
        if($post)
        {
            $post = json_decode($post, true);
            $school = new School();
            $school->name = $post['name'];
            if($school->save())
            {
                CLog::logSystem("createSchool", "Se agregó el colegio '$school->name'", $school->attributes);
                $this->success = array('id' => $school->id, 'name' => $school->name, 'count_coordinators' => 0, 'count_creators' => 0, 'count_rooms' => 0);
            }
            else
            {
                $this->error = "Ocurrio un error al crear un colegio.";
            }
        }
        else
        {
            $this->error = "No se envió correctamente el formulario, vuelva a intentarlo.";
        }
        
    }
    
    function actionDelete($id=null)
    {
        try
        {
            $model = School::model()->findByPk($id);
            $attr = $model->attributes;
            $model->delete();
            CLog::logSystem("deleteSchool", "Se eliminó el colegio '".$attr['name']."'", $attr);
            $this->success = true;
        }
        catch(Exception $e)
        {
            $this->error = 'Este colegio no se puede eliminar, debido a que tiene contenido.';
        }
    }
    
    function actionUpdate()
    {
        $result = array();
        if(!empty($_POST) && !empty($_POST['model']))
        {
            $attributes = json_decode($_POST['model'],true);
            $school_name = $attributes['name'];
            try
            {
                $school = School::model()->findByPk($attributes['id']); 
                $old_school = $school->name;
                $school->name = $school_name;
                $school->save();
                CLog::logSystem("updateSchool", "Se modificó el colegio '$old_school'", $school->attributes);
                $result['id'] = $school->id;
            }
            catch (Exception $e)
            {
                $this->error = "No se pudo actualizar:\n".  $e->getMessage(). "";
            }
        }
        $this->success = $result;
    }
    
    
    function actionListAll($include_instructors = false)
    {
        $include_instructors = ($include_instructors)?true:false;
        $user = Yii::app()->user;
        $user_id = $user->dbid;
        
        
        $schools = array();
        $with = array('school','level');
        $cond = array();
        if($include_instructors)
        {
            $with['users'] = array('select' => array('id','firstname','lastname','username'));
            $with['users.authassignment'] = array('select' => 'userid');
            $cond = array('condition' => "itemname='instructor'");
        }
        if( $user->checkAccess("admin") )
        {
            $rooms = Room::model()->with($with)->together()->findAllByAttributes(array('year_id' => $this->year()), $cond);
        }
        else if ( $user->checkAccess("coordinator") || $user->checkAccess("creator") )
        {
            $schools_manager = Functions::list_pluck(SchoolManager::model()->findAllByAttributes(array('user_id' => $user->dbid)), "school_id");;
            $rooms = Room::model()->with($with)->together()->findAllByAttributes(array('year_id' => $this->year(), 'school_id' => $schools_manager), $cond);
        }

        $levels = array();
        foreach ($rooms as $room)
        {

            $school = $room->school;
            $level = $room->level;
            if(!array_key_exists($school->id, $schools))
            {
                    $schools[$school->id] = array('id' => $school->id, 'name' => $school->name, 'levels' => array());
            }
            if(!array_key_exists($level->level_name, $schools[$school->id]['levels']))
            {
                    $schools[$school->id]['levels'][$level->level_name] = array('id' => $level->level_num,'name' => $level->level_name, 'grades' => array());
            }
            if(!array_key_exists($level->id, $schools[$school->id]['levels'][$level->level_name]['grades']))
            {
                    $schools[$school->id]['levels'][$level->level_name]['grades'][$level->id] = array('id' => $level->id, 'name' => $level->name, 'rooms' => array());
            }

            $add = array('id' => $room->id, 'name' => $room->name );

            if($include_instructors)
            {
                    $instructors_db = $room->users;
                    $instructors = array();
                    foreach ($instructors_db as $u)
                    {
                            $instructors[] = array('id' => $u->id, 'name' => $u->firstname . " " . $u->lastname, 'username' => $u->username);
                    }
                    $add['instructors'] = $instructors;
            }
            $schools[$school->id]['levels'][$level->level_name]['grades'][$level->id]['rooms'][] = $add;
        }
        $schools = array_values($schools);
        if($schools)
        {
            foreach ($schools as $k => $school)
            {
                    $schools[$k]['levels'] = array_values($school['levels']);
                    foreach ($schools[$k]['levels'] as $l => $level)
                    {
                            $schools[$k]['levels'][$l]['grades'] = array_values($level['grades']);
                    }
            }
            $this->success = $schools;
        }
        else if( $user->checkAccess("coordinator") || $user->checkAccess("creator") )
        {
                $this->error = "No se encontraron colegios asignados";
        }
        else
        {
                $this->error = "No se encontró ninguna sección, debe crear primero las secciones";
        }
    }
    
    function actionOwnSchoolsAndGrades()
    {
        $user = Yii::app()->user;
        $isAdmin = $user->checkAccess("admin");
        
        $schools_db = array();
        if($isAdmin)
        {
            $schools_db = School::model()->findAll();
        }
        else
        {
            $user_db = User::model()->with('schools')->findByPk($user->dbid);
            $schools_db = $user_db->schools;
        }
        $schools = array();
        foreach ($schools_db as $s)
        {
            $schools[] = array('name' => $s->name, 'id' => $s->id);
        }
        
        $grades_db = Level::model()->findAll('active=1');
        $levels_grades = array();
        foreach($grades_db as $grade)
        {
            $level_num = $grade->level_num;
            if(!array_key_exists($level_num, $levels_grades))
            {
                $levels_grades[$level_num] = array('id' => $level_num, 'name' => $grade->level_name, 'grades' => array());
            }
            $levels_grades[$level_num]['grades'][]= array('id' => $grade->id, 'name' => $grade->name);
        }
        
        $this->success = array('schools' => $schools, 'levels' => array_values($levels_grades) );
    }
    
}