<?php

class CreateAction extends CAction
{
    public function run()
    {
        $result = array();
        $post = Yii::app()->request->getPost("model");
        if($post)
        {    
            $model = json_decode($post,true);
            if($model['name'])
            {
                $strategy = new Strategy;
                $strategy->name = $model['name'];
                $strategy->description = $model['description'];
                if(isset($model['image_id']))
                    $strategy->image_id = $model['image_id'];
                $time = time();
                $strategy->created = $time;
                $strategy->modified = $time;
                
                if($strategy->save())
                {
                    $name = $strategy->name;
                    CLog::logSystem("createStrategy", "Se creó la estrategia $name", $strategy->attributes);
                    $this->actionRead($strategy->id);
                }
                else
                {
                    $this->controller->error = "Ocurrio un error al crear.";
                }
            }
            else
            {
                $this->controller->error = "faltan datos";
            }
        }
        else
        {
             $this->controller->error = "Ocurrio un error al enviar el formulario";
        }
    }
}