<?php

class UploadAction extends CAction
{
    
    function run()
    {
        $result = array('error'=>array());
        $room_id = isset($_POST['roomId'])?$_POST['roomId']:0;
        if($room_id>0){
            $name = $_FILES['files']['name'][0];
            $file = $_FILES['files']['tmp_name'][0];
            $input = fopen($file,"r");
            if($input)
            {
                $count = 0;
                $fila = 0;
                
                while ( ($datos = fgetcsv($input, 1000, ",")) !== FALSE) 
                {
                    $numero = count($datos);
                    $fila++;
                    if( $numero != 5 )
                    {
                        $result['error'] [] = 'La fila ' . $fila .' no es válida';
                    }
                    else
                    {
                        
                        $model = array(
                            'name' => trim($datos[0]),
                            'lastname' => trim($datos[1]),
                            'username' => trim($datos[2]),
                            'password' => crypt(trim($datos[3]),trim($datos[3])),
                            'email' => trim($datos[4])
                        );
                        
                        try
                        {
                            $student = UserHelper::validateUser($model);
                            $role = 'student';
                            if($student_db = UserHelper::upsertUser($student, $role))
                            {
                                RoomHelper::assignStudent($student_db->id, $room_id);
                                $count++;
                            }
                            else
                            {
                                throw new Exception("Ocurrió un error al intentar crear el usuario '$student->username'.");
                            }
                        }
                        catch (Exception $ex)
                        {
                            $result['error'] []= $ex->getMessage();
                        }
                    }
                }
                fclose($input);
                if($count>0)
                {
                    $result['success'] = 'Se importaron '.$count.' alumnos.';
                }
                else
                {
                    $result['error'] []= 'No se importo ningun alumno.';
                }
            }
            else
            {
                $result['error'] []= 'No se pudo abrir el archivo importado.';
            }
        }
        else
        {
            $result['error'] []= 'Se necesita seleccionar una sección.';
        }
        $this->controller->success = $result;
    }
    
}