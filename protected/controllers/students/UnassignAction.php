<?php

class UnassignAction extends CAction
{
    function run($school_id=NULL, $grade_id=NULL,$room_id=NULL,$student_id)
    {
        try
        {
            $unassign = RoomHelper::unassignStudent($student_id, $room_id);
            if($unassign === TRUE)
            {
                $this->controller->success = true;
            }
            else
            {
                $this->controller->error = "Ocurrió un error desconocido al intentar desasignar el alumno";
            }
        }
        catch (Exception $ex)
        {
            $this->controller->error = $ex->getMessage();
        }
    }
    
}