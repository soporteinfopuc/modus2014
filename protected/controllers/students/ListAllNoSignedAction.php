<?php

class ListAllNoSignedAction extends CAction
{
     
    public function run()
    {
        $result = array();
        $students_signed = Sign::model()->findAllByAttributes(array('year_id' => $this->controller->year(), 'active' => 1), array('select' => "user_id"));
        $students_signed_ids = implode(',', Functions::list_pluck($students_signed, "user_id"));
        $condition = "t.active=1 AND authassignment.itemname='student'";
        if($students_signed_ids)
        {
            $condition .= " AND t.id NOT IN ($students_signed_ids)";
        }
        $students_nosigned = User::model()->with('authassignment')->findAll(
                array(
                    'select' => array('id','username','firstname','lastname','email'),
                    'condition' => $condition,
                )
        );
        foreach($students_nosigned as $user)
        {
            $result[] = array('id' => $user->id, 'name' => $user->firstname." ".$user->lastname, 'username' => $user->username, 'firstname' => $user->firstname, 'lastname' => $user->lastname, 'email' => $user->email, 'password' => '**********');
        }
        $this->controller->success = $result;
    }
    
}