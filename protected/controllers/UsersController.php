<?php

class UsersController extends Controller
{
    
    
    public function accessRules()
    {
        return array_merge(parent::accessRules(), array(
            array('allow', 'actions' => array('list','create','update','delete','disable','enable'),'roles' => array('manageUsers')),
            array('allow', 'actions' => array('listCoordinators'),'roles' => array('viewAllManagers')),
            array('allow', 'actions' => array('createCoordinator','deleteCoordinator','updateCoordinator'),'roles' => array('manageCoordinators')),
            array('allow', 'actions' => array('listCreators'),'roles' => array('viewAllCreators')),
            array('allow', 'actions' => array('createCreator','deleteCreator','updateCreator'),'roles' => array('manageCreators')),
            array('allow', 'actions' => array('listInstructors','createInstructor','deleteInstructor','updateInstructor','importInstructors'),'roles' => array('manageInstructors')),
            array('allow', 'actions' => array('listStudent','createStudent','deleteStudent','updateStudent','importStudents'),'roles' => array('manageStudents')),
            array('deny', 'users'=>array('*')),
        ));
    }
    
    public function actions()
    {
        return array(
            'list'=>'application.controllers.users.ListAction',
            'create'=>'application.controllers.users.CreateAction',
            'update'=>'application.controllers.users.UpdateAction',
            'delete'=>'application.controllers.users.DeleteAction',
            'disable'=>'application.controllers.users.DisableAction',
            'enable'=>'application.controllers.users.EnableAction',
            
            'listCoordinators'=>'application.controllers.users.coordinator.ListCoordinatorsAction',
            'createCoordinator'=>'application.controllers.users.coordinator.CreateCoordinatorAction',
            'updateCoordinator'=>'application.controllers.users.coordinator.UpdateCoordinatorAction',
            'deleteCoordinator'=>'application.controllers.users.coordinator.DeleteCoordinatorAction',
            
            'listCreators'=>'application.controllers.users.creator.ListCreatorsAction',
            'createCreator'=>'application.controllers.users.creator.CreateCreatorAction',
            'updateCreator'=>'application.controllers.users.creator.UpdateCreatorAction',
            'deleteCreator'=>'application.controllers.users.creator.DeleteCreatorAction',
            
            'listInstructors'=>'application.controllers.users.instructor.ListInstructorsAction',
            'createInstructor'=>'application.controllers.users.instructor.CreateInstructorAction',
            'updateInstructor'=>'application.controllers.users.instructor.UpdateInstructorAction',
            'deleteInstructor'=>'application.controllers.users.instructor.DeleteInstructorAction',
            'importInstructors'=>'application.controllers.users.instructor.ImportInstructorsAction',
            
            //'listStudents'=>'application.controllers.users.student.ListStudentsAction',
            'createStudent'=>'application.controllers.users.student.CreateStudentAction',
            'updateStudent'=>'application.controllers.users.student.UpdateStudentAction',
            'deleteStudent'=>'application.controllers.users.student.DeleteStudentAction',
            'importStudents'=>'application.controllers.users.student.ImportStudentsAction',
        );
    }
  
    
    
    public function _saveUser($user, $role=NULL)
    {
        $this->verifyAndCreateRole($role);
        $user->username = strtolower($user->username);
        $user->firstname = ucwords($user->firstname);
        $user->lastname = ucwords($user->lastname);
        if(!$user->id)
        {
            $cant = User::model()->countByAttributes(array('username' => $user->username));
            if($cant > 0)
            {
                $this->error = "Ya existe un usuario con el mismo nombre '".$user->username."'.";
                return false;
            }
        }
        if($user->save())
        {
            if($role)
            {
                $user->authassignment = new Authassignment;
                $user->authassignment->userid = $user->username;
                $user->authassignment->itemname = $role;
                $user->authassignment->save();
            }
            return $user;
        }
        else
        {
            $this->error = "Los datos proporcionados para el usuario no son válidos: '". $user->username ."'";
            return false;
        }
    }
            
    
    
    function verifyAndCreateRole($role)
    {
        if($role=="admin")
        {
            $count_creator = Authitem::model()->countByAttributes(array("name" => "admin"));
            if($count_creator == 0)
            {
                $auth = new Authitem;
                $auth->name = "admin";
                $auth->type = 2;
                $auth->description = "Tiene acceso a todo.";
                $auth->save();                        
            }
        }
        else if($role=="creator")
        {
            $count_creator = Authitem::model()->countByAttributes(array("name" => "creator"));
            if($count_creator == 0)
            {
                $auth = new Authitem;
                $auth->name = "creator";
                $auth->type = 2;
                $auth->description = "Tiene acceso a ingresar contenido a las fases. No puede crear un proyecto, el proyecto solo lo crea el administrador.";
                $auth->save();                        
            }
        }
        else if($role=="coordinator")
        {
            $count_coordinator = Authitem::model()->countByAttributes(array("name" => "coordinator"));
            if($count_coordinator == 0)
            {
                $auth = new Authitem;
                $auth->name = "coordinator";
                $auth->type = 2;
                $auth->description = "Tiene acceso a ver todos los proyectos y ver la participación de los docentes y alumnos.";
                $auth->save();                        
            }
        }
    }
}