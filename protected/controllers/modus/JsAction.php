<?php

class JsAction extends CAction
{
    
    public function run($id, $time, $type)
    {
        $relPath = Cache::generateMinifiedScript($id, $time, $type);
        if(file_exists(Yii::app()->basePath.'/../'.$relPath))
            Yii::app()->controller->redirect(Yii::app()->request->baseUrl.'/'.$relPath);
        else
            throw new CHttpException(404,'The specified resource cannot be found.');
    }
}