<?php

class GroupsController extends Controller
{
        
    function actionView($room_id=0,$project_id=0){
        $result = array();
        $users = $this->_getStudentsByRoomId($room_id);
        
        
        $teams = array();
        if( $project_id > 0){ //Si el proyecto ya fue creado
            $teams = $this->_getTeams($room_id, $project_id);
        }
        
        foreach ($teams as $team_id => $team){
            $users_team = isset($team['User'])?$team['User']:array();
            foreach( $users_team as $user_id ){
                $teams[$team_id]['User'][$user_id] = $users[$user_id];
                unset($users[$user_id]);
            }
            $teams[$team_id]['User'] = array_values($teams[$team_id]['User']);
            unset($teams[$team_id]['project_id']);
            unset($teams[$team_id]['room_id']);
        }
        //Los que sobran o no tienen grupo
        $withoutTeam = array('id'=>0,'name'=>'0');
        $withoutTeam['User'] = array();
        foreach ($users as $user){
            $withoutTeam['User'][] = $user;
        }
        array_unshift($teams,$withoutTeam);
        if( count($teams) ){
            $result['success'] = array_values($teams);
        }
        return json_encode($result);
    }
    
    //Obtiene id y nombre de los grupos
    function _getTeams($room_id,$project_id){
        $teams_db = $this->Team->find('all',
              array('conditions'=>
                  array('room_id'=>$room_id,'project_id'=>$project_id),
                  'ord'=> 'name ASC')
        );
        $teams = Set::combine($teams_db,"{n}.Team.id","{n}.Team");
        if( count($teams)){
            $teams_id = array_keys($teams);
            $team_users = $this->TeamUser->find('list',
                    array(
                        'fields' => array('user_id','team_id'),
                        'conditions' => array('team_id'=>$teams_id)
                    )
            );
            foreach ($team_users as $user_id => $team_id){
                $teams[$team_id]['User'][$user_id]=$user_id;
            }
        }
        foreach($teams as $team_id => $team){
            if(!isset($team['User'])){
                $teams[$team_id]['User'] = array();
            }
        }
        return $teams;
    }
    function _getStudentsByRoomId($room_id){
        $roomUsers = $this->RoomUsers->find('list',
                array( 
                    'fields' => array('user_id','id'),
                    'conditions' => array(
                        'room_id' => $room_id
                    )
                )
        );
        //print_r($roomUsers);
        $users_id = array_keys($roomUsers);
        $users_db = $this->User->find('all',
                array(
                    'fields' => array('id','name','lastname'),
                    'conditions' => array('id'=>$users_id,'role_id' => STUDENT),
                    'ord' => 'lastname ASC'
                )
        );
        $users = Set::combine($users_db,"{n}.User.id","{n}.User");
        return $users;
    }
    
    function actionSetStudent()
    {
        $user_id = Yii::app()->user->dbid;
        if($user_id)
        {
            
        }
    }
}
