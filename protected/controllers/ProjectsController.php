<?php

class ProjectsController extends Controller
{
    
    public function actions()
    {
        return array(
            'list'=>'application.controllers.projects.ListAction',
            'export'=>'application.controllers.projects.ExportAction',
            'import'=>'application.controllers.projects.ImportAction',
            'create' => 'application.controllers.projects.CreateAction',
            'update' => 'application.controllers.projects.UpdateAction',
            'read' => 'application.controllers.projects.ReadAction',
            'view' => 'application.controllers.projects.ViewAction',
            'duplicate' => 'application.controllers.projects.DuplicateAction',
            'delete' => 'application.controllers.projects.DeleteAction',
            
            'viewLog' => 'application.controllers.projects.ViewLogAction',
            
            'postMark' => 'application.controllers.projects.PostMarkAction',
            
            'viewPhases' => 'application.controllers.projects.phases.ViewPhasesAction',
            
            'viewAnswers' => 'application.controllers.projects.answers.ListAction',
            'postAnswer' => 'application.controllers.projects.answers.CreateAction',
            'updateAnswer' => 'application.controllers.projects.answers.UpdateAction',
            'deleteAnswer' => 'application.controllers.projects.answers.DeleteAction',
            
            'postReAnswer' => 'application.controllers.projects.reanswers.CreateAction',
            'deleteReAnswer' => 'application.controllers.projects.reanswers.DeleteAction',
            
            'viewMarks' => 'application.controllers.projects.marks.ListAction',
        );
    }
    
    function actionListTypes()
    {
        $result = array();
        $types = ProjectType::model()->findAll();
        foreach ($types as $type)
        {
            $result[] = $type->attributes;
        }
        $this->success = $result;
    }
    
    function _parseAnswer($a, $view_reanswer = FALSE)
    {
        $result = array();
        if($a)
        {
            $result['id'] = $a->id;
            $result['answer'] = FileHelper::changeImageSrc($a->answer);
            $result['created'] = $a->created;
            $result['modified'] = $a->modified;
            $result['is_published'] = $a->is_published;
            $result['is_last'] = $a->is_last;
            if($author = $a->author)
            {
                $result['student_id'] = $author->id;
                $result['student_name'] = $author->firstname . " " . $author->lastname;
                $result['student_username'] = $author->username;
            }
            $result['created'] = date('d/m/Y h:i:sa', $a['created']);
            $result['created_ago'] = $this->_human_time_diff($a['created']);
            $result['modified'] = date( 'd/m/Y h:i:sa', $a['modified'] );
            $result['modified_ago'] = $this->_human_time_diff($a['modified']);
            
            if($a->files && ($attachment = $a->files[0]))
            {
                $result['attachment_id'] = $attachment->id;
                $result['attachment_url'] = $this->config->uriCDN . "/uploads/" . $attachment->md5 . "/" . $attachment->original_name;
                $result['attachment_name'] = $attachment->original_name;
                $result['attachment_type'] = FileHelper::getFileType($attachment->original_name, $attachment->md5);
            }
            
            
            if($view_reanswer)
            {
                $reanswers_db = Reanswer::model()->findAllByAttributes(array('answer_id'=>$a->id));
                if($reanswers_db)
                {
                    $reanswers = array();
                    foreach($reanswers_db as $reanswer) $reanswers[] = $this->_parseReAnswer($reanswer);
                    $result['reanswers'] = $reanswers;
                }
            }
        }
        return $result;
    }
    
    //Obtiene el grupo y los integrantes
    function _getTeamById($team_id,$inc_users=true)
    {
        $result = array();
        
        $team_db = Team::model()->findByPk($team_id);
        if($team_db){
            $result['id'] = $team_db->id;
            $result['ord'] = $team_db->ord;
            $result['name'] = $team_db->name;
        }
        $students = array();
        if($team_db && $team_db->users) foreach($team_db->users as $k => $u)
        {
            $students[] = array('id'=>$u->id, 'username'=>$u->username, 'email'=>$u->email, 'name'=>$u->firstname, 'lastname'=>$u->lastname);
        }
        if($inc_users)$result['students'] = $students;            

        return $result;
    }
    
    function getQualificationsOptions($phase_id,$team_id=0)
    {
        $now = time();
        $result = array();
        $user_id = Yii::app()->user->dbid;
        $is_admin = Yii::app()->user->checkAccess("admin");
        $is_creator = Yii::app()->user->checkAccess("creator");
        $isCoordinator = Yii::app()->user->checkAccess("coordinator");
        $phase = Phase::model()->findByPk($phase_id);
        $project = $phase->project;
        
        $team = intval($team_id)>=0
                    ? (($team_id>0)
                        ? $this->_getTeamById($team_id)
                        : $this->_getTeamByProjectAndUser($project->id, $user_id))
                    : $team_id;
        if($team && isset($team['id']))
        {
            $team_users_id = Functions::list_pluck($team['students'], "id");
            $is_my_team = in_array($user_id, $team_users_id); //Usuario pertenece al grupo
            $is_my_project = !$is_my_team && 
                            (
                                ($project->creator_id == $user_id) 
                                || (ProjectManager::model()->countByAttributes(array('project_id'=>$project->id,'manager_id'=>$user_id))) 
                            );
            $project_running = $now > $project['start_date'] &&  $now < $project['end_date'];

            if($is_my_team || $is_my_project || $is_admin || $is_creator || $isCoordinator )
            {
                $result['options']['can_member_mark'] = FALSE;
                $result['options']['can_manager_mark'] = FALSE;
                $result['options']['final_mark'] = null;
                
                $rules = $phase->rules;
                
                $rules_count = count($rules);
                $qualifications = Mark::model()->findAllByAttributes(array('team_id'=>$team['id'],'phase_id'=>$phase->id));
                $result['qualifications'] = array();
                
                $marks_student_assigned = array();$marks_admin_assigned = array();$allMarksWasAssigned = TRUE;
                $qualifications_ids = array();
                foreach ($qualifications as $qualification)
                {
                    if(in_array($qualification->team_id.$qualification->phase_id.$qualification->rule_id, $qualifications_ids))
                    {
                        Qualification::model()->deleteByPk($qualification->id);
                    }
                    else
                    {
                        if($is_my_project || ($is_my_team && !is_null($qualification->mark_member)) || $is_admin || $is_creator || $isCoordinator )
                        {
                            $attributes = $qualification->attributes;
                            if($attributes['mark_manager_date']>0) $attributes['mark_manager_date'] = date('d/m/Y h:i:sa', $attributes['mark_manager_date']);
                            if($attributes['mark_member_date']>0) $attributes['mark_member_date'] = date('d/m/Y h:i:sa', $attributes['mark_member_date']);

                            if($attributes['mark_member']!=null && $attributes['mark_member']>=0 && $attributes['mark_manager'] != null && $attributes['mark_manager']>=0)
                            {
                                $marks_student_assigned [] = $attributes['mark_member'];
                                $marks_admin_assigned [] = $attributes['mark_manager'];
                            }
                            else
                            {
                                $allMarksWasAssigned = FALSE;
                            }
                            $result['qualifications'][] = $attributes;
                        }
                        else
                        {
                            $allMarksWasAssigned = FALSE;
                        }
                    }
                }
                
                if($allMarksWasAssigned && (count($marks_student_assigned) === $rules_count && count($marks_admin_assigned) === $rules_count) && $rules_count > 0)
                {
                    $final_mark_member = array_sum($marks_student_assigned);
                    $final_mark_manager = array_sum($marks_admin_assigned);
                    $final_mark = round((($final_mark_member*25)+($final_mark_manager*75))/100,0);
                    $result['options']['final_mark_member'] = $final_mark_member;
                    $result['options']['final_mark_manager'] = $final_mark_manager;
                    $result['options']['final_mark'] = $final_mark;
                    
                    $final_note = FinalMark::model()->findByAttributes(array('phase_id'=>$phase_id,'team_id'=>$team_id));
                    if(!$final_note) 
                    {
                        $final_note = new FinalMark;
                    }
                    $final_note->phase_id = $phase_id;
                    $final_note->team_id = $team_id;
                    $final_note->mark = $final_mark;
                    $final_note->mark_member = $final_mark_member;
                    $final_note->mark_manager = $final_mark_manager;
                    $final_note->mark_date = time();
                    $final_note->save();
                            
                    $result['options']['cant_mark'] = 'Ya se asignaron todas las notas';                    
                }
                else
                {
                    if($project_running)
                    {
                        $hasLastAnswer = Answer::model()->countByAttributes(array('phase_id'=>$phase->id, 'team_id'=>$team['id'], 'is_last'=>1, 'is_published'=>1)) > 0;
                        if($hasLastAnswer)
                        {
                            if($is_my_team) $result['options']['can_member_mark'] = TRUE;
                            if($is_my_project) $result['options']['can_manager_mark'] = TRUE;
                        }
                        else
                        {
                            $result['options']['cant_mark'] = 'Aún no se ha publicado una respuesta final';
                        }
                        if($is_my_team)
                        {
                            foreach($result['qualifications'] as $k => $qual)
                            {
                                $result['qualifications'][$k]['mark_manager'] = NULL;
                                $result['qualifications'][$k]['mark_manager_by'] = NULL;
                                $result['qualifications'][$k]['mark_manager_comment'] = NULL;
                                $result['qualifications'][$k]['mark_manager_date'] = NULL;
                            }
                        }
                    }
                    else
                    {
                        $result['options']['cant_mark'] = 'El proyecto ha finalizado';
                    }
                    
                }
            }
            else
            {
                $this->error = "No tiene permiso para ver estas calificaciones.";
            }
        }
        else
        {
            $this->error = "No se puede acceder al grupo";
        }
        return $result;
    }
    
    function _parseReAnswer($reanswer)
    {
        $result = array();
        $result['id']          = $reanswer->id;
        $result['created']     = date('d/m/Y h:i:sa', $reanswer->created);
        $result['created_ago'] = $this->_human_time_diff($reanswer->created);
        $result['reanswer']    = FileHelper::changeImageSrc($reanswer->reanswer);
        $result['author_name'] = $reanswer->author->firstname . " " . $reanswer->author->lastname;
        $result['author_username'] = $reanswer->author->username;
        $attachments = array();
        foreach($reanswer->files as $att)
        {
            $attachments[] = array(
                'url' => $this->config->uriCDN . "/uploads/" . $att->md5 . "/" . $att->original_name, 
                'original_name' => $att->original_name,
                'type' => FileHelper::getFileType($att->original_name, $att->md5));
        }
        $result['attachments'] = $attachments;
        return $result;
    }
}