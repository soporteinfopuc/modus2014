<?php

class ToolsController extends Controller
{
    public function accessRules()
    {
        return array_merge(parent::accessRules(), array(
            array('allow', 'actions' => array('list','read','create','delete','update'),'roles' => array('manageTools')),
            array('allow', 'actions' => array('list'),'roles' => array('viewTools')),
            array('deny', 'users'=>array('*')),
        ));
    }
    
    public function actions()
    {
        return array(
            'create'=>'application.controllers.tools.CreateAction',
            'update'=>'application.controllers.tools.UpdateAction',
            'delete'=>'application.controllers.tools.DeleteAction',
        );
    }
    
    public function actionList()
    {
        $tools = $this->_getTools();
        $this->success = $tools;
    }
    
    
    function actionRead($id=null)
    {
        $tools = $this->_getTools($id);
        $this->success = reset($tools);
    }
    
    

    

    
  
    function _getTools($tool_id=NULL)
    {
        $tools_db = ($tool_id>0)?
                            Tool::model()->findAllByPk($tool_id):
                            Tool::model()->findAll();
        
        $tools = array();
        foreach($tools_db as $k => $s_db)
        {
            $tool = array('id' => $s_db->id, 
                'name' => $s_db->name, 
                'url' => $s_db->url, 
                'description' => $s_db->description, 
                'modified' => date( 'd-m-Y H:i:s', $s_db->modified ),
                'modified_ago' => $this->_human_time_diff($s_db->modified));
            if($image = $s_db->image)
            {
                $tool['image_id'] = $image->id;
                $tool['image_url'] = $this->config->uriCDN . "/uploads/" . $image->md5 . "/" . $image->original_name;
            }
            $tools[] = $tool;
        }

        return $tools;
    }
}
