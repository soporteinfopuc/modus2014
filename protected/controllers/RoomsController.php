<?php

class RoomsController extends Controller
{
    
    function actionCreate()
    {
        $result = array();
        if(!empty($_POST) && !empty($_POST['model']))
        {
            $attr = json_decode($_POST['model'],true);
            $room = new Room();
            $room->name = strtoupper($attr['name']);
            $room->school_id = $attr['school_id'];
            $room->level_id = $attr['grade_id'];
            $room->year_id = $this->year();
            $count = Room::model()->countByAttributes(array('year_id' => $room->year_id, 'name' => $room->name, 'school_id' => $room->school_id, 'level_id' => $room->level_id));
            if($count > 0 )
            {
                $this->error = "Ya existe una sección con el mismo nombre: \"".$room->name."\"";
            }
            else
            {
                try
                {
                    if($room->save())
                    {
                        $result['id'] = $room->id;
                        $result['name'] = $room->name;
                        $result['count_instructors'] = 0;
                        $result['count_students'] = 0;
                        $this->success = $result;
                    }
                    else
                    {
                        $this->error = YII_DEBUG?
                                    $room->getErrors():
                                    "Se produjo un error al crear la sección";
                    }
                }catch(Exception $e)
                {
                    $this->error = "Ocurrio un error a intentar crear la sección, compruebe que el colegio y grado exista.";
                }
            }
        }
        else
        {
            $this->error = "No se enviaron datos";
        }
        
    }
    
    function actionUpdate()
    {
        $result = array();
        if(!empty($_POST) && !empty($_POST['model']))
        {
            $model = json_decode($_POST['model'],true);
            try 
            {
                $room = Room::model()->findByPk($model['id']); 
                if($room)
                {
                    $room->name = strtoupper($model['name']);
                    $room->save();
                    $result['name'] = $room->name;
                    $result['id'] = $room->id;
                    $this->success = $result;
                }
                else
                {
                    $this->error = "La sección ya no existe o se ha eliminado.";
                }
            }
            catch (Exception $e)
            {
                $this->error = "No se pudo actualizar.";
                if(YII_DEBUG)
                {
                    $this->debug = $e->getMessage();
                }
            }
        }
        
    }
    
    function actionDelete($id=null)
    {
        $cant_sign = Sign::model()->countByAttributes(array('room_id' => $id));
        if($cant_sign>0)
        {
            $this->error = "Esta sección no se puede eliminar, debido a que tiene " . $cant_sign . " matriculados.";
        }
        else
        {
             try
            {
                Room::model()->deleteByPk($id);
                $this->success = true;
            }
            catch(Exception $e) 
            {
                $this->error = 'Esta sección no se puede eliminar.';
                if(YII_DEBUG)
                {
                    $this->debug = $e->getMessage();
                }
            }
        }
    }
}
