<?php

class CreateAction extends CAction
{
    public function run()
    {
        $post = Yii::app()->request->getPost("model");
        $model = json_decode($post, TRUE);
        try
        {
            $user = UserHelper::validateUser($model);
            $role = isset($model['role'])?$model['role']:'';

            if($role != 'admin' && $role != 'creator' && $role != 'coordinator')
            {
                $this->controller->error = "No se ha definido un rol";
                return;
            }

            $cant = User::model()->countByAttributes(array('username' => $user['username']));
            if($cant > 0)
            {
                $this->controller->error = "Ya existe un usuario con el mismo nombre.";
                return;
            }

            $transaction =  Yii::app()->db->beginTransaction();
            if($user_db = UserHelper::upsertUser($user, $role))
            {
                $transaction->commit();
                $this->controller->success = array('id' => $user_db->id,'active' => true);
            }
            else
            {
                $transaction->rollback();
                $this->controller->error = $user->getErrors();
            }
        }
        catch (Exception $ex)
        {
            $this->controller->error = $ex->getMessage();
            return;
        }

        
    }
}