<?php

class ImportStudentsAction extends CAction
{
    
    public function run()
    {
        $result = array();
        $post = Yii::app()->request;
        
        $name = $_FILES['files']['name'][0];
        $file = $_FILES['files']['tmp_name'][0];
        $type = $_FILES['files']['type'][0];
        $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
        if(in_array($type,$mimes))
        {

            $input = fopen($file,"r");
            if($input)
            {
                $users_ids = array();
                $count = 0;
                $fila = 0;
                while ( ($datos = fgetcsv($input, 1000, ",")) !== FALSE)
                {
                    $this->controller->error = NULL;
                    $numero = count($datos);
                    $fila++;
                    if( $numero != 5 )
                    {
                        $result['error'] [] = 'La fila ' . $fila .' no es válida';
                    }
                    else
                    {
                        $model = array(
                            'creator_id' => Yii::app()->user->dbid,
                            'firstname' => trim($datos[0]),
                            'lastname' => trim($datos[1]),
                            'username' => trim($datos[2]),
                            'password' => crypt(trim($datos[3]),trim($datos[3])),
                            'email' => trim($datos[4])
                        );
                        
                        $exist = User::model()->findByAttributes(array('username' => $model['username']));
                        if($exist)
                        {
                            $result['error'][] = "Ya existe un usuario con el mismo nombre '".$model['username']."', se intentará matricularlo en la sección actual.";
                            $users_ids[] = $exist->id;
                        }
                        else
                        {
                            try
                            {
                                $student = UserHelper::validateUser($model);
                                $role = "student";
                                $userdb = UserHelper::upsertUser($student, $role);
                                if(!$userdb)
                                {
                                    if($this->controller->error)
                                    {
                                        $result['error'][] = $this->controller->error;
                                    }
                                    else
                                    {
                                        $result['error'][] = "No se pudo importar la fila: " . implode(" ", $datos);
                                    }
                                }
                                else
                                {
                                    $users_ids[] = $userdb->id;
                                    $count++;
                                }
                            }
                            catch (Exception $ex)
                            {
                                $result['error'][] = $ex->getMessage();
                            }
                        }
                    }
                }
                fclose($input);
                if($count>0 || $users_ids)
                {
                    if($users_ids)
                    {
                        $result['success']['ids'] = $users_ids;
                    }
                    if($count>0)
                    {
                        $result['success']['msg'] = ('Se importaron '.$count.' alumno' . ($count>1?"s":"") .'.');
                    }
                    if($count==0)
                    {
                        unset($result['error']);
                    }
                }
                else
                {
                    if(!$result['error'])
                        $result['error'] = 'No se importo ningun alumno.';
                }
            }
            else
            {
                $result['error'] = 'No se pudo abrir el archivo a importar.';
            }
        }
        else
        {
            $result['error'] = "El tipo de archivo importado, no es válido. \nSe permite archivos tipo CSV";
        }
        $this->controller->success = $result;
    }
}