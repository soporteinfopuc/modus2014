<?php

class ListAction extends CAction
{
    
    public function run()
    {
        $request = Yii::app()->request;
        $page = $request->getParam("page");
        $per_page = $request->getParam("per_page")?$request->getParam("per_page"):20;
        $sort_by = $request->getParam("sort_by")?$request->getParam("sort_by"):'id';
        $find = $request->getParam("q")?$request->getParam("q"):"";
        $role = $request->getParam("role")?$request->getParam("role"):"";
        $order = $request->getParam("order")?strtoupper($request->getParam("order")):'ASC';
        
        $with = array();
        $criteria = array('condition' => '', 'params' => array());
        if($find && strlen($find))
        {
            $criteria['condition'] .= "(username LIKE :find OR firstname LIKE :find OR lastname LIKE :find)";
            $criteria['params'][':find'] = "%$find%";
        }
        
        if($role && (($role == 'student') || ($role == 'instructor') || ($role == 'coordinator') || ($role == 'creator') || ($role == 'admin')))
        {
            if($criteria['condition'])
            {
                $criteria['condition'] .= " AND ";
            }
            $criteria['condition'] .= "authassignment.itemname=:role";
            $criteria['params'][':role'] = $role;
            $with['authassignment'] = array();
        }
        
        $count_users = intval(User::model()->with($with)->count($criteria));
        
        $criteria['select'] = 'id,username,email,firstname,lastname,active';
        $criteria['order'] = 't.'.$sort_by.' '.$order;
        $criteria['limit'] = $per_page;
        $criteria['offset'] = ($page-1)*$per_page;
                
        $result = array();
        $users = User::model()->with($with)->findAll($criteria);
        
        $year_id = $this->controller->year();
        
        foreach($users as $user)
        {
            $auser = array_filter($user->attributes);
            $auser['active'] = $user->active==1?TRUE:FALSE;
            if($user->authassignment)
            {
                $auth = $user->authassignment;
                $auser['role'] = $auth?$auth->itemname:'-';
            }
            $auser['info'] = '';
            if($rooms = $user->rooms)
            {
                foreach ($rooms as $room)
                {
                    if($room->year_id == $year_id)
                    {
                        $level = $room->level;
                        $school = $room->school;
                        $auser['info'] .= $school->name." - " . $level->name . "° Grado " . $level->level_name . " - Sección " . $room->name . "\n";
                        $auser['link'] = array($school->id,$level->level_num,$level->id,$room->id);
                    }
                }
            }
            else if($schools = $user->schools)
            {
                foreach ($schools as $school)
                {
                    $auser['info'] .= $school->name . "\n";
                }
            }
            $result [] = $auser;
        }
        $this->controller->success = array(
            'options' => array('actualRecords' => count($users),'totalRecords' => $count_users, 'totalPages' => ceil($count_users/$per_page), 'sortKey' => $sort_by, 'order' => $order), 
            'collection' => $result
        );
    }
}