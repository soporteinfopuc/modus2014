<?php

class DeleteInstructorAction extends CAction
{
    
    public function actionDeleteInstructor($id)
    {
        $this->controller->actionDelete($id);
    }
}