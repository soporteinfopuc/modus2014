<?php

class ImportInstructorsAction extends CAction
{
    
    public function run()
    {
        $result = array();
        $post = Yii::app()->request;
        
        $name = $_FILES['files']['name'][0];
        $file = $_FILES['files']['tmp_name'][0];
        $type = $_FILES['files']['type'][0];
        $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
        if(in_array($type,$mimes))
        {

            $input = fopen($file,"r");
            if($input)
            {
                $count = 0;
                $fila = 0;
                while ( ($datos = fgetcsv($input, 1000, ",")) !== FALSE)
                {
                    $this->controller->error = NULL;
                    $numero = count($datos);
                    $fila++;
                    if( $numero != 5 )
                    {
                        $result['error'] [] = 'La fila ' . $fila .' no es válida';
                    }
                    else
                    {

                        $instructor = new User();

                        $instructor->creator_id = Yii::app()->user->dbid;
                        $instructor->firstname = trim($datos[0]);
                        $instructor->lastname = trim($datos[1]);
                        $instructor->username = trim($datos[2]);
                        $instructor->password = crypt(trim($datos[3]),trim($datos[3]));
                        $instructor->email = trim($datos[4]);
                        $instructor->created = time();
                        $instructor->modified = $instructor->created;
                        $instructor->active = 1;

                        $role = "instructor";
                        $userdb = $this->controller->_saveUser($instructor, $role);
                        if(!$userdb)
                        {
                            if($this->controller->error)
                            {
                                $result['error'][] = $this->controller->error;
                            }
                            else
                            {
                                $result['error'][] = "No se pudo importar la fila: " . implode(" ", $datos);
                            }
                        }
                        else
                        {
                            $count++;
                        }
                    }
                }
                fclose($input);
                if($count>0)
                {
                    $result['success'] = 'Se importaron '.$count.' docente' . ($count>1?"s":"") .'.';
                }
                else
                {
                    if(!$result['error'])
                        $result['error'] = 'No se importo ningun profesor.';
                }
            }
            else
            {
                $result['error'] = 'No se pudo abrir el archivo a importar.';
            }
        }
        else
        {
            $result['error'] = "El tipo de archivo importado, no es válido. \nSe permite archivos tipo CSV";
        }
        $this->controller->success = $result;
    }
}