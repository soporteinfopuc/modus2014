<?php

class ListInstructorsAction extends CAction
{
    
    /**
     * Devuelve una lista de Docentes
     */
    function run()
    {
        $result = array();
        $assigned_db = Authassignment::model()->with(array("user"))->together()->findAllByAttributes(array('itemname' => "instructor"));
        foreach ($assigned_db as $a_db)
        {
            $user = $a_db->user;
            $result[] = array('id' => $user->id, 'name' => $user->firstname." ".$user->lastname, 'username' => $user->username, 'firstname' => $user->firstname, 'lastname' => $user->lastname, 'email' => $user->email);
        }
        $this->controller->success = $result;
    }
}