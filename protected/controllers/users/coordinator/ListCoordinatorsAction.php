<?php

class ListCoordinatorsAction extends CAction
{
    
    
    /**
     * Devuelve una lista de Coordinadores
     */
    public function run()
    {
        $result = array();
        $assigned_db = Authassignment::model()->with(array("user"))->findAllByAttributes(array('itemname' => "coordinator"));
        foreach ($assigned_db as $a_db)
        {
            $user = $a_db->user;
            $schools = count($user->schools);
            $result[] = array(
                'id' => $user->id, 'name' => $user->firstname." ".$user->lastname, 'username' => $user->username, 'firstname' => $user->firstname, 'lastname' => $user->lastname, 'email' => $user->email,
                'count_schools' => $schools
            );
        }
        $this->controller->success = $result;
    }
}