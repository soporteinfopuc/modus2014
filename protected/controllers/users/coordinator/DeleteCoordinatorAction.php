<?php

class DeleteCoordinatorAction extends CAction
{
    
    public function run($id)
    {
        Yii::import("application.controllers.users.DeleteAction");
        DeleteAction::run($id);
    }
    
}