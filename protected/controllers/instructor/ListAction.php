<?php

class ListAction extends CAction
{
        
    function run($school_id=NULL, $grade_id=NULL)
    {
        $result = array();
        if($school_id>0 && $grade_id>0)
        {
            $instructors = User::model()->with('rooms','authassignment')->together()->findAll(
                    array(
                        'condition' => "rooms.level_id=:levelId AND rooms.school_id=:schoolId AND rooms.year_id=:yearId AND itemname='instructor'",
                        'params' => array(':levelId' => $grade_id, 'schoolId' => $school_id, 'yearId' => $this->controller->year() )
                    )
            );
            
            foreach($instructors as $user)
            {
                $instructor = array('id' => $user->id, 'name' => $user->firstname." ".$user->lastname, 'username' => $user->username, 'firstname' => $user->firstname, 'lastname' => $user->lastname, 'email' => $user->email, 'password' => '**********');
                $roomsId = array();
                foreach($user->rooms as $room)
                {
                    $roomsId[] = $room->id;
                }
                $instructor['rooms'] = $roomsId;
                $result[] = $instructor; 
            }
            if(!empty($result))$result = array_values($result);
            $this->controller->success = $result;
        }
        else
        {
            $this->controller->error = 'Se necesita el colegio y grado';
        }
    }
    
}