<?php

class UnassignAction extends CAction
{
    function run($school_id=0, $grade_id=0, $user_id=0)
    {
        $allSections = Functions::list_pluck(Room::model()->findAllByAttributes(array('year_id' => $this->controller->year(), 'school_id' => $school_id, 'level_id' => $grade_id)),"id");
        if($allSections)
        {
            foreach ($allSections as $room_id)
            {
                try
                {
                    RoomHelper::unassignInstructor($user_id, $room_id);
                    $this->controller->success = true;
                }
                catch (Exception $ex)
                {
                    $this->controller->error = $ex->getMessage();
                }
            }
        }
        if($this->controller->success !== true && !$this->controller->error)
        {
            $this->controller->error = "El docente no tenía asignada ninguna sección.";
        }
    }
}