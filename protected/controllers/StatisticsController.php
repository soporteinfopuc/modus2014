<?php

class StatisticsController extends Controller
{
    
    
    function actionAssessment($project_id, $room_id)
    {
        $result=array();
        
        $teams_db = Team::model()->findAllByAttributes(array('room_id'=>$room_id,'project_id'=>$project_id), array('order'=>'ord'));
        $teams_ids = Functions::list_pluck($teams_db, "id");
        
        $phases_db = Phase::model()->findAllByAttributes(array('project_id'=>$project_id, 'can_mark' => 1), array('order'=>'ord'));
        $phases_ids = Functions::list_pluck($phases_db, "id");
        
        $finals_db = FinalMark::model()->findAllByAttributes(array('phase_id'=>$phases_ids,'team_id'=>$teams_ids));
        
        $finals = array();
        foreach($finals_db as $final)
        {
            if(!array_key_exists($final->phase_id, $finals))
                    $finals[$final->phase_id] = array();
            $finals[$final->phase_id][$final->team_id] = array('mark'=>$final->mark,'mark_manager'=>$final->mark_manager,'mark_member'=>$final->mark_member);
        }
        
        $phases = array(); 
        foreach ($phases_db as $phase) 
            $phases[] = array('id'=>$phase->id,'name'=>$phase->name);
        
        $teams = array();
        foreach ($teams_db as $team)
        {
            $aTeam = array('final_mark'=>NULL,'marks'=>array(),'name'=>$team->name,'id'=>$team->id);
            $allMarks = array();$allMarksStudent = array();$allMarksAdmin = array();
            foreach ($phases as $phase)
            {
                if(array_key_exists($phase["id"], $finals) && array_key_exists($team->id, $finals[$phase["id"]]))
                {
                    $mark = intval($finals[$phase["id"]][$team->id]['mark']);
                    $mark_member = intval($finals[$phase["id"]][$team->id]['mark_member']);
                    $mark_manager = intval($finals[$phase["id"]][$team->id]['mark_manager']);
                    
                    $aTeam['marks'][$phase["id"]] = array(sprintf('%02d', $mark_member),sprintf('%02d', $mark_manager),sprintf('%02d', $mark));
                    $allMarksStudent []= $mark_member;
                    $allMarksAdmin []= $mark_manager;
                    $allMarks []= $mark;
                }
                else
                {
                    $aTeam['marks'][$phase["id"]] = NULL;
                }
                if(count($allMarks) == count($phases))
                {
                    $aTeam['final_mark_member'] =sprintf('%02d', round(array_sum($allMarksStudent)/count($phases)));
                    $aTeam['final_mark_manager'] = sprintf('%02d', round(array_sum($allMarksAdmin)/count($phases)));
                    $aTeam['final_mark'] = sprintf('%02d', round(array_sum($allMarks)/count($phases)));
                }
            }
            $teams []= $aTeam;            
        }
        
        $result['phases'] = $phases;
        $result['teams'] = $teams;
        $this->success = $result;
    }
    
    
    
    
    
    
    
    function actionGeneral($project_id, $room_id)
    {
        if(!$project_id || !$room_id)return;
        
        //TODO: security
        $result=array();
        $teams_db = Team::model()->findAllByAttributes(array('room_id'=>$room_id,'project_id'=>$project_id));
        $teams_ids = Functions::list_pluck($teams_db, "id");
        
        $students_count = intval(TeamUser::model()->countByAttributes(array('team_id'=>$teams_ids)));
        
        $phases_db = Phase::model()->findAllByAttributes(array('project_id'=>$project_id),array('order'=>'ord'));
        
        foreach ($phases_db as $phase_db)
        {
            if(isset($phase_db['can_answer']) && $phase_db['can_answer'])
            {
                $phase = array('id'=>$phase_db->id,'name'=>$phase_db->name);
                if($image = $phase_db->image)
                {
                    $phase['image'] = $this->config->uriCDN. "/uploads/" . $image->md5 . "/" . $image->original_name;
                }
                $draft_users = array(); $ended_users = array();
                $answers = $phase_db->answers;
                foreach($answers as $answer)
                {
                    if(in_array($answer->team_id,$teams_ids))
                    {
                        $uid = $answer->author_id;
                        if( ($answer->is_published == 1) )//ended
                            $ended_users[$uid] = true;
                        else if (($answer->is_published == 0) && !array_key_exists($uid, $ended_users))//draft
                            $draft_users[$uid] = true;
                    }
                }
                $ended = count($ended_users); $draft = count($draft_users);
                $phase['total'] = $students_count;
                $none = $students_count - $ended - $draft;
                $phase['none'] = $none;
                $phase['draft'] = $draft;
                $phase['ended'] = $ended;
                $result[] = $phase;
            }
        }
        
        $this->success = $result;
    }
    
    function actionProgress($project_id, $room_id)
    {
        if(!$project_id || !$room_id)return;
        $result=array();
        
        $teams_db = Team::model()->findAllByAttributes(array('room_id'=>$room_id,'project_id'=>$project_id), array('order'=>'ord'));
        $teams_ids = Functions::list_pluck($teams_db, "id");
        
        $original_teams = array();
        foreach ($teams_db as $team_db)
        {
            $add = array('id'=>$team_db->id, 'name' => $team_db->name,'users'=>array());
            $users = $team_db->users;
            foreach($users as $user)
            {
                $add['users'][$user->id] = array('id'=>$user->id,'name'=>$user->firstname . " " . $user->lastname,'result'=>0);
            }
            $original_teams[$team_db->id] = $add;
        }
        
        $phases_db = Phase::model()->findAllByAttributes(array('project_id'=>$project_id), array('order'=>'ord'));
        
        foreach ($phases_db as $phase_db)
        {
            if(isset($phase_db['can_answer']) && $phase_db['can_answer'])
            {
                $phase = array('id'=>$phase_db->id,'name'=>$phase_db->name,'teams'=>array());
                if($image = $phase_db->image)
                {
                    $phase['image'] = $this->config->uriCDN. "/uploads/" . $image->md5 . "/" . $image->original_name;
                }
                $answers = Answer::model()->findAllByAttributes(array('phase_id'=>$phase["id"],'team_id'=>$teams_ids));
                foreach($original_teams as $g => $team)
                {
                    foreach($answers as $answer)
                    {
                        if(isset($team['users'][$answer->author_id]))
                        {
                            if($answer->is_published == 1)
                                $team['users'][$answer->author_id]['result'] = 2;
                            else if( ($answer->is_published == 0) && ($team['users'][$answer->author_id]['result']==0))
                                $team['users'][$answer->author_id]['result'] = 1;
                        }
                    }
                    $team['users'] = array_values($team['users']);
                    $phase['teams'][] = $team;
                }
                $result[] = $phase;
            }
        }
        $this->success = $result;
    }
    
    

    function actionExportAssessment($project_id, $room_id)
    {
        
        $this->actionAssessment($project_id,$room_id);
        $room = Room::model()->findByPk($room_id);
        $teams = $this->success['teams'];
        $phases = $this->success['phases'];
        $this->success = array();
        $count_phases = count($phases);
        $count_teams = count($teams);
        
        $project_title = Project::model()->findByPk($project_id,array('select'=>'title'))->title;
        $title = "Notas finales y parciales - " . $project_title . " - Sección \"" . $room->name . "\"";
        
        Yii::import('application.vendors.PHPExcel', true);
        
        
        $doc = new PHPExcel();
        $doc->getProperties()->setCreator("")
                             ->setLastModifiedBy("")
                             ->setTitle($title)
                             ->setSubject($title)
                             ->setDescription($title . ". generado en Modus " . date('d-m-Y H:i:s'))
                             ->setKeywords("")
                             ->setCategory("notas");
        
        $sharedStyle1 = new PHPExcel_Style();
        $sharedStyle1->applyFromArray(
                array('borders' => array(
                        'allborders'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
                    )
                ));
        
        
        $doc->setActiveSheetIndex(0);
        
        $column = 'B';$row = 2;
        
        $sheet = $doc->getActiveSheet();
        
        $sheet->setCellValue($column.$row,$title);
        
        
        $endColumn = $column;for($i=0;$i<3*$count_phases+1;$i++)$endColumn=$this->getColumn($endColumn);
        
        $title_range = $column.$row.':'.$endColumn.$row;
        $sheet->mergeCells($title_range);
        $sheet->setSharedStyle($sharedStyle1, $title_range);
        $sheet->getStyle($title_range)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle($title_range)->getFont()->setSize(16);

        $head_col = $column;$head_row = $row+2;
        $head_range = $head_col.$head_row.':'.$head_col.($head_row+2);
        $sheet->mergeCells($head_range);
        
        $head_col = $this->getColumn($head_col);
        $sheet->setCellValue($head_col.$head_row,"Nota Parcial");
        
        
        $end_head_col = $head_col;for($i=0;$i<3*$count_phases-1;$i++)$end_head_col=$this->getColumn($end_head_col);
        $sheet->mergeCells($head_col.$head_row.':'.$end_head_col.$head_row);
        $next_head_col = $this->getColumn($end_head_col);
        $sheet->setCellValue($next_head_col.$head_row,"Nota Final");
        $sheet->mergeCells($next_head_col.$head_row.':'.$next_head_col.($head_row+2));
        
        $table_row_ini = $head_row; $table_column_ini = $column;
        $head_row++;
        foreach ($phases as $phase)
        {
            $ini_col = $head_col;
            $sheet->setCellValue($head_col.$head_row, $phase['name']);
            $head_col = $this->getColumn($head_col);
            $head_col = $this->getColumn($head_col);
            $sheet->mergeCells($ini_col.$head_row.':'.$head_col.$head_row);
            $head_col = $this->getColumn($head_col);
        }
        
        $head_col = $this->getColumn($column);
        $table_row_ini = $head_row; $table_column_ini = $column;
        $head_row++;
        
        foreach ($phases as $phase)
        {
            
            $sheet->setCellValue($head_col.$head_row, "Nota Alumno");
            $sheet->getStyle($head_col.$head_row)->getAlignment()->setWrapText(TRUE);
            $sheet->getColumnDimension($head_col)->setAutoSize(true);
            $head_col = $this->getColumn($head_col);
            
            $sheet->setCellValue($head_col.$head_row, "Nota Profesor");
            $sheet->getStyle($head_col.$head_row)->getAlignment()->setWrapText(TRUE);
            $sheet->getColumnDimension($head_col)->setAutoSize(true);
            $head_col = $this->getColumn($head_col);
            
            $sheet->setCellValue($head_col.$head_row, "Promedio");
            $sheet->getStyle($head_col.$head_row)->getAlignment()->setWrapText(TRUE);
            $sheet->getColumnDimension($head_col)->setAutoSize(true);
            $head_col = $this->getColumn($head_col);
        }
        
        $body_row = $head_row+1;
        foreach($teams as $team)
        {
            $body_col = $column;
            $sheet->setCellValue($body_col.$body_row, $team['name']);
            foreach($phases as $phase)
            {
                $body_col = $this->getColumn($body_col);
                if(isset($team['marks'][$phase['id']]))
                {
                    $sheet->setCellValue($body_col.$body_row, intval($team['marks'][$phase['id']][0]));
                    $body_col = $this->getColumn($body_col);
                    $sheet->setCellValue($body_col.$body_row, intval($team['marks'][$phase['id']][1]));
                    $body_col = $this->getColumn($body_col);
                    $sheet->setCellValue($body_col.$body_row, intval($team['marks'][$phase['id']][2]));
                }
            }
            if(isset($team['final_mark']) && $team['final_mark'])
            {
                $body_col = $this->getColumn($body_col);
                $sheet->setCellValue($body_col.$body_row, intval($team['final_mark']));
                $sheet->getColumnDimension($body_col)->setAutoSize(true);
            }
            
            $body_row++;
        }
               
        $table_range = $column.($head_row-2).':'.$endColumn.($head_row+$count_teams);
        $sheet->setSharedStyle($sharedStyle1, $table_range);
        $sheet->getStyle($table_range)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle($table_range)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        
        //$sheet->setCellValue($column.($body_row+5), date('d-m-Y H:i:s'));
        $sheet->getStyle($column.($body_row+5))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.Functions::create_slug($title).'.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');
        $objWriter->save('php://output'); 
        Yii::app()->end();
    }
    
    function getColumn($prevColumn)
    {
        if(strlen($prevColumn)==1&&$prevColumn!="Z")
        { $prevColumn++;return $prevColumn; }

        $colum = '';
        $list = str_split($prevColumn);

        if(count($list)==1&&$list[0]=='Z')
        {
            $list[0] = "A";
            $list[1] = "A";
        }
        else if(count($list)==2&&$list[1]!='Z')
        {
            $list[1]++;
        }
        else if(count($list)==2&&$list[1]=='Z')
        {
            $list[0] = "B";
            $list[1] = "A";
        }
        return implode('',$list);
    }
}