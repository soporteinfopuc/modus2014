<?php

class SectionsController extends Controller
{
    function actionCreate(){
        $result = array();
        if(!empty($_POST) && !empty($_POST['model'])){
            $section = new Section;
            $section->attributes = json_decode($_POST['model'],true);
            $section->save();
            if($section->id < 0){
                $activeYear = Year::model()->findByPk($this->year());
                
                $sectionYear = new SectionYear;
                $sectionYear->section_id = $section->id;
                $sectionYear->year_id = $activeYear->id;
                $sectionYear->save();
            }
            $result['id'] = $section->id;
        }
        $this->success = $result;
    }
    
    function actionUpdate(){
        $result = array();
        if(!empty($_POST) && !empty($_POST['model'])){
            $attributes = json_decode($_POST['model'],true);
            try {
                $section = Section::model()->findByPk($attributes['id']); 
                $section->attributes = $attributes;
                $section->save();
            
                $result['id'] = $section->id;
            }catch (Exception $e) {
                $this->error = "No se pudo actualizar:\n".  $e->getMessage(). "";
            }
        }
        $this->success = $result;
    }
    
    function actionDelete($id=null){
        try{
            $model = Section::model()->findByPk($id);
            $model->delete();
            $this->success = true;
        }catch(Exception $e) {
            $this->error = 'Esta sección no se puede eliminar. '.$e->getMessage();
        }
    }
}
