-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: modusprod.cz1cpascwckf.us-west-2.rds.amazonaws.com
-- Generation Time: Nov 15, 2016 at 05:36 PM
-- Server version: 5.6.19-log
-- PHP Version: 5.3.28

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `modus2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phase_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL,
  `is_last` int(1) NOT NULL DEFAULT '0',
  `is_published` int(1) NOT NULL DEFAULT '1' COMMENT '0:draft, 1:published',
  PRIMARY KEY (`id`),
  KEY `phase_id` (`phase_id`),
  KEY `team_id` (`team_id`),
  KEY `user_id` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61030 ;

-- --------------------------------------------------------

--
-- Table structure for table `answer_attachment`
--

CREATE TABLE IF NOT EXISTS `answer_attachment` (
  `answer_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`answer_id`,`file_id`),
  KEY `answer_id` (`answer_id`),
  KEY `attachment_id` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attachment`
--

CREATE TABLE IF NOT EXISTS `attachment` (
  `filename` varchar(255) NOT NULL,
  `originalname` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `md5` varchar(255) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `descargas` int(11) DEFAULT '0',
  PRIMARY KEY (`filename`),
  KEY `creator_id` (`creator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `authassignment`
--

CREATE TABLE IF NOT EXISTS `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `userid_2` (`userid`),
  KEY `itemname` (`itemname`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authassignment`
--

INSERT INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `authitem`
--

CREATE TABLE IF NOT EXISTS `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authitem`
--

INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Tiene acceso a todo.', NULL, NULL),
('canGetOwnSchoolAndGrades', 0, 'Puede obtener la lista de colegios y grados a los cuales pertenece', NULL, NULL),
('changeActiveYear', 0, 'Cambiar el Periodo seleccionado', NULL, NULL),
('coordinator', 2, 'Tiene acceso a ver todos los proyectos y ver la participación de los docentes y alumnos.', NULL, NULL),
('createProjects', 0, 'Tiene acceso a crear proyectos', NULL, NULL),
('creator', 2, 'Tiene acceso a ingresar contenido a las fases. No puede crear un proyecto, el proyecto solo lo crea el administrador.', NULL, NULL),
('instructor', 2, 'Docentes', NULL, NULL),
('manageCoordinators', 1, 'Administrar Coordinadores', NULL, NULL),
('manageCreators', 1, 'Administrar Creadores', NULL, NULL),
('manageInstructors', 1, 'Administrar Docentes', NULL, NULL),
('manageOwnSchoolProjects', 1, 'Administrar los proyectos de su colegio asignado', NULL, NULL),
('manageProjectTeams', 1, 'Administrar Equipos de Proyecto', NULL, NULL),
('manageSchoolCoordinators', 1, 'Administrar a Coordinadores de Colegios', NULL, NULL),
('manageSchoolCreators', 1, 'Administrar a Creadores de Colegios', NULL, NULL),
('manageSchools', 1, 'Administrar colegios', NULL, NULL),
('manageStrategies', 1, 'Administrar Estratégias', NULL, NULL),
('manageStudents', 1, 'Administrar Alumnos', NULL, NULL),
('manageTemplatePhases', 1, 'Administrar Plantillas de Fases', NULL, NULL),
('manageTools', 1, 'Administrar Herramientas', NULL, NULL),
('manageUsers', 1, 'Administrar Usuarios', NULL, NULL),
('manageYears', 1, 'Administrar años', NULL, NULL),
('student', 2, 'Estudiantes', NULL, NULL),
('viewActiveYear', 0, 'Ver el Periodo seleccionado', NULL, NULL),
('viewAllCreators', 0, 'Ver todos los Usuarios Creadores', NULL, NULL),
('viewAllManagers', 0, 'Ver todos los Usuarios Coordinadores', NULL, NULL),
('viewOwnSchoolProjects', 0, 'Ver Todos los Proyectos, su contenido y respuestas', NULL, NULL),
('viewProjectLog', 0, 'Ver los log de los proyectos', NULL, NULL),
('viewStrategies', 0, 'Ver todas las estratégias', NULL, NULL),
('viewTools', 0, 'Ver todas las herramientas', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `authitemchild`
--

CREATE TABLE IF NOT EXISTS `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authitemchild`
--

INSERT INTO `authitemchild` (`parent`, `child`) VALUES
('admin', 'canGetOwnSchoolAndGrades'),
('coordinator', 'canGetOwnSchoolAndGrades'),
('creator', 'canGetOwnSchoolAndGrades'),
('manageYears', 'changeActiveYear'),
('admin', 'createProjects'),
('creator', 'createProjects'),
('admin', 'manageCoordinators'),
('admin', 'manageCreators'),
('admin', 'manageInstructors'),
('creator', 'manageOwnSchoolProjects'),
('admin', 'manageProjectTeams'),
('creator', 'manageProjectTeams'),
('instructor', 'manageProjectTeams'),
('admin', 'manageSchoolCoordinators'),
('admin', 'manageSchoolCreators'),
('admin', 'manageSchools'),
('admin', 'manageStrategies'),
('admin', 'manageStudents'),
('admin', 'manageTemplatePhases'),
('admin', 'manageTools'),
('admin', 'manageUsers'),
('admin', 'manageYears'),
('changeActiveYear', 'viewActiveYear'),
('manageCreators', 'viewAllCreators'),
('manageCoordinators', 'viewAllManagers'),
('manageSchoolCoordinators', 'viewAllManagers'),
('coordinator', 'viewOwnSchoolProjects'),
('manageOwnSchoolProjects', 'viewOwnSchoolProjects'),
('admin', 'viewProjectLog'),
('creator', 'viewStrategies'),
('creator', 'viewTools');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`key`, `value`) VALUES
('file_extensions_allowed', 'bmp,cdr,doc,docx,gdoc,gif,gz,jpe,jpeg,jpg,odg,odp,ods,odt,pdf,png,ppt,pptx,ps,pub,rar,rtf,svg,txt,vsd,xls,xlsx,xps,zip'),
('image_extensions_allowed', 'bmp,gif,jpe,jpeg,jpg,png'),
('order_projects_by_type', '0'),
('uriCDN', 'http://d1btww31qdhbbu.cloudfront.net');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `md5` varchar(255) NOT NULL,
  `original_name` varchar(255) NOT NULL,
  `alternative_name` varchar(255) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `properties` text,
  PRIMARY KEY (`id`),
  KEY `creator_id` (`creator_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23679 ;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`id`, `md5`, `original_name`, `alternative_name`, `creator_id`, `created`, `properties`) VALUES
(1, 'def7620033c80085d2e825c8499c9cee', '9576552_1389029770_5385709418_g.jpg', '9576552_1389029770_5385709418_g.jpg', 4, 1389029770, '{"size":7712,"width":188,"height":200,"type":2,"mime":"image\\/jpeg"}'),
(2, '121567773dbda0884835cfcbc5a37a34', '2861828_1389029781_5648271868_g.jpg', '2861828_1389029781_5648271868_g.jpg', 4, 1389029781, '{"size":12675,"width":200,"height":200,"type":2,"mime":"image\\/jpeg"}'),
(3, '2459fc81e188ca9a2c7fabe915cad401', '5505919_1389029820_9504942058_g.jpg', '5505919_1389029820_9504942058_g.jpg', 4, 1389029820, '{"size":8656,"width":200,"height":199,"type":2,"mime":"image\\/jpeg"}'),
(4, '5045d5675549ba285faffedd085c4a9f', '5533883_1389029806_2879540595_g.jpg', '5533883_1389029806_2879540595_g.jpg', 4, 1389029806, '{"size":5973,"width":190,"height":200,"type":2,"mime":"image\\/jpeg"}'),
(5, '7ded0b854b71aa3aabcedab7940ec0e6', '5754276_1389029837_1244844448_g.jpg', '5754276_1389029837_1244844448_g.jpg', 4, 1389029837, '{"size":5208,"width":200,"height":125,"type":2,"mime":"image\\/jpeg"}'),
(6, 'e7b62999e825e324762a8401a7d0e3de', '2343672_1389029861_7256450277_g.jpg', '2343672_1389029861_7256450277_g.jpg', 4, 1389029861, '{"size":7211,"width":200,"height":189,"type":2,"mime":"image\\/jpeg"}'),
(7, 'a200e9c8c53409131f6f1b331da9a4eb', '7540873_1389029925_2683856445_g.jpg', '7540873_1389029925_2683856445_g.jpg', 4, 1389029925, '{"size":8970,"width":200,"height":200,"type":2,"mime":"image\\/jpeg"}'),
(8, '176ad52627b24008fdfc52424f10f5ff', '6485242_1389029952_6813705511_g.jpg', '6485242_1389029952_6813705511_g.jpg', 4, 1389029952, '{"size":7686,"width":200,"height":200,"type":2,"mime":"image\\/jpeg"}'),
(9, '78a367f377cd738fcce9495ebdd715cf', '8828666_1389029979_1641658266_g.jpg', '8828666_1389029979_1641658266_g.jpg', 4, 1389029979, '{"size":6495,"width":200,"height":200,"type":2,"mime":"image\\/jpeg"}'),
(10, '465c28f1a1ee2e4c52bb578e48e9fdb3', '6682515_1389030020_4250192229_g.jpg', '6682515_1389030020_4250192229_g.jpg', 4, 1389030020, '{"size":4336,"width":200,"height":85,"type":2,"mime":"image\\/jpeg"}'),
(11, 'fba7d87678e502a4d391abdfb7162846', '5437903_1389067919_4547092533_g.jpg', '5437903_1389067919_4547092533_g.jpg', 4, 1389067919, '{"size":8022,"width":200,"height":188,"type":2,"mime":"image\\/jpeg"}'),
(12, '6d41c1e96c72fe6cadb391216865cade', '7284321_1389068308_6133336306_g.jpg', '7284321_1389068308_6133336306_g.jpg', 4, 1389068308, '{"size":7593,"width":200,"height":200,"type":2,"mime":"image\\/jpeg"}'),
(13, '76dd01f1ee5053ffc9d020c0989d8027', '5372752_1389068347_8356220438_g.jpg', '5372752_1389068347_8356220438_g.jpg', 4, 1389068347, '{"size":10144,"width":200,"height":140,"type":2,"mime":"image\\/jpeg"}'),
(14, '7494b3307b0f16df82472fe32b5fda2d', '1643216_1389068393_1600488562_g.jpg', '1643216_1389068393_1600488562_g.jpg', 4, 1389068393, '{"size":6549,"width":200,"height":124,"type":2,"mime":"image\\/jpeg"}'),
(15, '51b1fe4900452c9476e19598ebacf14a', '3318835_1389068653_3472663749_g.jpg', '3318835_1389068653_3472663749_g.jpg', 4, 1389068653, '{"size":6504,"width":200,"height":104,"type":2,"mime":"image\\/jpeg"}'),
(16, 'ac1a90fff079a3c825a353ca3ad04b84', '3912507_1389068677_8310011174_g.jpg', '3912507_1389068677_8310011174_g.jpg', 4, 1389068677, '{"size":3116,"width":200,"height":149,"type":2,"mime":"image\\/jpeg"}'),
(17, '02736f162833c6de990d0b8c61266650', '1632201_1391096702_1465717734_g.gif', '1632201_1391096702_1465717734_g.gif', 4, 1391097144, '{"size":676,"width":86,"height":28,"type":1,"mime":"image\\/gif"}');

-- --------------------------------------------------------

--
-- Table structure for table `final_mark`
--

CREATE TABLE IF NOT EXISTS `final_mark` (
  `phase_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `mark` int(11) NOT NULL,
  `mark_member` int(11) NOT NULL DEFAULT '0',
  `mark_manager` int(11) NOT NULL DEFAULT '0',
  `mark_date` int(11) NOT NULL,
  PRIMARY KEY (`phase_id`,`team_id`),
  KEY `group_id` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(255) NOT NULL,
  `level_num` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `level_name`, `level_num`, `name`, `active`) VALUES
(12, 'Primaria', 3, '1', 1),
(13, 'Primaria', 3, '2', 1),
(14, 'Primaria', 3, '3', 1),
(15, 'Primaria', 3, '4', 1),
(16, 'Primaria', 3, '5', 1),
(17, 'Primaria', 3, '6', 1),
(18, 'Secundaria', 4, '1', 1),
(19, 'Secundaria', 4, '2', 1),
(20, 'Secundaria', 4, '3', 1),
(21, 'Secundaria', 4, '4', 1),
(22, 'Secundaria', 4, '5', 1);

-- --------------------------------------------------------

--
-- Table structure for table `log_auth`
--

CREATE TABLE IF NOT EXISTS `log_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `datetime` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `event` varchar(32) NOT NULL,
  `type` varchar(32) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `request_headers` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1830 ;

--
-- Dumping data for table `log_auth`
--

INSERT INTO `log_auth` (`id`, `user_id`, `username`, `datetime`, `ip`, `event`, `type`, `url`, `request_headers`) VALUES
(1811, NULL, 'admin', 1479241672, '200.37.4.195', 'login', 'Password mismatch', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 6.1; WOW64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.71 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1812, 4, 'admin', 1479241685, '200.37.4.195', 'login', NULL, 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 6.1; WOW64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.71 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1813, 4, 'admin', 1479242100, '200.37.4.195', 'login', NULL, 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 6.1; WOW64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.71 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1814, NULL, '74987985', 1479246223, '190.235.237.103', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1815, NULL, '74987985', 1479246255, '190.235.237.103', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1816, NULL, '74987985', 1479246267, '190.235.237.103', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1817, NULL, '74987985', 1479246272, '190.235.237.103', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1818, NULL, '74987985', 1479246280, '190.235.237.103', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1819, NULL, '75334128', 1479249007, '179.7.210.218', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1820, NULL, '75334128', 1479249015, '179.7.210.218', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1821, NULL, '75334128', 1479249022, '179.7.210.218', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1822, NULL, '75334128', 1479249027, '179.7.210.218', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1823, NULL, '75334128', 1479249050, '179.7.210.218', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1824, NULL, '75334128', 1479249135, '179.7.210.218', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1825, NULL, '75334128', 1479249219, '179.7.210.218', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1826, NULL, '75334128', 1479249324, '179.7.210.218', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1827, NULL, '75334128', 1479249327, '179.7.210.218', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1828, NULL, '75334128', 1479249343, '179.7.210.218', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}'),
(1829, NULL, '75334128', 1479249368, '179.7.210.218', 'login', 'Unknow username', 'http://modus.infopuc.pucp.edu.pe/login', '{"requestUri":"\\/modus\\/login","pathInfo":"modus\\/login","scriptFile":"\\/var\\/www\\/modus\\/index.php","scriptUrl":"\\/index.php","hostInfo":"http:\\/\\/modus.infopuc.pucp.edu.pe","baseUrl":"","userAgent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/54.0.2840.99 Safari\\/537.36","userHost":null,"acceptTypes":"application\\/json, text\\/javascript, *\\/*; q=0.01","requestType":"POST","serverName":"modus.infopuc.pucp.edu.pe"}');

-- --------------------------------------------------------

--
-- Table structure for table `log_project`
--

CREATE TABLE IF NOT EXISTS `log_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `datetime` int(11) NOT NULL,
  `controller_action` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `human_comment` text,
  `data` text,
  PRIMARY KEY (`id`),
  KEY `log_project` (`project_id`),
  KEY `log_user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4765 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_system`
--

CREATE TABLE IF NOT EXISTS `log_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `datetime` int(11) NOT NULL,
  `controller_action` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `human_comment` text,
  `data` text,
  PRIMARY KEY (`id`),
  KEY `log_user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1020 ;

-- --------------------------------------------------------

--
-- Table structure for table `mark`
--

CREATE TABLE IF NOT EXISTS `mark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `phase_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  `mark_member_by` int(11) DEFAULT NULL,
  `mark_member` int(11) DEFAULT NULL,
  `mark_member_date` int(11) DEFAULT NULL,
  `mark_member_comment` text,
  `mark_manager_by` int(11) DEFAULT NULL,
  `mark_manager` int(11) DEFAULT NULL,
  `mark_manager_date` int(11) DEFAULT NULL,
  `mark_manager_comment` text,
  PRIMARY KEY (`id`),
  KEY `group_id` (`team_id`),
  KEY `phase_id` (`phase_id`),
  KEY `rule_id` (`rule_id`),
  KEY `mark_student_by` (`mark_member_by`),
  KEY `mark_admin_by` (`mark_manager_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42062 ;

-- --------------------------------------------------------

--
-- Table structure for table `phase`
--

CREATE TABLE IF NOT EXISTS `phase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_phase_id` int(11) DEFAULT NULL,
  `ord` int(11) DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `image_id` int(11) DEFAULT NULL,
  `can_answer` int(11) DEFAULT NULL,
  `can_mark` int(11) DEFAULT NULL,
  `showfinal_others` int(11) DEFAULT NULL,
  `text_answer` varchar(255) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `id` (`id`,`project_id`),
  KEY `phase_template_phase` (`template_phase_id`),
  KEY `phase_image` (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3347 ;

-- --------------------------------------------------------

--
-- Table structure for table `phase_attachment`
--

CREATE TABLE IF NOT EXISTS `phase_attachment` (
  `phase_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`phase_id`,`file_id`),
  KEY `phase_id` (`phase_id`),
  KEY `attachment_id` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phase_strategy`
--

CREATE TABLE IF NOT EXISTS `phase_strategy` (
  `phase_id` int(11) NOT NULL,
  `strategy_id` int(11) NOT NULL,
  PRIMARY KEY (`phase_id`,`strategy_id`),
  KEY `phase_id` (`phase_id`),
  KEY `strategy_id` (`strategy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phase_tool`
--

CREATE TABLE IF NOT EXISTS `phase_tool` (
  `phase_id` int(11) NOT NULL,
  `tool_id` int(11) NOT NULL,
  PRIMARY KEY (`phase_id`,`tool_id`),
  KEY `phase_id` (`phase_id`),
  KEY `tool_id` (`tool_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text,
  `image_id` int(11) DEFAULT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `year_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `creator_id` (`creator_id`),
  KEY `grade_id` (`level_id`),
  KEY `school_id` (`school_id`),
  KEY `project_type` (`type_id`),
  KEY `project_year` (`year_id`),
  KEY `project_image` (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=694 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_manager`
--

CREATE TABLE IF NOT EXISTS `project_manager` (
  `project_id` int(11) NOT NULL,
  `manager_id` int(11) NOT NULL,
  PRIMARY KEY (`project_id`,`manager_id`),
  KEY `owner_id` (`manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_room`
--

CREATE TABLE IF NOT EXISTS `project_room` (
  `project_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  PRIMARY KEY (`project_id`,`room_id`),
  KEY `project_room_room` (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_type`
--

CREATE TABLE IF NOT EXISTS `project_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `reanswer`
--

CREATE TABLE IF NOT EXISTS `reanswer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `reanswer` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `answer_id` (`answer_id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3001 ;

-- --------------------------------------------------------

--
-- Table structure for table `reanswer_attachment`
--

CREATE TABLE IF NOT EXISTS `reanswer_attachment` (
  `reanswer_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`reanswer_id`,`file_id`),
  KEY `attachment_id` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE IF NOT EXISTS `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `name` varchar(31) NOT NULL,
  `year_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `school_id` (`school_id`),
  KEY `room_level` (`level_id`),
  KEY `room_year` (`year_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=221 ;

-- --------------------------------------------------------

--
-- Table structure for table `rule`
--

CREATE TABLE IF NOT EXISTS `rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phase_id` int(11) NOT NULL,
  `ord` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `rule` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `phase_id` (`phase_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21829 ;

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE IF NOT EXISTS `school` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

--
-- Table structure for table `school_manager`
--

CREATE TABLE IF NOT EXISTS `school_manager` (
  `user_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`school_id`),
  KEY `user_id_school_id` (`user_id`,`school_id`) USING BTREE,
  KEY `school_id` (`school_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sign`
--

CREATE TABLE IF NOT EXISTS `sign` (
  `room_id` int(11) NOT NULL,
  `year_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL,
  `active` int(1) DEFAULT '1',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`room_id`),
  KEY `sign_room` (`room_id`),
  KEY `sign_year` (`year_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `strategy`
--

CREATE TABLE IF NOT EXISTS `strategy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `creator_id` (`creator_id`),
  KEY `strategy_image` (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `strategy`
--

INSERT INTO `strategy` (`id`, `name`, `description`, `image_id`, `created`, `modified`, `creator_id`) VALUES
(17, 'Notas rápidas', 'Las "Notas rápidas" se utilizan para anotar ideas y/o datos importantes a recordar mientras realizas tu investigación.', NULL, 1389020997, 1403271978, 4),
(18, 'Lluvia de ideas', 'La estrategia "Lluvia de ideas" facilita el surgimiento de nuevas ideas, ya que consiste en escribir ideas sueltas relacionadas al tema que estás trabajando.', NULL, 1389020997, 1389020997, 4),
(19, 'Buscar información', 'La estrategia "Buscar información" tiene como objetivo el encontrar \ncontenido pertinente para responder a la pregunta planteada al inicio de\n la investigación.', NULL, 1389020997, 1389020997, 4),
(20, 'Seleccionar información', '<p>La estrategia "Seleccionar información" te permite diferenciar la \ninformación importante, válida y pertinente al momento de realizar una &nbsp;búsqueda.<br></p>', NULL, 1389020997, 1403272116, 4),
(21, 'Resumir información', '<p>La estrategia "Resumir información" te permite sintetizar una gran \ncantidad de contenido, utilizando solo las ideas centrales del texto.<br></p>', NULL, 1389020997, 1389020997, 4),
(22, 'Manejo de fuentes', '<p>Esta estrategia te permite organizar las diferentes fuentes \nbibliográficas a las que tienes acceso. Además requiere que selecciones \nlas fuentes en función de su pertinencia y relevancia.<br></p>', NULL, 1389020997, 1403272153, 4),
(23, 'Editar texto', '<p>Esta estrategia te permite modificar partes de un texto para adecuarlo \nal objetivo planteado o para responder la pregunta propuesta.<br></p>', NULL, 1389020997, 1389020997, 4),
(24, 'Elaboración de presentaciones', '<p>Esta estrategia te permite crear presentaciones organizando tus ideas y conclusiones.<br></p>', NULL, 1389020997, 1389020997, 4),
(25, 'Compartir información', '<p>La estrategia "Compartir información" se utiliza para intercambiar \nfuentes con tus compañeros, de manera que todos puedan analizarlas y \ndecidir sobre su validez.<br></p>', NULL, 1389020997, 1389020997, 4),
(26, 'Exposición', '<p>La estrategia "Exposición" te permite organizar y explicar tus ideas y \nconclusiones. Una exposición debe realizarse de manera clara.<br></p>', NULL, 1389020997, 1389020997, 4),
(27, 'Auto-evaluación', '<p>La estrategia "Auto-evaluación" te permite valorar tu propio trabajo, \nteniendo en cuenta los objetivos trazados al inicio de la actividad.<br></p>', NULL, 1389020997, 1389020997, 4),
(28, 'Co-evaluación', '<p>La estrategia "Co-evaluación" te permite evaluar a tus compañeros y que ellos te evalúen a ti.<br></p>', NULL, 1389020997, 1389020997, 4);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `ord` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `group_ibfk_2` (`room_id`),
  KEY `group_ibfk_1` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2835 ;

-- --------------------------------------------------------

--
-- Table structure for table `team_user`
--

CREATE TABLE IF NOT EXISTS `team_user` (
  `team_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`team_id`,`user_id`),
  KEY `team_id` (`team_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `template_phase`
--

CREATE TABLE IF NOT EXISTS `template_phase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ord` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `image_id` int(11) DEFAULT NULL,
  `can_answer` int(11) DEFAULT NULL,
  `can_mark` int(11) DEFAULT NULL,
  `showfinal_others` int(11) DEFAULT NULL,
  `text_answer` varchar(255) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `creator_id` (`creator_id`),
  KEY `template_phase_image` (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `template_phase`
--

INSERT INTO `template_phase` (`id`, `ord`, `creator_id`, `name`, `description`, `image_id`, `can_answer`, `can_mark`, `showfinal_others`, `text_answer`, `created`, `modified`) VALUES
(1, 1, 4, 'Pregunta', '', NULL, 1, 1, 1, 'pregunta', 1390564800, 1475770039),
(2, 2, 4, 'Investiga', '', NULL, 1, 1, 0, 'respuesta', 1390564800, 1390564800),
(3, 3, 4, 'Crea', '', NULL, 1, 1, 0, 'respuesta', 1390564800, 1390564800),
(4, 4, 4, 'Discute', '', NULL, 1, 1, 0, 'respuesta', 1390564800, 1390564800),
(5, 5, 4, 'Presenta', '', NULL, 1, 0, 0, 'respuesta', 1390564800, 1432743548);

-- --------------------------------------------------------

--
-- Table structure for table `template_phase_strategy`
--

CREATE TABLE IF NOT EXISTS `template_phase_strategy` (
  `template_phase_id` int(11) NOT NULL,
  `strategy_id` int(11) NOT NULL,
  PRIMARY KEY (`template_phase_id`,`strategy_id`),
  KEY `strategy_id` (`strategy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `template_phase_tool`
--

CREATE TABLE IF NOT EXISTS `template_phase_tool` (
  `template_phase_id` int(11) NOT NULL,
  `tool_id` int(11) NOT NULL,
  PRIMARY KEY (`template_phase_id`,`tool_id`),
  KEY `tool_id` (`tool_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `template_rule`
--

CREATE TABLE IF NOT EXISTS `template_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_phase_id` int(11) NOT NULL,
  `ord` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `rule` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `default_phase_id` (`template_phase_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `template_rule`
--

INSERT INTO `template_rule` (`id`, `template_phase_id`, `ord`, `score`, `rule`) VALUES
(11, 2, 1, 2, 'Recopila suficiente información del aspecto que está a cargo de investigar.'),
(12, 2, 2, 2, 'Recopila información pertinente y precisa para responder a la pregunta de investigación.'),
(13, 2, 3, 3, 'Documenta las fuentes de información en la recopilación realizada.(Formato APA)'),
(14, 2, 4, 2, 'Las fuentes consultadas poseen carácter científico.'),
(15, 2, 5, 2, 'Registra de manera clara y concisa lo que pretende demostrar por medio de la actividad de investigación  realizada.(experiencia, maqueta, encuesta, entre otros)'),
(16, 2, 6, 2, 'Registra de manera  clara y concisa las conclusiones obtenidas a partir de la actividad realizada. (Experiencia, maqueta, encuesta, entre otros).'),
(17, 2, 7, 2, 'La actividad realizada. (Experiencia, maqueta, encuesta, entre otros) se relaciona con el subtema designado.'),
(18, 2, 8, 3, 'La actividad realizada. (Experiencia, maqueta, encuesta, entre otros) ayuda a responder la pregunta de investigación.'),
(19, 2, 9, 2, 'En la actividad realizada. (Experiencia, maqueta, encuesta, entre otros)  se adaptaron materiales del entorno para reducir costos ambientales y económicos.'),
(20, 3, 1, 3, 'El producto elaborado considera la información recolectada en la fase investiga para cada aspecto.'),
(21, 3, 2, 4, 'El producto elaborado ayuda a responder a  la pregunta de investigación.'),
(22, 3, 3, 2, 'El producto elaborado denota originalidad en su producción. No se detecta plagio de información.'),
(23, 3, 4, 2, 'El producto elaborado considera un vocabulario científico adecuado.'),
(24, 3, 5, 2, 'La redacción de las ideas es clara y precisa.'),
(25, 3, 6, 2, 'El producto está elaborado es  bueno estéticamente (colores, tipos y tamaños de letra, ilustraciones , etc.)'),
(26, 3, 7, 3, 'La ideas presentadas son producto del parafraseado.'),
(27, 3, 8, 2, 'Se ha registrado adecuadamente la bibliografía utilizada.'),
(28, 4, 1, 3, 'Resuelve correctamente las interrogantes para corroborar el manejo de información.'),
(29, 4, 2, 2, 'Revisan los links proporcionados para verificar las respuestas a las interrogantes planteadas en esta fase.'),
(30, 4, 3, 3, 'El equipo participa en la elaboración de las conclusiones.'),
(31, 4, 4, 4, 'Las conclusiones responden a la pregunta de investigación.'),
(32, 4, 5, 3, 'Las conclusiones planteadas muestran el manejo de información confiable.'),
(33, 4, 6, 3, 'Se elaboran productos que permite repotenciar el trabajo final.'),
(34, 4, 7, 2, 'Las conclusiones figuran en el producto grupal.'),
(35, 1, 1, 2, 'La pregunta de investigación, no es una afirmación.'),
(36, 1, 2, 2, 'La pregunta de investigación está planteada utilizando vocabulario específico del tema.'),
(37, 1, 3, 2, 'La pregunta  de investigación  es factible de  ser resuelta.'),
(38, 1, 4, 2, 'Las pregunta de investigación es abierta y compleja de tal manera que su respuesta vayan más allá de un monosílabo. (“sí”, “no”, “más o menos”, etc.,)'),
(39, 1, 5, 2, 'La pregunta de investigación puede ser resuelta haciendo uso de un experimento y /o actividad tipo encuesta, maqueta entre otras.'),
(40, 1, 6, 2, 'En la pregunta de investigación se aprecia con claridad el tema que se está estudiando.'),
(41, 1, 7, 2, 'Si  la pregunta de investigación tiene que ver con la relación entre dos temas, estos dos temas se  aprecian en la pregunta.'),
(42, 1, 8, 2, 'La pregunta de investigación permite identificar con claridad el tipo de datos o información que se obtendrá como producto de la investigación.'),
(43, 1, 9, 2, 'La hipótesis está redactada de forma clara y precisa.'),
(44, 1, 10, 2, 'Existe una relación entre el subtema, pregunta e hipótesis de investigación.');

-- --------------------------------------------------------

--
-- Table structure for table `tool`
--

CREATE TABLE IF NOT EXISTS `tool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creator_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `creator_id` (`creator_id`),
  KEY `tool_image` (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `tool`
--

INSERT INTO `tool` (`id`, `creator_id`, `name`, `url`, `description`, `image_id`, `created`, `modified`) VALUES
(2, 4, 'Google Calendar', 'http://www.google.com/calendar', 'Calendario en línea que te permite organizar tus eventos y tareas de \nmanera sencilla. Para acceder a este servicio debes tener una cuenta de \ncorreo Gmail.', 1, 1389020997, 1389020997),
(3, 4, 'Wordpress', 'http://www.wordpress.com', '<div>Plataforma donde puedes crear un blog y compartir información.<br></div>', 2, 1389020997, 1389020997),
(4, 4, 'Blogger', 'http://www.blogger.com', '<p>Plataforma donde puedes crear un blog y compartir información.&nbsp;<br></p>', 3, 1389020997, 1389020997),
(5, 4, 'Dropbox', 'http://www.dropbox.com', '<p>Plataforma de almacenamiento en nube que te permite acceder al contenido\n que guardes en varios dispositivos (móviles, tabletas, laptops, etc.).<br></p>', 4, 1389020997, 1403272742),
(6, 4, 'Google drive', 'http://www.google.com/drive', '<p>Plataforma de almacenamiento en nube que te permite acceder al contenido\n que guardes en varios dispositivos (móviles, tabletas, laptops, etc.).<br></p>', 5, 1389020997, 1403272747),
(7, 4, 'Google docs', 'http://www.google.com/docs', '<p>Herramienta que permite trabajar documentos en línea y de manera colaborativa.<br></p>', 6, 1389020997, 1389020997),
(9, 4, 'Prezi', 'http://www.prezi.com', '<p>Herramienta para crear presentaciones en línea.<br></p>', 7, 1389020997, 1403272754),
(10, 4, 'Vimeo', 'http://www.vimeo.com', '<p>Repositorio de videos.<br></p>', 8, 1389020997, 1403272763),
(11, 4, 'Youtube', 'http://www.youtube.com', '<p>Repositorio de videos.<br></p>', 9, 1389020997, 1403272769),
(12, 4, 'Audio Pal', 'http://www.audiopal.com/', '<p>Grabador de audio mp3 para compartir en sitios web.<br></p>', 10, 1389020997, 1389020997),
(15, 4, 'Mindomo', 'http://www.mindomo.com/es/', '<p>Herramienta para crear y compartir mapas conceptuales.<br></p>', 11, 1389020997, 1389067921),
(16, 4, 'Mindmeister', 'http://www.mindmeister.com/es', '<p>Herramienta para crear y compartir mapas conceptuales.<br></p>', 12, 1389068318, 1389068318),
(17, 4, 'Google Books', 'http://books.google.es/', '<p>Repositorio de libros digitales.<br></p>', 13, 1389068349, 1403272663),
(18, 4, 'DocsPal', 'http://www.docspal.com/', '<p>Herramienta para convertir documentos a formatos compatibles como .doc.<br></p>', 14, 1389068395, 1403272781),
(19, 4, 'TitanPad', 'http://titanpad.com/', '<p>Herramienta para realizar notas colaborativas.<br></p>', 15, 1389068656, 1389068656),
(20, 4, 'Pegarlo', 'http://pegarlo.es/', '<p>Herramienta para realizar notas colaborativas.<br></p>', 16, 1389068680, 1389068680),
(21, 4, 'Wordle', 'http://www.wordle.net/', '<p>Wordle es una herramienta para generar "nubes de palabras" del texto que proporciones. Las nubes dan mayor importancia a las palabras que aparecen con más frecuencia en el texto de origen. Puedes ajustar las nubes con diferentes fuentes, diseños y combinaciones de colores. Las imágenes que se crean con Wordle son tuyas para utilizar como quieras. Puedes imprimirlas o guardarlas en la Galería Wordle para compartirlas con tus amigos.<br></p>', 17, 1391096503, 1391096705),
(22, 4, 'Issuu', 'http://issuu.com/', 'Aplicación web que te permite crear catálogos y revistas en línea. <br>', NULL, 1391625963, 1391625963),
(23, 4, 'Pixton', 'http://www.pixton.com/es/', 'Aplicación web para crear cómics en línea. <br>', NULL, 1391626041, 1391626041),
(24, 4, 'Jamendo', 'http://www.jamendo.com/es/', '<p>Repositorio de música y sonidos. <br></p>', NULL, 1391626192, 1391626192),
(25, 4, 'Flickr', 'www.flickr.com', '<p>Repositorio de imágenes.<br></p>', NULL, 1391626279, 1403272820),
(26, 4, 'Vocaroo', 'vocaroo.com', 'Aplicación web para grabar sonidos.<br>', NULL, 1391626344, 1391626344),
(27, 4, 'Pixlr', 'pixlr.com', 'Aplicación web para editar fotografías.', NULL, 1391626380, 1403272837),
(28, 4, 'Animoto', 'animoto.com', '<p>Aplicación web para hacer videos.<br></p>', NULL, 1391626624, 1403272848),
(30, 4, 'Infogram', 'Infogr.am', 'Aplicación web para crear infografías.', NULL, 1391626817, 1403272863);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creator_id` int(11) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `created` int(11) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `last_access` int(11) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `creator_id` (`creator_id`),
  KEY `username_2` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6977 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `creator_id`, `username`, `password`, `email`, `firstname`, `lastname`, `created`, `modified`, `last_access`, `active`) VALUES
(4, NULL, 'admin', '$2a$13$cbhx1RvCK20QuhBYKbegiuIH1PqSudgSgXaRTq8LJpKCII0ybPWka', 'admin@modus.com', 'Admin', 'istrator', 2147483647, 2147483647, 1479242100, 1);

-- --------------------------------------------------------

--
-- Table structure for table `year`
--

CREATE TABLE IF NOT EXISTS `year` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'Nombre a mostrar para el periodo',
  `ord` int(11) NOT NULL COMMENT 'El orden en que se crearon los periodos',
  `active` int(1) NOT NULL DEFAULT '0' COMMENT 'Indica si el año es el que se encuentra actualmente activo o no.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para establecer los periodos que pueden ser anuales o entre un rango de fechas';
--
-- Dumping data for table `tool`
--
INSERT INTO `year` (`id`, `name`, `ord`, `active`) VALUES
(187277, '2017', 1, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answer`
--
ALTER TABLE `answer`
  ADD CONSTRAINT `answer_author` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `answer_phase` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `answer_team` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `answer_attachment`
--
ALTER TABLE `answer_attachment`
  ADD CONSTRAINT `answer_attachment_file` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `answer_attachment_ibfk_2` FOREIGN KEY (`answer_id`) REFERENCES `answer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `attachment`
--
ALTER TABLE `attachment`
  ADD CONSTRAINT `attachment_ibfk_1` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `authassignment`
--
ALTER TABLE `authassignment`
  ADD CONSTRAINT `authassignment_authitem` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `authassignment_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `authitemchild`
--
ALTER TABLE `authitemchild`
  ADD CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `final_mark`
--
ALTER TABLE `final_mark`
  ADD CONSTRAINT `final_mark_phase` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `final_mark_team` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `log_auth`
--
ALTER TABLE `log_auth`
  ADD CONSTRAINT `log_auth_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `log_project`
--
ALTER TABLE `log_project`
  ADD CONSTRAINT `log_project_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `log_project_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `log_system`
--
ALTER TABLE `log_system`
  ADD CONSTRAINT `log_system_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `mark`
--
ALTER TABLE `mark`
  ADD CONSTRAINT `mark_manager` FOREIGN KEY (`mark_manager_by`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `mark_member` FOREIGN KEY (`mark_member_by`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `mark_phase` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mark_rule` FOREIGN KEY (`rule_id`) REFERENCES `rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mark_team` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `phase`
--
ALTER TABLE `phase`
  ADD CONSTRAINT `phase_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `phase_image` FOREIGN KEY (`image_id`) REFERENCES `file` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `phase_template_phase` FOREIGN KEY (`template_phase_id`) REFERENCES `template_phase` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `phase_attachment`
--
ALTER TABLE `phase_attachment`
  ADD CONSTRAINT `phase_attachment_file` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `phase_attachment_ibfk_1` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `phase_strategy`
--
ALTER TABLE `phase_strategy`
  ADD CONSTRAINT `phase_strategy_ibfk_1` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `phase_strategy_ibfk_2` FOREIGN KEY (`strategy_id`) REFERENCES `strategy` (`id`);

--
-- Constraints for table `phase_tool`
--
ALTER TABLE `phase_tool`
  ADD CONSTRAINT `phase_tool_ibfk_1` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `phase_tool_ibfk_2` FOREIGN KEY (`tool_id`) REFERENCES `tool` (`id`);

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_creator` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `project_image` FOREIGN KEY (`image_id`) REFERENCES `file` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `project_level` FOREIGN KEY (`level_id`) REFERENCES `level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_school` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_type` FOREIGN KEY (`type_id`) REFERENCES `project_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_year` FOREIGN KEY (`year_id`) REFERENCES `year` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `project_manager`
--
ALTER TABLE `project_manager`
  ADD CONSTRAINT `project_manager_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_manager_ibfk_2` FOREIGN KEY (`manager_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project_room`
--
ALTER TABLE `project_room`
  ADD CONSTRAINT `project_room_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_room_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reanswer`
--
ALTER TABLE `reanswer`
  ADD CONSTRAINT `reanswer_ibfk_1` FOREIGN KEY (`answer_id`) REFERENCES `answer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reanswer_ibfk_2` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reanswer_attachment`
--
ALTER TABLE `reanswer_attachment`
  ADD CONSTRAINT `reanswer_attachment_file` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reanswer_attachment_ibfk_1` FOREIGN KEY (`reanswer_id`) REFERENCES `reanswer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_2` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `room_level` FOREIGN KEY (`level_id`) REFERENCES `level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `room_year` FOREIGN KEY (`year_id`) REFERENCES `year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rule`
--
ALTER TABLE `rule`
  ADD CONSTRAINT `rule_ibfk_1` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `school_manager`
--
ALTER TABLE `school_manager`
  ADD CONSTRAINT `school_manager_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `school_manager_ibfk_2` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sign`
--
ALTER TABLE `sign`
  ADD CONSTRAINT `sign_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sign_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sign_year` FOREIGN KEY (`year_id`) REFERENCES `year` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `strategy`
--
ALTER TABLE `strategy`
  ADD CONSTRAINT `strategy_ibfk_1` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `strategy_image` FOREIGN KEY (`image_id`) REFERENCES `file` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `team`
--
ALTER TABLE `team`
  ADD CONSTRAINT `team_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `team_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `team_user`
--
ALTER TABLE `team_user`
  ADD CONSTRAINT `team_user_team` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `team_user_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `template_phase`
--
ALTER TABLE `template_phase`
  ADD CONSTRAINT `template_phase_ibfk_1` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `template_phase_image` FOREIGN KEY (`image_id`) REFERENCES `file` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `template_phase_strategy`
--
ALTER TABLE `template_phase_strategy`
  ADD CONSTRAINT `template_phase_strategy_phase` FOREIGN KEY (`template_phase_id`) REFERENCES `template_phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `template_phase_strategy_tool` FOREIGN KEY (`strategy_id`) REFERENCES `strategy` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `template_phase_tool`
--
ALTER TABLE `template_phase_tool`
  ADD CONSTRAINT `template_phase_tool_phase` FOREIGN KEY (`template_phase_id`) REFERENCES `template_phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `template_phase_tool_tool` FOREIGN KEY (`tool_id`) REFERENCES `tool` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `template_rule`
--
ALTER TABLE `template_rule`
  ADD CONSTRAINT `template_rule_phase` FOREIGN KEY (`template_phase_id`) REFERENCES `template_phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tool`
--
ALTER TABLE `tool`
  ADD CONSTRAINT `tool_ibfk_1` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `tool_image` FOREIGN KEY (`image_id`) REFERENCES `file` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
