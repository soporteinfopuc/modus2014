<?php

return array(
    'components'=>array(
        'urlManager'=>array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'urlSuffix' => '/',
            'rules'=>array(
                
                array('users/list'       , 'pattern'=>'users'               , 'verb'=>'GET'),
                array('users/update'       , 'pattern'=>'users/<id:\d+>'               , 'verb'=>'PUT'),
                array('users/create'       , 'pattern'=>'users'               , 'verb'=>'POST'),
                array('users/delete'       , 'pattern'=>'users/<id:\d+>'               , 'verb'=>'DELETE'),
                array('users/disable'       , 'pattern'=>'users/disable/<id:\d+>'               , 'verb'=>'DELETE'),
                array('users/enable'       , 'pattern'=>'users/enable/<id:\d+>'               , 'verb'=>'PUT'),

                array('users/listCoordinators'       , 'pattern'=>'users/coordinators'              , 'verb' => 'GET'),
                array('users/updateCoordinator'      , 'pattern'=>'users/coordinators/<id:\d+>'      , 'verb' => 'PUT'),
                array('users/deleteCoordinator'      , 'pattern'=>'users/coordinators/<id:\d+>'      , 'verb' => 'DELETE'),
                array('users/createCoordinator'      , 'pattern'=>'users/coordinators'              , 'verb' => 'POST'),

                array('users/listCreators'       , 'pattern'=>'users/creators'              , 'verb' => 'GET'),
                array('users/updateCreator'      , 'pattern'=>'users/creators/<id:\d+>'      , 'verb' => 'PUT'),
                array('users/deleteCreator'      , 'pattern'=>'users/creators/<id:\d+>'      , 'verb' => 'DELETE'),
                array('users/createCreator'      , 'pattern'=>'users/creators'              , 'verb' => 'POST'),

                array('users/listInstructors'       , 'pattern'=>'users/instructors'              , 'verb' => 'GET'),
                array('users/updateInstructor'      , 'pattern'=>'users/instructors/<id:\d+>'      , 'verb' => 'PUT'),
                array('users/deleteInstructor'      , 'pattern'=>'users/instructors/<id:\d+>'      , 'verb' => 'DELETE'),
                array('users/createInstructor'      , 'pattern'=>'users/instructors'              , 'verb' => 'POST'),
                array('users/importInstructors'      , 'pattern'=>'users/instructors/import'              , 'verb' => 'POST'),

                array('users/updateStudent'      , 'pattern'=>'users/students/<id:\d+>'      , 'verb' => 'PUT'),
                array('users/deleteStudent'      , 'pattern'=>'users/students/<id:\d+>'      , 'verb' => 'DELETE'),
                array('users/createStudent'      , 'pattern'=>'users/students'              , 'verb' => 'POST'),
                array('users/importStudents'      , 'pattern'=>'users/students/import'              , 'verb' => 'POST'),
                
                array('coordinators/readSchools'      , 'pattern'=>'coordinators/<id:\d+>/schools'      , 'verb' => 'GET'),
                array('schools/assignCoordinator'   , 'pattern'=>'coordinators/<user_id:\d+>/schools/<school_id:\d+>'      , 'verb'=>'PUT'),
                array('schools/refuseCoordinator'   , 'pattern'=>'coordinators/<user_id:\d+>/schools/<school_id:\d+>'      , 'verb'=>'DELETE'),

                array('creators/readSchools'      , 'pattern'=>'creators/<id:\d+>/schools'      , 'verb' => 'GET'),
                array('schools/assignCreator'   , 'pattern'=>'creators/<user_id:\d+>/schools/<school_id:\d+>'      , 'verb'=>'PUT'),
                array('schools/refuseCreator'   , 'pattern'=>'creators/<user_id:\d+>/schools/<school_id:\d+>'      , 'verb'=>'DELETE'),

                array('modus/year'           , 'pattern'=>'years/<id:\d+>'                              , 'verb'=>'GET'),

                array('modus/js'           , 'pattern'=>'js/<id:.+>/<time:\d+>/<type:.+>.js'                              , 'verb'=>'GET'),
                array('modus/js'           , 'pattern'=>'js/<id:.+>/<time:\d+>/<type:.+>.js'                              , 'verb'=>'HEAD'),
                array('modus/css'           , 'pattern'=>'css/<id:.+>/<time:\d+>/<type:.+>.css'                              , 'verb'=>'GET'),
                array('modus/css'           , 'pattern'=>'css/<id:.+>/<time:\d+>/<type:.+>.css'                              , 'verb'=>'HEAD'),
                array('modus/install'           , 'pattern'=>'install'                              , 'verb'=>'GET'),

                array('years/list'            , 'pattern'=>'years'               , 'verb'=>'GET'),
                array('years/create'          , 'pattern'=>'years'               , 'verb'=>'POST'),
                array('years/update'          , 'pattern'=>'years/<id:\d+>'      , 'verb'=>'PUT'),
                array('years/delete'          , 'pattern'=>'years/<id:\d+>'      , 'verb'=>'DELETE'),

                array('schools/ownSchoolsAndGrades'       , 'pattern'=>'schools/grades/own'      , 'verb'=>'GET'),
                array('schools/list'            , 'pattern'=>'schools'               , 'verb'=>'GET'),
                array('schools/listAll'            , 'pattern'=>'schools/listAll/<include_instructors:\d*>'               , 'verb'=>'GET'),
                array('schools/create'          , 'pattern'=>'schools'               , 'verb'=>'POST'),
                array('schools/read'            , 'pattern'=>'schools/<id:\d+>'      , 'verb'=>'GET'),
                array('schools/readCoordinators'    , 'pattern'=>'schools/<id:\d+>/coordinators'      , 'verb'=>'GET'),
                array('schools/assignCoordinator'   , 'pattern'=>'schools/<school_id:\d+>/coordinators/<user_id:\d+>'      , 'verb'=>'PUT'),
                array('schools/refuseCoordinator'   , 'pattern'=>'schools/<school_id:\d+>/coordinators/<user_id:\d+>'      , 'verb'=>'DELETE'),
                array('schools/readCreators'    , 'pattern'=>'schools/<id:\d+>/creators'      , 'verb'=>'GET'),
                array('schools/assignCreator'   , 'pattern'=>'schools/<school_id:\d+>/creators/<user_id:\d+>'      , 'verb'=>'PUT'),
                array('schools/refuseCreator'   , 'pattern'=>'schools/<school_id:\d+>/creators/<user_id:\d+>'      , 'verb'=>'DELETE'),
                array('schools/update'          , 'pattern'=>'schools/<id:\d+>'      , 'verb'=>'PUT'),
                array('schools/delete'          , 'pattern'=>'schools/<id:\d+>'      , 'verb'=>'DELETE'),

                array('rooms/list'       , 'pattern'=>'rooms'             , 'verb'=>'GET'),
                array('rooms/create'     , 'pattern'=>'rooms'             , 'verb'=>'POST'),
                array('rooms/read'       , 'pattern'=>'rooms/<id:\d+>'    , 'verb'=>'GET'),
                array('rooms/update'     , 'pattern'=>'rooms/<id:\d+>'    , 'verb'=>'PUT'),
                array('rooms/delete'     , 'pattern'=>'rooms/<id:\d+>'    , 'verb'=>'DELETE'),

                array('strategies/list'     , 'pattern'=>'strategies'           , 'verb'=>'GET'),
                array('strategies/create'   , 'pattern'=>'strategies'           , 'verb'=>'POST'),
                array('strategies/read'     , 'pattern'=>'strategies/<id:\d+>'  , 'verb'=>'GET'),
                array('strategies/update'   , 'pattern'=>'strategies/<id:\d+>'  , 'verb'=>'PUT'),
                array('strategies/delete'   , 'pattern'=>'strategies/<id:\d+>'  , 'verb'=>'DELETE'),

                array('tools/list'          , 'pattern'=>'tools'                , 'verb'=>'GET'),
                array('tools/create'        , 'pattern'=>'tools'                , 'verb'=>'POST'),
                array('tools/read'          , 'pattern'=>'tools/<id:\d+>'       , 'verb'=>'GET'),
                array('tools/update'        , 'pattern'=>'tools/<id:\d+>'       , 'verb'=>'PUT'),
                array('tools/delete'        , 'pattern'=>'tools/<id:\d+>'       , 'verb'=>'DELETE'),

                array('phases/ResetPhase'    , 'pattern'=>'phases/resetPhase'                , 'verb'=>'POST'),
                array('phases/list'          , 'pattern'=>'phases'                , 'verb'=>'GET'),
                array('phases/listBasic'          , 'pattern'=>'phases/basic'                , 'verb'=>'GET'),
                array('phases/create'        , 'pattern'=>'phases'                , 'verb'=>'POST'),
                array('phases/read'          , 'pattern'=>'phases/<id:\d+>'       , 'verb'=>'GET'),
                array('phases/update'        , 'pattern'=>'phases/<id:\d+>'       , 'verb'=>'PUT'),
                array('phases/delete'        , 'pattern'=>'phases/<id:\d+>'       , 'verb'=>'DELETE'),
                array('phases/deleteRule'    , 'pattern'=>'rules/<id:\d+>'       , 'verb'=>'DELETE'),

                array('instructors/list'    , 'pattern'=>'instructors/<school_id:\d+>/<grade_id:\d+>'                                           , 'verb'=>'GET'),
                array('instructors/assign'  , 'pattern'=>'instructors/<school_id:\d+>/<grade_id:\d+>/<user_id:\d+>'                             , 'verb'=>'PUT'),
                array('instructors/unassign'  , 'pattern'=>'instructors/<school_id:\d+>/<grade_id:\d+>/<user_id:\d+>'                           , 'verb'=>'DELETE'),
                array('instructors/update'  , 'pattern'=>'instructors/<school_id:\d+>/<grade_id:\d+>/<instructor_id:\d+>'                       , 'verb'=>'PUT'),
                array('instructors/update'  , 'pattern'=>'instructors/<school_id:\d+>/<grade_id:\d+>/<room_id:\d+>/<instructor_id:\d+>'         , 'verb'=>'PUT'),                            
                array('instructors/delete'  , 'pattern'=>'instructors/<school_id:\d+>/<grade_id:\d+>/<room_id:\d+>/<instructor_id:\d+>'         , 'verb'=>'DELETE'),

                array('students/list'    , 'pattern'=>'students/<school_id:\d+>/<grade_id:\d+>/<room_id:\d+>'                              , 'verb'=>'GET'),
                array('students/assign'  , 'pattern'=>'students/<school_id:\d+>/<grade_id:\d+>/<room_id:\d+>/<student_id:\d+>'             , 'verb'=>'PUT'),
                array('students/unassign'  , 'pattern'=>'students/<school_id:\d+>/<grade_id:\d+>/<room_id:\d+>/<student_id:\d+>'             , 'verb'=>'DELETE'),
                array('students/import', 'pattern'=>'students/import'                                                             , 'verb'=>'POST'),
                array('students/export', 'pattern' => 'students/export/<room_id:\d+>'                                               , 'verb'=>'GET'),

                array('projects/list'  , 'pattern'=>'projects/list'                                                            , 'verb'=>'GET'),
                array('projects/list'  , 'pattern'=>'projects/list/<page:\d*>'                                                  , 'verb'=>'GET'),
                array('projects/list'  , 'pattern'=>'projects/list/<page:\d*>/<school_id:\d*>/<level_id:\d*>'                   , 'verb'=>'GET'),
                array('projects/list'  , 'pattern'=>'projects/list/<page:\d*>/<school_id:\d*>/<level_id:\d*>/<search_text:.*>'  , 'verb'=>'GET'),

                array('projects/read'   , 'pattern'=>'projects/<id:\d+>' , 'verb'=>'GET'   ),
                array('projects/create' , 'pattern'=>'projects'          , 'verb'=>'POST'  ),
                array('projects/update' , 'pattern'=>'projects/<id:\d+>' , 'verb'=>'PUT'   ),
                array('projects/delete' , 'pattern'=>'projects/<id:\d+>' , 'verb'=>'DELETE'),

                array('projects/viewLog'        , 'pattern'=>'projects/log/<project_id:\d+>'                   , 'verb'=>'GET'),
                
                array('projects/view'        , 'pattern'=>'projects/view/<project_id:\d+>'                   , 'verb'=>'GET'),
                array('projects/viewPhases'  , 'pattern'=>'projects/view/<project_id:\d+>/phases'       , 'verb'=>'GET'),
                array('projects/viewAnswers' , 'pattern'=>'projects/view/<project_id:\d+>/phases/<phase_id:\d+>/teams/<team_id:\d+>/answers' , 'verb'=>'GET'),
                array('projects/viewAnswers' , 'pattern'=>'projects/view/<project_id:\d+>/phases/<phase_id:\d+>/teams/<team_id:\d+>/answers/<last_time:\d+>'                   , 'verb'=>'GET'),
                array('projects/postAnswer'  , 'pattern'=>'projects/view/<project_id:\d+>/phases/<phase_id:\d+>/teams/<team_id:\d+>/answers' , 'verb'=>'POST'),
                array('projects/postAnswer'  , 'pattern'=>'projects/view/<project_id:\d+>/phases/<phase_id:\d+>/teams/<team_id:\d+>/answers' , 'verb'=>'PUT'),
                array('projects/deleteAnswer'  , 'pattern'=>'projects/view/<project_id:\d+>/phases/<phase_id:\d+>/teams/<team_id:\d+>/answers/<answer_id:\d+>' , 'verb'=>'DELETE'),
                array('projects/postReAnswer'  , 'pattern'=>'projects/view/<project_id:\d+>/phases/<phase_id:\d+>/teams/<team_id:\d+>/answers/<answer_id:\d+>/reanswers' , 'verb'=>'POST'),
                array('projects/deleteReAnswer'  , 'pattern'=>'projects/view/<project_id:\d+>/phases/<phase_id:\d+>/teams/<team_id:\d+>/answers/<answer_id:\d+>/reanswers/<reanswer_id:\d+>', 'verb'=>'DELETE'),
                array('projects/viewMarks'    , 'pattern'=>'projects/view/<project_id:\d+>/phases/<phase_id:\d+>/teams/<team_id:\d+>/marks'  , 'verb'=>'GET'),
                array('projects/listTypes'    , 'pattern'=>'projects/types'                   , 'verb'=>'GET'),

                array('projects/export'  , 'pattern'=>'projects/export/<id:\d+>'                   , 'verb'=>'GET'),
                array('projects/import'  , 'pattern'=>'projects/import'                   , 'verb'=>'POST'),

                array('teams/list'    , 'pattern'=>'projects/<project_id:\d+>/teams'                    , 'verb'=>'GET'),
                array('teams/create'  , 'pattern'=>'projects/<project_id:\d+>/teams'                    , 'verb'=>'POST'),
                array('teams/delete'  , 'pattern'=>'projects/<project_id:\d+>/teams/<team_id:\d+>'      , 'verb'=>'DELETE'),
                array('teams/assign'  , 'pattern'=>'projects/<project_id:\d+>/teams/assign'                    , 'verb'=>'POST'),

                array('projects/postMark'  , 'pattern'=>'projects/postMark'                     , 'verb'=>'POST'),
                array('projects/postMark'  , 'pattern'=>'projects/postMark'                     , 'verb'=>'PUT'),

                array('statistics/general'    , 'pattern'=>'statistics/<project_id:\d+>/general/<room_id:\d+>'                              , 'verb'=>'GET'),
                array('statistics/progress'    , 'pattern'=>'statistics/<project_id:\d+>/progress/<room_id:\d+>'                              , 'verb'=>'GET'),
                array('statistics/assessment'    , 'pattern'=>'statistics/<project_id:\d+>/assessment/<room_id:\d+>'                              , 'verb'=>'GET'),
                array('statistics/exportAssessment'    , 'pattern'=>'statistics/<project_id:\d+>/exportAssessment/<room_id:\d+>'                              , 'verb'=>'GET'),


                
                
                array('uploads/upload'  , 'pattern'=>'uploads'                      , 'verb'=>'POST'),
                array('uploads/upload'  , 'pattern'=>'uploads/<type:.*>'                      , 'verb'=>'POST'),
                array('uploads/download'  , 'pattern'=>'uploads/<id:.+>/<name:.+>'                      , 'verb'=>'GET'),


                array('teams/setStudent'  , 'pattern'=>'teams/set'                             , 'verb'=>'POST'),


                'user*' => 'modus',
                'year*'=>'modus',
                'school*'=>'modus',
                'project*'=>'modus',
                'student*'=>'modus',
                'instructor*'=>'modus',
                'strategy*'=>'modus',
                'tool*'=>'modus',
                'phase*'=>'modus',
                'coordinator*'=>'modus',
                'creator*'=>'modus',
                'home*'=>'modus',

                '<action:(login|logout|about)>' => 'modus',
                
                //'<controller:\w+>/<id:\w+>'=>'<controller>/view',
                //'<controller:\w+>/<action:\w+>/<id:\w+>'=>'<controller>/<action>',
                //'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            )
        )
    )
);