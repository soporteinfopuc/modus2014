<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return CMap::mergeArray(
    require(dirname(__FILE__).'/General.php'),
    require(dirname(__FILE__).'/DataBase.php'),
    require(dirname(__FILE__).'/UrlManager.php')
);