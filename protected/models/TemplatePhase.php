<?php

/**
 * This is the model class for table "template_phase".
 *
 * The followings are the available columns in table 'template_phase':
 * @property integer $id
 * @property integer $ord
 * @property integer $creator_id
 * @property string $name
 * @property string $description
 * @property integer $image_id
 * @property integer $can_answer
 * @property integer $can_mark
 * @property integer $showfinal_others
 * @property string $text_answer
 * @property integer $created
 * @property integer $modified
 *
 * The followings are the available model relations:
 * @property Phase[] $phases
 * @property User $creator
 * @property File $image
 * @property Strategy[] $strategies
 * @property Tool[] $tools
 * @property TemplateRule[] $templateRules
 */
class TemplatePhase extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'template_phase';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ord, creator_id, image_id, can_answer, can_mark, showfinal_others, created, modified', 'numerical', 'integerOnly'=>true),
			array('name, text_answer', 'length', 'max'=>255),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ord, creator_id, name, description, image_id, can_answer, can_mark, showfinal_others, text_answer, created, modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'phases' => array(self::HAS_MANY, 'Phase', 'template_phase_id'),
			'creator' => array(self::BELONGS_TO, 'User', 'creator_id'),
			'image' => array(self::BELONGS_TO, 'File', 'image_id'),
			'strategies' => array(self::MANY_MANY, 'Strategy', 'template_phase_strategy(template_phase_id, strategy_id)'),
			'tools' => array(self::MANY_MANY, 'Tool', 'template_phase_tool(template_phase_id, tool_id)'),
			'templateRules' => array(self::HAS_MANY, 'TemplateRule', 'template_phase_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ord' => 'Ord',
			'creator_id' => 'Creator',
			'name' => 'Name',
			'description' => 'Description',
			'image_id' => 'Image',
			'can_answer' => 'Can Answer',
			'can_mark' => 'Can Mark',
			'showfinal_others' => 'Showfinal Others',
			'text_answer' => 'Text Answer',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ord',$this->ord);
		$criteria->compare('creator_id',$this->creator_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image_id',$this->image_id);
		$criteria->compare('can_answer',$this->can_answer);
		$criteria->compare('can_mark',$this->can_mark);
		$criteria->compare('showfinal_others',$this->showfinal_others);
		$criteria->compare('text_answer',$this->text_answer,true);
		$criteria->compare('created',$this->created);
		$criteria->compare('modified',$this->modified);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TemplatePhase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
