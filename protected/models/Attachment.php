<?php

/**
 * This is the model class for table "attachment".
 *
 * The followings are the available columns in table 'attachment':
 * @property string $filename
 * @property string $originalname
 * @property string $url
 * @property string $md5
 * @property integer $creator_id
 * @property string $created
 * @property integer $descargas
 *
 * The followings are the available model relations:
 * @property User $creator
 */
class Attachment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'attachment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('filename, originalname, creator_id', 'required'),
			array('creator_id, descargas', 'numerical', 'integerOnly'=>true),
			array('filename, originalname, url, md5', 'length', 'max'=>255),
			array('created', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('filename, originalname, url, md5, creator_id, created, descargas', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'creator' => array(self::BELONGS_TO, 'User', 'creator_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'filename' => 'Filename',
			'originalname' => 'Originalname',
			'url' => 'Url',
			'md5' => 'Md5',
			'creator_id' => 'Creator',
			'created' => 'Created',
			'descargas' => 'Descargas',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('filename',$this->filename,true);
		$criteria->compare('originalname',$this->originalname,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('md5',$this->md5,true);
		$criteria->compare('creator_id',$this->creator_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('descargas',$this->descargas);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Attachment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
