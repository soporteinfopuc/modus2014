<?php

/**
 * This is the model class for table "file".
 *
 * The followings are the available columns in table 'file':
 * @property integer $id
 * @property string $md5
 * @property string $original_name
 * @property string $alternative_name
 * @property integer $creator_id
 * @property integer $created
 * @property string $properties
 *
 * The followings are the available model relations:
 * @property Answer[] $answers
 * @property User $creator
 * @property Phase[] $phases
 * @property Phase[] $phases1
 * @property Project[] $projects
 * @property Reanswer[] $reanswers
 * @property Strategy[] $strategies
 * @property TemplatePhase[] $templatePhases
 * @property Tool[] $tools
 */
class File extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'file';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('md5, original_name, creator_id, created', 'required'),
			array('creator_id, created', 'numerical', 'integerOnly'=>true),
			array('md5, original_name, alternative_name', 'length', 'max'=>255),
			array('properties', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, md5, original_name, alternative_name, creator_id, created, properties', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'answers' => array(self::MANY_MANY, 'Answer', 'answer_attachment(file_id, answer_id)'),
			'creator' => array(self::BELONGS_TO, 'User', 'creator_id'),
			'phases' => array(self::HAS_MANY, 'Phase', 'image_id'),
			'phases1' => array(self::MANY_MANY, 'Phase', 'phase_attachment(file_id, phase_id)'),
			'projects' => array(self::HAS_MANY, 'Project', 'image_id'),
			'reanswers' => array(self::MANY_MANY, 'Reanswer', 'reanswer_attachment(file_id, reanswer_id)'),
			'strategies' => array(self::HAS_MANY, 'Strategy', 'image_id'),
			'templatePhases' => array(self::HAS_MANY, 'TemplatePhase', 'image_id'),
			'tools' => array(self::HAS_MANY, 'Tool', 'image_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'md5' => 'Md5',
			'original_name' => 'Original Name',
			'alternative_name' => 'Alternative Name',
			'creator_id' => 'Creator',
			'created' => 'Created',
			'properties' => 'Properties',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('md5',$this->md5,true);
		$criteria->compare('original_name',$this->original_name,true);
		$criteria->compare('alternative_name',$this->alternative_name,true);
		$criteria->compare('creator_id',$this->creator_id);
		$criteria->compare('created',$this->created);
		$criteria->compare('properties',$this->properties,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return File the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
