<?php

/**
 * This is the model class for table "final_mark".
 *
 * The followings are the available columns in table 'final_mark':
 * @property integer $phase_id
 * @property integer $team_id
 * @property integer $mark
 * @property integer $mark_member
 * @property integer $mark_manager
 * @property integer $mark_date
 */
class FinalMark extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'final_mark';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('phase_id, team_id, mark, mark_date', 'required'),
			array('phase_id, team_id, mark, mark_member, mark_manager, mark_date', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('phase_id, team_id, mark, mark_member, mark_manager, mark_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'phase_id' => 'Phase',
			'team_id' => 'Team',
			'mark' => 'Mark',
			'mark_member' => 'Mark Member',
			'mark_manager' => 'Mark Manager',
			'mark_date' => 'Mark Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('phase_id',$this->phase_id);
		$criteria->compare('team_id',$this->team_id);
		$criteria->compare('mark',$this->mark);
		$criteria->compare('mark_member',$this->mark_member);
		$criteria->compare('mark_manager',$this->mark_manager);
		$criteria->compare('mark_date',$this->mark_date);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FinalMark the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
