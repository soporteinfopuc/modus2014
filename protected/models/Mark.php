<?php

/**
 * This is the model class for table "mark".
 *
 * The followings are the available columns in table 'mark':
 * @property integer $id
 * @property integer $team_id
 * @property integer $phase_id
 * @property integer $rule_id
 * @property integer $mark_member_by
 * @property integer $mark_member
 * @property integer $mark_member_date
 * @property string $mark_member_comment
 * @property integer $mark_manager_by
 * @property integer $mark_manager
 * @property integer $mark_manager_date
 * @property string $mark_manager_comment
 *
 * The followings are the available model relations:
 * @property User $markManagerBy
 * @property User $markMemberBy
 * @property Phase $phase
 * @property Rule $rule
 * @property Team $team
 */
class Mark extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mark';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('team_id, phase_id, rule_id', 'required'),
			array('team_id, phase_id, rule_id, mark_member_by, mark_member, mark_member_date, mark_manager_by, mark_manager, mark_manager_date', 'numerical', 'integerOnly'=>true),
			array('mark_member_comment, mark_manager_comment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, team_id, phase_id, rule_id, mark_member_by, mark_member, mark_member_date, mark_member_comment, mark_manager_by, mark_manager, mark_manager_date, mark_manager_comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'markManagerBy' => array(self::BELONGS_TO, 'User', 'mark_manager_by'),
			'markMemberBy' => array(self::BELONGS_TO, 'User', 'mark_member_by'),
			'phase' => array(self::BELONGS_TO, 'Phase', 'phase_id'),
			'rule' => array(self::BELONGS_TO, 'Rule', 'rule_id'),
			'team' => array(self::BELONGS_TO, 'Team', 'team_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'team_id' => 'Team',
			'phase_id' => 'Phase',
			'rule_id' => 'Rule',
			'mark_member_by' => 'Mark Member By',
			'mark_member' => 'Mark Member',
			'mark_member_date' => 'Mark Member Date',
			'mark_member_comment' => 'Mark Member Comment',
			'mark_manager_by' => 'Mark Manager By',
			'mark_manager' => 'Mark Manager',
			'mark_manager_date' => 'Mark Manager Date',
			'mark_manager_comment' => 'Mark Manager Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('team_id',$this->team_id);
		$criteria->compare('phase_id',$this->phase_id);
		$criteria->compare('rule_id',$this->rule_id);
		$criteria->compare('mark_member_by',$this->mark_member_by);
		$criteria->compare('mark_member',$this->mark_member);
		$criteria->compare('mark_member_date',$this->mark_member_date);
		$criteria->compare('mark_member_comment',$this->mark_member_comment,true);
		$criteria->compare('mark_manager_by',$this->mark_manager_by);
		$criteria->compare('mark_manager',$this->mark_manager);
		$criteria->compare('mark_manager_date',$this->mark_manager_date);
		$criteria->compare('mark_manager_comment',$this->mark_manager_comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mark the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
