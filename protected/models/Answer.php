<?php

/**
 * This is the model class for table "answer".
 *
 * The followings are the available columns in table 'answer':
 * @property integer $id
 * @property integer $phase_id
 * @property integer $team_id
 * @property integer $author_id
 * @property string $answer
 * @property integer $created
 * @property integer $modified
 * @property integer $is_last
 * @property integer $is_published
 *
 * The followings are the available model relations:
 * @property User $author
 * @property Phase $phase
 * @property Team $team
 * @property File[] $files
 * @property Reanswer[] $reanswers
 */
class Answer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'answer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('phase_id, team_id, author_id, answer, created, modified', 'required'),
			array('phase_id, team_id, author_id, created, modified, is_last, is_published', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, phase_id, team_id, author_id, answer, created, modified, is_last, is_published', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'phase' => array(self::BELONGS_TO, 'Phase', 'phase_id'),
			'team' => array(self::BELONGS_TO, 'Team', 'team_id'),
			'files' => array(self::MANY_MANY, 'File', 'answer_attachment(answer_id, file_id)'),
			'reanswers' => array(self::HAS_MANY, 'Reanswer', 'answer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'phase_id' => 'Phase',
			'team_id' => 'Team',
			'author_id' => 'Author',
			'answer' => 'Answer',
			'created' => 'Created',
			'modified' => 'Modified',
			'is_last' => 'Is Last',
			'is_published' => 'Is Published',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('phase_id',$this->phase_id);
		$criteria->compare('team_id',$this->team_id);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('answer',$this->answer,true);
		$criteria->compare('created',$this->created);
		$criteria->compare('modified',$this->modified);
		$criteria->compare('is_last',$this->is_last);
		$criteria->compare('is_published',$this->is_published);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Answer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
