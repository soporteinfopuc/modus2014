<?php

class FileHelper
{
    /**
     * Mueve un archivo a la carpeta de subida
     * @param type $tmp_name
     */
    static function upload($tmp_name, $original_name = "", $type="file", $alternative_name = "")
    {
        $type = $type=="file"?"file":"image";
        $user_id = Yii::app()->user->dbid;
        if(file_exists($tmp_name))
        {
            $md5 = md5_file($tmp_name);
            $target = Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'file'.DIRECTORY_SEPARATOR .  $md5;

            if(!file_exists($target))
            {
                copy($tmp_name, $target);
            }
            if($original_name && strlen($original_name)>0)
            {
                $file_db = File::model()->findByAttributes(array("md5" => $md5, "creator_id" => $user_id, "original_name" => $original_name));
            }
            else
            {
                $file_db = File::model()->findByAttributes(array("md5" => $md5, "creator_id" => $user_id,));
                $original_name = $alternative_name;
            }
            //Si el mismo archivo no se ha agregado anteriormente
            if( !$file_db )
            {
                $file_db = new File;
                $file_db->md5 = $md5;
                $file_db->original_name = $original_name;
                $file_db->creator_id = Yii::app()->user->dbid;
                $file_db->created = time();

                $properties = array();
                $properties['size'] = filesize($target);

                if($imagesize = @getimagesize($target))
                {
                    $properties['mime'] = $imagesize['mime'];
                    $properties['width'] = $imagesize[0];
                    $properties['height'] = $imagesize[1];
                    $properties['type'] = $imagesize[2];
                }
                $file_db->properties = json_encode($properties);
                if($file_db->save())
                {
                    $username = Yii::app()->user->id;
                    CLog::logSystem("upload", "El usuario '$username' subió el archivo '$file_db->original_name'.", $file_db->attributes);
                }
            }
            
            return $file_db;
        }
    }
    
    /**
     * Cambia los atributos src de las imágenes anteponiendo la url del CDN
     * @param type $content : El contenido que se desea reeplazar
     * @return String : 
     */
    static function changeImageSrc($content)
    {
        
        $config = Yii::app()->controller->config;
        $uriCDN = $config->uriCDN;
        $result = preg_replace("/img[^\>]*src=\"\/uploads([^\"]+)/", "img img-modus-cdn src=\"". $uriCDN . "/uploads$1", $content);
        return $result;
    }
    
    /**
     * Reemplaza los enlaces que contienen la dirección del CDN, antes de ser guardados en la base de datos
     * @param String $content: El contenido que se desea reparar
     * @return String: el contenido modificado
     */
    static function  repareCDN($content)
    {
        $config = Yii::app()->controller->config;
        $uriCDN = $config->uriCDN;
        $description = str_replace($uriCDN, "", $content);
        $description = str_replace("src=\"//www", "src=\"http://www", $description);
        return $description;
    }
    
    
    static function getFileType($name, $optional_md5 = "")
    {
        $path_parts = pathinfo($name);
        $file_ext   = strtolower($path_parts['extension']);
        
        switch ($file_ext)
        {
            case "xps" :
                $file_ext = "pdf";
                break;
            case "doc" :
            case "docx" :
            case "rtf" :
            case "odt" :
                $file_ext = "word";
                break;
            case "ods" :
            case "xls" :
            case "xlsx" :
                $file_ext = "excel";
                break;
            case "ppt" :
            case "pptx" :
            case "odp" :
                $file_ext = "powerpoint";
                break;
            case "jpg" :
            case "png" :
            case "bmp" : 
            case "gif" :
            case "jpeg" :
            case "odg" :
            case "odc" :
            case "odi" :
                $file_ext = "image";
                break;
            case "rar" :
                $file_ext = "zip";
                break;
        }
        
        return $file_ext;
    }
}