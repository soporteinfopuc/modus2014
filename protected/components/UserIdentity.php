<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;
    
    private $event = 'login';
    
    public function validate()
    {
        
    }
    
    public function authenticate()
    {
        $record = User::model()->findByAttributes(array('username'=>$this->username));
        if( $record===null )
        {
            $this->errorCode  = self::ERROR_USERNAME_INVALID;
            $this->loginError("Unknow username");
        }
        else if(CPasswordHelper::verifyPassword($this->password,$record->password))
        {
            if($record->active == 0)
            {
                $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
                $this->errorMessage = "Esta cuenta ha sido deshabilitada.";
                $this->loginError("Disable account");
            }
            else
            {
                $this->_id=$record->username;
                $this->setStates($record);
                $record->last_access = time();
                $record->save();
                $this->loginSuccess($record);
                $this->errorCode = self::ERROR_NONE;
            }
        }
        else if( ($crypt = crypt($this->password,$record->password)) && $record->password===$crypt )
        {
            if($record->active == 0)
            {
                $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
                $this->errorMessage = "Esta cuenta ha sido deshabilitada.";
                $this->loginError("Disable account");
            }
            else
            {
                $this->_id = $record->username;
                $this->setStates($record);
                $record->last_access = time();
                if($record->password === crypt($this->password,$record->password))
                {
                    $record->password = CPasswordHelper::hashPassword($this->password);
                }
                $record->save();
                $this->loginSuccess($record);
                $this->errorCode = self::ERROR_NONE;
            }
        }
        else
        {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
            $this->loginError("Password mismatch");
        }

        return !$this->errorCode;
    }
    
    private function setStates($record)
    {
        $this->setState('firstname', $record->firstname);
        $this->setState('name', $record->firstname . " " . $record->lastname);
        $this->setState('lastname', $record->lastname);
        $this->setState('dbid', $record->id);
    }


    public function getId()
    {
        return $this->_id;
    }
    
    private function loginSuccess()
    {
        $this->saveLogAuth();
    }
    
    private function loginError($type = "")
    {
        $this->saveLogAuth($type);
    }
    
    private function saveLogAuth($type = NULL)
    {
        $app = Yii::app();
        $request = $app->request;
        
        $headers = array(
            'requestUri' => $request->requestUri,
            'pathInfo' =>  $request->pathInfo,
            'scriptFile' =>  $request->scriptFile,
            'scriptUrl' =>  $request->scriptUrl,
            'hostInfo' =>  $request->getHostInfo(),
            'baseUrl' =>  $request->baseUrl,
            'userAgent' => $request->getUserAgent(),
            'userHost' => $request->getUserHost(),
            'acceptTypes' => $request->getAcceptTypes(),
            'requestType' => $request->getRequestType(),
            'serverName' => $request->getServerName(),
        );
        
        $logAuth = new LogAuth();
        $logAuth->attributes = array(
            'user_id' => $this->getState('dbid'),
            'username' => $this->username,
            'datetime' => time(),
            'ip' => $request->userHostAddress,
            'event' => $this->event,
            'type' => $type,
            'url' => $request->urlReferrer,
            'request_headers' => json_encode($headers)
        );
        $logAuth->save();
    }
}