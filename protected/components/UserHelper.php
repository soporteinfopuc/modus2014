<?php

class UserHelper
{
    
    public static function upsertUser($attr, $role = NULL)
    {
        $time = time();
        $user = NULL;
        if(array_key_exists("id", $attr))
        {
            $user = User::model()->with('authassignment')->findByPk($attr['id']);
            
        }
        if(!$user)
        {
            $user = new User();
        }
        $is_new = $user->isNewRecord;
        $last_attr = $user->attributes;
        
        $user->username = $attr['username'];
        $user->email = $attr['email'];
        $user->firstname = $attr['firstname'];
        $user->lastname = $attr['lastname'];
        if(array_key_exists("password", $attr) && $attr['password'])
        {
            $user->password = CPasswordHelper::hashPassword($attr['password']);
        }
        
        if($is_new)
        {
            $user->creator_id = Yii::app()->user->dbid;
            $user->created = $time;
            $user->last_access = 0;
            $user->active = 1;
        }
        
        $attributes = $user->attributes;
        $diff = array_diff_assoc($last_attr, $attributes) || array_diff_assoc($attributes, $last_attr);
        
        $user->modified = $time;
        if($is_new || $diff)
        {
            $save = $user->save();
            if($save)
            {
                $name = $user->username;
                if($is_new)
                {
                    CLog::logSystem("createUser", "Se agregó el usuario '$name'", $user->attributes);
                }
                else
                {
                    CLog::logSystem("updateUser", "Se modificó información del usuario '$name'", array('old' => array_diff_assoc($last_attr, $attributes), 'new' => array_diff_assoc($attributes, $last_attr)));
                }
            }
        }
        if($user->id && $role)
        {
            $name = $user->username;
            if($is_new)
            {
                $user->authassignment = new Authassignment;
                $user->authassignment->userid = $user->username;
                $user->authassignment->itemname = $role;
                $user->authassignment->save();
                CLog::logSystem("createRole", "Se agregó el rol del usuario '$name' a '$role'", $role);
            }
            else
            {
                $last_role = $user->authassignment->itemname;
                if($user->authassignment && $user->authassignment->itemname != $role)
                {
                    if($last_role != 'student' && $last_role != 'instructor')
                    {
                        $user->authassignment->itemname = $role;
                        $user->authassignment->save();
                        CLog::logSystem("updateRole", "Se modificó el rol del usuario '$name' de '$last_role' a '$role'", array($last_role, $role));
                    }
                }
            }
        }
        return $user;    
    }

    
    public static function validateUser($model, $role = NULL)
    {
        $user = array();
        $is_new = true;
        if(isset($model['id']) && $model['id'] && is_numeric($model['id']))
        {
            $is_new = false;
            $user['id'] = $model['id'];
        }
        
        if(isset($model['username']) && $model['username'])
        {
            $username = trim(strtolower($model['username']));
            if(preg_match('/^[a-z0-9_\.]+$/u',$username,$match))
            {
                if($is_new && User::model()->countByAttributes(array('username' => $username)) > 0)
                {
                    throw new Exception("El nombre de usuario especificado ya existe en el sistema");
                }
                else
                {
                    $user['username'] = $username;
                }
            }
            else
            {
                throw new Exception("El nombre de usuario sólo puede contener caracteres alfanuméricos y '.'(punto).");
            }
        }
        else
        {
            throw new Exception("Se necesita un nombre de usuario");
        }
        
        if(isset($model['email']) && $model['email'])
        {
            $email = $model['email'];
            if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                throw new Exception("El email ingresado no es válido");
            }
            else
            {
                $user['email'] = $email;
            }
        }
        else
        {
            throw new Exception("Se necesita el email del usuario.");
        }
        
        if(isset($model['firstname']) && trim($model['firstname']))
        {
            $firstname = trim($model['firstname']);
            $user['firstname'] = $firstname;
        }
        else
        {
            throw new Exception("Se necesita el nombre del usuario.");
        }
        
        if(isset($model['lastname']) && trim($model['lastname']))
        {
            $lastname = trim($model['lastname']);
            $user['lastname'] = $lastname;
        }
        else
        {
            throw new Exception("Se necesita el apellido del usuario.");
        }
        
        if(isset($model['password']) && strlen($model['password'])>0 && $model['password'] != '**********')
        {
            $user['password'] = $model['password'];
        }
        
        return $user;
    }
    
    public static function deleteUser($user_id)
    {
        $model = User::model()->findByPk($user_id);
        $attr = $model->attributes;
        $transaction = Yii::app()->db->beginTransaction();
        try
        {
            if($model->delete())
            {
                $username = $attr['username'];
                unset($attr['password']);
                CLog::logSystem("deleteUser", "Se quitó el usuario $username", $attr);
                $transaction->commit();
                return TRUE;
            }
            else
            {
                $transaction->rollback();
                return FALSE;
            }
        }
        catch (Exception $ex)
        {
            $transaction->rollback();
            if(YII_DEBUG)
            {
                Yii::app()->controller->debug = $ex->getMessage();
            }
            return FALSE;
        }
    }
    
    public static function disableUser($user_id)
    {
        $model = User::model()->findByPk($user_id);
        $attr = $model->attributes;
        $transaction = Yii::app()->db->beginTransaction();
        try
        {
            $model->active = 0;
            if($model->save())
            {
                $username = $attr['username'];
                unset($attr['password']);
                CLog::logSystem("deleteUser", "Se deshabilitó el usuario $username", $attr);
                $transaction->commit();
                return TRUE;
            }
            else
            {
                $transaction->rollback();
                return FALSE;
            }
        }
        catch (Exception $ex)
        {
            $transaction->rollback();
            if(YII_DEBUG)
            {
                Yii::app()->controller->debug = $ex->getMessage();
            }
            return FALSE;
        }
    }
    
    public static function enableUser($user_id)
    {
        $model = User::model()->findByPk($user_id);
        $attr = $model->attributes;
        $transaction = Yii::app()->db->beginTransaction();
        try
        {
            $model->active = 1;
            if($model->save())
            {
                $username = $attr['username'];
                unset($attr['password']);
                CLog::logSystem("deleteUser", "Se habilitó el usuario $username", $attr);
                $transaction->commit();
                return TRUE;
            }
            else
            {
                $transaction->rollback();
                return FALSE;
            }
        }
        catch (Exception $ex)
        {
            $transaction->rollback();
            if(YII_DEBUG)
            {
                Yii::app()->controller->debug = $ex->getMessage();
            }
            return FALSE;
        }
    }
}