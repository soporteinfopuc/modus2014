<?php

class CConfig
{
    
    private $uriCDN = "";
    private $fileExtAllowed = array();
    private $imageExtAllowed = array();
    private $emailAdmin = "";
    private $orderProjectsByType = false;
    
    public function __get($name)
    {
        switch ($name)
        {
            case "uriCDN" : return $this->getUriCDN();
            case "fileExtAllowed" : return $this->getFileExtAllowed();
            case "imageExtAllowed" : return $this->getImageExtAllowed();
            case "emailAdmin" : return $this->getEmailAdmin();
            case "orderProjectsByType" : return $this->getOrderProjectsByType();
        }
    }
    
    private function getUriCDN()
    {
        if(!$this->uriCDN)
        {
            $value = $this->getConfig("uriCDN");
            if(!$value)
            {
                $request = Yii::app()->request;
                $value = $request->hostInfo . $request->baseUrl;
            }
            $this->uriCDN = $value;
        }
        return $this->uriCDN;
    }
    
    private function getFileExtAllowed()
    {
        if(!$this->fileExtAllowed)
        {
            $value = $this->getConfig("file_extensions_allowed");
            if($value)
            {
                $value = explode(",", $value);
            }
            else
            {
                $value = array();
            }
            $this->fileExtAllowed = $value;
        }
        return $this->fileExtAllowed;
    }

    private function getImageExtAllowed()
    {
        if(!$this->imageExtAllowed)
        {
            $value = $this->getConfig("image_extensions_allowed");
            if($value)
            {
                $value = explode(",", $value);
            }
            else
            {
                $value = array();
            }
            $this->imageExtAllowed = $value;
        }
        return $this->imageExtAllowed;
    }

    private function getEmailAdmin()
    {
        if(!$this->emailAdmin)
        {
            $value = $this->getConfig("email_admin");
            if(!$value)
            {
                $value = "modus@pucp.edu.pe";
            }
            $this->emailAdmin = $value;
        }
        return $this->emailAdmin;
    }
	
	private function getOrderProjectsByType()
    {
        if(!$this->orderProjectsByType)
        {
            $value = $this->getConfig("order_projects_by_type");
            if(isset($value))
            {
                $this->orderProjectsByType = $value==1?true:false;
            }            
        }
        return $this->orderProjectsByType;
    }

    private function getConfig($key)
    {
        /*$value_cache = Yii::app()->cache->get($key);
        if(!$value_cache)
        {
            try {
                $value = Config::model()->findByPk($key);
            } catch (Exception $ex) {
                $value = NULL;
            }
            
            if($value)
            {
                $value = $value->value;
                Yii::app()->cache->set($key, $value);
                return $value; 
            }
        }
        else
        {
            return $value_cache;
        }*/
        
        try {
                $value = Config::model()->findByPk($key);
            } catch (Exception $ex) {
                $value = NULL;
            }
            
            if($value)
            {
                $value = $value->value;
                Yii::app()->cache->set($key, $value);
                return $value; 
            }
    }
}