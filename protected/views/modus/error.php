<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
?>
<center>
<?php
if($code == 404)
{
?>
<h3 class="error">La página solicitada no existe</h3>
<?php
}
else if($code == 500)
{
?>
<h3 class="error">Ha ocurrido un error, vuelva a intentarlo más tarde.</h3>
<?php
}
else if($code == 403)
{
?>
<h3 class="error">No esta autorizado para realizar esta acción.</h3>
<?php
}
else if(YII_DEBUG)
{
    var_dump($code);
}
?>
<a href="<?php echo Yii::app()->request->baseUrl."/"; ?>">Ir al inicio</a>
</center>