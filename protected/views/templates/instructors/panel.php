<div class="table-responsive">
    <table class="table table-bordered table-striped table-editable">
            <thead>
              <tr>
                    <th class="name">Usuario</th>
                    <th class="lastname hidden-xs">Nombres y Apellidos</th>
                    <th class="email hidden-xs">Email</th>
                    <th class="room">Secciones</th>
              </tr>
            </thead>
            <tbody id="instructos-list">

            </tbody>
    </table>
</div>