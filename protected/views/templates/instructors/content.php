<td column="username">
    <span class="view input-sm" rel="username" id="username-view"></span>&nbsp<a class="action-edit" title="Editar información de usuario"><i class="fa fa-pencil"></i></a>
</td>
<td column="name" class="hidden-xs">
        <div class="view input-sm" rel="name" id="name-view"></div>
</td>
<td column="email" class="hidden-xs">
        <div class="view input-sm" rel="email" id="email-view"></div>
</td>
<td column="room" >
        <div class="dropdown">
                <div class="input-sm label-room" >
                    <a data-toggle="dropdown"><%=_.pluck(_.filter(rooms,function(room){return _.contains(instructor.rooms,room.id);}),"name").join(",")%></a>
                    &nbsp;
                    <a class="action-delete" title="Quitar todas las secciones"><i class="fa fa-trash-o"></i></a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                        <%_.each(rooms,function(room){%>
                                <li value="<%=room.id%>" class="checbox-room">
                                        <a>
                                            <div class="checkbox">
                                                        <label>
                                                                <input <%=_.contains(instructor.rooms,room.id)?"checked='checked'":""%>type="checkbox" value="<%=room.id%>"/> Sección "<%=room.name%>"
                                                        </label>
                                            </div>
                                        </a>
                                </li>
                        <%})%>
                </ul>
                </div>
                
        </div>
    
</td>