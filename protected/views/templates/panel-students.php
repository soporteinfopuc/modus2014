<div>
	<table class="table table-bordered table-striped table-editable">
		<thead>
		  <tr>
			<th><%=_.size(students)%></th>
			<th class="name">Nombre</th>
			<th class="lastname">Apellido</th>
			<th class="username">Usuario</th>
			<th class="password">Contraseña</th>
			<th class="email">Email</th>
		  </tr>
		</thead>
		<tbody>
			<%_.each(students, function(student,index){%>
			<tr student-id="<%=student.id%>" student-index="<%=index%>" class="<%=student.id?"":"editing"%>">
				<td class="select <%=student.selected?"checked":""%>">
						<span class="index"> <%=index+1%></span>
						<input type="checkbox" class="check" <%=student.selected?"checked":""%>>
				</td>
				<td column="name">
					<div class="view input-sm" rel="name"><%=student.name%></div>
					<input class="form-control input-sm edit" rel="name" type="text" value="<%=student.name%>"/>
				</td>
				<td column="lastname">
					<div class="view input-sm" rel="lastname"><%=student.lastname%></div>
					<input class="form-control input-sm edit" rel="lastname" type="text" value="<%=student.lastname%>"/>
				</td>
				<td column="username">
					<div class="view input-sm" rel="username"><%=student.username%></div>
					<input class="form-control input-sm edit" rel="username" type="text" value="<%=student.username%>"/>
				</td>
				<td column="password">
					<div class="view input-sm" rel="password">**********</div>
					<input class="form-control input-sm edit" rel="password" type="password" value="<%=student.password%>"/>
				</td>
				<td column="email">
					<div class="view input-sm" rel="email"><%=student.email%></div>
					<input class="form-control input-sm edit" rel="email" type="text" value="<%=student.email%>"/>
				</td>
			</tr>
			<%})%>
		</tbody>
	</table>
	<button type="button" class="btn btn-primary btn-save">Guardar</button>
	<button type="button" class="btn btn-danger btn-delete <%=_.where(students,{selected:true}).length>0?"":"disabled"%>">Eliminar <span><%=_.where(students,{selected:true}).length>0?_.where(students,{selected:true}).length:""%></span></button>
</div>