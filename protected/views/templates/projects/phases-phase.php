<div class="form-group">
        <label class="col-sm-3 col-md-2 control-label">Fase <%=ord%></label>
        <div class="col-sm-3">
            <h4 class="text-modus accordion-toggle" data-toggle="collapse" data-parent="#phases-list" data-target="#phase-<%=ord%>"><%=name%></h4>
        </div>
</div>
<div id="phase-<%= ord %>" class="panel-collapse collapse">
    <div>
        <div class="form-group">
            <label class="col-sm-3 col-md-2 control-label">Descripción</label>
            <div class="col-sm-9 col-md-10">
                <div>
                <textarea class="form-control mce" rows="5" id="phase-description" aria-hidden="true"></textarea>
                </div>
                <div>
                        <button type="button" class="btn btn-default btn-xs " values="" id="phase-attach-link"> Añadir archivo</button>
                        <div class="phase-attachment">
                            <ul class="list-inline" id="phase-attach-values"></ul>
                        </div>
                </div>
            </div>

        </div>
        <div class="form-group">
            <label class="col-sm-3 col-md-2 control-label">Imagen de perfil</label>
            <div class="col-sm-3">
                <div class="input-group">
                    <div id="phase-image"></div>
                </div>
            </div>
        </div>
        <div style="min-height: 50px;">
            <div class="form-group strategy select-dropdown">
                <label class="col-sm-3 col-md-2 control-label">Estrategias</label>
                <div class="col-sm-9 col-md-10 group-strategy-select">
                    <div class="input-group dropdown-toggle" data-toggle="dropdown">
                        <input type="text" placeholder="Buscar..." class="form-control" id="strategy-select">
                        <div class="input-group-btn"><button type="button" class="btn btn-default "><span class="caret"></span></button></div>
                        
                    </div>
                    <ul class="dropdown-menu" role="menu"></ul>
                </div>
            </div>
        </div>
        <div style="min-height: 200px;">
            <div class="form-group tool select-dropdown">
                <label class="col-sm-3 col-md-2 control-label">Herramientas</label>
                <div class="col-sm-9 col-md-10 group-tool-select">
                    <div class="input-group dropdown-toggle" data-toggle="dropdown">
                        <input type="text" placeholder="Buscar..." class="form-control" id="tool-select">
                        <div class="input-group-btn"><button type="button" class="btn btn-default "><span class="caret"></span></button></div>
                        
                    </div>
                    <ul class="dropdown-menu" role="menu"></ul>
                </div>
            </div>
        </div>
    </div>
</div>