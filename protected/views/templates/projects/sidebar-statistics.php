<div class="panel-heading teams-heading">
    <h3 class="panel-title side-title">
        <i class="fa fa-bar-chart-o"></i> Dashboard
        <a class="collapse-btn pull-right collapse-my-team" data-toggle="collapse" data-target="#statistic-side-list">
            <i class="fa fa-angle-double-up up "></i>
            <i class="fa fa-angle-double-down down"></i>
        </a>
    </h3>
</div>
<div class="panel-body teams-list  collapse in" id="statistic-side-list">
        <dl>
            <dt>General</dt>
            <dd class="side-list">
                <%_.each(rooms, function(room){
                    if(room.teams && room.teams.length>0){%>
                    <li room-id="<%=room.id%>" page-dash="general" class="gotoTeamBtn">
                            <a><i class="fa fa-caret-right"></i> SECCIÓN <%=room.name%></a>
                    </li>
                    <%}%>
                <%})%>
            </dd>
            <dt>Avances por grupos</dt>
            <dd class="side-list">
                <%_.each(rooms, function(room){
                    if(room.teams && room.teams.length>0){%>
                    <li room-id="<%=room.id%>" page-dash="progress" class="gotoTeamBtn">
                            <a><i class="fa fa-caret-right"></i> SECCIÓN <%=room.name%></a>
                    </li>
                    <%}%>
                <%})%>
            </dd>
        </dl>
</div>
