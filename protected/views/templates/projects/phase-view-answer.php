<div class="title-answer">
    <i class="fa fa-angle-double-right"></i> <strong><%=(answer.is_last == '1')?'<span class="final">' + text_answer + ' final </span>':''%>&nbsp;<span title="<%=answer.student_username%>"><%=answer.student_name%></span></strong> 
    <span class="date" title="<%=answer.modified%>">aproximadamente hace <%=answer.modified_ago+" "%><%=answer.modified%></span>
</div>
<div class="content modus-description"><%=answer.answer%></div>
<div class="footer-answer">
    <%if(project && project.is_manager)
    {%>
    <div class="pull-right" style="margin-right: 5px;">
        <a class="respond-answer-btn"><i class="fa fa-share-square-o"></i> Responder </a>
        <a class="delete-answer-btn" title="Eliminar esta <%=text_answer%>" answer-id='<%=answer.id%>'><i class="fa fa-trash-o"></i></a>
    </div>
    <%}%>
    <%if(answer.attachment_id){%>
    <div class="attachments">
        <div class="btn-group dropup">
            <a type="button" class="btn btn-sm btn-attach <%=answer.attachment_type?('btn-attach-'+answer.attachment_type):''%>" href='//docs.google.com/viewer?url=<%=answer.attachment_url%>' target="_blank" title="Visualizar archivo">
                <i class="fa fa-file<%=answer.attachment_type?('-'+answer.attachment_type):''%>-o"></i>&nbsp;<%=answer.attachment_name%>
            </a>
            <a type="button" class="btn btn-sm btn-attach <%=answer.attachment_type?('btn-attach-'+answer.attachment_type):''%>" href="<%=answer.attachment_url%>" target="_blank" title="Descargar">
                <i class="fa fa-download"></i>
            </a>
        </div>
    </div>
    <%}%>
</div>