<div class="title-answer">
    <i class="fa fa-angle-double-right"></i> <strong><%=reanswer.author_name%></strong> 
    <span class="date" title="<%=reanswer.created%>">aproximadamente hace <%=reanswer.created_ago+" "%> <%=reanswer.created%></span>
</div>
<div class="content modus-description"><%=reanswer.reanswer%></div>
<div class="footer-answer">
    <%if(can_remove){%>
    <div class="pull-right">
        <a class="delete-reanswer-btn" title="Eliminar" reanswer-id='<%=reanswer.id%>'><i class="fa fa-trash-o"></i></a>
    </div>
    <%}%>
    <%if(reanswer.attachments && reanswer.attachments.length>0){%>
    <div class="attachments">
            <%if(reanswer.attachments){%>
                <ul class="list-inline">
                <% _.each(reanswer.attachments, function(attachment,index){%>
                <li>
                    <div class="btn-group dropup">
                        <a type="button" class="btn btn-sm btn-attach <%=attachment.type?('btn-attach-'+attachment.type):''%>" href='//docs.google.com/viewer?url=<%=attachment.url%>' target="_blank" title="Visualizar archivo">
                            <i class="fa fa-file<%=attachment.type?('-'+attachment.type):''%>-o"></i>&nbsp;<%=attachment.original_name%>
                        </a>
                        <a type="button" class="btn btn-sm btn-attach <%=attachment.type?('btn-attach-'+attachment.type):''%>" href="<%=attachment.url%>" target="_blank" title="Descargar">
                            <i class="fa fa-download"></i>
                        </a>
                    </div>
                </li>
                <%})%>
                </ul>
            <%}%>
    </div>
    <%}%>
</div>