<div class="panel-heading teams-heading">
    <h3 class="panel-title side-title">
        <i class="fa fa-users"></i>&nbsp;<%=team.name + " - " + room.name%> 
        <a class="collapse-btn pull-right collapse-answers" data-toggle="collapse" data-target="#my-team-side-list">
            <i class="fa fa-angle-double-up up "></i>
            <i class="fa fa-angle-double-down down"></i>
        </a>
    </h3>
</div>
<div class="panel-body teams-list  collapse in" id="my-team-side-list">
    <dd class="side-list">
    <%if(team.students){ _.each(team.students, function(student){%>
        <li title="<%=student.username%>"><%=student.name%></li>
    <%}) }else{%>
        <li>No tiene alumnos</li>
    <%}%>
    </dd>
</div>