<div class="form post-reanswer" id="form-post-reanswer" style="display:none;">
        <textarea id="input-reanswer"></textarea>
        <div>
            <ul class="list-inline">
                <li><button type="button" class="btn btn-default btn-xs attach-btn-reanswer"><i class="fa fa-search-plus"></i> Adjuntar archivo</button></li>
                <li id="attach-values"></li>
            </ul>
        </div>
        <a class="btn btn-secondary" id="btn-publish">Enviar <i class="fa fa-chevron-circle-right"></i></a>
</div>
<div id="reanswers-zone"></div>