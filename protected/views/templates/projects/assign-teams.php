<div class="page-title">
    <h2 class="title-modus">Asignar grupos</h2>
</div>
<div>
   <div class="form-group col-sm-12">
        <label class="control-label">Sección </label>
        <div class="input-group">
             <div class="form-control" id="teams-select-room" value=""></div>
             <div class="input-group-btn">
                 <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i></button>
                 <ul class="dropdown-menu pull-right" id="teams-select-room-ddm"></ul>
             </div>
        </div>
   </div>
   <div class="form-group" id="team-zone"></div>
   <div class="clearfix"></div>
   <div class="form-group">
        <div class="col-sm-12">
                <a modus-link href="<%=Modus.uriRoot%>/" type="button" rel="button" class="btn btn-secondary">Finalizar&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
        </div>
    </div>
</div>