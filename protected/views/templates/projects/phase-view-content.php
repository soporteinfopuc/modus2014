<div class="row">
    <div class="col-sm-12 modus-description table-responsive">
        <%=phase.description%>
    </div>

        <div class="col-sm-12">
                <div class="phase-attachment">
                        <%if(phase.attachments){%>
                            <ul class="list-inline">
                            <% _.each(phase.attachments, function(attachment,index){%>
                            <li>
                                <div class="btn-group dropup">
                                    <a type="button" class="btn btn-sm btn-attach <%=attachment.type?('btn-attach-'+attachment.type):''%>" href='<%=attachment.url%>' target="_blank" title="Visualizar archivo">
                                        <i class="fa fa-file<%=attachment.type?('-'+attachment.type):''%>-o"></i>&nbsp;<%=attachment.original_name%>
                                    </a>
                                    <a type="button" class="btn btn-sm btn-attach <%=attachment.type?('btn-attach-'+attachment.type):''%>" href="<%=attachment.url%>" target="_blank" title="Descargar">
                                        <i class="fa fa-download"></i>
                                    </a>
                                </div>
                            </li>
                            <%})%>
                            </ul>
                        <%}%>
                </div>
        </div>
</div>
<div class="phase-subcontent">
        <ul class="nav nav-tabs">
                <%if(phase.strategies.length){%>
                <li><a class="strategies-tab" data-toggle="tab" data-target="#phase-<%=phase.id%>-strategy"><i class="fa fa-cubes"></i> Estrategias</a></li>
                <%}%>
                <%if(phase.tools.length){%>
                <li><a class="tools-tab" data-toggle="tab" data-target="#phase-<%=phase.id%>-tool"><i class="fa fa-wrench"></i> Herramientas</a></li>
                <%}%>
                <li><a class="answers-tab" data-toggle="tab" data-target="#phase-<%=phase.id%>-assignment"><i class="fa fa-comments-o"></i> Tareas</a></li>
                <li><a class="qualifications-tab" data-toggle="tab" data-target="#phase-<%=phase.id%>-qualification"><i class="fa fa-check-square-o"></i> Evaluación</a></li>
        </ul>
        <div class="tab-content">
                <%if(phase.strategies.length){%>
                <div class="tab-pane phase-tab-content " id="phase-<%=phase.id%>-strategy">
                        <%_.each(phase.strategies,function(strategy){ %>
                                <h3 class="phase-subtitle">
                                <%if(strategy.image){%>
                                <div style="background:url(<%=strategy.image%>) 50% 50% no-repeat;" class="icon"></div>
                                <%}else{%>
                                <div class="icon"><i class="fa fa-angle-double-right fa-2x"></i></div>
                                <%}%>
                                &nbsp;<%=strategy.name%></h3>
                                <div class="subsubcontent-body">
                                        <%=strategy.description%>
                                </div>
                        <%})%>
                </div>
                <%}%>
                <%if(phase.tools.length){%>
                <div class="tab-pane phase-tab-content" id="phase-<%=phase.id%>-tool">
                        <% _.each(phase.tools,function(tool) { %>
                                <h3 class="phase-subtitle">        
                                <%if(tool.image){%>
                                <div style="background:url(<%=tool.image%>) 50% 50% no-repeat;" class="icon"></div>
                                <%}else{%>
                                <div class="icon"><i class="fa fa-angle-double-right"></i></div>
                                <%}%>
                                &nbsp;<%=tool.name%><small><a modus-link  href="<%=tool.url%>" target="_blank"><%=tool.url%></a></small></h3>
                                <div class="subsubcontent-body">
                                        <%=tool.description%>
                                </div>
                        <%})%>
                </div>
                <%}%>
                <div class="tab-pane phase-tab-content answer" id="phase-<%=phase.id%>-assignment">
                        <div class="panel-body row">
                            <div class="pull-left header-answers"></div>
                            <div class="pull-right"><i class="fa fa-refresh refresh-answers" title="Actualizar <%=phase.text_answer%>s"></i></div>
                        </div>
                        <div class="panel-body row">
                            <div class="header-include-answers">
                                <div class="panel panel-info">
                                    <div class="panel-heading"><h3 class="panel-title">Pregunta final</h3></div>
                                    <div class="panel-body"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form" id="form-post-answer" style="display:none;">
                                <!--<p><code>Pregunta: </code></p>-->
                                <span id="draftinfo"></span>
                                <textarea id="input-answer"></textarea>
                                <div>
                                    <ul class="list-inline">
                                        <li><button type="button" class="btn btn-default btn-xs" id="attach-btn"><i class="fa fa-search-plus"></i> Adjuntar archivo</button></li>
                                        <li id="attach-values"></li>
                                    </ul>
                                </div>
                                <div class="checkbox">
                                        <label>
                                          <input type="checkbox" id="is-final-answer" <%=phase.canLastAnswer?'':"disabled='disabled'"%> > ¿Esta es tu <%=phase.text_answer%> final? 
                                          <small class="help-block" style="display:inline;">Esta opción se activara cuantos todos los miembros del grupo hayan publicado al menos una <%=phase.text_answer%></small>
                                        </label>
                                </div>
                                <ul class="list-inline">
                                    <li class="pull-right"><a class="btn btn-default" id="btn-save-draft">Guardar en borrador <i class="fa fa-chevron-circle-right"></i></a></li>
                                    <li><a class="btn btn-secondary" id="btn-publish">Publicar <i class="fa fa-chevron-circle-right"></i></a></li>
                                    <li id="status-saving"></li>
                                </ul>
                        </div>
                        <div class="answers" id="answers"></div>
                </div>
                <div class="tab-pane phase-tab-content" id="phase-<%=phase.id%>-qualification">
                    <div class="panel-body row">
                            <div class="pull-right">
                                <i class="fa fa-refresh refresh-qualifications" title="Actualizar calificaciones"></i>
                            </div>    
                            <div class="message-area"></div>
                    </div>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th rowspan="2">N°</th>
                                    <th rowspan="2">Criterios a considerar</th>
                                    <th rowspan="2">Puntaje máx.</th>
                                    <th colspan="2">Calificación (de 0 a 20)</th>
                                </tr>
                                <tr>
                                    <th>De los estudiantes</th>
                                    <th>Del docente</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%_.each(phase.rules, function(rule){ %>
                                <tr>
                                    <td class="center"><%=rule.ord%></td>
                                    <td><%=rule.rule%></td>
                                    <td class="center"><%=rule.score + " punto" + (rule.score>1?"s":"")%></td>
                                    <td class="center" member-mark="<%=rule.id%>"></td>
                                    <td class="center" manager-mark="<%=rule.id%>"></td>

                                </tr>
                                <%})%>
                                <tr>
                                    <td class="center" colspan="4" style="text-align:right;">Nota Final: </td>
                                    <td class="center final-mark" style="font-size: 1.5em"></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title"></h4>
                                    </div>
                                    <div class="modal-body">
                                        <p></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        <button type="button" class="btn btn-primary">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade assign-qualification-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form">
                                            <div class="form-group">
                                                <p><label>Nota:</label></p>
                                                <center>
                                                <div class="btn-group">
                                                    <%_.each(["00","01","02","03","04","05","06","07","08","09","10"],function(val,index){%>
                                                    <button type="button" class="btn btn-default btn-mark" mark="<%=val%>"><%=val%></button>
                                                    <%})%>
                                                </div>
                                                <p></p>
                                                <div class="btn-group">
                                                    <%_.each([11,12,13,14,15,16,17,18,19,20],function(val,index){%>
                                                    <button type="button" class="btn btn-default btn-mark" mark="<%=val%>"><%=val%></button>
                                                    <%})%>
                                                </div>
                                                </center>
                                            </div>
                                            <div class="form-group">
                                                <label>Comentario: <small>(opcional)</small></label>
                                                <textarea class="form-control text-comment" rows="3"></textarea>
                                            </div>
                                            <p class="help-block"></p>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="button" class="btn btn-primary disabled btn-post-qualification">Enviar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
        </div>
</div>