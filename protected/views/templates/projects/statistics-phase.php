<div>
    <h2 class="accordion-header title-phase collapsed">
        <%if(phase.image){%>
        <img src="<%=phase.image%>" alt=""  class="img-circle">
        <%}%><%=" "+phase.name%>        
    </h2>
    <%_.each(phase.teams, function(team){%>
    <table class="table table-bordered table-progress-statistics">
        <thead>
            <tr>
                <th><h4 class="group-title"><%=team.name%></h4></th>
                <th>Sin Avance</th>
                <th>En Borrador</th>
                <th>Terminado</th>
            </tr>
        </thead>
        <tbody>
            <%_.each(team.users, function(user){%>
            <tr>
                <td><%=user.name%></td>
                <td class="circle-mark noprogress result-<%=user.result%>"><i class="fa fa-circle"></i></td>
                <td class="circle-mark draft result-<%=user.result%>"><i class="fa fa-circle"></i></td>
                <td class="circle-mark ended result-<%=user.result%>"><i class="fa fa-circle"></i></td>
            </tr>
            <%})%>
        </tbody>
    </table>
    <%})%>
</div>