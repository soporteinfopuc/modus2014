<td column="ord">
    <span class="view" rel="ord" id="ord-view"></span>
</td>
<td column="username">
    <span class="view" rel="username" id="username-view"></span>&nbsp<a class="action-edit" title="Editar información de usuario"><i class="fa fa-pencil"></i></a>
</td>
<td column="name" class="hidden-xs">
        <div class="view " rel="name" id="name-view"></div>
</td>
<td column="email" class="hidden-xs">
        <div class="view " rel="email" id="email-view"></div>
</td>
<td column="action">
        <a class="action-delete" title="Quitar de esta sección"><i class="fa fa-trash-o"></i></a>
</td>