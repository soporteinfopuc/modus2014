<div class="table-responsive">
	<table class="table table-striped ">
		<thead>
		  <tr>
                        <th class="ord">#</th>
			<th class="name">Usuario</th>
			<th class="lastname hidden-xs">Nombres y Apellidos</th>
			<th class="email hidden-xs">Email</th>
			<th class="actions"></th>
		  </tr>
		</thead>
		<tbody id="students-list">
			
		</tbody>
	</table>
</div>
<div class="pull-right">
    <button type="button" class="btn btn-info btn-xs export-to-csv" style="display: none;" data-toggle="tooltip" data-placement="top" title="Descargar la lista de alumnos en un archivo CSV">Exporta lista a archivo CSV</button>
</div>