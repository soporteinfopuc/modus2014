<div class="col-lg-12 col-md-5 col-sm-5 col-xs-12">
    <div class="btn-block">
        <button type="button" class="btn btn-primary btn-sm btn-block import-from-csv" data-toggle="tooltip" data-placement="left" title="Crea usuarios tipo Alumnos a partir de un archivo CSV. Si se ha elegido una Sección, se intentará asignar los Alumno a la Sección. Si un Alumno ya existe, también se intentará asignarlo a la Sección elegida.">Importar desde archivo CSV</button>
    </div>
    <div class="btn-block">
        <button type="button" class="btn btn-primary btn-block create-user-student" data-toggle="tooltip" data-placement="left" title="Crea un usuario tipo Alumno, además, si se ha elegido una Sección, se asigna al nuevo Alumno a la Sección elegida.">Crear Usuario Alumno</button>
    </div>
</div>

<div class="modal fade" id="modal-student" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <form role="form" id="form-new-student" autocomplete="off">
            <div class="alert alert-warning alert-dismissible alert-modal" role="alert">
                    &nbsp;
            </div>
            <div class="form-group">
              <label for="newUser">Usuario</label>
              <input type="text" class="form-control" name="username" id="newUser" placeholder="" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="newName">Nombre</label>
              <input type="text" class="form-control" name="firstname" id="newName" placeholder="" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="newLastname">Apellido</label>
              <input type="text" class="form-control" name="lastname" id="newLastname" placeholder="" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="newEmail">Email</label>
              <input type="text" class="form-control" name="email" id="newEmail" placeholder="" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="newPassword">Contraseña</label>
              <input type="password" style="display: none;"/>
              <input type="password" class="form-control" name="password" id="newPassword" placeholder="" autocomplete="off">
            </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary btn-save">Guardar</button>
      </div>
    </div>
  </div>
</div>