<div class="col-lg-12 col-md-5 col-sm-5 col-xs-12">
    
        <div class="form-group">
                <label class="control-label">Colegio</label>
                <div class="input-group">
                    <div class="form-control" id="school-input" value=""></div>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i></button>
                        <ul class="dropdown-menu pull-right" role="menu" style="min-width:220px" id="school-dropdown-menu"></ul>
                    </div>
                </div>
        </div>
</div>
<div class="col-lg-12 col-md-4 col-sm-4 col-xs-8">
        <div class="form-group">
                <label class="control-label">Grado</label>
                <div class="input-group">
                    <div class="form-control" id="grade-input" value=""></div>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i></button>
                        <ul class="dropdown-menu pull-right" role="menu" style="min-width:220px" id="grade-dropdown-menu"></ul>
                    </div>
                </div>
        </div>
</div>
<div class="col-lg-12 col-md-3 col-sm-3 col-xs-4">
        <div class="form-group">
                <label class="control-label">Sección</label>
                <div class="input-group">
                    <div class="form-control" id="section-input" value=""></div>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i></button>
                        <ul class="dropdown-menu pull-right" role="menu" style="min-width:220px" id="section-dropdown-menu"></ul>
                    </div>
                </div>
        </div>
</div>