<div class="panel panel-modus">
        <div>
                <div class="col-xs-3 panel-left-modus">
                        <div class="panel-heading"></div>
                        <div class="panel-body">
                                <div class="user-img"><a><img class="img-thumbnail" src='<%=model.image_url?model.image_url:Modus.uriRoot+"/img/default_tool.png"%>' alt=""/></a></div>
                        </div>
                </div>
                <div class="col-xs-9 panel-right-modus">    						
                        <div class="panel-heading"></div>
                        <div class="panel-body">
                                <div class="modus-title"><a modus-link  href="<%=Modus.uriRoot%>/tool/edit/<%=model.id%>" class="action-edit"><%= model.name %></a></div>
                                <div class="modus-subtitle">
                                        <span title="Modificado <%=model.modified%>">Modificado hace <%=model.modified_ago%></span>
                                </div>
                                <div class="modus-content">
                                    <%=$('<div>'+model.description+'</div>').text()%>
                                </div>
                                <div class="modus-footer">
                                    <div class="pull-right">
                                        <a type="button" rel="button" class="btn btn-secondary btn-xs btn-modus action-edit"  href="<%=Modus.uriRoot%>/tool/edit/<%=model.id%>"><i class="fa fa-pencil"></i></a>&nbsp;
                                        <a type="button" rel="button" class="btn btn-danger btn-xs  btn-modus action-delete" value="<%=model.id%>"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                        </div>
                </div>
        </div>
</div>
	