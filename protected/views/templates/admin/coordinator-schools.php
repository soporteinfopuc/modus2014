<div class="col-sm-12">
    <h2 class="title-modus">
        <a modus-link-back  href="<%=Modus.uriRoot%>/" title="Volver"><i class="fa fa-arrow-left"></i></a>&nbsp;
        <i class="fa fa-graduation-cap"></i>&nbsp;<%=name%> 
        <small>Colegios asignados</small>
    </h2>
</div>
<div class="clearfix"></div>
<div class="col-sm-8 col-sm-offset-2 message-area">
    
</div>
<div class="clearfix"></div>
<div class="col-sm-8 col-sm-offset-2">
        <div id="coordinatorSchoolsApp" class="panel panel-default">
                <div class="panel-heading">
                    <input id="select-school" type="text" class="form-control typehead" placeholder="Nombre del colegio a asignar">
                </div>
                <div id="main" class="panel-body table-responsive">
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        &nbsp;
                    </div>
                    <table class="table table-hover table-condensed">
                        <tbody class="school-list"></tbody>
                    </table>
                </div>
                <div class="panel-footer">
                    <div class="status"><b></b></div>
                    <div class="rooms-count"><b></b> </div>
                </div>
        </div>
</div>