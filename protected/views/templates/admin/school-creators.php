<div class="col-sm-12">
    <h2 class="title-modus">
        <a modus-link-back  href="<%=Modus.uriRoot%>/" title="Volver"><i class="fa fa-arrow-left"></i></a>
        &nbsp;<i class="fa fa-university"></i>&nbsp;<%=school_name%> 
        <small>Creadores asignados</small>
    </h2>
</div>
<div class="clearfix"></div>
<div class="col-sm-8 col-sm-offset-2 message-area">
    
</div>
<div class="clearfix"></div>
<div class="col-sm-8 col-sm-offset-2">
        <div id="schoolsCreatorsApp" class="panel panel-default">
                <div class="panel-heading">
                    <input id="select-creator" type="text" class="form-control typehead" placeholder="Nombre del coordinador a agregar">
                </div>
                <div id="main" class="panel-body table-responsive">
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        &nbsp;
                    </div>
                    <table class="table table-hover table-condensed">
                        <tbody class="creators-list"></tbody>
                    </table>
                </div>
                <div class="panel-footer">
                    <div class="status"><b></b></div>
                    <div class="rooms-count"><b></b> </div>
                </div>
        </div>
</div>