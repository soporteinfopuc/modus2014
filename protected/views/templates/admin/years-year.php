<td>
    <div class="view"><label><i class="fa fa-calendar"></i>&nbsp;<%=name%></label></div>
    <input class="edit form-control input-sm input-edit-year" type="text" value="<%=name%>">
</td>
<td>
    <div class="actions">
        <div class="pull-right">
            <a type="button" rel="button" class="btn <%=active==1?'btn-success':'btn-opaque'%> btn-xs btn-modus action-active" data-toggle="tooltip" title="Establecer Activo"><i class="fa fa-check"></i></a>&nbsp;
            <a type="button" rel="button" class="btn btn-secondary btn-xs btn-modus action-edit" data-toggle="tooltip" title="Editar nombre">
                <i class="fa fa-pencil"></i>
            </a>&nbsp;
            <a type="button" rel="button" class="btn btn-danger btn-xs  btn-modus action-delete" data-toggle="tooltip" title="Eliminar periodo"><i class="fa fa-times"></i></a>
        </div>
    </div>
</td>
