<div class="row">
    <?php if(Yii::app()->user->checkAccess("manageUsers")){ ?>
    <div class="col-sm-4">
        <h2 class="title-modus"><a modus-link  href="<%=Modus.uriRoot%>/user"><i class="fa fa-users"></i> Usuarios <i class="fa fa-arrow-circle-right"></i></a></h2>
        <div class="dotted-line visible-xs"></div>
    </div>
    <?php } ?>
    <?php if(Yii::app()->user->checkAccess("manageCoordinators")){ ?>
    <div class="col-sm-4">
        <h2 class="title-modus"><a modus-link  href="<%=Modus.uriRoot%>/coordinator"><i class="fa fa-graduation-cap"></i> Coordinadores <i class="fa fa-arrow-circle-right"></i></a></h2>
        <div class="dotted-line visible-xs"></div>
    </div>
    <?php } ?>
    <?php if(Yii::app()->user->checkAccess("manageCreators")){ ?>
    <div class="col-sm-4">
        <h2 class="title-modus"><a modus-link  href="<%=Modus.uriRoot%>/creator"><i class="fa fa-male"></i> Creadores <i class="fa fa-arrow-circle-right"></i></a></h2>
        <div class="dotted-line visible-xs"></div>
    </div>
    <?php } ?>
    <?php if(Yii::app()->user->checkAccess("manageUsers") || Yii::app()->user->checkAccess("manageCoordinators") || Yii::app()->user->checkAccess("manageCreators")){ ?>
    <div class="clearfix"></div>
    <div class="dotted-line hidden-xs"></div>
    <?php } ?>
    <?php if(Yii::app()->user->checkAccess("manageSchools")){ ?>
    <div class="col-sm-4">
        <h2 class="title-modus"><a modus-link  href="<%=Modus.uriRoot%>/school"><i class="fa fa-university"></i> Colegios <i class="fa fa-arrow-circle-right"></i></a></h2>
        <div class="dotted-line visible-xs"></div>
    </div>
    <?php } ?>
    <?php if(Yii::app()->user->checkAccess("manageInstructors")){ ?>
    <div class="col-sm-4">
        <h2 class="title-modus"><a modus-link  href="<%=Modus.uriRoot%>/instructor"><i class="fa fa-user"></i> Profesores <i class="fa fa-arrow-circle-right"></i></a></h2>
        <div class="dotted-line visible-xs"></div>
    </div>
    <?php } ?>
    <?php if(Yii::app()->user->checkAccess("manageStudents")){ ?>
    <div class="col-sm-4">
        <h2 class="title-modus"><a modus-link  href="<%=Modus.uriRoot%>/student"><i class="fa fa-child"></i> Alumnos <i class="fa fa-arrow-circle-right"></i></a></h2>
        <div class="dotted-line visible-xs"></div>
    </div>
    <?php } ?>
    <?php if(Yii::app()->user->checkAccess("manageSchools") || Yii::app()->user->checkAccess("manageInstructors") || Yii::app()->user->checkAccess("manageStudents")){ ?>
    <div class="clearfix"></div>
    <div class="dotted-line hidden-xs"></div>
    <?php } ?>
    <?php if(Yii::app()->user->checkAccess("manageStrategies")){ ?>
    <div class="col-xs-12 col-sm-4 col-md-4">
        <h2 class="title-modus"><a modus-link  href="<%=Modus.uriRoot%>/strategy"><i class="fa fa-cubes"></i> Estrategias <i class="fa fa-arrow-circle-right"></i></a></h2>
        <div class="dotted-line visible-xs"></div>
    </div>
    <?php } ?>
    <?php if(Yii::app()->user->checkAccess("manageTools")){ ?>
    <div class="col-xs-12 col-sm-4 col-md-4">
        <h2 class="title-modus"><a modus-link  href="<%=Modus.uriRoot%>/tool"><i class="fa fa-wrench"></i> Herramientas <i class="fa fa-arrow-circle-right"></i></a></h2>
        <div class="dotted-line visible-xs"></div>
    </div>
    <?php } ?>
    <div class="clearfix  visible-sm"></div>
    <div class="dotted-line visible-sm"></div>
    <?php if(Yii::app()->user->checkAccess("manageTemplatePhases")){ ?>
    <div class="col-xs-12 col-sm-4 col-md-4">
        <h2 class="title-modus"><a modus-link  href="<%=Modus.uriRoot%>/phase"><i class="fa fa-tasks"></i> Fases <i class="fa fa-arrow-circle-right"></i></a></h2>
        <div class="dotted-line visible-xs"></div>
    </div>
    <?php } ?>
    <?php if(Yii::app()->user->checkAccess("manageStrategies") || Yii::app()->user->checkAccess("manageTools") || Yii::app()->user->checkAccess("manageTemplatePhases")){ ?>
    <div class="clearfix"></div>
    <div class="dotted-line hidden-xs"></div>
    <?php } ?>
    
    <?php if(Yii::app()->user->checkAccess("createProjects")){ ?>
    <div class="col-sm-12">
        <h2 class="title-modus"><i class="fa fa-file-o"></i> Crear proyecto</h2>
        <p>Aquí podrás gestionar todas tus entradas. Añadir o editar tus Proyectos, editar fases y seleccionar nuevas estrategias o herramientas.</p>
        <a class="link-modus" modus-link  href="<%=Modus.uriRoot%>/project/add" modus-link>Crear Proyecto <i class="fa fa-arrow-circle-right"></i></a>
        <a class="link-modus" id="import-project">Importar Proyecto <i class="fa fa-arrow-circle-right"></i></a>
        <div class="dotted-line"></div>
    </div>
    <?php } ?>
    <div class="col-sm-12 project-list-section">
        <div><h2 class="title-modus"><i class="fa fa-list-alt"></i><%=Modus.isAdmin?" Editar proyectos":" Proyectos"%></h2></div>
        
        
        <nav class="navbar navbar-default navbar-modus" role="navigation">
            <div>
                <div class="navbar-header">
                    
                </div>
                <div class="projects-general-tabs"></div>
            </div>
        </nav>
        
        <div class="clearfix"></div>
        <div class="tab-content">
            <div class="body-content">
            <h2><i class="fa fa-spinner fa-spin"></i> Cargando proyectos...</h2>
            </div>
        </div>        
    </div>
    <div class="modal fade" id="duplicate-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default action-cancel" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary action-save">Duplicar</button>
                </div>
            </div>
        </div>
    </div>
</div>