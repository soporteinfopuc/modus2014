<div class="col-sm-12">
    <h2 class="title-modus">
        <a modus-link-back  href="<%=Modus.uriRoot%>/" title="Volver"><i class="fa fa-arrow-left"></i></a>&nbsp;
        <i class="fa fa-university"></i>&nbsp;Administrar Colegios
    </h2>
</div>
<div class="clearfix"></div>
<div class="col-sm-8 col-sm-offset-2 message-area">
    
</div>
<div class="clearfix"></div>
<div class="col-sm-8 col-sm-offset-2">
        <div id="schoolsapp" class="panel panel-default">
                <div class="panel-heading">
                        
                        <div class="input-group">
                            <input id="new-school" type="text" class="form-control input-edit-school" placeholder="Nombre del nuevo colegio" title="Escriba un nombre y presione Enter para agregar">
                            <span class="input-group-btn">
                                <button id="add-school-btn" class="btn btn-default" type="button">Agregar</button>
                            </span>
                        </div>
                </div>
                <div id="main" class="panel-body table-responsive">
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        &nbsp;
                    </div>
                    <table class="table table-hover table-condensed">
                        <tbody id="schools-list">

                        </tbody>
                    </table>
                </div>
                <div class="panel-footer">
                    <div class="status"><b></b></div>
                    <div class="rooms-count"><b></b> </div>
                </div>
        </div>
</div>