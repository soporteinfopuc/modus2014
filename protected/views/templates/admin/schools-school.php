<td>
    <div class="view"><label><i class="fa fa-university"></i>&nbsp;<%=name%></label></div>
    <input class="edit form-control input-sm input-edit-school" type="text" value="<%=name%>">
</td>
<td>
    <div class="actions">
        <div class="pull-right">
            <a type="button" rel="button" class="btn btn-secondary btn-sm btn-modus" modus-link  href="school/<%=id%>/creators" data-toggle="tooltip" title="Ver creadores">
                <i class="fa fa-male"></i>
                <span class="badge"><%=count_creators%><span>
            </a>&nbsp;
            <a type="button" rel="button" class="btn btn-secondary btn-sm btn-modus" modus-link  href="school/<%=id%>/coordinators" data-toggle="tooltip" title="Ver coordinadores">
                <i class="fa fa-graduation-cap"></i>
                <span class="badge"><%=count_coordinators%><span>
            </a>&nbsp;
            <a type="button" rel="button" class="btn btn-secondary btn-sm btn-modus" modus-link  href="school/<%=id%>/rooms" data-toggle="tooltip" title="Administrar secciones.">
                <i class="fa fa-sitemap"></i>
                <span class="badge"><%=count_rooms%><span>
            </a>&nbsp;
            <a type="button" rel="button" class="btn btn-danger btn-sm  btn-modus delete" data-toggle="tooltip" title="Eliminar colegio"><i class="fa fa-times"></i></a>
        </div>
    </div>
</td>
