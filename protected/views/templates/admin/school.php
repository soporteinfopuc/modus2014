<div class="col-sm-12">
    <h2 class="title-modus">
        <a modus-link-back  href="<%=Modus.uriRoot%>/" title="Volver"><i class="fa fa-arrow-left"></i></a>&nbsp;<i class="fa fa-university"></i>&nbsp;<%=school.name%><small>Administrar Secciones</small>
    </h2>
</div>
<div class="col-sm-12" id="schoolsapp">
    
    <div id="main">
        <div class="">
            <div id="alert-rooms" class="alert alert-warning alert-dismissible" style="display: none;" role="alert">&nbsp;</div>
        </div>
        <div class="panel-group row levels-list">
        
        </div>
    </div>
    <div class="">
            <div class="status"><b></b></div>
            <div class="rooms-count"><b></b> </div>
    </div>
</div>