<div class="view input-sm">
  <label><%=name%></label>
  <div class="actions pull-right">
        <a title="Ver colegios asignados" data-toggle="tooltip" modus-link  href="<%=Modus.uriRoot%>/coordinator/<%=id%>"><span class="fa-stack icon-link"><i class="fa fa-circle fa-stack-2x shadow"></i><i class="fa fa-sitemap fa-stack-1x fa-inverse shadow-in"></i></span></a>
        <a title="Eliminar" data-toggle="tooltip" class="delete"><span class="fa-stack icon-link"><i class="fa fa-circle fa-stack-2x shadow"></i><i class="fa fa-times fa-stack-1x fa-inverse fa-flip-horizontal shadow-in"></i></span></a>
  </div>
</div>