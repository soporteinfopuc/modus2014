<div class="col-sm-12">
        <h2 class="title-modus"><a modus-link-back  href="<%=Modus.uriRoot%>/" title="Volver"><i class="fa fa-arrow-left"></i></a> Administrar Usuarios</h2>
</div>
<div class="clearfix"></div>
<div class="col-sm-12">
    <div class="content table-responsive users-list"></div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
          <form role="form" id="form-new-user">
            <div class="form-group">
              <label for="newUser">Usuario</label>
              <input type="text" class="form-control" name="username" id="newUser" placeholder="" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="newName">Nombre</label>
              <input type="text" class="form-control" name="firstname" id="newName" placeholder="" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="newLastname">Apellido</label>
              <input type="text" class="form-control" name="lastname" id="newLastname" placeholder="" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="newEmail">Email</label>
              <input type="text" class="form-control" name="email" id="newEmail" placeholder="" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="newPassword">Contraseña</label>
              <input type="password" class="form-control" name="password" id="newPassword" placeholder="" autocomplete="off">
            </div>
            <div class="form-group user-type">
              <label for="inputUserType">Tipo de usuario</label>
              <div class="radio">
                <label><input type="radio" name="role" id="optionAdmin" value="admin" checked> Administrador</label>
              </div>
              <div class="radio">  
                <label><input type="radio" name="role" id="optionCreator" value="creator"> Creador</label>
              </div>
              <div class="radio">  
                <label><input type="radio" name="role" id="optionCoordinator" value="coordinator"> Coordinador</label>
              </div>
            </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary btn-save">Guardar</button>
      </div>
    </div>
  </div>
</div>