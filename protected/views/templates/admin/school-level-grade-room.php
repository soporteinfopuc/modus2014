<td>
    <div class="view input-sm">
        <label>Sección "<%=name%>"</label>
    </div>
    <input class="edit form-control input-sm input-edit-room" type="text" value="<%=name%>">
</td>
<td>
    <div class="actions pull-right">
        <a type="button" rel="button" class="btn btn-secondary btn-xs btn-modus" modus-link href="<%=Modus.uriRoot+'/instructor/'+schoolId+'/'+levelId+'/'+gradeId+'/'+id%>" data-toggle="tooltip" title="Ver Docentes">
            <i class="fa fa-user"></i>
            <span class="badge"><%=count_instructors%></span>
        </a>&nbsp;
        <a type="button" rel="button" class="btn btn-secondary btn-xs btn-modus" modus-link href="<%=Modus.uriRoot+'/student/'+schoolId+'/'+levelId+'/'+gradeId+'/'+id%>" data-toggle="tooltip" title="Ver alumnos">
            <i class="fa fa-child"></i>
            <span class="badge"><%=count_students%></span>
        </a>&nbsp;
        <a type="button" rel="button" class="btn btn-secondary btn-xs btn-modus action-edit" data-toggle="tooltip" title="Modificar nombre"><i class="fa fa-pencil"></i></a>&nbsp;
        <a type="button" rel="button" class="btn btn-danger btn-xs btn-modus action-delete" data-toggle="tooltip" title="Eliminar" value="<%=id%>"><i class="fa fa-times"></i></a>
    </div>
</td>
