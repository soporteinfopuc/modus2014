<div class="col-sm-12">
    <h2 class="title-modus">
        <a modus-link-back  href="<%=Modus.uriRoot%>/" title="Volver"><i class="fa fa-arrow-left"></i></a>
        <%=" " + school_name%> 
        <small>Coordinadores asignados</small>
    </h2>
</div>
<div class="clearfix"></div>
<div class="col-sm-8 col-sm-offset-2 message-area">
    
</div>
<div class="clearfix"></div>
<div class="col-sm-8 col-sm-offset-2">
        <div id="schoolsManagersApp" class="panel panel-default">
                <div class="panel-heading">
                    <input id="select-manager" type="text" class="form-control typehead" placeholder="Nombre del administrador a agregar">
                </div>
                <div id="main" class="panel-body">
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        &nbsp;
                    </div>
                    <div id="managers-list">

                    </div>
                </div>
                <div class="panel-footer">
                    <div class="status"><b></b></div>
                    <div class="sections-count"><b></b> </div>
                </div>
        </div>
</div>