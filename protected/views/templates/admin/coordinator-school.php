<td>
  <i class="fa fa-university"></i>&nbsp;<label><%=name%></label>
</td>
<td>
  <div class="actions">
      <div class="pull-right">
        <a type="button" rel="button" class="btn btn-secondary btn-sm btn-modus" modus-link  href="<%=Modus.uriRoot%>/school/<%=id%>/coordinators" data-toggle="tooltip" title="Ver coordinadores asignados">
                <i class="fa fa-graduation-cap"></i>
        </a>&nbsp;
        <a type="button" rel="button" class="btn btn-danger btn-sm  btn-modus delete" data-toggle="tooltip" title="Quitar colegio asignado"><i class="fa fa-times"></i></a>
      </div>
  </div>
</td>