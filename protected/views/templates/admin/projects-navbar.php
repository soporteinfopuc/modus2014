<ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle school-value" data-toggle="dropdown">Todos los colegios <b class="caret"></b></a>
        <ul class="dropdown-menu school-list"></ul>
    </li>
    <li class="dropdown">
        <a class="dropdown-toggle grade-value" data-toggle="dropdown">Todos los grados <b class="caret"></b></a>
        <ul class="dropdown-menu grade-list" style="width: 230px;"></ul>
    </li>
</ul>
<form class="navbar-form navbar-right" role="search">
        <div class="form-group has-feedback">
            <input type="text" class="form-control" id="input-search-project" placeholder="Buscar...">
            <span class="form-control-feedback" title="Escriba un criterio y presione 'Enter' para buscar"><i class=" fa fa-search fa-2"></i></span>
        </div>
</form>