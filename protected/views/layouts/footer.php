<div class="footer">
	<div class="container">
	  <img class="back-top" src="<?php echo $this->config->uriCDN; ?>/img/modus_back_to_top.png" onclick="$('html, body').animate({ scrollTop:0 }, '1000');"></img>
	  <div class="row show-grid">
	        <div class="col-xs-12 col-sm-6">
                    <div class="col-sm-12">
	        	<span class="pull-left">© Copyright <?php echo date("Y"); ?> | Pontificia Univeridad Católica del Perú</span>
                    </div>
                    <div class="col-sm-12">
                        <span class="pull-left">Instituto de Informática (PUCP) | <a href="http://infopuc.pucp.edu.pe/" target="_blank">INFOPUC</a></span>
                    </div>
	        </div>
              <div class="clear-fix"></div>
	        <div class="col-xs-12 col-sm-6">
                    <div class="col-sm-12">
                        <span class="pull-right">Av. Universitaria 1801, San Miguel, Lima 32, Perú</span>
                    </div>
                    <div class="col-sm-12">
                        <span class="pull-right">(511) 6262000 | <?php  echo $this->config->emailAdmin;?></span>
                    </div>
	        </div>
	  </div>  
	</div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43399019-6', 'pucp.edu.pe');
  ga('send', 'pageview');

</script>
