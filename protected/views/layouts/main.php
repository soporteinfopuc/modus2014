<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="default" />-->
        <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="<?php echo $this->config->uriCDN;?>/favicon1.ico">
        <title><?php echo Yii::app()->name?></title>
        <?php        
        if (isset($this->internal_css))
        {
            foreach ($this->internal_css as $int_css)
            {
                echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->config->uriCDN. "/css/". $int_css . "\">";
            }
        }
        if($this->css_included)
        {
            echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->config->uriCDN. $this->css_included . "\">";
        }
        ?>
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
        <!--[if lt IE 9]>
            <script type="text/javascript" src="<?php echo $this->config->uriCDN; ?>/js/third-party/html5shiv.js" />
            <script type="text/javascript" src="<?php echo $this->config->uriCDN; ?>/js/third-party/respond.min.js" />
        <![endif]-->
        <script type="text/javascript">
            window.Modus = {config:{}};
            Modus.config.order_projects_by_type = <?php echo $this->config->orderProjectsByType?"true":"false"; ?>;
            Modus.uriRoot = "<?php echo Yii::app()->request->baseUrl; ?>";
            Modus.uriCDN = "<?php echo $this->config->uriCDN; ?>";
            <?php if(Yii::app()->user && isset(Yii::app()->user->dbid)){ ?>
            Modus.uid = <?php echo Yii::app()->user->dbid; ?>;
            Modus.isAdmin = <?php echo Yii::app()->user->checkAccess("admin")?"true":"false"; ?>;
            Modus.isCreator = <?php echo Yii::app()->user->checkAccess("creator")?"true":"false"; ?>;
            Modus.isCoordinator = <?php echo Yii::app()->user->checkAccess("coordinator")?"true":"false"; ?>;
            Modus.isInstructor = <?php echo (Yii::app()->user->checkAccess("admin") || Yii::app()->user->checkAccess("coordinator"))?"false":(Yii::app()->user->checkAccess("instructor")?"true":"false"); ?>;
            Modus.isStudent = <?php echo Yii::app()->user->checkAccess("admin")?"false":(Yii::app()->user->checkAccess("instructor")?"false":(Yii::app()->user->checkAccess("student")?"true":"false")); ?>;
            Modus.maxUploadSize = <?php echo 1024*1024*(min((int)ini_get('upload_max_filesize'), (int)ini_get('post_max_size')));?>;
            Modus.maxUploadSizeDesc = "<?php echo (min((int)ini_get('upload_max_filesize'), (int)ini_get('post_max_size')));?>MB";
            Modus.fileExtAccept = "<?php echo implode("|", @$this->config->fileExtAllowed); ?>";
            Modus.imgExtAccept = "<?php echo implode("|", @$this->config->imageExtAllowed); ?>";
            <?php } ?>
        </script>        
    </head>
    <body>
        <?php echo $this->renderPartial('/layouts/header'); ?>
        <?php if(Yii::app()->user->isGuest) { ?>
        <div class="front-page flash">
            <div class="container banner">
                <div class="banner-image">
                  
                </div>
                <div class="banner-text">
                  <!--<p>¡Bienvenido al proyecto MODUS!<br />A continuacion te indicamos como puedes sacarle provecho a esta nueva plataforma</p>-->
                  <!--<a class="see-more-link" href="<?php echo Yii::app()->request->baseUrl; ?>/about">¿Quiere saber más?</a>-->
                </div>
            </div>
        </div>
        <?php } else { ?> 
        <div class="front-page"><div class="container banner"><div class="banner-image"></div></div></div>
        <?php } ?>
        <div class="container main-container">
          <div class="panel-body" id="panel-principal">
              <?php echo $content ?>
          </div>
        </div>
        <?php echo $this->renderPartial('/layouts/footer'); ?>
    
    </body>
<?php
if(isset($this->templates) && is_string($this->templates))
{
    echo $this->templates; 
}
?>
    
<?php
if($this->js_main)
{
    echo "<script type=\"text/javascript\" src=\"". $this->config->uriCDN . $this->js_main . "\"></script>";
}
if($this->js_included)
{
    echo "<script type=\"text/javascript\" src=\"". $this->config->uriCDN. $this->js_included . "\"></script>";
}
?>
</html>
