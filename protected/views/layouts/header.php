<header id="header" class="navbar navbar-top navbar-inverse navbar-fixed-top" role="banner">
  <div class="container">
    <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#bs-navbar-collapse">
          <span class="sr-only"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" modus-link href="<?php echo Yii::app()->request->baseUrl; ?>/"><i class="fa fa-home"></i> Modus</a>
    </div>
    <nav class="collapse navbar-collapse" id="bs-navbar-collapse" role="navigation">
      <ul class="nav navbar-nav">
          <li><a href="mailto:<?php echo $this->config->emailAdmin;?>" onclick="alert('si desea ponerse en contacto con nosotros, evíenos un correo a <?php echo $this->config->emailAdmin;?>')" class="contact-link"><i class="fa fa-envelope"></i> Contacto</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <?php
        $user = Yii::app()->user;
        if (!$user->isGuest)
        {
            if($user->checkAccess("viewActiveYear"))
            {
        ?>
            <li class="dropdown user-menu">
                <a class="dropdown-toggle" data-toggle="dropdown" title=""><i class="fa fa-calendar"></i><?php if($this->year) echo " ".$this->year['name'];?></a>
                <?php if($user->checkAccess("changeActiveYear")) { ?>
                <ul class="dropdown-menu" role="menu">
                    <?php if($this->years) foreach($this->years as $year){ if($year['id'] != $this->year['id']){ ?>
                        <li><a class="year-select" y-id="<?php echo $year['id'];?>"><?php echo $year['name'];?></a></li>
                    <?php } } ?>
                    <?php if($user->checkAccess("manageYears")) { ?>
                    <li><a modus-link href="<?php echo Yii::app()->request->baseUrl; ?>/year"><i class="fa fa-cog"></i> Administrar</a></li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </li>
        <?php } ?>
            <li class="dropdown user-menu">
                <a class="dropdown-toggle" data-toggle="dropdown" title="<?php echo isset($user->firstname)?$user->firstname:'' . " " . isset($user->lastname)?$user->lastname:''; ?>"><i class="fa fa-arrow-circle-down"></i> <?php echo " ".$user->id; ?></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/"><i class="fa fa-list-alt"></i> Mis contenidos</a></li>
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/modus/logout"><i class="fa fa-sign-out"></i> Salir</a></li>
                </ul>
            </li>
        <?php    
        }
        else
        {
        ?>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/login" class="menu-link"><i class="fa fa-sign-in"></i> Ingresar</a></li>

        <?php
        }?>
      </ul>
        
    </nav>
  </div>
</header>
<div class="navbar second-bar"></div>
<div class="container logo"></div>